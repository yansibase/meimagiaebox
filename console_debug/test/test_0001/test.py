import os
import sys
import unittest
import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import at_helper                as hp

class MyTest00(unittest.TestCase):
    m_id        = ''

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        hp.ATHelper.tryToLoadPlugin('console_debug')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        hp.ATHelper.tryToUnloadPlugin('console_debug')

    def test_basic_parameters(self):
        log.debug('---- begin %s.basic_parameters() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        
        log.debug('This is a test message for console_debug')

        log.debug('---- end   %s.basic_parameters() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
