#pragma once

#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <common/IncludeMFnPluginClass.h>

class ConsoleDebug : public MPxCommand
{
public:
    ConsoleDebug();
    ~ConsoleDebug();

    virtual MStatus doIt( const MArgList &args );
    virtual MStatus undoIt();
    virtual MStatus redoIt();

    virtual bool isUndoable() const;
    virtual bool hasSyntax() const;

    bool isHistoryOn() const;

    static MStatus registerCmd(MFnPlugin &plugin);
    static MStatus deregisterCmd(MFnPlugin &plugin);

    static void* creator() {return new ConsoleDebug();};

    static MString name();

protected:
    static MSyntax mySyntax();

    static const MString getUsageString();

    MStatus onOpen();
    MStatus onClose();
};
