//#include "prerequest.h"

#include <xxcommon/prerequest_std.h>

#include <xxcommon/prerequest_maya.h>
#include <xxcommon/prerequest_local.h>
#include <maya/MFnPlugin.h>
#include <xxcommon/mayacheck.h>


#include "ConsoleDebug.h"

// required to link the libraries under VisualC++/Win32
#ifdef WIN32
	#pragma comment(lib,"Foundation.lib")
	#pragma comment(lib,"OpenMaya.lib")
	#pragma comment(lib,"OpenMayaFx.lib")
	#pragma comment(lib,"OpenMayaUi.lib")
	#pragma comment(lib,"Image.lib")
	#pragma comment(lib,"OpenMayaAnim.lib")
#endif

#ifdef WIN32
#define EXPORT __declspec(dllexport)
#else 
#define EXPORT
#endif


EXPORT MStatus initializePlugin( MObject obj )
{
	_LogFunctionCall("initializePlugin()");

	init_logs("c:/consoleDebug.log");
	MStatus   status;

	MFnPlugin plugin( obj, "yaoyansi", "1.0", "Any");

	status = plugin.registerCommand( 
		ConsoleDebug::typeName, 
		ConsoleDebug::creator, 
		ConsoleDebug::newSyntax);
	IfMErrorWarn(status);

	//
	MGlobal::executeCommand( ConsoleDebug::typeName+ " -o" );

	return status;
}

EXPORT MStatus uninitializePlugin( MObject obj)
{
	_LogFunctionCall("uninitializePlugin()");

	MStatus   status;
	MFnPlugin plugin( obj );

	MGlobal::executeCommand( ConsoleDebug::typeName+ " -c" );

 	status = plugin.deregisterCommand( ConsoleDebug::typeName );
	IfMErrorWarn(status);

	close_logs();
	return status;
}
