#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MStatus.h>

#include <common/log.h>
#include "version.h"
#include "ConsoleDebug.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya

PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('"+ConsoleDebug::name()+"')", true, false) );

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

	CHECK_MSTATUS(ConsoleDebug::registerCmd(plugin));

	CHECK_MSTATUS(MGlobal::executeCommand( ConsoleDebug::name()+ " -o" ));

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('"+ConsoleDebug::name()+"')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('"+ConsoleDebug::name()+"')", true, false) );

	MFnPlugin plugin( obj );

    CHECK_MSTATUS(MGlobal::executeCommand( ConsoleDebug::name()+ " -c" ));

    CHECK_MSTATUS(plugin.deregisterCommand( ConsoleDebug::name() ));

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('"+ConsoleDebug::name()+"')", true, false) );

	return MS::kSuccess;
}

