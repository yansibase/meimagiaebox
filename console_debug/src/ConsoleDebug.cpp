#include "ConsoleDebug.h"

#include <cassert>

#if defined _WIN32 || defined _WIN64
#include <windows.h>
#endif

#include <maya/MArgList.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MGlobal.h>

#include <common/log.h>
#include <common/node_ids.h>

#define kHelp_s         ("-h")
#define kHelp_l         ("-help")
#define kOpen_s			("-o")
#define kOpen_l			("-open")
#define kClose_s  		("-c")
#define kClose_l	    ("-close")

//////////////////////////////////////////////////////////////////////
const MString ConsoleDebug::getUsageString()
{
    MString usage;

    usage += "\n ";
    usage += "\n "+name()+"  [options]";
    usage += "\n Options:";
    usage += "\n "+MString(kHelp_s) +" / "+MString(kHelp_l) +"\t\t\t"+"print this message ";

    usage += "\n "+MString(kOpen_s) +" / "+MString(kOpen_l) +"\t\t\t"+"open console window ";
    usage += "\n "+MString(kClose_s)+" / "+MString(kClose_l)+"\t\t\t"+"close console window";
    usage += "\n ";

    return usage;
}

ConsoleDebug::ConsoleDebug()
{
}
//
ConsoleDebug::~ConsoleDebug()
{
}
//
MSyntax ConsoleDebug::mySyntax()
{
    MSyntax syntax;
    CHECK_MSTATUS(syntax.addFlag(kHelp_s,  kHelp_l,  MSyntax::kNoArg));
	CHECK_MSTATUS(syntax.addFlag(kOpen_s,  kOpen_l,  MSyntax::kNoArg));
	CHECK_MSTATUS(syntax.addFlag(kClose_s, kClose_l, MSyntax::kNoArg));

    syntax.enableQuery(true);
    syntax.enableEdit(true);

	return syntax;
}
//
MStatus ConsoleDebug::doIt ( const MArgList &args )
{
    MStatus status;

    MArgDatabase argData( syntax(), args, &status);
    CHECK_MSTATUS(status);

    //const bool queryUsed = argData.isQuery();

    // help
    if (argData.isFlagSet(kHelp_l))
    {
        MGlobal::displayInfo(getUsageString());
        return MS::kSuccess;
    }

    if (argData.isFlagSet(kOpen_l))
    {
        onOpen();
    }

    if (argData.isFlagSet(kClose_l))
    {
        onClose();
    }

	return redoIt();
}
//
//////////////////////////////////////////////////////////////////////
MStatus ConsoleDebug::undoIt()
{
    return MStatus::kSuccess;
}
//////////////////////////////////////////////////////////////////////
MStatus ConsoleDebug::redoIt()
{
    return MStatus::kSuccess;
}
//////////////////////////////////////////////////////////////////////
bool ConsoleDebug::isUndoable() const
{
    return false;
}
//////////////////////////////////////////////////////////////////////
bool ConsoleDebug::hasSyntax() const
{
    return true;
}
//////////////////////////////////////////////////////////////////////
bool ConsoleDebug::isHistoryOn() const
{
    //what is this supposed to do?
    return false;
}
//////////////////////////////////////////////////////////////////////
MString ConsoleDebug::name()
{
    return CmdName_ConsoleDebug;
}
//
MStatus ConsoleDebug::registerCmd(MFnPlugin &plugin)
{
    MStatus status;

    //register a helper command for querying shader information
    CHECK_MSTATUS(plugin.registerCommand(
                           ConsoleDebug::name(),
                           ConsoleDebug::creator,
                           ConsoleDebug::mySyntax)
                  );
    return MS::kSuccess;
}
//
MStatus ConsoleDebug::deregisterCmd(MFnPlugin &plugin)
{
    CHECK_MSTATUS(
                  plugin.deregisterCommand( ConsoleDebug::name() )
                  );

    return MS::kSuccess;
}
//
MStatus ConsoleDebug::onOpen()
{
	MStatus stat = MS::kSuccess;

#if defined _WIN32 || defined _WIN64
	BOOL ret = AllocConsole();
	freopen("CONOUT$","w+t",stdout);

	if(ret!=TRUE)
    {
        stat = MS::kFailure;
		return stat;
	}

	//unable close button
	char buf[MAX_PATH];
	GetConsoleTitle(buf, MAX_PATH);
	HWND hwnd = ::FindWindow(NULL, buf);
	HMENU hmenu = ::GetSystemMenu(hwnd, FALSE);
	if (hwnd)
	{
		// disable the close button of the console window
		::RemoveMenu(hmenu, SC_CLOSE, MF_BYCOMMAND);

		// minimize the console window
		::SendMessage(hwnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
	}else{
		LDbg("hwnd is NULL.");
	}
#else
	LWrn("Console Debug Window is only implemented on Windows platform");
#endif

	LInf("Welcome to Console Debug Window.");
	return  stat;
}
//
MStatus ConsoleDebug::onClose()
{
	MStatus stat = MS::kSuccess;

#if defined _WIN32 || defined _WIN64
	BOOL ret = FreeConsole();
	return ret==TRUE ? MS::kSuccess : MS::kFailure;
#else
    // todo ...
#endif
    return stat;
}
