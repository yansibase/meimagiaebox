'''
import maya.cmds as cmds
cmds.cmd_template_py()
'''

import sys
import maya.OpenMaya            as OpenMaya
import maya.OpenMayaMPx         as OpenMayaMPx
import mymagicbox.plugin_setup  as plugin_setup

kPluginCmdName = "cmd_template_py"

# Command
class scriptedCommand(OpenMayaMPx.MPxCommand):
    def __init__(self):
        OpenMayaMPx.MPxCommand.__init__(self)
        
    # Invoked when the command is run.
    def doIt(self,argList):
        print "Hello World!"

# Creator
def cmdCreator():
    return OpenMayaMPx.asMPxPtr( scriptedCommand() )
    
################################################################################ 
# Initialize the script plug-in
def initializePlugin(mobject):
    # callback function
    plugin_setup.initializePlugin_beginCB(kPluginCmdName)
    
    mplugin = OpenMayaMPx.MFnPlugin(mobject)
    try:
        mplugin.registerCommand( kPluginCmdName, cmdCreator )
    except:
        sys.stderr.write( "Failed to register command: %s\n" % kPluginCmdName )
        raise
        
    # callback function
    plugin_setup.initializePlugin_endCB(kPluginCmdName)

# Uninitialize the script plug-in
def uninitializePlugin(mobject):
    # callback function
    plugin_setup.uninitializePlugin_beginCB(kPluginCmdName)

    mplugin = OpenMayaMPx.MFnPlugin(mobject)
    try:
        mplugin.deregisterCommand( kPluginCmdName )
    except:
        sys.stderr.write( "Failed to unregister command: %s\n" % kPluginCmdName )

    # callback function
    plugin_setup.uninitializePlugin_endCB(kPluginCmdName)



