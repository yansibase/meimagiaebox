#pragma once

#include <maya/MNodeMessage.h>
#include <maya/MPxNode.h>

#include <common/IncludeMFnPluginClass.h>
#include <common/version_helper.h>


class Point : public MPxNode
{
public:
                            Point();
                            ~Point();

	static  void *	        creator();
	static  MStatus	        initialize();
	virtual MStatus	        compute(const MPlug& plug, MDataBlock& data);
	virtual void	        postConstructor();

	static MString          sTypeName();
	static MTypeId          sTypeId();
	static MPxNode::Type    sType();
	static const MString&   sClassification();

protected:
    //MString getStringPosition();
    //MString getStartBlock();
    //MString getEndBlock();

    MString getStrFunction_position_POINT(const MString &nodeName, const unsigned int vertexCount, const double currentTime);

	static MString          m_classification;
	static VersionHelper    m_version_helper;

    static MObject          a_iInCurrentTime;
    static MObject          a_iInGeometry;
    static MObject          a_iInWeightPP;

    static MObject          a_iEnablePosition;
    static MObject          a_iApplyChangesPosition;
    static MObject          a_iStringPosition;

    static MObject          a_oOutGeometry;
    static MObject          a_oOutIdPP;
    static MObject          a_oOutPositionPP;

private:
    static  void            setDefaultStrings();
    static  void			sAttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug, void* inUserPtr);
            void            AttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug);

            MCallbackId		m_callbackId;

            MString         m_stringStartBlock;
    static  MString         m_stringStartBlockDefault;
            MString         m_stringPosition;
    static  MString         m_stringPositionDefault;
            MString         m_stringWeight;
    static  MString         m_stringWeightDefault;
            MString         m_stringColor;
    static  MString         m_stringColorDefault;
            MString         m_stringNormalVelocity;
    static  MString         m_stringNormalVelocityDefault;
            MString         m_stringRadius;
    static  MString         m_stringRadiusDefault;
            MString         m_stringMapUV;
    static  MString         m_stringMapUVDefault;
            MString         m_stringEndBlock;
    static  MString         m_stringEndBlockDefault;
    static  MString         m_quickHelp;

            MString         m_cmd_position_POINT;
};

MStatus initializePlugin_Point(MFnPlugin &plugin);
MStatus uninitializePlugin_Point(MFnPlugin &plugin);
