//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+

// Example Plugin: lambertShader.cpp
//
// Produces dependency graph node LambertShader
// This node is an example of a Lambert shader and how to build a
// dependency node as a surface shader in Maya. The inputs for this node
// are many, and can be found in the Maya UI on the Attribute Editor for
// the node. The output attributes for the node are "outColor" and
// "outTransparency". To use this shader, create a lambertShader with
// Shading Group or connect the outputs to a Shading Group's
// "SurfaceShader" attribute.
//
#pragma once

#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnLightDataAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MGlobal.h>

#include <common/IncludeMFnPluginClass.h>
#include <common/version_helper.h>
/////////////////////////////////
// Plugin Lambert Shader Class //
/////////////////////////////////


// This class will create a new shader. Shaders are custom dependency
// graph objects so we will derive this class from the basic DG node
// type MPxNode
//

class CreateArray : public MPxNode
{
public:
                            CreateArray();
                            ~CreateArray();

	static  void *	        creator();
	virtual MStatus	        compute( const MPlug&, MDataBlock& );
	static  MStatus	        initialize();
	virtual void	        postConstructor();

	static MString          sTypeName();
	static MTypeId          sTypeId();
	static MPxNode::Type    sType();
	static const MString&   sClassification();

protected:
    enum OutputType
    {
        OT_1integer = 0,
        OT_1double,
        OT_2doubles,
        OT_3doubles,
        OT_vector,
        OT_4doubles,
        OT_Count
    };
	static MString          m_classification;
	static VersionHelper    m_version_helper;

	static MObject          a_iLength;
	static MObject          a_iOutputType;
	static MObject          a_iDefaultValueX;
	static MObject          a_iDefaultValueY;
	static MObject          a_iDefaultValueZ;
	static MObject          a_iDefaultValueW;
	static MObject          a_oOutArray;
};

MStatus initializePlugin_CreateArray(MFnPlugin &plugin);
MStatus uninitializePlugin_CreateArray(MFnPlugin &plugin);
