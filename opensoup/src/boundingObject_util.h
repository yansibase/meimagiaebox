#pragma once

#include <boost/shared_ptr.hpp>

#include <maya/MBoundingBox.h>
#include <maya/MFnMesh.h>
#include <maya/MStatus.h>
#include <maya/MPoint.h>

enum BoundingObjectType
{
    BOT_Sphere = 0,
    BOT_Cube,
    BOT_Capsule,
    BOT_InputGeometry,
    BOT_Count
};
//
class BoundingObjectUtilInterface
{
public:
    BoundingObjectUtilInterface();
    virtual ~BoundingObjectUtilInterface();

    virtual MBoundingBox    getBoundingBox()                = 0;
    virtual MStatus         draw()                          = 0;
    virtual bool            isInside(const MPoint& point)   = 0;
    virtual MColor          onInSide()                      = 0;
    virtual MColor          onOutSide()                     = 0;
    virtual double          getWeight(const MPoint& point)const                     = 0;

};
//
class DefaultUtil : public BoundingObjectUtilInterface
{
public:
    DefaultUtil();
    ~DefaultUtil();

    virtual MBoundingBox    getBoundingBox();
    virtual MStatus         draw();
    virtual bool            isInside(const MPoint& point);
    virtual MColor          onInSide();
    virtual MColor          onOutSide();
    virtual double          getWeight(const MPoint& point)const;
};
//
class SphereUtil : public BoundingObjectUtilInterface
{
public:
    SphereUtil();
    ~SphereUtil();

    virtual MBoundingBox    getBoundingBox();
    virtual MStatus         draw();
    virtual bool            isInside(const MPoint& point);
    virtual MColor          onInSide();
    virtual MColor          onOutSide();
    virtual double          getWeight(const MPoint& point)const;

private:
    static const float  R;
};
//
class CubeUtil : public BoundingObjectUtilInterface
{
public:
    CubeUtil();
    ~CubeUtil();

    virtual MBoundingBox    getBoundingBox();
    virtual MStatus         draw();
    virtual bool            isInside(const MPoint& point);
    virtual MColor          onInSide();
    virtual MColor          onOutSide();
    virtual double          getWeight(const MPoint& point)const;

private:
    static const float  R;
};
//
class BoundingObjectUtilMgr
{
public:
    BoundingObjectUtilMgr();
    ~BoundingObjectUtilMgr();
    boost::shared_ptr<BoundingObjectUtilInterface> getShape(int objectType);

protected:
    boost::shared_ptr<BoundingObjectUtilInterface> m_defaultAgent;
    boost::shared_ptr<BoundingObjectUtilInterface> m_sphereAgent;
    boost::shared_ptr<BoundingObjectUtilInterface> m_cubeAgent;
};
//




