#include "group.h"

#include <maya/MFnEnumAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnComponentListData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>

#include <common/api_macro.h>
#include <common/node_ids.h>
#include <common/log.h>
#include "version.h"
#include "boundingObject.h"
#include "boundingObject_util.h"

MString Group::m_classification("utility/general");
VersionHelper   Group::m_version_helper;
MObject Group::a_iInGeometry;
MObject Group::a_iGroupId;
MObject Group::a_iBoundingObjects;
MObject Group::     a_iBoundParentMatrix;
MObject Group::     a_iBoundControlData;
MObject Group::          a_iBoundType;
MObject Group::a_oOutGeometry;
MObject Group::a_oOutComponents;

///////////////////////////////////////////////////////
// DESCRIPTION: attribute information
///////////////////////////////////////////////////////
//



// the postConstructor() function is called immediately after the objects
// constructor. It is not safe to call MPxNode member functions from the
// constructor, instead they should be called here.
//
void Group::postConstructor( )
{
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

MString Group::sTypeName()
{
	return NodeTypeName_Group;
}

MTypeId Group::sTypeId()
{
	return NodeID_Group;
}

MPxNode::Type Group::sType()
{
	return MPxNode::kDependNode;
}

const MString& Group::sClassification()
{
	return m_classification;
}


// This node does not need to perform any special actions on creation or
// destruction
//

Group::Group()
{
}

Group::~Group()
{
}


// The creator() method allows Maya to instantiate instances of this node.
// It is called every time a new instance of the node is requested by
// either the createNode command or the MFnDependencyNode::create()
// method.
//
// In this case creator simply returns a new node_template object.
//

void* Group::creator()
{
	return new Group();
}


// The initialize method is called only once when the node is first
// registered with Maya. In this method you define the attributes of the
// node, what data comes in and goes out of the node that other nodes may
// want to connect to.
//

MStatus Group::initialize()
{
	MStatus status; // Status will be used to hold the MStatus value
	// returned by each api function call. It is important
	// to check the status returned by a call to aid in
	// debugging. Failed API calls can result in subtle
	// errors that can be difficult to track down, you may
	// wish to use the CHECK_MSTATUS macro for any API
	// call where you do not need to provide your own
	// error handling.
	//

	// Attribute Initialization:
	//
	// create      - The create function creates a new attribute for the
	//				 node, it takes a long name for the attribute, a short
	//				 name for the attribute, the type of the attribute,
	//				 and a status object to determine if the api call was
	//				 successful.
	//
	// setKeyable  - Sets whether this attribute should accept keyframe
	//				 data, Attributes are not keyable by default.
	//
	// setStorable - Sets whether this attribute should be storable. If an
	//				 attribute is storable, then it will be writen out
	//				 when the node is stored to a file. Attributes are
	//               storable by default.
	//
	// setDefault  - Sets the default value for this attribute.
	//
	// setUsedAsColor - Sets whether this attribute should be presented as
	//				 a color in the UI.
	//
	// setHidden   - Sets whether this attribute should be hidden from the
	//				 UI. This is useful if the attribute is being used for
	//				 blind data, or if it is being used as scratch space
	//				 for a geometry calculation (should also be marked
	//				 non-connectable in that case). Attributes are not
	//				 hidden by default.
	//
	// setReadable - Sets whether this attribute should be readable. If an
	//				 attribute is readable, then it can be used as the
	//				 source in a dependency graph connection. Attributes
	//				 are readable by default.
	//
	// setWritable - Sets whether this attribute should be readable. If an
	//				 attribute is writable, then it can be used as the
	//				 destination in a dependency graph connection. If an
	//			     attribute is not writable then setAttr commands will
	//				 fail to change the attribute. If both keyable and
	//				 writable for an attribute are set to true it will be
	//				 displayed in the channel box when the node is
	//				 selected. Attributes are writable by default.
	//
	// setArray    - Sets whether this attribute should have an array of
	//				 data. This should be set to true if the attribute
	//				 needs to accept multiple incoming connections.
	//				 Attributes are single elements by default.
	//

    //
    // Input Attributes
    //
	{// a_iInGeometry
	    MFnTypedAttribute tAttr;
	    a_iInGeometry = tAttr.create("inGeometry", "ig", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_INPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_iInGeometry));
	}

	{// a_iGroupId
	    MFnNumericAttribute nAttr;
	    a_iGroupId = nAttr.create("groupId", "gid",  MFnNumericData::kLong, 0, &status); CHECK_MSTATUS(status);
	    MAKE_INPUT(nAttr);
	    CHECK_MSTATUS(addAttribute(a_iGroupId));
	}

	{// a_iBoundingObjects
	    {// a_iBoundParentMatrix
            MFnMatrixAttribute mAttr;
            a_iBoundParentMatrix = mAttr.create("boundParentMatrix", "bpm", MFnMatrixAttribute::kDouble, &status); CHECK_MSTATUS(status);
            MAKE_INPUT(mAttr);
            CHECK_MSTATUS(addAttribute(a_iBoundParentMatrix));
	    }
	    {// a_iBoundControlData
	        {// a_iBoundType
                MFnNumericAttribute nAttr;
                a_iBoundType= nAttr.create("boundType", "btp", MFnNumericData::kShort, 0,  &status ); CHECK_MSTATUS(status);
                MAKE_INPUT(nAttr);
                //CHECK_MSTATUS(nAttr.setReadable(false));
                CHECK_MSTATUS(addAttribute(a_iBoundType));
	        }
            MFnCompoundAttribute cAttr;
            a_iBoundControlData = cAttr.create( "boundControlData", "bcd", &status); CHECK_MSTATUS(status);
            CHECK_MSTATUS(cAttr.addChild(a_iBoundType));
            MAKE_INPUT(cAttr);
            //CHECK_MSTATUS(cAttr.setReadable(false));
            CHECK_MSTATUS(addAttribute(a_iBoundControlData));
	    }
        MFnCompoundAttribute cAttr;
        a_iBoundingObjects = cAttr.create( "boundingObjects", "bos", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(cAttr.addChild(a_iBoundParentMatrix));
        CHECK_MSTATUS(cAttr.addChild(a_iBoundControlData));
        MAKE_INPUT(cAttr);
        CHECK_MSTATUS(cAttr.setArray(true));
        CHECK_MSTATUS(cAttr.setUsesArrayDataBuilder(true));
	    CHECK_MSTATUS(addAttribute(a_iBoundingObjects));
	}

    //
    // Output Attributes
    //
	{// a_oOutGeometry
	    MFnTypedAttribute tAttr;
	    a_oOutGeometry = tAttr.create("outGeometry", "og", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_OUTPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_oOutGeometry));
	}
	{// a_oOutComponents
	    MFnTypedAttribute tAttr;
	    a_oOutComponents = tAttr.create("outComponents", "oc", MFnComponentListData::kComponentList, &status); CHECK_MSTATUS(status);
	    MAKE_OUTPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_oOutComponents));
	}


    CHECK_MSTATUS(attributeAffects(a_iInGeometry,               a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iGroupId,                  a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iBoundingObjects,          a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(     a_iBoundParentMatrix,   a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(     a_iBoundControlData,    a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(         a_iBoundType,       a_oOutGeometry));

    CHECK_MSTATUS(attributeAffects(a_iInGeometry,               a_oOutComponents));
    CHECK_MSTATUS(attributeAffects(a_iGroupId,                  a_oOutComponents));
    CHECK_MSTATUS(attributeAffects(a_iBoundingObjects,          a_oOutComponents));
    CHECK_MSTATUS(attributeAffects(     a_iBoundParentMatrix,   a_oOutComponents));
    CHECK_MSTATUS(attributeAffects(     a_iBoundControlData,    a_oOutComponents));
    CHECK_MSTATUS(attributeAffects(         a_iBoundType,       a_oOutComponents));
    //
	m_version_helper.initialize();

	return( MS::kSuccess );
}


// The compute() method does the actual work of the node using the inputs
// of the node to generate its output.
//
// Compute takes two parameters: plug and data.
// - Plug is the the data value that needs to be recomputed
// - Data provides handles to all of the nodes attributes, only these
//   handles should be used when performing computations.
//
MStatus Group::compute( const MPlug& plug, MDataBlock& data )
{
    static int iEnterCount = 0;
    iEnterCount++;

    MStatus status;

	MString plugname(plug.name(&status));
	CHECK_MSTATUS(status);
	LDbg("%d, plugname=%s\n", iEnterCount, plugname.asChar());

    if ((plug == a_oOutGeometry) || (plug == a_oOutComponents))
    {
        ////////////////////////////////////////
        MArrayDataHandle adhBoundingObjects = data.inputArrayValue( a_iBoundingObjects, &status );  CHECK_MSTATUS( status );
        //LDbg("%d, bounding objects count=%d\n", iEnterCount, adhBoundingObjects.elementCount());


        /////////////////////////////////////////
        MDataHandle dhGroupId     = data.inputValue(a_iGroupId,         &status);  CHECK_MSTATUS(status);
        //long groupId = dhGroupId.asLong();

        MDataHandle dhInGeometry  = data.inputValue(a_iInGeometry,      &status);   CHECK_MSTATUS(status);
        MDataHandle dhOutGeometry = data.outputValue(a_oOutGeometry,    &status);   CHECK_MSTATUS(status);

        CHECK_MSTATUS(dhOutGeometry.copy(dhInGeometry));


        //MFnMesh fnInMesh (dhInGeometry.asMesh(),  &status); CHECK_MSTATUS(status);
        MFnMesh fnOutMesh(dhOutGeometry.asMesh(), &status); CHECK_MSTATUS(status);

        MPointArray position_ws;
        CHECK_MSTATUS(fnOutMesh.getPoints(position_ws, MSpace::kWorld));

        unsigned int numPolygons = fnOutMesh.numPolygons(&status);   CHECK_MSTATUS(status);
        //LDbg("numPolygons=%d", numPolygons);

        MIntArray polygonInSideBoundingObjects;// which polygons locate in the bounding objects

        // for each polygon
        for(unsigned int polyId=0; polyId<numPolygons; ++polyId)
        {
            MIntArray vertexInPoly;
            fnOutMesh.getPolygonVertices(polyId, vertexInPoly);
            //std::cout<< "poly["<<polyId<<"]=" << vertexInPoly<< std::endl;

            // whether this polygon in the boundingObject
            bool bPolygonInSideBoundingObject = false;
            {
            // for each vertex in this polygon
            for(unsigned int vIdInPoly=0; vIdInPoly<vertexInPoly.length(); ++vIdInPoly)
            {
                // map the vertex index to mesh points buffer
                int vId = vertexInPoly[vIdInPoly];

                bool bVertexInSideBoundingObject = false;
                {
                MColor displayColor(0.0, 0.0, 0.0);

                for(unsigned int bosIdx=0; bosIdx < adhBoundingObjects.elementCount(); ++bosIdx )
                {
                    CHECK_MSTATUS(adhBoundingObjects.jumpToArrayElement(bosIdx));

                    MDataHandle bos_i = adhBoundingObjects.inputValue(&status); CHECK_MSTATUS(status);

                    MMatrix bpm_i = bos_i.child(a_iBoundParentMatrix).asMatrix(); CHECK_MSTATUS(status);
                    //std::cout <<iEnterCount<<", bos["<<bosIdx<<"]="<< bpm_i <<std::endl;

                    BoundingObjectType bot_i = BoundingObjectType(bos_i.child(a_iBoundControlData).child(a_iBoundType).asShort());
                    //std::cout <<iEnterCount<<", bot="<< bot <<std::endl;

                    boost::shared_ptr<BoundingObjectUtilInterface> boundObject = BoundingObject::m_bouMgr.getShape(bot_i);

                    // check whether this vertex locates inside this bounding object
                    MPoint position_in_bound_os = position_ws[vId] * bpm_i.inverse();
                    if(boundObject->isInside(position_in_bound_os))
                    {
                        displayColor += boundObject->onInSide();
                        bVertexInSideBoundingObject = true;
                        break;// this vertex locates inside a bounding object
                    }else{
                        displayColor += boundObject->onOutSide();
                    }
                }//for(bosIdx, ...
                // set displayColor to vertex
                CHECK_MSTATUS(fnOutMesh.setVertexColor(displayColor, vId));
                }//bool bVertexInSideBoundingObject


                // The polygon is considered as being inside the bounding object
                // if one vertex of the polygon is in the bounding object
                if(bVertexInSideBoundingObject)
                {
                    bPolygonInSideBoundingObject = true;
                    break;
                }
            }//for(vIdInPoly, ...
            }//bool bPolygonInBoundingObject

            if(bPolygonInSideBoundingObject)
            {
                polygonInSideBoundingObjects.append(polyId);
            }
        }//for(polyId, ...
        //std::cout<< "polygonInSideBoundingObjects=" << polygonInSideBoundingObjects<< std::endl;

        dhOutGeometry.setClean();
        //
        MDataHandle dhOutComponents = data.outputValue(a_oOutComponents, &status); CHECK_MSTATUS(status);

        MFnComponentListData fnComponentList;
        MObject componentData(fnComponentList.create(&status)); CHECK_MSTATUS(status);
        {
            MFnSingleIndexedComponent vertices;
            MObject components(vertices.create(MFn::kMeshPolygonComponent, &status)); CHECK_MSTATUS(status);

            vertices.addElements(polygonInSideBoundingObjects);

            fnComponentList.add(components);
        }
        dhOutComponents.set(componentData);
        dhOutComponents.setClean();

        CHECK_MSTATUS(data.setClean(plug));
	}
	else{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}

MStatus initializePlugin_Group(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
		plugin.registerNode(
		Group::sTypeName(),
		Group::sTypeId(),
		Group::creator,
		Group::initialize,
		Group::sType(),
		&Group::sClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += Group::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}

MStatus uninitializePlugin_Group(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
        plugin.deregisterNode( Group::sTypeId() )
    );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += Group::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}
