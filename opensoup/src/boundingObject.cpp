#include "boundingObject.h"

#include <maya/MDistance.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MMatrix.h>
#include <common/api_macro.h>
#include <common/node_ids.h>
#include <common/log.h>
#include "version.h"

/////////////////////////////////////////////////////////////////////
// SHAPE NODE IMPLEMENTATION
/////////////////////////////////////////////////////////////////////
MString BoundingObject::m_classification("utility/general");
VersionHelper   BoundingObject::m_version_helper;
MObject BoundingObject::a_inParentMatrix;
MObject BoundingObject::a_outData;
MObject BoundingObject::    a_outParentMatrix;
MObject BoundingObject::    a_outControlData;
MObject BoundingObject::        a_outType;
//const float BoundingObject::R = 0.5f;
BoundingObjectUtilMgr BoundingObject::m_bouMgr;
/////////////////////////////////////////////////////////////////////
BoundingObject::BoundingObject()
{
}

BoundingObject::~BoundingObject()
{
}

/* override */
void BoundingObject::postConstructor()
//
// Description
//
//    When instances of this node are created internally, the MObject associated
//    with the instance is not created until after the constructor of this class
//    is called. This means that no member functions of MPxSurfaceShape can
//    be called in the constructor.
//    The postConstructor solves this problem. Maya will call this function
//    after the internal object has been created.
//    As a general rule do all of your initialization in the postConstructor.
//
{
    MStatus status;

	// This call allows the shape to have shading groups assigned
	//
	MPxNode::postConstructor();

	MObject this_mobj(thisMObject());
	m_callbackId = MNodeMessage::addAttributeChangedCallback(this_mobj, sAttrChangedCallback, this, &status);   CHECK_MSTATUS(status);

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

//bool BoundingObject::getInternalValueInContext(const MPlug& plug, MDataHandle& handle,  MDGContext& context)
//{
//    if (plug == a_outType)
//    {
//        handle.set( 0 );
//        LDbg("set object type to 0");
//        return true;
//    }else{
//        return MPxLocatorNode::getInternalValueInContext(plug, handle, context);
//    }
//}
//
//bool BoundingObject::setInternalValueInContext(const MPlug& plug, const MDataHandle& handle, MDGContext& context)
//{
//    if (plug == a_outType)
//    {
//        LDbg("get object type");
//        BoundingObjectType objType = BoundingObjectType(handle.asInt());
//        m_boundObject = createBoundingObjectUtil(objType);
//        return true;
//    }else{
//        return MPxLocatorNode::setInternalValueInContext(plug, handle, context);
//    }
//}

MString BoundingObject::sTypeName()
{
	return NodeTypeName_BoundingObject;
}

MTypeId BoundingObject::sTypeId()
{
	return NodeID_BoundingObject;
}

MPxNode::Type BoundingObject::sType()
{
	return MPxNode::kDependNode;
}

const MString& BoundingObject::sClassification()
{
	return m_classification;
}

/* override */
bool BoundingObject::isBounded() const { return true; }

/* override */
MBoundingBox BoundingObject::boundingBox() const
//
// Returns the bounding box for the shape.
// In this case just use the radius and height attributes
// to determine the bounding box.
//
{
	// Get the size
	//
	MPlug pOutType(thisMObject(), a_outType);
	int objectType;
    CHECK_MSTATUS(pOutType.getValue(objectType));

    return m_bouMgr.getShape(objectType)->getBoundingBox();
}

void* BoundingObject::creator()
{
	return new BoundingObject();
}

MStatus BoundingObject::initialize()
{
	MStatus				status;

	{// a_inParentMatrix
	    MFnMatrixAttribute mAttr;
	    a_inParentMatrix = mAttr.create("inParentMatrix", "ipm", MFnMatrixAttribute::kDouble, &status); CHECK_MSTATUS(status);
	    CHECK_MSTATUS(mAttr.setKeyable(true));
        CHECK_MSTATUS(mAttr.setStorable(true));
        CHECK_MSTATUS(mAttr.setReadable(false));// !
        CHECK_MSTATUS(mAttr.setWritable(true));
	    CHECK_MSTATUS(addAttribute(a_inParentMatrix));
	}

	{// a_outData
	    {// a_outParentMatrix
            MFnMatrixAttribute mAttr;
            a_outParentMatrix = mAttr.create("outParentMatrix", "opm", MFnMatrixAttribute::kDouble, &status); CHECK_MSTATUS(status);
            CHECK_MSTATUS(mAttr.setKeyable(false));
            CHECK_MSTATUS(mAttr.setStorable(false));
            CHECK_MSTATUS(mAttr.setReadable(true));
            CHECK_MSTATUS(mAttr.setWritable(true));// !
            CHECK_MSTATUS(addAttribute(a_outParentMatrix));
	    }
	    {// a_outControlData
	        {// a_outType
                MFnEnumAttribute eAttr;
                a_outType= eAttr.create("type", "tp", 0,  &status ); CHECK_MSTATUS(status);
                CHECK_MSTATUS(eAttr.addField("Sphere",          BOT_Sphere));
                CHECK_MSTATUS(eAttr.addField("Cube",            BOT_Cube));
                CHECK_MSTATUS(eAttr.addField("Capsule",         BOT_Capsule));
                CHECK_MSTATUS(eAttr.addField("Input Geometry",  BOT_InputGeometry));

                MAKE_INPUT(eAttr);
                CHECK_MSTATUS(addAttribute(a_outType));
	        }
            MFnCompoundAttribute cAttr;
            a_outControlData = cAttr.create( "controlData", "cd", &status); CHECK_MSTATUS(status);
            CHECK_MSTATUS(cAttr.addChild(a_outType));
            MAKE_INPUT(cAttr);
            CHECK_MSTATUS(addAttribute(a_outControlData));
	    }

        MFnCompoundAttribute cAttr;
        a_outData = cAttr.create( "outData", "od", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(cAttr.addChild(a_outParentMatrix));
        CHECK_MSTATUS(cAttr.addChild(a_outControlData));
        MAKE_INPUT(cAttr);//MAKE_OUTPUT(cAttr);
	    CHECK_MSTATUS(addAttribute(a_outData));
	}

    //
    //CHECK_MSTATUS(attributeAffects(a_outType,                   a_outControlData));

    //CHECK_MSTATUS(attributeAffects(a_outControlData,            a_outData));
    //CHECK_MSTATUS(attributeAffects(a_outControlData,                a_outParentMatrix));

    //CHECK_MSTATUS(attributeAffects(a_inParentMatrix,            a_outData));
    CHECK_MSTATUS(attributeAffects(a_inParentMatrix,                a_outParentMatrix));

	m_version_helper.initialize();

	return MS::kSuccess;
}

/* override */
MStatus BoundingObject::compute( const MPlug& plug, MDataBlock& datablock )
//
// Since there are no output attributes this is not necessary but
// if we wanted to compute an output mesh for rendering it would
// be done here base on the inputs.
//
{
    MStatus status;

	MString plugname(plug.name(&status));
	CHECK_MSTATUS(status);
	//LDbg("plugname=%s\n", plugname.asChar());


    if (plug == a_outData || plug.parent() == a_outData)
    {
        MDataHandle inhMatrix = datablock.inputValue(a_inParentMatrix, &status);    CHECK_MSTATUS( status );
        MMatrix imat = inhMatrix.asMatrix();	// Now get it
        //std::cout << "boundingObject::inParentMatrix="<< imat << std::endl;

        MDataHandle outhMatrix = datablock.outputValue(a_outParentMatrix, &status); CHECK_MSTATUS( status );
        outhMatrix.set(imat);
        outhMatrix.setClean();
    }else{
        return MS::kUnknownParameter;
    }

	return MS::kSuccess;
}

void BoundingObject::draw(M3dView &view, const MDagPath &path,
							M3dView::DisplayStyle style,
							M3dView::DisplayStatus displaystatus)
{
    MStatus status;

	MPlug pOutType(thisMObject(), a_outType);
	int objectType;
	CHECK_MSTATUS(pOutType.getValue(objectType));


	view.beginGL();


	if ((style == M3dView::kFlatShaded) ||
		(style == M3dView::kGouraudShaded))
	{
		// Push the color settings
		//
		glPushAttrib(GL_CURRENT_BIT);

		if (displaystatus == M3dView::kActive) {
			view.setDrawColor(13, M3dView::kActiveColors);
		} else {
			view.setDrawColor(13, M3dView::kDormantColors);
		}

		// draw elements here...

		glPopAttrib();
	}

	m_bouMgr.getShape(objectType)->draw();

	view.endGL();
}
//
// Dispatch to <this>
void BoundingObject::sAttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug, void* inUserPtr)
{
    assert(inUserPtr != NULL);

	BoundingObject* this_ = (BoundingObject*)inUserPtr;
	this_->AttrChangedCallback(inMsg, ioPlug, ioOtherPlug);
}

// Called when some attribute changed for our node.
void BoundingObject::AttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug)
{
	if (inMsg & MNodeMessage::kAttributeSet)
	{
		if (ioPlug==BoundingObject::a_outType)
		{
			// Get "paintTrans" plug
			MPlug pOutType(thisMObject(), BoundingObject::a_outType);

            int outType;
            CHECK_MSTATUS(pOutType.getValue(outType));
			LDbg("boundingObject type=%d", outType);
		}
	}
}

/////////////////////////////////////////////////////////////////////
MStatus initializePlugin_BoundingObject(MFnPlugin &plugin)
{
    CHECK_MSTATUS_AND_RETURN_IT(
        plugin.registerNode(
            BoundingObject::sTypeName(),
            BoundingObject::sTypeId(),
            &BoundingObject::creator,
            &BoundingObject::initialize,
            MPxNode::kLocatorNode
        )
    );

	return MS::kSuccess;
}

MStatus uninitializePlugin_BoundingObject(MFnPlugin &plugin)
{
    CHECK_MSTATUS_AND_RETURN_IT(
        plugin.deregisterNode(BoundingObject::sTypeId())
    );

	return MS::kSuccess;
}
