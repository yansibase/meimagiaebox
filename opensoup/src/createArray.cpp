//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+

// Example Plugin: lambertShader.cpp
//
// Produces dependency graph node LambertShader
// This node is an example of a Lambert shader and how to build a
// dependency node as a surface shader in Maya. The inputs for this node
// are many, and can be found in the Maya UI on the Attribute Editor for
// the node. The output attributes for the node are "outColor" and
// "outTransparency". To use this shader, create a lambertShader with
// Shading Group or connect the outputs to a Shading Group's
// "SurfaceShader" attribute.
//

#include "createArray.h"

#include <maya/MFnEnumAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MVectorArray.h>

#include <common/api_macro.h>
#include <common/node_ids.h>
#include <common/log.h>
#include "version.h"


/////////////////////////////////
// Plugin Lambert Shader Class //
/////////////////////////////////


// This class will create a new shader. Shaders are custom dependency
// graph objects so we will derive this class from the basic DG node
// type MPxNode
//


// IFF type ID
// Each node requires a unique identifier which is used by
// MFnDependencyNode::create() to identify which node to create, and by
// the Maya file format.
//
// For local testing of nodes you can use any identifier between
// 0x00000000 and 0x0007ffff, but for any node that you plan to use for
// more permanent purposes, you should get a universally unique id from
// Autodesk Support. You will be assigned a unique range that you
// can manage on your own.
//
MString CreateArray::m_classification("utility/general");
VersionHelper   CreateArray::m_version_helper;
MObject CreateArray::a_iLength;
MObject CreateArray::a_iOutputType;
MObject CreateArray::a_iDefaultValueX;
MObject CreateArray::a_iDefaultValueY;
MObject CreateArray::a_iDefaultValueZ;
MObject CreateArray::a_iDefaultValueW;
MObject CreateArray::a_oOutArray;
///////////////////////////////////////////////////////
// DESCRIPTION: attribute information
///////////////////////////////////////////////////////
//



// the postConstructor() function is called immediately after the objects
// constructor. It is not safe to call MPxNode member functions from the
// constructor, instead they should be called here.
//
void CreateArray::postConstructor( )
{
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

MString CreateArray::sTypeName()
{
	return NodeTypeName_CreateArray;
}

MTypeId CreateArray::sTypeId()
{
	return NodeID_CreateArray;
}

MPxNode::Type CreateArray::sType()
{
	return MPxNode::kDependNode;
}

const MString& CreateArray::sClassification()
{
	return m_classification;
}


// This node does not need to perform any special actions on creation or
// destruction
//

CreateArray::CreateArray()
{
}

CreateArray::~CreateArray()
{
}


// The creator() method allows Maya to instantiate instances of this node.
// It is called every time a new instance of the node is requested by
// either the createNode command or the MFnDependencyNode::create()
// method.
//
// In this case creator simply returns a new node_template object.
//

void* CreateArray::creator()
{
	return new CreateArray();
}


// The initialize method is called only once when the node is first
// registered with Maya. In this method you define the attributes of the
// node, what data comes in and goes out of the node that other nodes may
// want to connect to.
//

MStatus CreateArray::initialize()
{
	MStatus status; // Status will be used to hold the MStatus value
	// returned by each api function call. It is important
	// to check the status returned by a call to aid in
	// debugging. Failed API calls can result in subtle
	// errors that can be difficult to track down, you may
	// wish to use the CHECK_MSTATUS macro for any API
	// call where you do not need to provide your own
	// error handling.
	//

	// Attribute Initialization:
	//
	// create      - The create function creates a new attribute for the
	//				 node, it takes a long name for the attribute, a short
	//				 name for the attribute, the type of the attribute,
	//				 and a status object to determine if the api call was
	//				 successful.
	//
	// setKeyable  - Sets whether this attribute should accept keyframe
	//				 data, Attributes are not keyable by default.
	//
	// setStorable - Sets whether this attribute should be storable. If an
	//				 attribute is storable, then it will be writen out
	//				 when the node is stored to a file. Attributes are
	//               storable by default.
	//
	// setDefault  - Sets the default value for this attribute.
	//
	// setUsedAsColor - Sets whether this attribute should be presented as
	//				 a color in the UI.
	//
	// setHidden   - Sets whether this attribute should be hidden from the
	//				 UI. This is useful if the attribute is being used for
	//				 blind data, or if it is being used as scratch space
	//				 for a geometry calculation (should also be marked
	//				 non-connectable in that case). Attributes are not
	//				 hidden by default.
	//
	// setReadable - Sets whether this attribute should be readable. If an
	//				 attribute is readable, then it can be used as the
	//				 source in a dependency graph connection. Attributes
	//				 are readable by default.
	//
	// setWritable - Sets whether this attribute should be readable. If an
	//				 attribute is writable, then it can be used as the
	//				 destination in a dependency graph connection. If an
	//			     attribute is not writable then setAttr commands will
	//				 fail to change the attribute. If both keyable and
	//				 writable for an attribute are set to true it will be
	//				 displayed in the channel box when the node is
	//				 selected. Attributes are writable by default.
	//
	// setArray    - Sets whether this attribute should have an array of
	//				 data. This should be set to true if the attribute
	//				 needs to accept multiple incoming connections.
	//				 Attributes are single elements by default.
	//

	// Input Attributes
	//



	{// a_iLength
        MFnNumericAttribute nAttr;
        a_iLength = nAttr.create( "length", "l", MFnNumericData::kInt, 0, &status );
        CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);

        CHECK_MSTATUS(addAttribute(a_iLength));
	}

	{// a_iOutputType, ,
	    MFnEnumAttribute eAttr;
        a_iOutputType= eAttr.create("outputType", "ot", 0,  &status );
        CHECK_MSTATUS(status);

        CHECK_MSTATUS(eAttr.addField("1 integer",           OT_1integer));
        CHECK_MSTATUS(eAttr.addField("1 double",            OT_1double));
        CHECK_MSTATUS(eAttr.addField("2 doubles(UV)",       OT_2doubles));
        CHECK_MSTATUS(eAttr.addField("3 doubles(XYZ/RGB)",  OT_3doubles));
        CHECK_MSTATUS(eAttr.addField("vector(XYZ/RGB)",     OT_vector));
        CHECK_MSTATUS(eAttr.addField("4 doubles(RGBA)",     OT_4doubles));

        MAKE_INPUT(eAttr);

        CHECK_MSTATUS(addAttribute(a_iOutputType));
	}

	{// a_iDefaultValueX
        MFnNumericAttribute nAttr;
        a_iDefaultValueX = nAttr.create( "defaultValueX", "dvx", MFnNumericData::kDouble, 0.0, &status);
        CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iDefaultValueX));
	}
	{// a_iDefaultValueY
        MFnNumericAttribute nAttr;
        a_iDefaultValueY = nAttr.create( "defaultValueY", "dvy", MFnNumericData::kDouble, 0.0, &status);
        CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iDefaultValueY));
	}
	{// a_iDefaultValueZ
        MFnNumericAttribute nAttr;
        a_iDefaultValueZ = nAttr.create( "defaultValueZ", "dvz", MFnNumericData::kDouble, 0.0, &status);
        CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iDefaultValueZ));
	}
	{// a_iDefaultValueW
        MFnNumericAttribute nAttr;
        a_iDefaultValueW = nAttr.create( "defaultValueW", "dvw", MFnNumericData::kDouble, 0.0, &status);
        CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iDefaultValueW));
	}

	// Output Attributes
	//
	{// a_oOutArray, ,
        MFnGenericAttribute gAttr;
        a_oOutArray = gAttr.create( "outArray", "oa", &status);
        CHECK_MSTATUS(status);
        //CHECK_MSTATUS(gAttr.addNumericDataAccept(MFnNumericData::kInt));
        //CHECK_MSTATUS(gAttr.addNumericDataAccept(MFnNumericData::kDouble));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kVectorArray));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kDoubleArray));
        //CHECK_MSTATUS(gAttr.addDataAccept(MFnData::k2Double));
        //CHECK_MSTATUS(gAttr.addDataAccept(MFnData::k3Double));
        //CHECK_MSTATUS(gAttr.addDataAccept(MFnData::k4Double));

        MAKE_OUTPUT(gAttr);
        //CHECK_MSTATUS(gAttr.setArray(true));
        //CHECK_MSTATUS(gAttr.setUsesArrayDataBuilder(true));

        CHECK_MSTATUS(addAttribute(a_oOutArray));
	}

    CHECK_MSTATUS(attributeAffects(a_iLength,           a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iOutputType,       a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iDefaultValueX,    a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iDefaultValueY,    a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iDefaultValueZ,    a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iDefaultValueW,    a_oOutArray));
    //
	m_version_helper.initialize();

	return( MS::kSuccess );
}


// The compute() method does the actual work of the node using the inputs
// of the node to generate its output.
//
// Compute takes two parameters: plug and data.
// - Plug is the the data value that needs to be recomputed
// - Data provides handles to all of the nodes attributes, only these
//   handles should be used when performing computations.
//
MStatus CreateArray::compute( const MPlug& plug, MDataBlock& data )
{
    MStatus status;
	// The plug parameter will allow us to determine which output attribute
	// needs to be calculated.
	//
    if (plug == a_oOutArray)
    {
        MDataHandle dhLength = data.inputValue(a_iLength, &status);  CHECK_MSTATUS(status);
        const int& length = dhLength.asInt();
        //LDbg("length=%d", length);

        if(length > 0)
        {
            MDataHandle dhOutputType = data.inputValue(a_iOutputType, &status);  CHECK_MSTATUS(status);
            const int& outputType = dhOutputType.asInt();
            //LDbg("outputType=%d", outputType);

            MDataHandle dhDefaultValueX = data.inputValue(a_iDefaultValueX, &status);  CHECK_MSTATUS(status);
            MDataHandle dhDefaultValueY = data.inputValue(a_iDefaultValueY, &status);  CHECK_MSTATUS(status);
            MDataHandle dhDefaultValueZ = data.inputValue(a_iDefaultValueZ, &status);  CHECK_MSTATUS(status);
            MDataHandle dhDefaultValueW = data.inputValue(a_iDefaultValueW, &status);  CHECK_MSTATUS(status);
            const double& defaultValueX = dhDefaultValueX.asDouble();
            const double& defaultValueY = dhDefaultValueY.asDouble();
            const double& defaultValueZ = dhDefaultValueZ.asDouble();
            const double& defaultValueW = dhDefaultValueW.asDouble();
            //LDbg("defaultValues=%f, %f, %f, %f", defaultValueX, defaultValueY, defaultValueZ, defaultValueW);

            switch(outputType)
            {
            case OT_1integer:
                {
                    MIntArray array;
                    CHECK_MSTATUS(array.setLength(length));

                    // set values
                    for(unsigned int i=0; i<array.length(); ++i)
                    {
                        array[i] = int(defaultValueX);
                    }

                    MFnIntArrayData fn;
                    MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
                    MDataHandle dh(data.outputValue(a_oOutArray, &status));    CHECK_MSTATUS(status);
                    CHECK_MSTATUS(dh.set(o));
                }break;
            case OT_1double:
                {
                    MDoubleArray array;
                    CHECK_MSTATUS(array.setLength(length));

                    // set values
                    for(unsigned int i=0; i<array.length(); ++i)
                    {
                        array[i] = defaultValueX;
                    }

                    MFnDoubleArrayData fn;
                    MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
                    MDataHandle dh(data.outputValue(a_oOutArray, &status));    CHECK_MSTATUS(status);
                    CHECK_MSTATUS(dh.set(o));
                }break;
            case OT_2doubles:
                {
                    MDoubleArray array;
                    CHECK_MSTATUS(array.setLength(2 * length));

                    // set values
                    for(unsigned int i=0; i<array.length(); i=i+2)
                    {
                        array[i  ] = defaultValueX;
                        array[i+1] = defaultValueY;
                    }

                    MFnDoubleArrayData fn;
                    MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
                    MDataHandle dh(data.outputValue(a_oOutArray, &status));    CHECK_MSTATUS(status);
                    CHECK_MSTATUS(dh.set(o));
                }break;
            case OT_3doubles:
                {
                    MDoubleArray array;
                    CHECK_MSTATUS(array.setLength(3 * length));

                    // set values
                    for(unsigned int i=0; i<array.length(); i=i+3)
                    {
                        array[i  ] = defaultValueX;
                        array[i+1] = defaultValueY;
                        array[i+2] = defaultValueZ;
                    }

                    MFnDoubleArrayData fn;
                    MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
                    MDataHandle dh(data.outputValue(a_oOutArray, &status));    CHECK_MSTATUS(status);
                    CHECK_MSTATUS(dh.set(o));
                }break;
            case OT_vector:
                {
                    MVectorArray array;
                    CHECK_MSTATUS(array.setLength(length));

                    // set values
                    for(unsigned int i=0; i<array.length(); i=i+1)
                    {
                        array[i].x = defaultValueX;
                        array[i].y = defaultValueY;
                        array[i].z = defaultValueZ;
                    }

                    MFnVectorArrayData fn;
                    MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
                    MDataHandle dh(data.outputValue(a_oOutArray, &status));    CHECK_MSTATUS(status);
                    CHECK_MSTATUS(dh.set(o));
                }break;
            case OT_4doubles:
                {
                    MDoubleArray array;
                    CHECK_MSTATUS(array.setLength(4 * length));

                    // set values
                    for(unsigned int i=0; i<array.length(); i=i+4)
                    {
                        array[i  ] = defaultValueX;
                        array[i+1] = defaultValueY;
                        array[i+2] = defaultValueZ;
                        array[i+3] = defaultValueW;
                    }

                    MFnDoubleArrayData fn;
                    MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
                    MDataHandle dh(data.outputValue(a_oOutArray, &status));    CHECK_MSTATUS(status);
                    CHECK_MSTATUS(dh.set(o));
                }break;
            default:
                {
                    LErr("Unknown output type: %d", outputType);
                }break;
            }

        }
        data.setClean(plug);
	}
	else
		return MS::kUnknownParameter;

	return MS::kSuccess;
}

MStatus initializePlugin_CreateArray(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
		plugin.registerNode(
		CreateArray::sTypeName(),
		CreateArray::sTypeId(),
		CreateArray::creator,
		CreateArray::initialize,
		CreateArray::sType(),
		&CreateArray::sClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += CreateArray::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}

MStatus uninitializePlugin_CreateArray(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
        plugin.deregisterNode( CreateArray::sTypeId() )
    );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += CreateArray::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}
