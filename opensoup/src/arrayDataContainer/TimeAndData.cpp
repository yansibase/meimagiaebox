#include "TimeAndData.h"
#include <common/log.h>
/*
TimeAndData::TimeAndData(const double time_, const MDoubleArray& data_)
{
    m_time = time_;
    m_data = data_;
}

TimeAndData::TimeAndData(const TimeAndData& o)
{
    m_time = o.m_time;
    m_data = o.m_data;
}

TimeAndData::~TimeAndData()
{
}

TimeAndData& TimeAndData::operator=(const TimeAndData& o)
{
    TimeAndData tmp(o);
    ::swap(tmp, *this);
    return *this;
}

void TimeAndData::swap(TimeAndData& o)
{
    using std::swap;
    swap(this->m_time, o.m_time);
    swap(this->m_data, o.m_data);
}

void TimeAndData::swap(MDoubleArray &l, MDoubleArray &r)
{
    MDoubleArray tmp;
    tmp = l;
    l   = r;
    r   = tmp;
}

bool TimeAndData::operator<(const TimeAndData& o) const
{
    return (m_time < o.m_time);
}

void swap(TimeAndData& l, TimeAndData &r)
{
    l.swap(r);
}
*/
/////////////////////////////////////////////////////////////////////
TimeAndDataQueue::TimeAndDataQueue()
{

}

TimeAndDataQueue::~TimeAndDataQueue()
{

}

void TimeAndDataQueue::push(const MTime& time, const MDoubleArray& array)
{
/*
    if(m_data.find(time) == m_data.end())// not found
    {
        LDbg("array at time %d is NOT found", time);
        if(array.length() != 0)
        {
            LDbg("add new array");
            std::vector<double> tmp;
            tmp.reserve(array.length());
            for(unsigned int i = 0; i < array.length(); ++i)
            {
                tmp.push_back(array[i]);
            }
            m_data[time] = tmp;

        }else{
            LDbg("trying to add array at time %d, BUT the array is empty", time);
        }

    }else{// found
        LDbg("array at time %d is found", time);
        if(m_data.find(time)->second.size() == 0)
        {
            LDbg("BUT the old array is empty");
            if(array.length() == 0)
            {
                LDbg("the new array is empty TOO");
            }else{
                LDbg("the new array is NOT empty, set the new array");
                std::vector<double> tmp;
                tmp.reserve(array.length());
                for(unsigned int i = 0; i < array.length(); ++i)
                {
                    tmp.push_back(array[i]);
                }
                m_data[time] = tmp;
            }
        }
    }
*/
    std::vector<double> tmp;
    tmp.reserve(array.length());
    for(unsigned int i = 0; i < array.length(); ++i)
    {
        tmp.push_back(array[i]);
    }
    m_data[time] = tmp;

    print("after push()");
}
//
//void TimeAndDataQueue::push(const MTime& time, const MFloatArray&  array)
//{
//    MDoubleArray doubleArray(array.length(), 0.0);
//    for(unsigned int i = 0; i < doubleArray.length(); ++i)
//    {
//        doubleArray[ i ] = array[ i ];
//    }
//
//    push(time, doubleArray);
//}

bool TimeAndDataQueue::query(const MTime& time, MDoubleArray& array)
{
    if(m_data.find(time) == m_data.end())
    {
        return false;// not found
    }else{
        array.clear();

        const std::vector<double> &tmp = m_data.at(time);
        for(std::size_t i = 0; i < tmp.size(); ++i)
        {
            array.append(tmp[i]);
        }

        return true;// found
    }
}

void TimeAndDataQueue::print(const std::string& msg)
{
    LDbg("%s", msg.c_str());

    std::map<const MTime, std::vector<double> >::iterator b = m_data.begin();
    std::map<const MTime, std::vector<double> >::iterator e = m_data.end();

    for (std::map<const MTime, std::vector<double> >::iterator i = b; i!=e; ++i)
    {
        std::cout << i->first.value() << " => ";//<< i->second << std::endl;

        for(std::size_t j = 0; j < i->second.size(); ++j)
        {
            printf("%.2f, ", i->second[j]);
        }
        std::cout << std::endl;
    }
}
