#pragma once

#include <map>
#include <queue>
#include <string>
#include <vector>

#include <maya/MDoubleArray.h>
#include <maya/MFloatArray.h>
#include <maya/MTime.h>
/*
class TimeAndData
{
public:
    TimeAndData(const double time_, const MDoubleArray& data_);
    ~TimeAndData();

    TimeAndData(const TimeAndData& o);
    TimeAndData& operator=(const TimeAndData& o);
    void swap(TimeAndData& o);
    void swap(MDoubleArray &l, MDoubleArray &r);
    bool operator<(const TimeAndData& o) const;

    double       m_time;
    MDoubleArray m_data;

protected:

};

void swap(TimeAndData& l, TimeAndData &r);
*/
/////////////////////////////////////////////////////////////////////
class TimeAndDataQueue
{
public:
    TimeAndDataQueue();
    ~TimeAndDataQueue();

    void push(const MTime& time, const MDoubleArray& array);
    //void push(const MTime& time, const MFloatArray&  array);
    bool query(const MTime& time, MDoubleArray& array);

    void print(const std::string& msg);
protected:
    TimeAndDataQueue(const TimeAndDataQueue& o);
    TimeAndDataQueue& operator=(const TimeAndDataQueue& o);

    //std::priority_queue<TimeAndData> m_queue;
    std::map<const MTime, std::vector<double> > m_data;
};
