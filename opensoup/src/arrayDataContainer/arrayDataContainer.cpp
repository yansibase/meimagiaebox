#include "arrayDataContainer.h"

#include <cassert>

#include <maya/MFnEnumAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnFloatArrayData.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>

#include <common/api_macro.h>
#include <common/log.h>
#include <common/node_ids.h>

#include "../version.h"
//#include "../boundingObject.h"
//#include "../boundingObject_util.h"
/////////////////////////////////
// Plugin Lambert Shader Class //
/////////////////////////////////


// This class will create a new shader. Shaders are custom dependency
// graph objects so we will derive this class from the basic DG node
// type MPxNode
//


// IFF type ID
// Each node requires a unique identifier which is used by
// MFnDependencyNode::create() to identify which node to create, and by
// the Maya file format.
//
// For local testing of nodes you can use any identifier between
// 0x00000000 and 0x0007ffff, but for any node that you plan to use for
// more permanent purposes, you should get a universally unique id from
// Autodesk Support. You will be assigned a unique range that you
// can manage on your own.
//
MString ArrayDataContainer::m_classification("utility/general");
VersionHelper   ArrayDataContainer::m_version_helper;
MObject ArrayDataContainer::a_iEnvelope;
MObject ArrayDataContainer::a_iInArray;
MObject ArrayDataContainer::a_iInTime;
MObject ArrayDataContainer::a_iSink;
MObject ArrayDataContainer::a_oOutArray;

///////////////////////////////////////////////////////
// DESCRIPTION: attribute information
///////////////////////////////////////////////////////
//



// the postConstructor() function is called immediately after the objects
// constructor. It is not safe to call MPxNode member functions from the
// constructor, instead they should be called here.
//
void ArrayDataContainer::postConstructor( )
{
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

MString ArrayDataContainer::sTypeName()
{
	return NodeTypeName_ArrayDataContainer;
}

MTypeId ArrayDataContainer::sTypeId()
{
	return NodeID_ArrayDataContainer;
}

MPxNode::Type ArrayDataContainer::sType()
{
	return MPxNode::kDependNode;
}

const MString& ArrayDataContainer::sClassification()
{
	return m_classification;
}


// This node does not need to perform any special actions on creation or
// destruction
//

ArrayDataContainer::ArrayDataContainer()
{
}

ArrayDataContainer::~ArrayDataContainer()
{
}


// The creator() method allows Maya to instantiate instances of this node.
// It is called every time a new instance of the node is requested by
// either the createNode command or the MFnDependencyNode::create()
// method.
//
// In this case creator simply returns a new node_template object.
//

void* ArrayDataContainer::creator()
{
	return new ArrayDataContainer();
}


// The initialize method is called only once when the node is first
// registered with Maya. In this method you define the attributes of the
// node, what data comes in and goes out of the node that other nodes may
// want to connect to.
//

MStatus ArrayDataContainer::initialize()
{
	MStatus status; // Status will be used to hold the MStatus value
	// returned by each api function call. It is important
	// to check the status returned by a call to aid in
	// debugging. Failed API calls can result in subtle
	// errors that can be difficult to track down, you may
	// wish to use the CHECK_MSTATUS macro for any API
	// call where you do not need to provide your own
	// error handling.
	//

	// Attribute Initialization:
	//
	// create      - The create function creates a new attribute for the
	//				 node, it takes a long name for the attribute, a short
	//				 name for the attribute, the type of the attribute,
	//				 and a status object to determine if the api call was
	//				 successful.
	//
	// setKeyable  - Sets whether this attribute should accept keyframe
	//				 data, Attributes are not keyable by default.
	//
	// setStorable - Sets whether this attribute should be storable. If an
	//				 attribute is storable, then it will be writen out
	//				 when the node is stored to a file. Attributes are
	//               storable by default.
	//
	// setDefault  - Sets the default value for this attribute.
	//
	// setUsedAsColor - Sets whether this attribute should be presented as
	//				 a color in the UI.
	//
	// setHidden   - Sets whether this attribute should be hidden from the
	//				 UI. This is useful if the attribute is being used for
	//				 blind data, or if it is being used as scratch space
	//				 for a geometry calculation (should also be marked
	//				 non-connectable in that case). Attributes are not
	//				 hidden by default.
	//
	// setReadable - Sets whether this attribute should be readable. If an
	//				 attribute is readable, then it can be used as the
	//				 source in a dependency graph connection. Attributes
	//				 are readable by default.
	//
	// setWritable - Sets whether this attribute should be readable. If an
	//				 attribute is writable, then it can be used as the
	//				 destination in a dependency graph connection. If an
	//			     attribute is not writable then setAttr commands will
	//				 fail to change the attribute. If both keyable and
	//				 writable for an attribute are set to true it will be
	//				 displayed in the channel box when the node is
	//				 selected. Attributes are writable by default.
	//
	// setArray    - Sets whether this attribute should have an array of
	//				 data. This should be set to true if the attribute
	//				 needs to accept multiple incoming connections.
	//				 Attributes are single elements by default.
	//

    //
    // Input Attributes
    //
    {// a_iEnvelope
        MFnNumericAttribute nAttr;
        a_iEnvelope = nAttr.create("envelope", "en", MFnNumericData::kBoolean, true, &status );    CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iEnvelope));
    }
	{// a_iInArray
        MFnGenericAttribute gAttr;
        a_iInArray = gAttr.create( "inArray", "ia", &status);     CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kDoubleArray));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kFloatArray));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kVectorArray));
        MAKE_INPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_iInArray));
	}
    {// a_iInTime
        MFnNumericAttribute nAttr;
        a_iInTime = nAttr.create("inTime", "it", MFnNumericData::kDouble, 0.0, &status );    CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iInTime));
    }
    {// a_iInSink
        MFnNumericAttribute nAttr;
        a_iSink = nAttr.create("sink", "s", MFnNumericData::kDouble, 0.3, &status );    CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iSink));
    }

    //
    // Output Attributes
    //
	{// a_oOutArray
        MFnGenericAttribute gAttr;
        a_oOutArray = gAttr.create( "outArray", "oa", &status);     CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kDoubleArray));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kFloatArray));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kVectorArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutArray));
	}


    CHECK_MSTATUS(attributeAffects(a_iEnvelope,               a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iInArray,                a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iInTime,                 a_oOutArray));
    CHECK_MSTATUS(attributeAffects(a_iSink,                 a_oOutArray));

    //
	m_version_helper.initialize();

	return( MS::kSuccess );
}


// The compute() method does the actual work of the node using the inputs
// of the node to generate its output.
//
// Compute takes two parameters: plug and data.
// - Plug is the the data value that needs to be recomputed
// - Data provides handles to all of the nodes attributes, only these
//   handles should be used when performing computations.
//
MStatus ArrayDataContainer::compute( const MPlug& plug, MDataBlock& data )
{
    static int iEnterCount = 0;
    iEnterCount++;

    MStatus status;

	MString plugname(plug.name(&status));
	CHECK_MSTATUS(status);
	LDbg("%d, plugname=%s\n", iEnterCount, plugname.asChar());

	m_queue.print("---------------");

    if (plug == a_oOutArray)
    {
        ////////////////////////////////////////
        MDataHandle dhEnvelope       = data.inputValue(a_iEnvelope, &status);      CHECK_MSTATUS(status);
        MDataHandle dhInTime         = data.inputValue(a_iInTime, &status);        CHECK_MSTATUS(status);
        MDataHandle dhSink           = data.inputValue(a_iSink, &status);          CHECK_MSTATUS(status);
        MDataHandle dhInArray        = data.inputValue(a_iInArray, &status);       CHECK_MSTATUS(status);
        MDataHandle dhOutArray       = data.outputValue(a_oOutArray, &status);     CHECK_MSTATUS(status);

        //MArrayDataHandle adhInArray(dhInArray, &status);       CHECK_MSTATUS(status);

        const bool   envelope         = dhEnvelope.asBool();
        const double inTime           = dhInTime.asDouble();
        const double startTime        = 0.0;
        const double sink             = dhSink.asDouble();
        const MFnData::Type arrayType = dhInArray.type();

        LDbg("envelope=%d, inTime=%f, sink=%f, arraytype=%d", envelope, inTime, sink, arrayType);

        MObject mobjOutArray;
        if(envelope)
        {
            switch(arrayType)
            {
            case MFnData::kDoubleArray:
                {
                    MFnDoubleArrayData fn(dhInArray.data(), &status); CHECK_MSTATUS(status);
                    MDoubleArray inArray = fn.array(&status);  CHECK_MSTATUS(status);

                    LDbg("inArray.length()=%d", inArray.length());
                    MDoubleArray outArray(inArray.length(), 0.0);

                    kernel(inTime, startTime, sink, inArray, outArray);

                    print("outArray", outArray);
                    mobjOutArray = fn.create(outArray, &status);                       CHECK_MSTATUS(status);
                }break;
            case MFnData::kFloatArray:
                {
                    MFnFloatArrayData fn(dhInArray.data(), &status); CHECK_MSTATUS(status);
                    MFloatArray inArray = fn.array(&status);  CHECK_MSTATUS(status);

                    LDbg("inArray.length()=%d", inArray.length());
                    MFloatArray outArray(inArray.length(), 0.0f);

                    kernel(inTime, startTime, sink, inArray, outArray);

                    print("outArray", outArray);
                    mobjOutArray = fn.create(outArray, &status);                       CHECK_MSTATUS(status);
                }break;
            case MFnData::kVectorArray:
                {
                    MFnVectorArrayData fn(dhInArray.data(), &status); CHECK_MSTATUS(status);
                    MVectorArray inArray = fn.array(&status);  CHECK_MSTATUS(status);

                    LDbg("inArray.length()=%d", inArray.length());
                    MVectorArray outArray(inArray.length(), MVector::zero);

                    kernel(inTime, startTime, sink, inArray, outArray);

                    print("outArray", outArray);
                    mobjOutArray = fn.create(outArray, &status);                       CHECK_MSTATUS(status);
                }break;
            default:
                {
                    assert(0 && "unhandled input array type");
                }
            }


        }// if envelope
        else
        {
            switch(arrayType)
            {
            case MFnData::kDoubleArray:
                {
                    MFnDoubleArrayData fn(dhInArray.data(), &status); CHECK_MSTATUS(status);
                    MDoubleArray outArray(fn.length(), 0.0);
                    mobjOutArray = fn.create(outArray, &status);                       CHECK_MSTATUS(status);
                }break;
            case MFnData::kFloatArray:
                {
                    MFnFloatArrayData fn(dhInArray.data(), &status); CHECK_MSTATUS(status);
                    MFloatArray outArray(fn.length(), 0.0f);
                    mobjOutArray = fn.create(outArray, &status);                       CHECK_MSTATUS(status);
                }break;
            case MFnData::kVectorArray:
                {
                    MFnVectorArrayData fn(dhInArray.data(), &status); CHECK_MSTATUS(status);
                    MVectorArray outArray(fn.length(), MVector::zero);
                    mobjOutArray = fn.create(outArray, &status);                       CHECK_MSTATUS(status);
                }break;
            default:
                {
                    assert(0 && "unhandled input array type");
                }
            }
        }// if envelope else
        CHECK_MSTATUS(dhOutArray.set(mobjOutArray));
        dhOutArray.setClean();

        CHECK_MSTATUS(data.setClean(plug));
	}//if (plug == a_oOutArray)
	else{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}
void ArrayDataContainer::kernel(const double inTime, const double startTime, const double sink, const MDoubleArray& in, MDoubleArray& out)
{
    const double delta = 5.0;

    m_queue.push(MTime(inTime), in);

//    MDoubleArray privious;
//    if(m_queue.query(MTime(inTime-delta), privious))
//    {
//        const double clamp_min = 0.0;
//        const double clamp_max = 1.0;
//
//    //    const double deltaTime = inTime - startTime;
//
//        for(unsigned int i = 0; i < in.length(); ++i)
//        {
//            out[ i ] = in[ i ]*(1.0-sink) + privious[ i ]*sink;
//
//            clamp(out[ i ], clamp_min, clamp_max);
//        }
//    }

    std::vector<double> weights;
    double sumWeight = 0.0;
    for(int i=0; i <= delta; ++i)
    {
        const double weight_i = evaluate(i);
        weights.push_back( weight_i );
        sumWeight += weight_i;
        //LDbg("weight[%d]=%f", i, weight_i);
    }

//    const double clamp_min = 0.0;
//    const double clamp_max = 1.0;

    for(int j=0; j <= delta; ++j)
    {
        MDoubleArray preTime;
        if(m_queue.query(MTime(inTime-j), preTime))
        {
            //LDbg("time %f", inTime-j);
            for(unsigned int i = 0; i < in.length(); ++i)
            {
//                double pre_out_i = out[ i ];
                out[ i ] += preTime[ i ] * (weights[j]);
                //LDbg("%f = %f + %f * %f", out[ i ], pre_out_i, preTime[ i ], weights[j]/sumWeight);
                //clamp(out[ i ], clamp_min, clamp_max);

            }
        }
    }
}
void ArrayDataContainer::kernel(const double inTime, const double startTime, const double sink, const MFloatArray& in,  MFloatArray& out)
{
    MDoubleArray inDouble(in.length(), 0.0);
    for(unsigned int i = 0; i < inDouble.length(); ++i)
    {
        inDouble[ i ] = in[ i ];
    }

    MDoubleArray outDouble(in.length(), 0.0);
    kernel(inTime, startTime, sink, inDouble, outDouble);

    for(unsigned int i = 0; i < out.length(); ++i)
    {
        out[ i ] = outDouble[ i ];
    }
}
void ArrayDataContainer::kernel(const double inTime, const double startTime, const double sink, const MVectorArray& in, MVectorArray& out)
{
    const double clamp_min = 0.0;
    const double clamp_max = 1.0;

//    const double deltaTime = inTime - startTime;

    for(unsigned int i = 0; i < in.length(); ++i)
    {
        out[ i ].x = in[ i ].x - sink;
        out[ i ].y = in[ i ].y - sink;
        out[ i ].z = in[ i ].z - sink;

        clamp(out[ i ], clamp_min, clamp_max);
    }
}
double ArrayDataContainer::evaluate(const double x)
{
    //assert(x != -1.0);
    //return 1.0/(x + 0.5);
    //return 1.0;
    //return -x + 5.0;
    return (exp(-0.5 * x*x)/sqrt(2.0 * M_PI));//
}
//-------------------------------------------------------------------
MStatus initializePlugin_ArrayDataContainer(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
		plugin.registerNode(
		ArrayDataContainer::sTypeName(),
		ArrayDataContainer::sTypeId(),
		ArrayDataContainer::creator,
		ArrayDataContainer::initialize,
		ArrayDataContainer::sType(),
		&ArrayDataContainer::sClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += ArrayDataContainer::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}

MStatus uninitializePlugin_ArrayDataContainer(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
        plugin.deregisterNode( ArrayDataContainer::sTypeId() )
    );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += ArrayDataContainer::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}
