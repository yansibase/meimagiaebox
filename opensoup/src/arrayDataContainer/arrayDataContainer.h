#pragma once


//#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatArray.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>


#include <common/IncludeMFnPluginClass.h>
#include <common/version_helper.h>
#include "TimeAndData.h"


class ArrayDataContainer : public MPxNode
{
public:
                            ArrayDataContainer();
                            ~ArrayDataContainer();

	static  void *	        creator();
	virtual MStatus	        compute( const MPlug&, MDataBlock& );
	static  MStatus	        initialize();
	virtual void	        postConstructor();

	static MString          sTypeName();
	static MTypeId          sTypeId();
	static MPxNode::Type    sType();
	static const MString&   sClassification();

protected:
	static MString          m_classification;
	static VersionHelper    m_version_helper;

    static MObject          a_iEnvelope;
    static MObject          a_iInArray;
    static MObject          a_iInTime;
    static MObject          a_iSink;
    static MObject          a_oOutArray;
private:
            void            kernel(const double inTime, const double startTime, const double sink, const MDoubleArray& in, MDoubleArray& out) ;
            void            kernel(const double inTime, const double startTime, const double sink, const MFloatArray& in,  MFloatArray& out)  ;
            void            kernel(const double inTime, const double startTime, const double sink, const MVectorArray& in, MVectorArray& out) ;

    inline static void clamp(double&  value, const double min_, const double max_)
    {
        value = (value < min_) ? min_ : value;
        value = (value > max_) ? max_ : value;
    }

    inline static void clamp(float&   value, const float  min_, const float  max_)
    {
        value = (value < min_) ? min_ : value;
        value = (value > max_) ? max_ : value;
    }

    inline static void clamp(MVector& value, const double min_, const double max_)
    {
        clamp(value.x, min_, max_);
        clamp(value.y, min_, max_);
        clamp(value.z, min_, max_);
    }

    inline static double    evaluate(const double x);

    TimeAndDataQueue        m_queue;
};

MStatus initializePlugin_ArrayDataContainer(MFnPlugin &plugin);
MStatus uninitializePlugin_ArrayDataContainer(MFnPlugin &plugin);
