#pragma once

//#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
//#include <maya/MString.h>
//#include <maya/MTypeId.h>
//#include <maya/MPlug.h>
//#include <maya/MDataBlock.h>
//#include <maya/MDataHandle.h>
//#include <maya/MArrayDataHandle.h>
//#include <maya/MFnNumericAttribute.h>
//#include <maya/MFnLightDataAttribute.h>
//#include <maya/MFloatVector.h>
//#include <maya/MGlobal.h>

#include <common/IncludeMFnPluginClass.h>
#include <common/version_helper.h>



class AttributeTransfer : public MPxNode
{
public:
                            AttributeTransfer();
                            ~AttributeTransfer();

	static  void *	        creator();
	virtual MStatus	        compute( const MPlug&, MDataBlock& );
	static  MStatus	        initialize();
	virtual void	        postConstructor();

	static MString          sTypeName();
	static MTypeId          sTypeId();
	static MPxNode::Type    sType();
	static const MString&   sClassification();

protected:
	static MString          m_classification;
	static VersionHelper    m_version_helper;

    static MObject          a_iInGeometry;
    static MObject          a_iGroupId;
    static MObject          a_iBoundingObjects;
    static MObject              a_iBoundParentMatrix;
    static MObject              a_iBoundControlData;
	static MObject                  a_iBoundType;// The type of the bounding object
    static MObject          a_oOutGeometry;
    static MObject          a_oOutWeightPP;
};

MStatus initializePlugin_AttributeTransfer(MFnPlugin &plugin);
MStatus uninitializePlugin_AttributeTransfer(MFnPlugin &plugin);
