#include <maya/MFnPlugin.h>
#include <maya/MStatus.h>

#include <common/log.h>
#include "arrayDataContainer/arrayDataContainer.h"
#include "attributeTransfer.h"
#include "boundingObject.h"
#include "createArray.h"
#include "group.h"
#include "point.h"
#include "vertexMapper/vertexMapper.h"
#include "version.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya
//
PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('opensoup')", true, false) );


    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

    CHECK_MSTATUS_AND_RETURN_IT(initializePlugin_CreateArray(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(initializePlugin_BoundingObject(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(initializePlugin_AttributeTransfer(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(initializePlugin_Group(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(initializePlugin_Point(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(initializePlugin_ArrayDataContainer(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(initializePlugin_VertexMapper(plugin));

	// import node_templateSetup
	CHECK_MSTATUS( MGlobal::executePythonCommand("import opensoupSetup", true, false) );
	CHECK_MSTATUS(plugin.registerUI(
                    "opensoup_setupUI()",
                    "opensoup_unsetupUI()",
                    "opensoup_setup()",
                    "opensoup_unsetup()"
                    ));

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('opensoup')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('opensoup')", true, false) );

	MFnPlugin plugin( obj );

    CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_VertexMapper(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_ArrayDataContainer(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_Point(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_Group(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_AttributeTransfer(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_BoundingObject(plugin));
    CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_CreateArray(plugin));


    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('opensoup')", true, false) );

	return MS::kSuccess;
}
