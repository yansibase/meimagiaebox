#include "attributeTransfer.h"

#include <maya/MFnEnumAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>

#include <common/api_macro.h>
#include <common/node_ids.h>
#include <common/log.h>
#include "version.h"
#include "boundingObject.h"
#include "boundingObject_util.h"
/////////////////////////////////
// Plugin Lambert Shader Class //
/////////////////////////////////


// This class will create a new shader. Shaders are custom dependency
// graph objects so we will derive this class from the basic DG node
// type MPxNode
//


// IFF type ID
// Each node requires a unique identifier which is used by
// MFnDependencyNode::create() to identify which node to create, and by
// the Maya file format.
//
// For local testing of nodes you can use any identifier between
// 0x00000000 and 0x0007ffff, but for any node that you plan to use for
// more permanent purposes, you should get a universally unique id from
// Autodesk Support. You will be assigned a unique range that you
// can manage on your own.
//
MString AttributeTransfer::m_classification("utility/general");
VersionHelper   AttributeTransfer::m_version_helper;
MObject AttributeTransfer::a_iInGeometry;
MObject AttributeTransfer::a_iGroupId;
MObject AttributeTransfer::a_iBoundingObjects;
MObject AttributeTransfer::     a_iBoundParentMatrix;
MObject AttributeTransfer::     a_iBoundControlData;
MObject AttributeTransfer::          a_iBoundType;
MObject AttributeTransfer::a_oOutGeometry;
MObject AttributeTransfer::a_oOutWeightPP;

///////////////////////////////////////////////////////
// DESCRIPTION: attribute information
///////////////////////////////////////////////////////
//



// the postConstructor() function is called immediately after the objects
// constructor. It is not safe to call MPxNode member functions from the
// constructor, instead they should be called here.
//
void AttributeTransfer::postConstructor( )
{
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

MString AttributeTransfer::sTypeName()
{
	return NodeTypeName_AttributeTransfer;
}

MTypeId AttributeTransfer::sTypeId()
{
	return NodeID_AttributeTransfer;
}

MPxNode::Type AttributeTransfer::sType()
{
	return MPxNode::kDependNode;
}

const MString& AttributeTransfer::sClassification()
{
	return m_classification;
}


// This node does not need to perform any special actions on creation or
// destruction
//

AttributeTransfer::AttributeTransfer()
{
}

AttributeTransfer::~AttributeTransfer()
{
}


// The creator() method allows Maya to instantiate instances of this node.
// It is called every time a new instance of the node is requested by
// either the createNode command or the MFnDependencyNode::create()
// method.
//
// In this case creator simply returns a new node_template object.
//

void* AttributeTransfer::creator()
{
	return new AttributeTransfer();
}


// The initialize method is called only once when the node is first
// registered with Maya. In this method you define the attributes of the
// node, what data comes in and goes out of the node that other nodes may
// want to connect to.
//

MStatus AttributeTransfer::initialize()
{
	MStatus status; // Status will be used to hold the MStatus value
	// returned by each api function call. It is important
	// to check the status returned by a call to aid in
	// debugging. Failed API calls can result in subtle
	// errors that can be difficult to track down, you may
	// wish to use the CHECK_MSTATUS macro for any API
	// call where you do not need to provide your own
	// error handling.
	//

	// Attribute Initialization:
	//
	// create      - The create function creates a new attribute for the
	//				 node, it takes a long name for the attribute, a short
	//				 name for the attribute, the type of the attribute,
	//				 and a status object to determine if the api call was
	//				 successful.
	//
	// setKeyable  - Sets whether this attribute should accept keyframe
	//				 data, Attributes are not keyable by default.
	//
	// setStorable - Sets whether this attribute should be storable. If an
	//				 attribute is storable, then it will be writen out
	//				 when the node is stored to a file. Attributes are
	//               storable by default.
	//
	// setDefault  - Sets the default value for this attribute.
	//
	// setUsedAsColor - Sets whether this attribute should be presented as
	//				 a color in the UI.
	//
	// setHidden   - Sets whether this attribute should be hidden from the
	//				 UI. This is useful if the attribute is being used for
	//				 blind data, or if it is being used as scratch space
	//				 for a geometry calculation (should also be marked
	//				 non-connectable in that case). Attributes are not
	//				 hidden by default.
	//
	// setReadable - Sets whether this attribute should be readable. If an
	//				 attribute is readable, then it can be used as the
	//				 source in a dependency graph connection. Attributes
	//				 are readable by default.
	//
	// setWritable - Sets whether this attribute should be readable. If an
	//				 attribute is writable, then it can be used as the
	//				 destination in a dependency graph connection. If an
	//			     attribute is not writable then setAttr commands will
	//				 fail to change the attribute. If both keyable and
	//				 writable for an attribute are set to true it will be
	//				 displayed in the channel box when the node is
	//				 selected. Attributes are writable by default.
	//
	// setArray    - Sets whether this attribute should have an array of
	//				 data. This should be set to true if the attribute
	//				 needs to accept multiple incoming connections.
	//				 Attributes are single elements by default.
	//

    //
    // Input Attributes
    //
	{// a_iInGeometry
	    MFnTypedAttribute tAttr;
	    a_iInGeometry = tAttr.create("inGeometry", "ig", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_INPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_iInGeometry));
	}

	{// a_iGroupId
	    MFnNumericAttribute nAttr;
	    a_iGroupId = nAttr.create("groupId", "gid",  MFnNumericData::kLong, 0, &status); CHECK_MSTATUS(status);
	    MAKE_INPUT(nAttr);
	    CHECK_MSTATUS(addAttribute(a_iGroupId));
	}

	{// a_iBoundingObjects
	    {// a_iBoundParentMatrix
            MFnMatrixAttribute mAttr;
            a_iBoundParentMatrix = mAttr.create("boundParentMatrix", "bpm", MFnMatrixAttribute::kDouble, &status); CHECK_MSTATUS(status);
            MAKE_INPUT(mAttr);
            CHECK_MSTATUS(addAttribute(a_iBoundParentMatrix));
	    }
	    {// a_iBoundControlData
	        {// a_iBoundType
                MFnNumericAttribute nAttr;
                a_iBoundType= nAttr.create("boundType", "btp", MFnNumericData::kShort, 0,  &status ); CHECK_MSTATUS(status);
                MAKE_INPUT(nAttr);
                //CHECK_MSTATUS(nAttr.setReadable(false));
                CHECK_MSTATUS(addAttribute(a_iBoundType));
	        }
            MFnCompoundAttribute cAttr;
            a_iBoundControlData = cAttr.create( "boundControlData", "bcd", &status); CHECK_MSTATUS(status);
            CHECK_MSTATUS(cAttr.addChild(a_iBoundType));
            MAKE_INPUT(cAttr);
            //CHECK_MSTATUS(cAttr.setReadable(false));
            CHECK_MSTATUS(addAttribute(a_iBoundControlData));
	    }
        MFnCompoundAttribute cAttr;
        a_iBoundingObjects = cAttr.create( "boundingObjects", "bos", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(cAttr.addChild(a_iBoundParentMatrix));
        CHECK_MSTATUS(cAttr.addChild(a_iBoundControlData));
        MAKE_INPUT(cAttr);
        CHECK_MSTATUS(cAttr.setArray(true));
        CHECK_MSTATUS(cAttr.setUsesArrayDataBuilder(true));
	    CHECK_MSTATUS(addAttribute(a_iBoundingObjects));
	}

    //
    // Output Attributes
    //
	{// a_oOutGeometry
	    MFnTypedAttribute tAttr;
	    a_oOutGeometry = tAttr.create("outGeometry", "og", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_OUTPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_oOutGeometry));
	}
	{// a_oOutWeightPP
	    MFnTypedAttribute tAttr;
        a_oOutWeightPP = tAttr.create("outWeightPP", "owpp", MFnData::kDoubleArray, &status); CHECK_MSTATUS(status);
        MAKE_OUTPUT(tAttr);
        CHECK_MSTATUS(addAttribute(a_oOutWeightPP));
	}

    CHECK_MSTATUS(attributeAffects(a_iInGeometry,               a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iGroupId,                  a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iBoundingObjects,          a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(     a_iBoundParentMatrix,   a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(     a_iBoundControlData,    a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(         a_iBoundType,       a_oOutGeometry));

    CHECK_MSTATUS(attributeAffects(a_iInGeometry,               a_oOutWeightPP));
    CHECK_MSTATUS(attributeAffects(a_iGroupId,                  a_oOutWeightPP));
    CHECK_MSTATUS(attributeAffects(a_iBoundingObjects,          a_oOutWeightPP));
    CHECK_MSTATUS(attributeAffects(     a_iBoundParentMatrix,   a_oOutWeightPP));
    CHECK_MSTATUS(attributeAffects(     a_iBoundControlData,    a_oOutWeightPP));
    CHECK_MSTATUS(attributeAffects(         a_iBoundType,       a_oOutWeightPP));

    //
	m_version_helper.initialize();

	return( MS::kSuccess );
}


// The compute() method does the actual work of the node using the inputs
// of the node to generate its output.
//
// Compute takes two parameters: plug and data.
// - Plug is the the data value that needs to be recomputed
// - Data provides handles to all of the nodes attributes, only these
//   handles should be used when performing computations.
//
MStatus AttributeTransfer::compute( const MPlug& plug, MDataBlock& data )
{
    static int iEnterCount = 0;
    iEnterCount++;

    MStatus status;

	MString plugname(plug.name(&status));
	CHECK_MSTATUS(status);
	LDbg("%d, plugname=%s\n", iEnterCount, plugname.asChar());

    if (plug == a_oOutGeometry || plug == a_oOutWeightPP)
    {
        ////////////////////////////////////////
        MArrayDataHandle adhBoundingObjects = data.inputArrayValue( a_iBoundingObjects, &status );  CHECK_MSTATUS( status );
        LDbg("%d, bounding objects count=%d\n", iEnterCount, adhBoundingObjects.elementCount());


        /////////////////////////////////////////
        MDataHandle dhGroupId     = data.inputValue(a_iGroupId,         &status);  CHECK_MSTATUS(status);
        //long groupId = dhGroupId.asLong();

        MDataHandle dhInGeometry  = data.inputValue(a_iInGeometry,      &status);   CHECK_MSTATUS(status);
        MDataHandle dhOutGeometry = data.outputValue(a_oOutGeometry,    &status);   CHECK_MSTATUS(status);
        //MFnMesh fnInMesh(dhInGeometry.asMesh(), &status);                           CHECK_MSTATUS(status);
        //MFnMesh fnInMeshTransformed(dhInGeometry.asMeshTransformed(), &status);     CHECK_MSTATUS(status);

        CHECK_MSTATUS(dhOutGeometry.copy(dhInGeometry));
        MFnMesh fnOutMesh(dhOutGeometry.asMesh(), &status); CHECK_MSTATUS(status);

        MPointArray position_ws;
        CHECK_MSTATUS(fnOutMesh.getPoints(position_ws, MSpace::kWorld));

        MDoubleArray weightArray;
        CHECK_MSTATUS(weightArray.setLength(position_ws.length()));

        for(unsigned int vi=0; vi<position_ws.length(); ++vi)// for each vertex
        {
            MColor displayColor(0.0, 0.0, 0.0);
            double weight = 0.0;
            ////////////////for each bounding object
            for(unsigned int bosIdx=0; bosIdx < adhBoundingObjects.elementCount(); ++bosIdx )
            {
                // i-th bounding object
                CHECK_MSTATUS(adhBoundingObjects.jumpToArrayElement(bosIdx));
                MDataHandle bos_i = adhBoundingObjects.inputValue(&status); CHECK_MSTATUS(status);

                // get matrix
                MMatrix bpm_i = bos_i.child(a_iBoundParentMatrix).asMatrix(); CHECK_MSTATUS(status);
                //std::cout <<iEnterCount<<", bos["<<bosIdx<<"]="<< bpm_i <<std::endl;

                // get bounding object type
                BoundingObjectType bot_i = BoundingObjectType(bos_i.child(a_iBoundControlData).child(a_iBoundType).asShort());
                //std::cout <<iEnterCount<<", bot_i="<< bot_i <<std::endl;

                //get ptr
                boost::shared_ptr<BoundingObjectUtilInterface> boundObject = BoundingObject::m_bouMgr.getShape(bot_i);


                //////////////////
                MPoint position_in_bound_os = position_ws[vi] * bpm_i.inverse();

                if(boundObject->isInside(position_in_bound_os))
                {
                    displayColor += boundObject->onInSide();
                    weight       += boundObject->getWeight(position_in_bound_os);
                }else{
                    displayColor += boundObject->onOutSide();
                    weight       += 0.0;
                }
            }//bosIdx

            //
            CHECK_MSTATUS(fnOutMesh.setVertexColor(displayColor, vi));
            weightArray[vi] = weight;

        }//vi



        {// a_oOutWeightPP
            MFnDoubleArrayData fn;
            MObject o(fn.create(weightArray, &status));   CHECK_MSTATUS(status);
            MDataHandle dhOutWeightPP(data.outputValue(a_oOutWeightPP, &status));    CHECK_MSTATUS(status);
            CHECK_MSTATUS(dhOutWeightPP.set(o));
            dhOutWeightPP.setClean();
        }

        dhOutGeometry.setClean();
        CHECK_MSTATUS(data.setClean(plug));
	}
	else{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}

MStatus initializePlugin_AttributeTransfer(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
		plugin.registerNode(
		AttributeTransfer::sTypeName(),
		AttributeTransfer::sTypeId(),
		AttributeTransfer::creator,
		AttributeTransfer::initialize,
		AttributeTransfer::sType(),
		&AttributeTransfer::sClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += AttributeTransfer::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}

MStatus uninitializePlugin_AttributeTransfer(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
        plugin.deregisterNode( AttributeTransfer::sTypeId() )
    );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += AttributeTransfer::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}
