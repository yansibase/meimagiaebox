#pragma once

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <iterator>
#include <maya/MArrayDataBuilder.h>
#include <maya/MItGeometry.h>

class MFloatArray;

enum MethodOfMappingVertexIndex
{
    MMVI_ObjectSpace = 0,
    MMVI_UVSpace     = 1,
    NUM_MMVI
};
// this class is modified from partial_blendshape/src/driver_mesh.h
class VertexMappingUtil
{
public:
    VertexMappingUtil();
    VertexMappingUtil(const VertexMappingUtil& o);

    // construct data from MObject
    VertexMappingUtil(const MObject& mobjId, /*const*/ MObject& dvnGeo, const short methodOfMappingVertexIndex);
    ~VertexMappingUtil();

    VertexMappingUtil& operator=(const VertexMappingUtil& o);

    // getter & setter
    const std::string&      id() const{ return m_id; }

    //  dvrVtxIdx --> dvnVtxIdx
    // \param   dvrVtxIdx   the vertex index on driver mesh
    //  \ret                the vertex index on driven mesh
    //std::size_t getDvnVtxIdx(const std::size_t dvrVtxIdx)const;

    // set this->m_from to vertex.outVertexIndexSrc
    //void setVtxIdxSrcToPlug(MArrayDataHandle& vtxIdxHandle)const;
    // set this->m_to to vertex.outVertexIndexDst
    //void setVtxIdxDstToPlug(MArrayDataHandle& vtxIdxHandle)const;

    const std::vector<int>& getVertexIndex_from()  const{ return m_from;  }
    const std::vector<int>& getVertexIndex_to()    const{ return m_to;  }

    // utility
    void swap(VertexMappingUtil& o);
    //
    std::ostream& print(std::ostream &os) const;
    //
    bool compare(const VertexMappingUtil& o) const;

    // return the intersection of pair (from_on_src, to_on_dst) and pair (to_on_src, from_on_dst)
    static void intersectOfFromToPair(
        // dst <-- src
        const std::vector<int>& from_on_src,
        const std::vector<int>&   to_on_dst,
        // src <-- dst
        const std::vector<int>& from_on_dst,
        const std::vector<int>&   to_on_src,

        std::vector<int>& outsrc,
        std::vector<int>& outdst);

    // outComplement = (all vertex index of mesh) - vertexIndex
    static void getComplement(const MObject &mesh, const std::vector<int> &inSet, std::vector<int> &outComplement);
    // get the MOST interested faces from the given vertexes
    static void getMostInterestedFaces(MObject mesh, const std::vector<int> &vertexIndex_, std::vector<int>& outFaceIndex);

    static void mapFaces(MObject moInGeometrySrc,
                         MObject moInGeometryDst,
                         const std::vector<int> &mappedVertexSrc,
                         const std::vector<int> &mappedVertexDst,
                         const std::vector<int> &connectedFaceOfMappedVertexSrc,
                         const std::vector<int> &connectedFaceOfMappedVertexDst,
                         std::vector<int> & outMappedFaceSrc,
                         std::vector<int> & outMappedFaceDst);

protected:
    // param pointOnDriver  map this point to a vertex on the driven mesh
    // param itDvnGeo       be used to walk through all the vertice on driven mesh
    int getIndexOfClosestVertexOnDrivenMesh(const MPoint& pointOnDriver, MItGeometry& itDvnGeo)const;

    int getIndexOfClosestUVOnDrivenMesh(const MFloatArray &uBuffer, const MFloatArray &vBuffer, const float2 uv) const;
    int mapUVIdxToVertexIdx(int uvIdx, const std::map<int, std::vector<int> > &vtxIdxToUVIdx);

//    static MStatus getOrigNode(const MObject& mobjId, MObject& origMObjId);

    // set this->m_v to PBS.drivers[*].vtxIdx
    //void setVtxIdxToPlug(const std::vector<int>& dataarray, MArrayDataHandle& vtxIdxHandle)const;

protected:
    MObject             m_mobjId;
    std::string         m_id; // mesh id

    // "m_from[i]==j0 and m_to[i]==j1" means dvrVtxIdx[j0] is mapped to dvnVtxIdx[j1]
    std::vector<int>    m_from;
    std::vector<int>    m_to;

protected:


};
//
void swap(VertexMappingUtil& a, VertexMappingUtil& b);
