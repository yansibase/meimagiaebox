#include "vertexMapper_util.h"

#include <stdio.h>
#include <cassert>
#include <algorithm>

#include <maya/MArrayDataHandle.h>
#include <maya/MDagPath.h>
#include <maya/MDataHandle.h>
#include <maya/MFnDagNode.h>
#include <maya/MGlobal.h>
#include <maya/MPoint.h>
#include <maya/MSelectionList.h>
#include <maya/MStatus.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshPolygon.h>

#include <common/log.h>

VertexMappingUtil::VertexMappingUtil()
:m_mobjId(MObject::kNullObj), m_id("empty")
{

}
//
VertexMappingUtil::VertexMappingUtil(const MObject& mobjId, /*const*/ MObject& dvnGeo, const short methodOfMappingVertexIndex)
:m_mobjId(mobjId), m_id("empty")
{
    MStatus status;

    // Debug
    //MFnDagNode fnDagNode(m_mobjId, &status);            CHECK_MSTATUS(status);
    printMeshInfo("VertexMappingUtil(), mobjId:", m_mobjId);

    // map vertex index from driver mesh to driven mesh, and construct this->m_v
    MItGeometry itDvrGeo(m_mobjId, &status);        CHECK_MSTATUS(status);
    m_from.reserve(itDvrGeo.count());
    m_to.reserve(itDvrGeo.count());

    MString strCount; strCount.set(itDvrGeo.count());
    //CHECK_MSTATUS(MGlobal::executePythonCommand("import pbs_progressbar as pbar; pbar.pbs_BeginProgressBar("+strCount+", 'Map vertex index begins:"+path.fullPathName()+"')"));

    MString strIndex;
    if(methodOfMappingVertexIndex == MMVI_ObjectSpace)
    {
        MItGeometry itDvnGeo(dvnGeo,  &status);            CHECK_MSTATUS(status);
        while( !itDvrGeo.isDone(&status) )
        {
            CHECK_MSTATUS(status);

            // map 'from'(vertex index on driver mesh)
            int from = itDvrGeo.index();
            // map 'to'  (vertex index on driven mesh)
            int to   = getIndexOfClosestVertexOnDrivenMesh(itDvrGeo.position(MSpace::kObject, &status), itDvnGeo);

            //assert(m_v.size() == from);
            m_from.push_back(from);
            m_to.push_back(to);

            CHECK_MSTATUS(itDvrGeo.next());

            strIndex.set(from);
            //CHECK_MSTATUS(MGlobal::executePythonCommand("import pbs_progressbar as pbar; pbar.pbs_UpdateProgressBar('"+strIndex+"/"+strCount+",   "+path.fullPathName()+"')"));
        }
    }else if(methodOfMappingVertexIndex == MMVI_UVSpace)
    {
/*
Given a cube and the vertex index (see doc/vertex-and-uv.png)

   4____________5
   /.          /|
  / .         / |
2/___________/3 |
 |  .       |   |
 |  ........|...|7
 | . 6      |  /
 |.         | /
 |__________|/
 0          1

 Its uv index could be:

           8 +----------+ 9
             |          |
             |          |
             |          |
             |          |
           6 +----------+ 7
             |          |
             |          |
             |          |
             |          |
           4 +----------+ 5
             |          |
             |          |
             |          |
 13        2 |          | 3        11
  +----------+----------+----------+
  |          |          |          |
  |          |          |          |
  |          |          |          |
  |          |          |          |
  +----------+----------+----------+
  12         0          1          10

The following code will get the uv values:
        MFloatArray u, v;
        CHECK_MSTATUS(fnDvnMesh.getUVs(u, v, &uvSet));
It could be:
    uv index:     0      1      2      3      4      5      6      7      8      9     10     11     12     13
           u: 0.375, 0.625, 0.375, 0.625, 0.375, 0.625, 0.375, 0.625, 0.375, 0.625, 0.875, 0.875, 0.125, 0.125,
           v:     0,     0,  0.25,  0.25,   0.5,   0.5,  0.75,  0.75,     1,     1,     0,  0.25,     0,  0.25,

The cube has 8 vertex and 13 uv values. So one vertex may have multiple uv values. The function MItMeshVertex::getUVIndices()
can get this relationship. For example, it could be:

vertex index ~ uv index   Description
           0   0, 0, 8,   vertex 0 has 2 uv index: 0, 8
           1   9, 1, 1,   vertex 1 has 2 uv index: 9, 1
           2   2, 2, 2,   vertex 2 has 1 uv index: 2
           3   3, 3, 3,   vertex 3 has 1 uv index: 3
           4   4, 4, 13,  vertex 4 has 2 uv index: 4, 13
           5   11,5, 5,   vertex 5 has 2 uv index: 11, 5
           6   6, 6, 12,  vertex 6 has 2 uv index: 6, 12
           7   10,7, 7,   vertex 7 has 2 uv index: 10, 7

I call it the vtxIdxToUVIdx table.


*/
        /// driven mesh
        //printMeshInfo("dvn mesh", dvnGeo);// DEBUG

        //
        std::map<int, std::vector<int> > vtxIdxToUVIdxOnDvnMesh;// vtxIdxToUVIdx table of the driven mesh
        MFnMesh         fnDvnMesh(dvnGeo,  &status);                        CHECK_MSTATUS(status);
        MItMeshVertex   itDvnMeshVtx(dvnGeo,  &status);                     CHECK_MSTATUS(status);
        const MString uvSet("map1");
        MFloatArray uDvnMesh, vDvnMesh;
        CHECK_MSTATUS(fnDvnMesh.getUVs(uDvnMesh, vDvnMesh, &uvSet));

        // DEBUG
        //::print("u=", uDvnMesh);
        //::print("v=", vDvnMesh);

        while( !itDvnMeshVtx.isDone(&status) )
        {
            CHECK_MSTATUS(status);

            // for each vertex on the driven mesh ...

            /// the index of the current vertex in the vertex list for this polygonal object.
            // It could be 0~7 for a cube
            int vtxIdx = itDvnMeshVtx.index(&status);

            /// how many uv index does this vertex has?
            MIntArray uvIndices;
            CHECK_MSTATUS(itDvnMeshVtx.getUVIndices(uvIndices, &uvSet));

            /// build vtxIdxToUVIdx table
            std::vector<int> tmp;// assign tmp to uvIndices
            tmp.reserve(uvIndices.length());
            for(std::size_t i=0; i<uvIndices.length(); ++i)
            {
                tmp.push_back(uvIndices[i]);
            }
            vtxIdxToUVIdxOnDvnMesh[vtxIdx] = tmp;

            // DEBUG. print vtxIdxToUVIdx table
            LDbg("driven mesh vtxIdx[%d], uvIndices:", vtxIdx);
            std::copy(tmp.begin(), tmp.end(), std::ostream_iterator<int>(std::cout, ","));
            std::cout<< std::endl;

            CHECK_MSTATUS(itDvnMeshVtx.next());// turn to next vertex
        }

/*
Now, how we map the vertex index from driver mesh to driven mesh with uv?

(driver mesh)                               (driven mesh)

for each vertex,                get the vertex index from uv index: 'to'
get uv: 'uv'                                   /\
    and                                        ||
vertex index: 'from'                           || mapUVIdxToVertexIdx()
   ||                                          ||
   ||                           get the uv index on driven mesh
   ||                                          /\
   ||                                          ||
   ||                                          ||
    \===========================================/
     Given uv value on driver mesh, which uv index
      is the closest uv value on driven mesh?
     The function getIndexOfClosestUVOnDrivenMesh() gives the answer.

Now we map the vertex index 'from' on driver mesh to vertex index 'to' on driven mesh.
We save the relationship to this->m_v.
*/
        /// driver mesh
        //printMeshInfo("dvr mesh", whichMObjIdIsUsedtoMapVtxId);// DEBUG

        MFnMesh         fnDvrMesh(m_mobjId,     &status);   CHECK_MSTATUS(status);
        MItMeshVertex   itDvrMeshVtx(m_mobjId,  &status);    CHECK_MSTATUS(status);

        while( !itDvrMeshVtx.isDone(&status) )
        {
            CHECK_MSTATUS(status);

            float2 uv;
            CHECK_MSTATUS(itDvrMeshVtx.getUV(uv, &uvSet));

            // Given uv value on driver mesh, which uv index is the closest uv value on driven mesh?
            const int theClosestUVIdxOnDvnMesh = getIndexOfClosestUVOnDrivenMesh(uDvnMesh, vDvnMesh, uv);

            // map 'from'(vertex index on driver mesh)
            int from = itDvrMeshVtx.index(&status);// the index of the current vertex in the vertex list for this polygonal object.
            // map 'to'  (vertex index on driven mesh)
            int to   = mapUVIdxToVertexIdx(theClosestUVIdxOnDvnMesh, vtxIdxToUVIdxOnDvnMesh);

            //assert(m_v.size() == from);
            m_from.push_back(from);
            m_to.push_back(to);

            CHECK_MSTATUS(itDvrMeshVtx.next());


        }

    }else{
        LFat("Only MMVI_ObjectSpace(%d) and MMVI_UVSpace(%d) are supported for vertex mapping method, the current map method is %d", MMVI_ObjectSpace, MMVI_UVSpace, methodOfMappingVertexIndex);
        assert(0 && "the map method is not supported");
    }



    //CHECK_MSTATUS(MGlobal::executePythonCommand("import pbs_progressbar as pbar; pbar.pbs_EndProgressBar('Map vertex index ends:"+path.fullPathName()+"')"));

    //Debug
    //print(std::cout);
}
//
VertexMappingUtil::VertexMappingUtil(const VertexMappingUtil& o)
{
    m_id    = o.m_id;
    m_from  = o.m_from;
    m_to    = o.m_to;
}
//
VertexMappingUtil::~VertexMappingUtil()
{

}
//
VertexMappingUtil& VertexMappingUtil::operator=(const VertexMappingUtil& o)
{
    VertexMappingUtil tmp(o);
    swap(tmp);
    return (*this);
}
//
void VertexMappingUtil::swap(VertexMappingUtil& o)
{
    std::swap(m_id,   o.m_id);
    std::swap(m_from, o.m_from);
    std::swap(m_to,   o.m_to);
}
//
std::ostream& VertexMappingUtil::print(std::ostream &os) const
{
    os<< std::endl;
    os<< "VertexMappingUtil:   " << m_id<<":"<< std::endl;

    os<< "from: ";
    std::copy(m_from.begin(), m_from.end(), std::ostream_iterator<int>(os, ","));
    os<< std::endl;

    os<< "to: ";
    std::copy(m_to.begin(), m_to.end(), std::ostream_iterator<int>(os, ","));
    os<< std::endl;

    return os;
}
//
bool VertexMappingUtil::compare(const VertexMappingUtil& o) const
{
    if (m_id != o.m_id)
        return false;

    if (m_from != o.m_from)
        return false;

    if (m_to != o.m_to)
        return false;

    return true;
}
//
/*
std::size_t VertexMappingUtil::getDvnVtxIdx(const std::size_t dvrVtxIdx)const
{
    assert(dvrVtxIdx < m_v.size());
    return m_v[dvrVtxIdx];
}
*/
//
//void VertexMappingUtil::setVtxIdxToPlug(const std::vector<int>& dataarray, MArrayDataHandle& vtxIdxHandle)const
//{
//    MStatus status;
//
//    MArrayDataBuilder builder(vtxIdxHandle.builder(&status)); CHECK_MSTATUS(status);
//
//    for(std::size_t i=0; i<dataarray.size(); ++i)
//    {
//        MDataHandle h = builder.addElement(i, &status);CHECK_MSTATUS(status);
//        h.setInt(dataarray[i]);
//        h.setClean();
//    }
//
//    CHECK_MSTATUS(vtxIdxHandle.set(builder));
//    CHECK_MSTATUS(vtxIdxHandle.setAllClean());
//}
//
//void VertexMappingUtil::setVtxIdxSrcToPlug(MArrayDataHandle& vtxIdxHandle)const
//{
//    setVtxIdxToPlug(m_from, vtxIdxHandle);
//}
////
//void VertexMappingUtil::setVtxIdxDstToPlug(MArrayDataHandle& vtxIdxHandle)const
//{
//    setVtxIdxToPlug(m_to, vtxIdxHandle);
//}
//void VertexMappingUtil::accumulateDeformTo(MPointArray& output, const MPointArray& dvnOriPos)const
//{
//    const std::size_t dvrVtxCnt = m_v.size();
//    for(std::size_t dvrVtxIdx = 0; dvrVtxIdx<dvrVtxCnt; ++dvrVtxIdx)
//    {
//        //output[] += dvrPos[i] - dvnOriPos[i];
//        assert(dvrVtxIdx < output.length());
//
//        const std::size_t dvnVtxIdx = m_v.at(dvrVtxIdx);// driver vtx idx --> driven vtx idx
//
//        output[dvnVtxIdx] += dvrPos[dvnVtxIdx] - dvnOriPos[dvnVtxIdx];
//    }
//}
//
int VertexMappingUtil::getIndexOfClosestVertexOnDrivenMesh(const MPoint& pointOnDriver, MItGeometry& itDvnGeo)const
{
    MStatus status;
    int     ptIndex = -1;
    float   nearestDistanceSoFar = 9e99;

    itDvnGeo.reset();
    while( !itDvnGeo.isDone(&status) )
    {
        CHECK_MSTATUS( status );

        float dist = pointOnDriver.distanceTo(itDvnGeo.position(MSpace::kObject, &status)); CHECK_MSTATUS(status);
        if(dist < nearestDistanceSoFar )
        {
            ptIndex = itDvnGeo.index();
            nearestDistanceSoFar = dist;
        }

        itDvnGeo.next();
    }

    assert(ptIndex != -1 && "can't find the nearest point on the driven mesh");
    return ptIndex;
}

int VertexMappingUtil::getIndexOfClosestUVOnDrivenMesh(const MFloatArray &uBuffer, const MFloatArray &vBuffer, const float2 uv) const
{
    MStatus status;
    int     ptIndex = -1;
    float   nearestDistanceSoFar = 9e99;

    const std::size_t BUFFER_SIZE = uBuffer.length();
    const MPoint pt(uv[0], uv[1], 0.0);

    for(std::size_t i = 0; i < BUFFER_SIZE; ++i)
    {
        const MPoint pt_in_buffer(uBuffer[i], vBuffer[i], 0.0);

        float dist = pt.distanceTo(pt_in_buffer); CHECK_MSTATUS(status);
        if(dist < nearestDistanceSoFar )
        {
            ptIndex = i;
            nearestDistanceSoFar = dist;
        }
    }

    assert(ptIndex != -1 && "can't find the nearest point on the driven mesh");
    return ptIndex;
}

int VertexMappingUtil::mapUVIdxToVertexIdx(int uvIdx, const std::map<int, std::vector<int> > &vtxIdxToUVIdx)
{
    std::map<int, std::vector<int> >::const_iterator b = vtxIdxToUVIdx.begin();
    std::map<int, std::vector<int> >::const_iterator e = vtxIdxToUVIdx.end();
    for(std::map<int, std::vector<int> >::const_iterator i = b; i != e; ++i)
    {
        std::vector<int>::const_iterator valuebegin = i->second.begin();
        std::vector<int>::const_iterator valueend   = i->second.end();
        for(std::vector<int>::const_iterator j = valuebegin; j != valueend; ++j)
        {
            if(*j == uvIdx)
            {
                // found
                return i->first;
            }
        }
/*        if(std::find(valuebegin, valueend, uvIdx) != valueend)
        {
            // found
            return i->first;
        }*/
    }
    return -1; // not found
}

void VertexMappingUtil::intersectOfFromToPair(
        // dst <-- src
        const std::vector<int>& from_on_src,
        const std::vector<int>&   to_on_dst,
        // src <-- dst
        const std::vector<int>& from_on_dst,
        const std::vector<int>&   to_on_src,

        std::vector<int>& outsrc,
        std::vector<int>& outdst)
{
    for(std::size_t i=0; i<from_on_src.size(); ++i)
    {
        const int key0   = from_on_src[i];
        const int value0 = to_on_dst[i];

        for(std::size_t j=0; j<to_on_src.size(); ++j)
        {
            const int key1   = to_on_src[j];
            const int value1 = from_on_dst[j];

            if(key0 == key1 && value0 == value1)
            {
                outsrc.push_back(key0);
                outdst.push_back(value0);
            }
        }
    }
}
//
void VertexMappingUtil::getComplement(const MObject &mesh, const std::vector<int> &inSet_, std::vector<int> &outComplementSet)
{
    MStatus status;

    MFnMesh fnMesh(mesh,  &status); CHECK_MSTATUS(status);
    const int numVertices = fnMesh.numVertices();

    std::vector<int> completeSet(numVertices);
    for (std::size_t i = 0; i != completeSet.size(); ++i) completeSet[i] = i;

    std::vector<int> inSet(inSet_);
    std::sort(inSet.begin(), inSet.end());

    // outComplementSet = completeSet - inSet
    outComplementSet.resize(numVertices);
    std::vector<int>::iterator it =
    std::set_difference(completeSet.begin(), completeSet.end(), inSet.begin(), inSet.end(), outComplementSet.begin());
    outComplementSet.resize(it - outComplementSet.begin());
}
void add(std::vector<int>& out, const MIntArray& items)
{
    out.reserve(out.size() + items.length());
    for(unsigned int i=0; i<items.length(); ++i)
    {
        out.push_back(items[i]);
    }
}

class TPair
{
public:
    int faceindex;
    int count;
};

int findItem(int faceindex, std::vector<TPair>& array)
{
    for(std::size_t i=0; i<array.size(); ++i)
    {
        if(array[i].faceindex == faceindex)
            return i;
    }
    return -1;
}

void buildTmp(std::vector<TPair>& out, const std::vector<int>& data)
{
    out.reserve(data.size());

    for(std::size_t i=0; i<data.size(); ++i)
    {
        int faceindex = data[i];

        int foundIndex = findItem(faceindex, out);
        if( foundIndex != -1)// found
        {
            out[foundIndex].count++;
        }else{
            TPair newItem;
            newItem.faceindex = faceindex;
            newItem.count     = 1;
            out.push_back(newItem);
        }
    }
}

int MaxCount(std::vector<TPair>& array)
{
    int maxcount = 0;
    for(std::size_t i=0; i<array.size(); ++i)
    {
        if(array[i].count > maxcount)
            maxcount = array[i].count;
    }
    return maxcount;
}

void filterItem(std::vector<int>& out, const int count_min, std::vector<TPair>& array)
{
    for(std::size_t i=0; i<array.size(); ++i)
    {
        if(array[i].count >= count_min)
            out.push_back(array[i].faceindex);
    }
}

void VertexMappingUtil::getMostInterestedFaces(MObject mesh, const std::vector<int> &interestedVertex_, std::vector<int>& outFaceIndex)
{
    MStatus status;

    //MFnMesh fnMesh(mesh,  &status); CHECK_MSTATUS(status);
    MItMeshVertex itMeshVertex(mesh,  &status); CHECK_MSTATUS(status);

    std::vector<int> interestedVertex(interestedVertex_);
    std::sort(interestedVertex.begin(), interestedVertex.end());

    std::vector<int> interestedFaces;

    std::size_t howManyInterestedVertexIsVisited = 0;

    while( !itMeshVertex.isDone(&status) )
    {
        CHECK_MSTATUS(status);

        //std::cout<<"howManyInterestedVertexIsVisited="<< howManyInterestedVertexIsVisited<<std::endl;

        if (howManyInterestedVertexIsVisited >= interestedVertex.size())
        {
            // we have visited all the insterested vertexes
            break;
        }

        // which vertex we are going to process?
        int veretexIndex = interestedVertex[howManyInterestedVertexIsVisited];
        //LDbg("veretexIndex=%d\n", veretexIndex);

        int candidateVeretexIndex = itMeshVertex.index(&status); CHECK_MSTATUS(status);// the index of the current vertex in the vertex list for this polygonal object.
        //LDbg("candidateVeretexIndex=%d\n", candidateVeretexIndex);

        if(candidateVeretexIndex != veretexIndex)
        {
            // we are not insterested in this vertex
            CHECK_MSTATUS(itMeshVertex.next());
            continue;
        }

        // which faces are connected to this vertex?
        MIntArray connectedFaces;
        CHECK_MSTATUS(itMeshVertex.getConnectedFaces(connectedFaces));
        add(interestedFaces, connectedFaces);


        // iterat next vertex
        CHECK_MSTATUS(itMeshVertex.next());
        ++howManyInterestedVertexIsVisited;
    }
    //::print(std::cout, "interestedFace=", interestedFaces);
    //std::sort(interestedFaces.begin(), interestedFaces.end());
    //::print(std::cout, "sorted interestedFace=", interestedFaces);

    //
    std::vector<TPair> tmp;
    buildTmp(tmp, interestedFaces);

    //int maxcount = MaxCount(tmp);
    //filterItem(outFaceIndex, maxcount, tmp);
    filterItem(outFaceIndex, 0, tmp);
}
// which vertex does the face contains?
void getFaceVertexIdx(MObject moGeometry, const int faceIdx, std::vector<int> &outVertexIdxOfFace)
{
    MStatus status;

    MItMeshPolygon itMeshPoly(moGeometry, &status);  CHECK_MSTATUS(status);


    while( !itMeshPoly.isDone(&status) )
    {
        CHECK_MSTATUS(status);

        if((unsigned int)faceIdx == itMeshPoly.index(&status))
        {
            // this is the interested face
            CHECK_MSTATUS(status);

            // which vertex does this face contains?
            MIntArray vertexIdx;
            CHECK_MSTATUS(itMeshPoly.getVertices(vertexIdx));

            // vertexIdx --> outVertexIdxOfFace
            for(std::size_t i=0; i<vertexIdx.length(); ++i)
            {
                outVertexIdxOfFace.push_back(vertexIdx[i]);
            }

            break;
        }

        // iterat next polygon
        CHECK_MSTATUS(itMeshPoly.next());
    }
}

void getMappedFaceVertexIdx(
    const std::vector<int> &mappedVertexSrc,
    const std::vector<int> &mappedVertexDst,
    const std::vector<int> &vertexIdxOfFaceSrc,
    std::vector<int> &outVertexIdxOfFaceSrc_mapped)
{
    outVertexIdxOfFaceSrc_mapped.reserve(vertexIdxOfFaceSrc.size());

    for(std::size_t i=0; i<vertexIdxOfFaceSrc.size(); ++i)
    {
        const int vIdxSrc = vertexIdxOfFaceSrc[i];

        std::vector<int>::const_iterator it = std::find(mappedVertexSrc.begin(), mappedVertexSrc.end(), vIdxSrc);
        if (it != mappedVertexSrc.end())
        {
            std::size_t offset = std::distance(mappedVertexSrc.begin(), it);
            const int vIdxDst = mappedVertexDst.at(offset);
            outVertexIdxOfFaceSrc_mapped.push_back(vIdxDst);
        }
        else{
            outVertexIdxOfFaceSrc_mapped.push_back(-1);
        }

    }
    assert(vertexIdxOfFaceSrc.size() == outVertexIdxOfFaceSrc_mapped.size());
}

bool DoesFaceMatch(const std::vector<int>& vertexIdxOfFaceSrc_mapped, const std::vector<int>& vertexIdxOfFaceDst)
{
    //LDbg("1\n");
    ::print(std::cout, "vertexIdxOfFaceSrc_mapped=", vertexIdxOfFaceSrc_mapped);
    ::print(std::cout, "vertexIdxOfFaceDst       =", vertexIdxOfFaceDst);

//    std::vector<int>::const_iterator it = std::find(vertexIdxOfFaceSrc_mapped.begin(), vertexIdxOfFaceSrc_mapped.end(), -1);
//    if (it != vertexIdxOfFaceSrc_mapped.end())
//    {
//        // some vertex is not mapped at all
//        LDbg("some vertex is not mapped at all\n");
//        return false;
//    }
    int howManyVertexMapped = 0;
    for(std::size_t ii=0; ii<vertexIdxOfFaceSrc_mapped.size(); ++ii)
    {
        const int vIdxSrc_mapped = vertexIdxOfFaceSrc_mapped[ii];
        if(vIdxSrc_mapped != -1)
        {
            howManyVertexMapped++;
            continue;
        }
    }
    if(howManyVertexMapped<3)
    {
        LDbg("can't construct a face");
        return false;
    }

    //LDbg("2\n");

    for(std::size_t ii=0; ii<vertexIdxOfFaceSrc_mapped.size(); ++ii)
    {
        const int vIdxSrc_mapped = vertexIdxOfFaceSrc_mapped[ii];
        std::cout <<"vertexIdxOfFaceSrc_mapped["<<ii<<"]="<< vIdxSrc_mapped <<std::endl;
        if(vIdxSrc_mapped == -1)
        {
            continue;
        }

        std::vector<int>::const_iterator it = std::find(vertexIdxOfFaceDst.begin(), vertexIdxOfFaceDst.end(), vIdxSrc_mapped);
        if (it != vertexIdxOfFaceDst.end())
        {
            LDbg("found\n");
        }
        else{
            LDbg("not found\n");
            return false;
        }
//        bool found = false;
//        for(std::size_t jj=0; jj<vertexIdxOfFaceDst.size(); ++jj)
//        {
//            //LDbg("3\n");
//
//            if(vertexIdxOfFaceDst[jj]==vIdxSrc_mapped)
//            {
//                //LDbg("4\n");
//                found = true;
//            }
//        }
//
//        //LDbg("5\n");
//        if(found==false)
//        {
//            LDbg("not found\n");
//            return false;
//        }

        //LDbg("6\n");
    }

    //LDbg("7\n");
    return true;
}
void VertexMappingUtil::mapFaces(
                        MObject moInGeometrySrc,
                        MObject moInGeometryDst,
                        const std::vector<int> &mappedVertexSrc,
                        const std::vector<int> &mappedVertexDst,
                        const std::vector<int> &connectedFaceOfMappedVertexSrc,
                        const std::vector<int> &connectedFaceOfMappedVertexDst,
                        std::vector<int> & outMappedFaceSrc,
                        std::vector<int> & outMappedFaceDst)
{
    outMappedFaceSrc.reserve(connectedFaceOfMappedVertexSrc.size());
    outMappedFaceDst.reserve(connectedFaceOfMappedVertexDst.size());

    LDbg("========================================");
    for(std::size_t fsrc=0; fsrc<connectedFaceOfMappedVertexSrc.size(); ++fsrc)
    {
        //LDbg("------------------");
        // face index on source mesh
        const int faceIdxSrc = connectedFaceOfMappedVertexSrc[fsrc];
        //LDbg("faceIdxSrc=%d\n", faceIdxSrc);

        std::vector<int> vertexIdxOfFaceSrc;// the object-relative index of the vertexes of the current polygon.
        getFaceVertexIdx(moInGeometrySrc, faceIdxSrc, vertexIdxOfFaceSrc);
        //::print(std::cout, "vertexIdxOfFaceSrc=", vertexIdxOfFaceSrc);

        std::vector<int> vertexIdxOfFaceSrc_mapped;
        getMappedFaceVertexIdx(mappedVertexSrc, mappedVertexDst, vertexIdxOfFaceSrc, vertexIdxOfFaceSrc_mapped);
        //::print(std::cout, "vertexIdxOfFaceSrc_mapped=", vertexIdxOfFaceSrc_mapped);

        // iterator faces on destination mesh and find the matched face
        //std::cout<<"connectedFaceOfMappedVertexDst.size()="<<connectedFaceOfMappedVertexDst.size()<<std::endl;
        for(std::size_t fdst=0; fdst<connectedFaceOfMappedVertexDst.size(); ++fdst)
        {
            // face index on destination mesh
            const int faceIdxDst = connectedFaceOfMappedVertexDst[fdst];
            LDbg("......check srcface[%d]   dstface[%d]\n", faceIdxSrc, faceIdxDst);

            std::vector<int> vertexIdxOfFaceDst;// the object-relative index of the vertexes of the current polygon.
            getFaceVertexIdx(moInGeometryDst, faceIdxDst, vertexIdxOfFaceDst);
            //::print(std::cout, "vertexIdxOfFaceDst=", vertexIdxOfFaceDst);

            if(DoesFaceMatch(vertexIdxOfFaceSrc_mapped, vertexIdxOfFaceDst))
            {
                outMappedFaceSrc.push_back(faceIdxSrc);
                outMappedFaceDst.push_back(faceIdxDst);
            }else{
                LDbg("...... not match\n");
            }
        }
        LDbg("\n\n\n\n");
    }

}
//
void swap(VertexMappingUtil& a, VertexMappingUtil& b)
{
    printf("::%s()\n", __FUNCTION__);
    a.swap(b);
}
