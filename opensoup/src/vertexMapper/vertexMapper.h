#pragma once

//#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
//#include <maya/MString.h>
//#include <maya/MTypeId.h>
//#include <maya/MPlug.h>
//#include <maya/MDataBlock.h>
//#include <maya/MDataHandle.h>
//#include <maya/MArrayDataHandle.h>
//#include <maya/MFnNumericAttribute.h>
//#include <maya/MFnLightDataAttribute.h>
//#include <maya/MFloatVector.h>
//#include <maya/MGlobal.h>

#include <common/IncludeMFnPluginClass.h>
#include <common/version_helper.h>
#include "vertexMapper_util.h"

// map vertex from source mesh to destination mesh
class VertexMapperNode : public MPxNode
{
public:
                            VertexMapperNode();
                            ~VertexMapperNode();

	static  void *	        creator();
	virtual MStatus	        compute( const MPlug&, MDataBlock& );
	static  MStatus	        initialize();
	virtual void	        postConstructor();

	static MString          sTypeName();
	static MTypeId          sTypeId();
	static MPxNode::Type    sType();
	static const MString&   sClassification();

protected:
	static MString          m_classification;
	static VersionHelper    m_version_helper;

    static MObject          a_iInGeometrySrc;// source mesh
    static MObject          a_iInGeometryDst;// destination mesh
    static MObject          a_iMapMethod; // use UV or point position to map vertex index

    // some vertexes are mapped from source mesh to destination mesh,
    // the following attributes store vertex index
    static MObject          a_oOutMappedVertexSrc;// on source mesh
    static MObject          a_oOutMappedVertexDst;// on destination mesh


    // get the faces which are connected with the mapped vertexes
    // the following attributes store face index
    static MObject          a_oOutConnectedFaceOfMappedVertexSrc;// on source mesh
    static MObject          a_oOutConnectedFaceOfMappedVertexDst;// on destination mesh

    // some faces are mapped from source mesh to destination mesh,
    // the following attributes store face index
    static MObject          a_oOutMappedFaceSrc;// on source mesh
    static MObject          a_oOutMappedFaceDst;// on destination mesh

    // but some vertexes can't be mapped
    // the following attributes store vertex index
    static MObject          a_oOutUnmappedVertexSrc;// on source mesh
    static MObject          a_oOutUnmappedVertexDst;// on destination mesh

    // get the faces which are connected with the unmapped vertexes
    // the following attributes store face index
    static MObject          a_oOutConnectedFaceOfUnmappedVertexSrc;// on source mesh
    static MObject          a_oOutConnectedFaceOfUnmappedVertexDst;// on destination mesh

protected:
    void setOutAttribute(MDataBlock& data, MObject &outAttribute, const std::vector<int> &value);
};

MStatus initializePlugin_VertexMapper(MFnPlugin &plugin);
MStatus uninitializePlugin_VertexMapper(MFnPlugin &plugin);
