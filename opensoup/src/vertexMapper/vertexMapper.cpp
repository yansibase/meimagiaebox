#include "vertexMapper.h"


#include <set>

#include <maya/MFnEnumAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>

#include <common/api_macro.h>
#include <common/node_ids.h>
#include <common/log.h>
#include "../version.h"

/////////////////////////////////
// Plugin Lambert Shader Class //
/////////////////////////////////


// This class will create a new shader. Shaders are custom dependency
// graph objects so we will derive this class from the basic DG node
// type MPxNode
//


// IFF type ID
// Each node requires a unique identifier which is used by
// MFnDependencyNode::create() to identify which node to create, and by
// the Maya file format.
//
// For local testing of nodes you can use any identifier between
// 0x00000000 and 0x0007ffff, but for any node that you plan to use for
// more permanent purposes, you should get a universally unique id from
// Autodesk Support. You will be assigned a unique range that you
// can manage on your own.
//
MString VertexMapperNode::m_classification("utility/general");
VersionHelper   VertexMapperNode::m_version_helper;
MObject VertexMapperNode::a_iInGeometrySrc;
MObject VertexMapperNode::a_iInGeometryDst;
MObject VertexMapperNode::a_iMapMethod;
MObject VertexMapperNode::a_oOutMappedVertexSrc;
MObject VertexMapperNode::a_oOutMappedVertexDst;
MObject VertexMapperNode::a_oOutConnectedFaceOfMappedVertexSrc;
MObject VertexMapperNode::a_oOutConnectedFaceOfMappedVertexDst;
MObject VertexMapperNode::a_oOutMappedFaceSrc;
MObject VertexMapperNode::a_oOutMappedFaceDst;
MObject VertexMapperNode::a_oOutUnmappedVertexSrc;
MObject VertexMapperNode::a_oOutUnmappedVertexDst;
MObject VertexMapperNode::a_oOutConnectedFaceOfUnmappedVertexSrc;
MObject VertexMapperNode::a_oOutConnectedFaceOfUnmappedVertexDst;
///////////////////////////////////////////////////////
// DESCRIPTION: attribute information
///////////////////////////////////////////////////////
//



// the postConstructor() function is called immediately after the objects
// constructor. It is not safe to call MPxNode member functions from the
// constructor, instead they should be called here.
//
void VertexMapperNode::postConstructor( )
{
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

MString VertexMapperNode::sTypeName()
{
	return NodeTypeName_VertexMapper;
}

MTypeId VertexMapperNode::sTypeId()
{
	return NodeID_VertexMapper;
}

MPxNode::Type VertexMapperNode::sType()
{
	return MPxNode::kDependNode;
}

const MString& VertexMapperNode::sClassification()
{
	return m_classification;
}


// This node does not need to perform any special actions on creation or
// destruction
//

VertexMapperNode::VertexMapperNode()
{
}

VertexMapperNode::~VertexMapperNode()
{
}


// The creator() method allows Maya to instantiate instances of this node.
// It is called every time a new instance of the node is requested by
// either the createNode command or the MFnDependencyNode::create()
// method.
//
// In this case creator simply returns a new node_template object.
//

void* VertexMapperNode::creator()
{
	return new VertexMapperNode();
}


// The initialize method is called only once when the node is first
// registered with Maya. In this method you define the attributes of the
// node, what data comes in and goes out of the node that other nodes may
// want to connect to.
//

MStatus VertexMapperNode::initialize()
{
	MStatus status; // Status will be used to hold the MStatus value
	// returned by each api function call. It is important
	// to check the status returned by a call to aid in
	// debugging. Failed API calls can result in subtle
	// errors that can be difficult to track down, you may
	// wish to use the CHECK_MSTATUS macro for any API
	// call where you do not need to provide your own
	// error handling.
	//

	// Attribute Initialization:
	//
	// create      - The create function creates a new attribute for the
	//				 node, it takes a long name for the attribute, a short
	//				 name for the attribute, the type of the attribute,
	//				 and a status object to determine if the api call was
	//				 successful.
	//
	// setKeyable  - Sets whether this attribute should accept keyframe
	//				 data, Attributes are not keyable by default.
	//
	// setStorable - Sets whether this attribute should be storable. If an
	//				 attribute is storable, then it will be writen out
	//				 when the node is stored to a file. Attributes are
	//               storable by default.
	//
	// setDefault  - Sets the default value for this attribute.
	//
	// setUsedAsColor - Sets whether this attribute should be presented as
	//				 a color in the UI.
	//
	// setHidden   - Sets whether this attribute should be hidden from the
	//				 UI. This is useful if the attribute is being used for
	//				 blind data, or if it is being used as scratch space
	//				 for a geometry calculation (should also be marked
	//				 non-connectable in that case). Attributes are not
	//				 hidden by default.
	//
	// setReadable - Sets whether this attribute should be readable. If an
	//				 attribute is readable, then it can be used as the
	//				 source in a dependency graph connection. Attributes
	//				 are readable by default.
	//
	// setWritable - Sets whether this attribute should be readable. If an
	//				 attribute is writable, then it can be used as the
	//				 destination in a dependency graph connection. If an
	//			     attribute is not writable then setAttr commands will
	//				 fail to change the attribute. If both keyable and
	//				 writable for an attribute are set to true it will be
	//				 displayed in the channel box when the node is
	//				 selected. Attributes are writable by default.
	//
	// setArray    - Sets whether this attribute should have an array of
	//				 data. This should be set to true if the attribute
	//				 needs to accept multiple incoming connections.
	//				 Attributes are single elements by default.
	//
    // By default, attributes are:
    //    Readable.
    //    Writable.
    //    Connectable.
    //    Storable.
    //    Cached.
    //    Not arrays.
    //    Have indices that matter.
    //    Do not use an array builder.
    //    Not keyable.
    //    Not hidden.
    //    Not used as colors.
    //    Not indeterminant.
    //    Set to disconnect behavior kNothing.

    //
    // Input Attributes
    //
	{// a_iInGeometrySrc
	    MFnTypedAttribute tAttr;
	    a_iInGeometrySrc = tAttr.create("inGeometrySrc", "igs", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_INPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_iInGeometrySrc));
	}

	{// a_iInGeometryDst
	    MFnTypedAttribute tAttr;
	    a_iInGeometryDst = tAttr.create("inGeometryDst", "igd", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_INPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_iInGeometryDst));
	}

	{// a_iMapMethod
	    MFnEnumAttribute enumAttr;
        a_iMapMethod = enumAttr.create("mapmethod", "mm", MMVI_UVSpace/*default*/, &status); CHECK_MSTATUS( status );
        CHECK_MSTATUS(enumAttr.addField("point position in object space", MMVI_ObjectSpace));
        CHECK_MSTATUS(enumAttr.addField("uv",                             MMVI_UVSpace));
        CHECK_MSTATUS(enumAttr.setDefault(MMVI_UVSpace));
        MAKE_INPUT(enumAttr);
        CHECK_MSTATUS(addAttribute(a_iMapMethod));
	}

    //
    // Output Attributes
    //
    {//a_oOutMappedVertexSrc
        MFnGenericAttribute gAttr;
        a_oOutMappedVertexSrc = gAttr.create( "outMappedVertexSrc", "omvs", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutMappedVertexSrc));
    }

    {//a_oOutMappedVertexDst
        MFnGenericAttribute gAttr;
        a_oOutMappedVertexDst = gAttr.create( "outMappedVertexDst", "omvd", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutMappedVertexDst));
    }

    {//a_oOutConnectedFaceOfMappedVertexSrc
        MFnGenericAttribute gAttr;
        a_oOutConnectedFaceOfMappedVertexSrc = gAttr.create( "outConnectedFaceOfMappedVertexSrc", "ocfmvs", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutConnectedFaceOfMappedVertexSrc));
    }

    {//a_oOutConnectedFaceOfMappedVertexDst
        MFnGenericAttribute gAttr;
        a_oOutConnectedFaceOfMappedVertexDst = gAttr.create( "outConnectedFaceOfMappedVertexDst", "ocfmvd", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutConnectedFaceOfMappedVertexDst));
    }

    {//a_oOutMappedFaceSrc
        MFnGenericAttribute gAttr;
        a_oOutMappedFaceSrc = gAttr.create( "outMappedFaceSrc", "omfs", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutMappedFaceSrc));
    }

    {//a_oOutMappedFaceDst
        MFnGenericAttribute gAttr;
        a_oOutMappedFaceDst = gAttr.create( "outMappedFaceDst", "omfd", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutMappedFaceDst));
    }

    {//a_oOutUnmappedVertexSrc
        MFnGenericAttribute gAttr;
        a_oOutUnmappedVertexSrc = gAttr.create( "outUnmappedVertexSrc", "ouvs", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutUnmappedVertexSrc));
    }

    {//a_oOutUnmappedVertexDst
        MFnGenericAttribute gAttr;
        a_oOutUnmappedVertexDst = gAttr.create( "outUnmappedVertexDst", "ouvd", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutUnmappedVertexDst));
    }

    {//a_oOutConnectedFaceOfUnmappedVertexSrc
        MFnGenericAttribute gAttr;
        a_oOutConnectedFaceOfUnmappedVertexSrc = gAttr.create( "outConnectedFaceOfUnmappedVertexSrc", "ocfuvs", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutConnectedFaceOfUnmappedVertexSrc));
    }

    {//a_oOutConnectedFaceOfUnmappedVertexDst
        MFnGenericAttribute gAttr;
        a_oOutConnectedFaceOfUnmappedVertexDst = gAttr.create( "outConnectedFaceOfUnmappedVertexDst", "ocfuvd", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kIntArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutConnectedFaceOfUnmappedVertexDst));
    }

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutMappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutMappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutMappedVertexSrc));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutMappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutMappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutMappedVertexDst));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutConnectedFaceOfMappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutConnectedFaceOfMappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutConnectedFaceOfMappedVertexSrc));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutConnectedFaceOfMappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutConnectedFaceOfMappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutConnectedFaceOfMappedVertexDst));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutMappedFaceSrc));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutMappedFaceSrc));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutMappedFaceSrc));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutMappedFaceDst));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutMappedFaceDst));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutMappedFaceDst));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutUnmappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutUnmappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutUnmappedVertexSrc));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutUnmappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutUnmappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutUnmappedVertexDst));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutConnectedFaceOfUnmappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutConnectedFaceOfUnmappedVertexSrc));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutConnectedFaceOfUnmappedVertexSrc));

    CHECK_MSTATUS(attributeAffects(a_iInGeometrySrc,               a_oOutConnectedFaceOfUnmappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iInGeometryDst,               a_oOutConnectedFaceOfUnmappedVertexDst));
    CHECK_MSTATUS(attributeAffects(a_iMapMethod,                   a_oOutConnectedFaceOfUnmappedVertexDst));

    //
	m_version_helper.initialize();

	return( MS::kSuccess );
}


// The compute() method does the actual work of the node using the inputs
// of the node to generate its output.
//
// Compute takes two parameters: plug and data.
// - Plug is the the data value that needs to be recomputed
// - Data provides handles to all of the nodes attributes, only these
//   handles should be used when performing computations.
//
MStatus VertexMapperNode::compute( const MPlug& plug, MDataBlock& data )
{
    static int iEnterCount = 0;
    iEnterCount++;

    MStatus status;

	MString plugname(plug.name(&status));
	CHECK_MSTATUS(status);
	LDbg("%d, plugname=%s\n", iEnterCount, plugname.asChar());

    if (plug == a_oOutMappedVertexSrc || plug == a_oOutConnectedFaceOfMappedVertexSrc || plug == a_oOutMappedFaceSrc || plug == a_oOutUnmappedVertexSrc || plug == a_oOutConnectedFaceOfUnmappedVertexSrc
     || plug == a_oOutMappedVertexDst || plug == a_oOutConnectedFaceOfMappedVertexDst || plug == a_oOutMappedFaceDst || plug == a_oOutUnmappedVertexDst || plug == a_oOutConnectedFaceOfUnmappedVertexDst)
    {
        MDataHandle dhInGeometrySrc = data.inputValue( a_iInGeometrySrc, &status); CHECK_MSTATUS(status);
        //MFnMesh fnInMesh(dhInGeometrySrc.asMesh(), &status);                     CHECK_MSTATUS(status);

        MDataHandle dhInGeometryDst = data.inputValue( a_iInGeometryDst, &status); CHECK_MSTATUS(status);
        //MFnMesh fnInMesh(dhInGeometryDst.asMesh(), &status);                     CHECK_MSTATUS(status);


        // how to map vertex index? use UV or point position to map vertex index
        short mapmethod = data.inputValue( a_iMapMethod, &status).asShort();
        LDbg("mapmethod=%d", mapmethod);
        //CHECK_MSTATUS(status);

        MObject moInGeometrySrc(dhInGeometrySrc.asMesh());
        MObject moInGeometryDst(dhInGeometryDst.asMesh());
        printMeshInfo("src mesh", moInGeometrySrc);// DEBUG
        printMeshInfo("dst mesh", moInGeometryDst);// DEBUG

        VertexMappingUtil mapFromSrcToDst(moInGeometrySrc, moInGeometryDst, mapmethod);
        mapFromSrcToDst.print(std::cout);
        const std::vector<int>& from_on_src(mapFromSrcToDst.getVertexIndex_from());
        const std::vector<int>& to_on_dst(mapFromSrcToDst.getVertexIndex_to());

        VertexMappingUtil mapFromDstToSrc(moInGeometryDst, moInGeometrySrc, mapmethod);
        mapFromDstToSrc.print(std::cout);
        const std::vector<int>& from_on_dst(mapFromDstToSrc.getVertexIndex_from());
        const std::vector<int>& to_on_src(mapFromDstToSrc.getVertexIndex_to());

        std::vector<int> mappedVertexSrc, mappedVertexDst;
        VertexMappingUtil::intersectOfFromToPair(from_on_src, to_on_dst,  from_on_dst, to_on_src, mappedVertexSrc, mappedVertexDst);
        // DEBUG
        ::print(std::cout, "intersect mappedVertexSrc:", mappedVertexSrc);
        ::print(std::cout, "intersect mappedVertexDst:", mappedVertexDst);


        std::vector<int> connectedFaceOfMappedVertexSrc;
        VertexMappingUtil::getMostInterestedFaces(moInGeometrySrc, mappedVertexSrc, connectedFaceOfMappedVertexSrc);
        ::print(std::cout, "connectedFaceOfMappedVertexSrc:", connectedFaceOfMappedVertexSrc);// DEBUG

        std::vector<int> connectedFaceOfMappedVertexDst;
        VertexMappingUtil::getMostInterestedFaces(moInGeometryDst, mappedVertexDst, connectedFaceOfMappedVertexDst);
        ::print(std::cout, "connectedFaceOfMappedVertexDst:", connectedFaceOfMappedVertexDst);// DEBUG

        std::vector<int> mappedFaceSrc, mappedFaceDst;
        VertexMappingUtil::mapFaces(moInGeometrySrc, moInGeometryDst, mappedVertexSrc, mappedVertexDst, connectedFaceOfMappedVertexSrc, connectedFaceOfMappedVertexDst, mappedFaceSrc, mappedFaceDst);
        // DEBUG
        ::print(std::cout, "mappedFaceSrc:", mappedFaceSrc);
        ::print(std::cout, "mappedFaceDst:", mappedFaceDst);

        std::vector<int> unmappedVertexSrc;
        VertexMappingUtil::getComplement(moInGeometrySrc, mappedVertexSrc, unmappedVertexSrc);
        ::print(std::cout, "unmappedVertexSrc:", unmappedVertexSrc);// DEBUG

        std::vector<int> unmappedVertexDst;
        VertexMappingUtil::getComplement(moInGeometryDst, mappedVertexDst, unmappedVertexDst);
        ::print(std::cout, "unmappedVertexDst:", unmappedVertexDst);// DEBUG

        std::vector<int> connectedFaceOfUnmappedVertexSrc;
        VertexMappingUtil::getMostInterestedFaces(moInGeometrySrc, unmappedVertexSrc, connectedFaceOfUnmappedVertexSrc);
        ::print(std::cout, "connectedFaceOfUnmappedVertexSrc:", connectedFaceOfUnmappedVertexSrc);// DEBUG

        std::vector<int> connectedFaceOfUnmappedVertexDst;
        VertexMappingUtil::getMostInterestedFaces(moInGeometryDst, unmappedVertexDst, connectedFaceOfUnmappedVertexDst);
        ::print(std::cout, "connectedFaceOfUnmappedVertexDst:", connectedFaceOfUnmappedVertexDst);// DEBUG

        ////////////////////////////////////////
        //MArrayDataHandle adhOutVertexIndexSrc = data.inputArrayValue(a_oOutVertexIndexSrc, &status);  CHECK_MSTATUS(status);
        //MArrayDataHandle adhOutVertexIndexDst = data.inputArrayValue(a_oOutVertexIndexDst, &status);  CHECK_MSTATUS(status);
        //LDbg("%d, bounding objects count=%d\n", iEnterCount, adhBoundingObjects.elementCount());


        setOutAttribute(data, a_oOutMappedVertexSrc, mappedVertexSrc);
        setOutAttribute(data, a_oOutMappedVertexDst, mappedVertexDst);

        setOutAttribute(data, a_oOutConnectedFaceOfMappedVertexSrc, connectedFaceOfMappedVertexSrc);
        setOutAttribute(data, a_oOutConnectedFaceOfMappedVertexDst, connectedFaceOfMappedVertexDst);

        setOutAttribute(data, a_oOutMappedFaceSrc, mappedFaceSrc);
        setOutAttribute(data, a_oOutMappedFaceDst, mappedFaceDst);

        setOutAttribute(data, a_oOutUnmappedVertexSrc, unmappedVertexSrc);
        setOutAttribute(data, a_oOutUnmappedVertexDst, unmappedVertexDst);

        setOutAttribute(data, a_oOutConnectedFaceOfUnmappedVertexSrc, connectedFaceOfUnmappedVertexSrc);
        setOutAttribute(data, a_oOutConnectedFaceOfUnmappedVertexDst, connectedFaceOfUnmappedVertexDst);


        CHECK_MSTATUS(data.setClean(plug));
	}
	else{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}

void VertexMapperNode::setOutAttribute(MDataBlock& data, MObject &outAttribute, const std::vector<int> &value)
{
    MStatus status;

    MIntArray array;
    CHECK_MSTATUS(array.setLength(value.size()));

    // set values
    for(unsigned int i=0; i<array.length(); ++i)
    {
        array[i] = value[i];
    }

    MFnIntArrayData fn;
    MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
    MDataHandle dh(data.outputValue(outAttribute, &status));    CHECK_MSTATUS(status);
    CHECK_MSTATUS(dh.set(o));
}

MStatus initializePlugin_VertexMapper(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
		plugin.registerNode(
		VertexMapperNode::sTypeName(),
		VertexMapperNode::sTypeId(),
		VertexMapperNode::creator,
		VertexMapperNode::initialize,
		VertexMapperNode::sType(),
		&VertexMapperNode::sClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += VertexMapperNode::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}

MStatus uninitializePlugin_VertexMapper(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
        plugin.deregisterNode( VertexMapperNode::sTypeId() )
    );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += VertexMapperNode::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}
