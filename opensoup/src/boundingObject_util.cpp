#include "boundingObject_util.h"

#include <cstddef>
#include <boost/make_shared.hpp>
#if defined(OSMac_MachO_)
#   include <OpenGL/glu.h>
#else
#	if defined _WIN32 || defined _WIN64
#		include <windows.h>
#	endif
#   include <GL/glu.h>
#endif

static MColor RED(1.0, 0.0, 0.0);
static MColor GREEN(0.0, 1.0, 0.0);
static MColor BLUE(0.0, 0.0, 1.0);
static MColor YELLOW(1.0, 1.0, 0.0);
static MColor BLACK(0.0, 0.0, 0.0);
static MColor WHITE(1.0, 1.0, 1.0);
static MColor OUTSIDE(0.1, 0.1, 0.1);

BoundingObjectUtilInterface::BoundingObjectUtilInterface()
{
}
//
BoundingObjectUtilInterface::~BoundingObjectUtilInterface()
{
}
//
MBoundingBox BoundingObjectUtilInterface::getBoundingBox()
{
    return MBoundingBox(MPoint::origin, MPoint::origin);
}
//
MStatus BoundingObjectUtilInterface::draw()
{
    return MS::kSuccess;
}
//
bool BoundingObjectUtilInterface::isInside(const MPoint& point)
{
    return false;
}
//
MColor BoundingObjectUtilInterface::onInSide()
{
    return OUTSIDE * 1.0;
}
//
MColor BoundingObjectUtilInterface::onOutSide()
{
    return OUTSIDE;
}
//
double BoundingObjectUtilInterface::getWeight(const MPoint& point)const
{
    return 0.0;
}
/////////////////////////////////////////////////////////////
DefaultUtil::DefaultUtil()
{
}
//
DefaultUtil::~DefaultUtil()
{
}
//
MBoundingBox DefaultUtil::getBoundingBox()
{
    return MBoundingBox(MPoint::origin, MPoint::origin);
}
//
MStatus DefaultUtil::draw()
{
    return MS::kSuccess;
}
//
bool DefaultUtil::isInside(const MPoint& point)
{
    return true;
}
//
MColor DefaultUtil::onInSide()
{
    return OUTSIDE * 2.0;
}
//
MColor DefaultUtil::onOutSide()
{
    return OUTSIDE;
}
//
double DefaultUtil::getWeight(const MPoint& point)const
{
    return 1.0;
}
////////////////////////////////////////////////////////////
const float SphereUtil::R = 1.0f;
SphereUtil::SphereUtil()
{
}
//
SphereUtil::~SphereUtil()
{
}
//
MBoundingBox SphereUtil::getBoundingBox()
{
    MPoint corner1(-1.0f, -1.0f, -1.0f);
	MPoint corner2( 1.0f,  1.0f,  1.0f);

    corner1 = corner1 * R;
    corner2 = corner2 * R;

	return MBoundingBox(corner1, corner2);
}
//
MStatus SphereUtil::draw()
{
    const int   N = 24;

    // x-z plane
    glBegin(GL_LINE_LOOP);
    for(int i = 0; i < N; ++i)
    {
        const float theta = float(i)/float(N) * 2.0f * M_PI;
        const float x = R * cos(theta);
        const float z = R * sin(theta);
        glVertex3f(x, 0.0f, z);
    }
    glEnd();
    // x-y plane
    glBegin(GL_LINE_LOOP);
    for(int i = 0; i < N; ++i)
    {
        const float theta = float(i)/float(N) * 2.0f * M_PI;
        const float x = R * cos(theta);
        const float y = R * sin(theta);
        glVertex3f(x, y, 0.0f);
    }
    glEnd();
    // y-z plane
    glBegin(GL_LINE_LOOP);
    for(int i = 0; i < N; ++i)
    {
        const float theta = float(i)/float(N) * 2.0f * M_PI;
        const float z = R * cos(theta);
        const float y = R * sin(theta);
        glVertex3f(0.0f, y, z);
    }
    glEnd();
    return MS::kSuccess;
}
//
bool SphereUtil::isInside(const MPoint& point)
{
    return (point.distanceTo(MPoint::origin) <= R);
}
//
MColor SphereUtil::onInSide()
{
    return RED;
}
//
MColor SphereUtil::onOutSide()
{
    return OUTSIDE;
}
//
double SphereUtil::getWeight(const MPoint& point)const
{
    double t = point.distanceTo(MPoint::origin);
    return (1.0 - t/R);
}
/////////////////////////////////////////////////////////////
const float CubeUtil::R = 1.0f;
CubeUtil::CubeUtil()
{
}
//
CubeUtil::~CubeUtil()
{
}
//
MBoundingBox CubeUtil::getBoundingBox()
{
    MPoint corner1(-1.0f, -1.0f, -1.0f);
	MPoint corner2( 1.0f,  1.0f,  1.0f);

    corner1 = corner1 * R;
    corner2 = corner2 * R;

	return MBoundingBox(corner1, corner2);
}
//
MStatus CubeUtil::draw()
{
    //Multi-colored side - FRONT
    glBegin(GL_LINE_LOOP);
    glVertex3f(  R, -R, -R );
    glVertex3f(  R,  R, -R );
    glVertex3f( -R,  R, -R );
    glVertex3f( -R, -R, -R );
    glEnd();

    // White side - BACK
    glBegin(GL_LINE_LOOP);
    glVertex3f(  R, -R, R );
    glVertex3f(  R,  R, R );
    glVertex3f( -R,  R, R );
    glVertex3f( -R, -R, R );
    glEnd();

    // Purple side - RIGHT
    glBegin(GL_LINE_LOOP);
    glVertex3f( R, -R, -R );
    glVertex3f( R,  R, -R );
    glVertex3f( R,  R,  R );
    glVertex3f( R, -R,  R );
    glEnd();

    // Green side - LEFT
    glBegin(GL_LINE_LOOP);
    glVertex3f( -R, -R,  R );
    glVertex3f( -R,  R,  R );
    glVertex3f( -R,  R, -R );
    glVertex3f( -R, -R, -R );
    glEnd();

    // TOP
    glBegin(GL_LINE_LOOP);
    glVertex3f(  R,  R,  R );
    glVertex3f(  R,  R, -R );
    glVertex3f( -R,  R, -R );
    glVertex3f( -R,  R,  R );
    glEnd();

    // BOTTOM
    glBegin(GL_LINE_LOOP);
    glVertex3f(  R, -R, -R );
    glVertex3f(  R, -R,  R );
    glVertex3f( -R, -R,  R );
    glVertex3f( -R, -R, -R );
    glEnd();
    return MS::kSuccess;
}
//
bool CubeUtil::isInside(const MPoint& point)
{
    bool ret =
         (-R <= point.x) && (point.x <= R)&&
         (-R <= point.y) && (point.y <= R)&&
         (-R <= point.z) && (point.z <= R);
    return ret;
}
//
MColor CubeUtil::onInSide()
{
    return GREEN;
}
//
MColor CubeUtil::onOutSide()
{
    return OUTSIDE;
}
//
double CubeUtil::getWeight(const MPoint& point)const
{
    double t = point.distanceTo(MPoint::origin);
    return (1.0 - t/R);
}
//////////////////////////////////////////////////////////
BoundingObjectUtilMgr::BoundingObjectUtilMgr()
{
    m_defaultAgent = boost::make_shared<DefaultUtil>();
    m_sphereAgent  = boost::make_shared<SphereUtil>();
    m_cubeAgent    = boost::make_shared<CubeUtil>();
}
//
BoundingObjectUtilMgr::~BoundingObjectUtilMgr()
{

}
//
boost::shared_ptr<BoundingObjectUtilInterface> BoundingObjectUtilMgr::getShape(int objectType)
{
    boost::shared_ptr<BoundingObjectUtilInterface> ret;

    switch(objectType)
    {
    case BOT_Sphere:
        {
            ret = m_sphereAgent;
        }break;
    case BOT_Cube:
        {
            ret = m_cubeAgent;
        }break;
    default:
        {
            ret = m_defaultAgent;
        }break;
    }

    return ret;
}
