#include "point.h"

#include <maya/MFnEnumAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnComponentListData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>

#include <common/api_macro.h>
#include <common/node_ids.h>
#include <common/log.h>
#include "version.h"
#include "boundingObject.h"
#include "boundingObject_util.h"

MString Point::m_classification("utility/general");
VersionHelper   Point::m_version_helper;
MObject Point::a_iInCurrentTime;
MObject Point::a_iInGeometry;
MObject Point::a_iInWeightPP;
MObject Point::a_iEnablePosition;
MObject Point::a_iApplyChangesPosition;
MObject Point::a_iStringPosition;
MObject Point::a_oOutGeometry;
MObject Point::a_oOutIdPP;
MObject Point::a_oOutPositionPP;

MString Point::m_stringStartBlockDefault;
MString Point::m_stringPositionDefault;
MString Point::m_stringWeightDefault;
MString Point::m_stringColorDefault;
MString Point::m_stringNormalVelocityDefault;
MString Point::m_stringRadiusDefault;
MString Point::m_stringMapUVDefault;
MString Point::m_stringEndBlockDefault;
MString Point::m_quickHelp;

#define _(str) str "\n"

// This function is called when I create this node.
Point::Point()
{
    m_stringStartBlock      = m_stringStartBlockDefault;
    m_stringPosition        = m_stringPositionDefault;
    m_stringWeight          = m_stringWeightDefault;
    m_stringColor           = m_stringColorDefault;
    m_stringNormalVelocity  = m_stringNormalVelocityDefault;
    m_stringRadius          = m_stringRadiusDefault;
    m_stringMapUV           = m_stringMapUVDefault;
    m_stringEndBlock        = m_stringEndBlockDefault;
}

Point::~Point()
{
}
//
void* Point::creator()
{
	return new Point();
}

MString Point::sTypeName()
{
	return NodeTypeName_Point;
}

MTypeId Point::sTypeId()
{
	return NodeID_Point;
}

MPxNode::Type Point::sType()
{
	return MPxNode::kDependNode;
}

const MString& Point::sClassification()
{
	return m_classification;
}
//
void Point::setDefaultStrings()
{
    m_stringStartBlockDefault =
    _("// START BLOCK (this code is executed before the main loop)")
    _("//")
    _("// Be careful with the \"getAttr\" and \"setAttr\" commands,")
    _("// they may cause cycle loops if you are trying to access")
    _("// nodes located in the downstream graph of this one.")
    _("")
    ;
    m_stringPositionDefault =
    _("// POINT POSITION")
    _("$X=$TX;")
    _("$Y=$TY;")
    _("$Z=$TZ;")
    ;
    m_stringWeightDefault =
    _("// POINT WEIGHT")
    _("$W=$WEIGHT;")
    ;
    m_stringColorDefault =
    _("// POINT COLOR (RGBA)")
    _("$R=$CR;")
    _("$G=$CG;")
    _("$B=$CB;")
    _("$A=$ALPHA;")
    ;
    m_stringNormalVelocityDefault =
    _("// POINT NORMAL (PARTICLE VELOCITY)")
    _("$X=$NX;")
    _("$Y=$NY;")
    _("$Z=$NZ;")
    ;
    m_stringRadiusDefault =
    _("// POINT RADIUS")
    _("$R=$RADIUS;")
    ;
    m_stringMapUVDefault =
    _("// MAP (UV POSITION)")
    _("$U=$MAPU;")
    _("$V=$MAPV;")
    ;
    m_stringEndBlockDefault =
    _("// END BLOCK (this code is executed after the main loop)")
    _("//")
    _("// Paceholder for code that will be executed after the main loop.")
    _("// Be careful with the \"getAttr\" and \"setAttr\" commands,")
    _("// they may cause cycle loops if you are trying to access")
    _("// nodes located in the downstream graph of this one.")
    _("")
    ;

    m_quickHelp =
    _("There are multiple local variables accessible in the")
    _("\"position\", \"weight\", \"color\", \"normal/velocity\",")
    _("\"radius\", \"weight\", \"color\", \"normal/velocity\",")
    _("\"radius\", \"map\" expressions. Each variable contains")
    _("information only for the current point in the loop.")
    _("This node has two input geometry attributes. All")
    _("variables with index 2 at the end are related to")
    _("the points coming from the second geometry input.")
    _("")
    _("$ID - point id")
    _("$TX $TY $TZ / $TX2 $TY2 $TZ2 - input point position")
    _("$WEIGHT / $WEIGHT2 - input point weight")
    _("$CR $CG $CB $ALPHA / $CR2 $CB2 $CG2 $ALPHA2 - input point color and alpha")
    _("$NX $NY $NZ / $NX2 $NY2 $NZ2 - input point normal/velocity")
    _("$RADIUS / $RADIUS2 - input point radius")
    _("$MAPU $MAPV / $MAPU2 $MAPV2 - input point UV")
    ;
}
// This function is called when the plugin is loaded.
MStatus Point::initialize()
{
	MStatus status; // Status will be used to hold the MStatus value

	setDefaultStrings();

	// returned by each api function call. It is important
	// to check the status returned by a call to aid in
	// debugging. Failed API calls can result in subtle
	// errors that can be difficult to track down, you may
	// wish to use the CHECK_MSTATUS macro for any API
	// call where you do not need to provide your own
	// error handling.
	//

	// Attribute Initialization:
	//
	// create      - The create function creates a new attribute for the
	//				 node, it takes a long name for the attribute, a short
	//				 name for the attribute, the type of the attribute,
	//				 and a status object to determine if the api call was
	//				 successful.
	//
	// setKeyable  - Sets whether this attribute should accept keyframe
	//				 data, Attributes are not keyable by default.
	//
	// setStorable - Sets whether this attribute should be storable. If an
	//				 attribute is storable, then it will be writen out
	//				 when the node is stored to a file. Attributes are
	//               storable by default.
	//
	// setDefault  - Sets the default value for this attribute.
	//
	// setUsedAsColor - Sets whether this attribute should be presented as
	//				 a color in the UI.
	//
	// setHidden   - Sets whether this attribute should be hidden from the
	//				 UI. This is useful if the attribute is being used for
	//				 blind data, or if it is being used as scratch space
	//				 for a geometry calculation (should also be marked
	//				 non-connectable in that case). Attributes are not
	//				 hidden by default.
	//
	// setReadable - Sets whether this attribute should be readable. If an
	//				 attribute is readable, then it can be used as the
	//				 source in a dependency graph connection. Attributes
	//				 are readable by default.
	//
	// setWritable - Sets whether this attribute should be readable. If an
	//				 attribute is writable, then it can be used as the
	//				 destination in a dependency graph connection. If an
	//			     attribute is not writable then setAttr commands will
	//				 fail to change the attribute. If both keyable and
	//				 writable for an attribute are set to true it will be
	//				 displayed in the channel box when the node is
	//				 selected. Attributes are writable by default.
	//
	// setArray    - Sets whether this attribute should have an array of
	//				 data. This should be set to true if the attribute
	//				 needs to accept multiple incoming connections.
	//				 Attributes are single elements by default.
	//

    //
    // Input Attributes
    //
	{// a_iInCurrentTime
        MFnNumericAttribute nAttr;
        a_iInCurrentTime = nAttr.create("currentTime", "ct", MFnNumericData::kDouble, 0.0, &status );    CHECK_MSTATUS(status);
        MAKE_INPUT(nAttr);
        CHECK_MSTATUS(addAttribute(a_iInCurrentTime));
	}
	{// a_iInGeometry
	    MFnTypedAttribute tAttr;
	    a_iInGeometry = tAttr.create("inGeometry", "ig", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_INPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_iInGeometry));
	}
	{// a_iInWeightPP
        MFnTypedAttribute tAttr;
        a_iInWeightPP = tAttr.create("inWeightPP", "iwpp", MFnData::kDoubleArray, &status); CHECK_MSTATUS(status);
        MAKE_INPUT(tAttr);
        CHECK_MSTATUS(addAttribute(a_iInWeightPP));
	}
	//------------------------------- Position ----------------------------------------
    {
        {// a_iEnablePosition
            MFnNumericAttribute nAttr;
            a_iEnablePosition = nAttr.create("enablePosition", "ep", MFnNumericData::kBoolean, true, &status );    CHECK_MSTATUS(status);
            MAKE_INPUT(nAttr);
            CHECK_MSTATUS(addAttribute(a_iEnablePosition));
        }
        {// a_iApplyChangesPosition
            MFnNumericAttribute nAttr;
            a_iApplyChangesPosition = nAttr.create("applyChangesPosition", "ac", MFnNumericData::kBoolean, true, &status );    CHECK_MSTATUS(status);
            MAKE_INPUT(nAttr);
            CHECK_MSTATUS(addAttribute(a_iApplyChangesPosition));
        }
        {// a_iStringPosition
            MFnStringData fnString;
            MObject objDefaultValue(fnString.create(m_stringPositionDefault, &status)); CHECK_MSTATUS(status);

            MFnTypedAttribute tAttr;
            a_iStringPosition = tAttr.create("stringPosition", "sp", MFnData::kString, objDefaultValue, &status); CHECK_MSTATUS(status);
            MAKE_INPUT(tAttr);
            CHECK_MSTATUS(addAttribute(a_iStringPosition));
        }
    }

    //
    // Output Attributes
    //
	{// a_oOutGeometry
	    MFnTypedAttribute tAttr;
	    a_oOutGeometry = tAttr.create("outGeometry", "og", MFnData::kMesh, &status); CHECK_MSTATUS(status);
	    MAKE_OUTPUT(tAttr);
	    CHECK_MSTATUS(addAttribute(a_oOutGeometry));
	}
	{// a_oOutIdPP
	    MFnTypedAttribute tAttr;
        a_oOutIdPP = tAttr.create("outIdPP", "oipp", MFnData::kIntArray, &status); CHECK_MSTATUS(status);
        MAKE_OUTPUT(tAttr);
        CHECK_MSTATUS(addAttribute(a_oOutIdPP));
	}
	{// a_oOutPositionPP
        MFnGenericAttribute gAttr;
        a_oOutPositionPP = gAttr.create( "outPositionPP", "oppp", &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kVectorArray));
        CHECK_MSTATUS(gAttr.addDataAccept(MFnData::kPointArray));
        MAKE_OUTPUT(gAttr);
        CHECK_MSTATUS(addAttribute(a_oOutPositionPP));
	}

    CHECK_MSTATUS(attributeAffects(a_iInCurrentTime,            a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iInGeometry,               a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iInWeightPP,               a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iStringPosition,           a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iEnablePosition,           a_oOutGeometry));
    CHECK_MSTATUS(attributeAffects(a_iApplyChangesPosition,     a_oOutGeometry));

    CHECK_MSTATUS(attributeAffects(a_iInCurrentTime,            a_oOutPositionPP));
    CHECK_MSTATUS(attributeAffects(a_iInGeometry,               a_oOutPositionPP));
    CHECK_MSTATUS(attributeAffects(a_iInWeightPP,               a_oOutPositionPP));
    CHECK_MSTATUS(attributeAffects(a_iStringPosition,           a_oOutPositionPP));
    CHECK_MSTATUS(attributeAffects(a_iEnablePosition,           a_oOutPositionPP));
    CHECK_MSTATUS(attributeAffects(a_iApplyChangesPosition,     a_oOutPositionPP));

    CHECK_MSTATUS(attributeAffects(a_iInCurrentTime,            a_oOutIdPP));
    CHECK_MSTATUS(attributeAffects(a_iInGeometry,               a_oOutIdPP));
    CHECK_MSTATUS(attributeAffects(a_iInWeightPP,               a_oOutIdPP));
    CHECK_MSTATUS(attributeAffects(a_iStringPosition,           a_oOutIdPP));
    CHECK_MSTATUS(attributeAffects(a_iEnablePosition,           a_oOutIdPP));
    CHECK_MSTATUS(attributeAffects(a_iApplyChangesPosition,     a_oOutIdPP));

    //
	m_version_helper.initialize();

	return( MS::kSuccess );
}

// the postConstructor() function is called immediately after the objects
// constructor. It is not safe to call MPxNode member functions from the
// constructor, instead they should be called here.
//
void Point::postConstructor( )
{
    MStatus status;
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	MObject this_mobj(thisMObject());
	m_callbackId = MNodeMessage::addAttributeChangedCallback(this_mobj, sAttrChangedCallback, this, &status);   CHECK_MSTATUS(status);

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

void Point::sAttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug, void* inUserPtr)
{
    assert(inUserPtr != NULL);

	Point* this_ = (Point*)inUserPtr;
	this_->AttrChangedCallback(inMsg, ioPlug, ioOtherPlug);
}

// Called when some attribute changed for our node.
void Point::AttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug)
{
	if (inMsg & MNodeMessage::kAttributeSet)
	{
		if (ioPlug==Point::a_iStringPosition)
		{
			MPlug pStringPosition(thisMObject(), Point::a_iStringPosition);
            CHECK_MSTATUS(pStringPosition.getValue(m_stringPosition));// update m_stringPosition
			LDbg("m_stringPosition=%s", m_stringPosition.asChar());
		}
	}
}
// The compute() method does the actual work of the node using the inputs
// of the node to generate its output.
//
// Compute takes two parameters: plug and data.
// - Plug is the the data value that needs to be recomputed
// - Data provides handles to all of the nodes attributes, only these
//   handles should be used when performing computations.
//
MStatus Point::compute( const MPlug& plug, MDataBlock& data )
{
    static int iEnterCount = 0;
    iEnterCount++;

    MStatus status;

	MString plugname(plug.name(&status));
	CHECK_MSTATUS(status);
	//LDbg("%d, plugname=%s\n", iEnterCount, plugname.asChar());

    if (plug == a_oOutGeometry || plug == a_oOutPositionPP || plug == a_oOutIdPP)
    {
        MDataHandle dhInCurrentTime  = data.inputValue(a_iInCurrentTime,   &status);   CHECK_MSTATUS(status);
        MDataHandle dhInGeometry     = data.inputValue(a_iInGeometry,      &status);   CHECK_MSTATUS(status);
        MDataHandle dhOutGeometry    = data.outputValue(a_oOutGeometry,    &status);   CHECK_MSTATUS(status);
        MDataHandle dhEnablePosition = data.outputValue(a_iEnablePosition, &status);   CHECK_MSTATUS(status);

        double currentTime    = dhInCurrentTime.asDouble();
        bool   enablePosition = dhEnablePosition.asBool();
        // copy inGeometry to outGeometry
        CHECK_MSTATUS(dhOutGeometry.copy(dhInGeometry));

        // construct MFnMesh for outGeometry
        MFnMesh fnOutMesh(dhOutGeometry.asMesh(), &status); CHECK_MSTATUS(status);
        // get positions of outGeometry
        MPointArray position_os;
        CHECK_MSTATUS(fnOutMesh.getPoints(position_os, MSpace::kObject));

        {// a_oOutIdPP
            MIntArray array;
            CHECK_MSTATUS(array.setLength(position_os.length()));
            for(unsigned int i=0; i<array.length(); ++i)
            {
                array[i] = i;
            }
            MFnIntArrayData fn;
            MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
            MDataHandle dhOutIdPP(data.outputValue(a_oOutIdPP, &status));    CHECK_MSTATUS(status);
            CHECK_MSTATUS(dhOutIdPP.set(o));
            dhOutIdPP.setClean();
        }

        if(enablePosition)
        {
            {// a_oOutPositionPP
                MPointArray array;
                CHECK_MSTATUS(array.setLength(position_os.length()));
                for(unsigned int i=0; i<array.length(); ++i)
                {
                    array[i] = position_os[i];
                }

                MFnPointArrayData fn;
                MObject o(fn.create(array, &status));   CHECK_MSTATUS(status);
                MDataHandle dhOutPositionPP(data.outputValue(a_oOutPositionPP, &status));    CHECK_MSTATUS(status);
                CHECK_MSTATUS(dhOutPositionPP.set(o));
                dhOutPositionPP.setClean();
            }
            {// evaluate positions
                MFnDependencyNode fnDNode(thisMObject(), &status);           CHECK_MSTATUS(status);
                MString nodeName(fnDNode.name());
                //LDbg("fnDNode.name()=%s", nodeName.asChar());

                MString cmd_position_POINT(getStrFunction_position_POINT(nodeName, position_os.length(), currentTime));
                //LDbg("cmd_position_POINT=%s", cmd_position_POINT.asChar());
                MDoubleArray new_position;
                CHECK_MSTATUS(MGlobal::executeCommand(cmd_position_POINT, new_position, false, false));
                //LDbg("position_os's size=%d, new_position's size=%d", position_os.length(), new_position.length());

                for(unsigned int i = 0; i < position_os.length(); ++i)
                {
                    position_os[ i ].x = new_position[3*i    ];
                    position_os[ i ].y = new_position[3*i + 1];
                    position_os[ i ].z = new_position[3*i + 2];
                }
                // set positions of outGeometry
                CHECK_MSTATUS(fnOutMesh.setPoints(position_os, MSpace::kObject));
            }
        }//if(enablePosition)

        dhOutGeometry.setClean();
        CHECK_MSTATUS(data.setClean(plug));
	}
	else{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}

MString Point::getStrFunction_position_POINT(const MString &nodeName, const unsigned int vertexCount, const double currentTime)
{
    MStatus status;

    MString strCurrentFrame;
    MString strVertexCount;
    MString strVertexCountX4;
    strCurrentFrame.set(currentTime);
    strVertexCount.set(vertexCount);
    strVertexCountX4.set(vertexCount * 4);

    MString text;
    text =
    _("")
    _("global proc float[] position_POINT()")
    _("{")
    _(" string $NODE=\""+nodeName+"\";")
    _(" float $FF="+strCurrentFrame+";int $F=$FF;")
    _(" int $IDS[]    =`getAttr ($NODE+\".outIdPP\")`;")
    _("")
    _(" int $MAPID[];")
    _(" float $POS[]  =`getAttr ($NODE+\".outPositionPP\")`;")
//  _(" float $POS2[] =`getAttr ($NODE+\".outPositionPP2\")`;")
//  _(" float $CLR[]  =`getAttr ($NODE+\".outRgbaPP\")`;")
//  _(" float $CLR2[] =`getAttr ($NODE+\".outRgbaPP2\")`;")
    _(" float $WGT[]  =`getAttr ($NODE+\".inWeightPP\")`;")
//  _(" float $WGT2[] =`getAttr ($NODE+\".inWeightPP2\")`;")
//  _(" float $NML[]  =`getAttr ($NODE+\".outNormalPP\")`;")
//  _(" float $NML2[] =`getAttr ($NODE+\".outNormalPP2\")`;")
//  _(" float $RDS[]  =`getAttr ($NODE+\".inRadiusPP\")`;")
//  _(" float $RDS2[] =`getAttr ($NODE+\".inRadiusPP2\")`;")
//  _(" float $MAP[]  =`getAttr ($NODE+\".outSharedMapPP\")`;")
//  _(" float $MAP2[] =`getAttr ($NODE+\".outSharedMapPP2\")`;")
    _("")
    _(" int $ox,$oy,$oz,  $ox3,$oy3,$oz3,  $ox4,$oy4,$oz4,  $ou,$ov;")
    _(" float $OUTPUT["+strVertexCountX4+"];")
    _(" float $ID,$MP,$X,$Y,$Z,$TX,$TY,$TZ,$TX2,$TY2,$TZ2,$WEIGHT,$WEIGHT2,$NX,$NY,$NZ,$NX2,$NY2,$NZ2,$CR,$CB,$CG,$CR2,$CB2,$CG2,$RADIUS,$RADIUS2,$MAPU,$MAPV,$MAPU2,$MAPV2;")
    _(" $ID=$X=$Y=$Z=$TX=$TY=$TZ=$TX2=$TY2=$TZ2=$R=$G=$B=$A=$R2=$G2=$B2=$A2=$WEIGHT=$WEIGHT2=$NX=$NY=$NZ=$NX2=$NY2=$NZ2=$CR=$CB=$CG=$CR2=$CB2=$CG2=$RADIUS=$RADIUS2=$MAPU=$MAPV=$MAPU2=$MAPV2=0;")
    _("")
    +m_stringStartBlock+
    _("")
    _(" for($i=0;$i<"+strVertexCount+";$i++)")
    _(" {")
    _("     $ID=(float)$IDS[$i];")
    _("     $ox =$i*3;              $oy =$ox +1;                $oz =$ox +2;")
    _("     $ox3=$IDS[$i]*3;        $oy3=$ox3+1;                $oz3=$ox3+2;")
    _("     $ox4=$IDS[$i]*4;        $oy4=$ox4+1;                $oz4=$ox4+2;        $oa4=$ox4+3;")
    _("     $ou =$i*2;              $ov =$ou +1;")
    _("")
    _("     $X  =$POS [$ox4];       $Y  =$POS [$oy4];           $Z  =$POS [$oz4];")
    _("     $TX =$POS [$ox4];       $TY =$POS [$oy4];           $TZ =$POS [$oz4];")
//  _("     $TX2=$POS2[$ox4];       $TY2=$POS2[$oy4];           $TZ2=$POS2[$oz4];")
//  _("")
//  _("     $CR =$CLR [$ox4];       $CG =$CLR [$oy4];           $CB =$CLR [$oz4];   $ALPHA =$CLR [$oa4];")
//  _("     $CR2=$CLR2[$ox4];       $CG2=$CLR2[$oy4];           $CB2=$CLR2[$oz4];   $ALPHA2=$CLR2[$oa4];")
//  _("")
    _("     $WEIGHT =$WGT [$IDS[$i]];")
//  _("     $WEIGHT2=$WGT2[$IDS[$i]];")
//  _("")
//  _("     $NX =$NML [$ox3];       $NY =$NML [$oy3];           $NZ =$NML [$oz3];")
//  _("     $NX2=$NML2[$ox3];       $NY2=$NML2[$oy3];           $NZ2=$NML2[$oz3];")
//  _("")
//  _("     $RADIUS =$RDS [$IDS[$i]];")
//  _("     $RADIUS2=$RDS2[$IDS[$i]];")
//  _("")
//  _("     $MAPU =$MAP [$ou];      $MAPV =$MAP [$ov];")
//  _("     $MAPU2=$MAP2[$ou];      $MAPV2=$MAP2[$ov];")
    _("")
    +m_stringPosition+
    _("")
    _("     $OUTPUT[$ox]=$X;        $OUTPUT[$oy]=$Y;            $OUTPUT[$oz]=$Z;")
    _(" }")
    _("")
    +m_stringEndBlock+
    _("")
    _("return $OUTPUT;")
    _("};")
    _("")
    _("position_POINT();")
    _("")
      ;


    return text;
}

/////////////////////////////////////////////////////////////////////
MStatus initializePlugin_Point(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
		plugin.registerNode(
		Point::sTypeName(),
		Point::sTypeId(),
		Point::creator,
		Point::initialize,
		Point::sType(),
		&Point::sClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += Point::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}

MStatus uninitializePlugin_Point(MFnPlugin &plugin)
{
	CHECK_MSTATUS_AND_RETURN_IT(
        plugin.deregisterNode( Point::sTypeId() )
    );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += Point::sClassification();
	command += "\");}\n";

	CHECK_MSTATUS_AND_RETURN_IT(
        MGlobal::executeCommand( command )
    );

	return MS::kSuccess;
}
