#pragma once

#include <maya/MIOStream.h>
#include <maya/MNodeMessage.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>

#include <common/IncludeMFnPluginClass.h>
#include <common/version_helper.h>
#include "boundingObject_util.h"

/////////////////////////////////////////////////////////////////////

#define MCHECKERROR(STAT,MSG)       \
    if ( MS::kSuccess != STAT ) {   \
        cerr << MSG << endl;        \
        return MS::kFailure;        \
    }

#define MAKE_NUMERIC_ATTR( NAME, SHORTNAME, TYPE, DEFAULT, KEYABLE ) \
	MStatus NAME##_stat;                                             \
	MFnNumericAttribute NAME##_fn;                                   \
	NAME = NAME##_fn.create( #NAME, SHORTNAME, TYPE, DEFAULT );      \
	MCHECKERROR(NAME##_stat, "numeric attr create error");		     \
	NAME##_fn.setHidden( !KEYABLE );								 \
	NAME##_fn.setKeyable( KEYABLE );								 \
	NAME##_fn.setInternal( true );									 \
	NAME##_stat = addAttribute( NAME );                              \
	MCHECKERROR(NAME##_stat, "addAttribute error");

#define LEAD_COLOR				18	// green
#define ACTIVE_COLOR			15	// white
#define ACTIVE_AFFECTED_COLOR	8	// purple
#define DORMANT_COLOR			4	// blue
#define HILITE_COLOR			17	// pale blue

/////////////////////////////////////////////////////////////////////
//
// Shape class - defines the non-UI part of a shape node
//
class BoundingObject : public MPxLocatorNode
{
public:
	BoundingObject();
	virtual ~BoundingObject();

	virtual void			postConstructor();
    //virtual bool            getInternalValueInContext(const MPlug&, MDataHandle&,  MDGContext&);
    //virtual bool            setInternalValueInContext(const MPlug&, const MDataHandle&, MDGContext&);
	virtual MStatus			compute( const MPlug&, MDataBlock& );
	virtual void            draw(M3dView &view, const MDagPath &path,
								 M3dView::DisplayStyle style,
								 M3dView::DisplayStatus status);
	virtual bool            isBounded() const;
	virtual MBoundingBox    boundingBox() const;

	static  void *		    creator();
	static  MStatus		    initialize();

    static MString          sTypeName();
	static MTypeId          sTypeId();
	static MPxNode::Type    sType();
	static const MString&   sClassification();


protected:
    static MString          m_classification;
	static VersionHelper    m_version_helper;

private:
    static void			sAttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug, void* inUserPtr);
	void				AttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug);

	// Attributes
	//
    static MObject      a_inParentMatrix;
    static MObject      a_outData;
    static MObject          a_outParentMatrix;
    static MObject          a_outControlData;
	static MObject              a_outType;// The type of the bounding object

    //static const float  R;
    static BoundingObjectUtilMgr m_bouMgr;
    MCallbackId			    m_callbackId;

    friend class AttributeTransfer;
    friend class Group;
};

//
MStatus initializePlugin_BoundingObject(MFnPlugin &plugin);
MStatus uninitializePlugin_BoundingObject(MFnPlugin &plugin);
