"""
To create an Attribute Editor template using python, do the following:
     1. create a subclass of `uitypes.AETemplate`
    2. set its ``_nodeType`` class attribute to the name of the desired node type, or name the class using the
convention ``AE<nodeType>Template``
    3. import the module

AETemplates which do not meet one of the two requirements listed in step 2 will be ignored.  To ensure that your
Template's node type is being detected correctly, use the ``AETemplate.nodeType()`` class method::

    import AETemplates
    AETemplates.AEmib_amb_occlusionTemplate.nodeType()

As a convenience, when pymel is imported it will automatically import the module ``AETemplates``, if it exists,
thereby causing any AETemplates within it or its sub-modules to be registered. Be sure to import pymel
or modules containing your ``AETemplate`` classes before opening the Atrribute Editor for the node types in question.

To check which python templates are loaded::

    from pymel.core.uitypes import AELoader
    print AELoader.loadedTemplates()

The example below demonstrates the simplest case, which is the first. It provides a layout for the mib_amb_occlusion
mental ray shader.
"""
import inspect                      as isp
import maya.cmds                    as cmds
import pymel.core                   as pm
import mymagicbox.AETemplateBase    as AETemplateBase
import mymagicbox.log               as log
import mymagicbox.mmb_flog          as flog

class AEos_pointTemplate(AETemplateBase.mmbTemplateBase):
    @flog.trace
    def buildBody(self, nodeName):
        self.AEswatchDisplay(nodeName)

        self.beginLayout("Global", collapse=0)
        self.addControl("currentTime")
        self.endLayout()#"Global"

        self.beginLayout("Inputs A", collapse=1)
        self.addControl("inGeometry")
        self.endLayout()#"Inputs A"

        self.beginLayout("Position", collapse=1)
        self.addControl('enablePosition',       label='Enable')
        self.addControl('applyChangesPosition', label='Apply Changes')
        self.callCustom(self.createLayoutPosition_POINT_SOUP, self.replaceLayoutPosition_POINT_SOUP, "stringPosition", '')
        self.endLayout()

        pm.mel.AEdependNodeTemplate(self.nodeName)

        self.addExtraControls()
        pass

    #@flog.trace
    # NOTE: flog.trace can't be used with callback functions, or there will be a runtime error.
    def createLayoutPosition_POINT_SOUP(self, sNodeAttr, sAttrName):
        log.debug('%s.%s(%s)', self.__class__.__name__,   isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )

        with pm.columnLayout('layoutPosition_POINT_SOUP', adj=True) as cl:
            sText        = cmds.getAttr(sNodeAttr);
            sScrollField = cmds.scrollField(tx=sText);
            with pm.columnLayout():
                with pm.rowColumnLayout(nc=2, cw=[(1, 190), (2, 190)]) as rl:
                    pm.uitypes.Button(l='Apply',  command=pm.Callback(self.apply_POINT_SOUP,  sNodeAttr, sScrollField));
                    pm.uitypes.Button(l='Reload', command=pm.Callback(self.reload_POINT_SOUP, sNodeAttr, sScrollField));
        pass

    #@flog.trace
    # NOTE: flog.trace can't be used with callback functions, or there will be a runtime error.
    def replaceLayoutPosition_POINT_SOUP(self, sNodeAttr, sAttrName):
        log.debug('%s.%s(%s)', self.__class__.__name__,   isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )

        cmds.deleteUI('layoutPosition_POINT_SOUP', layout=True)
        self.createLayoutPosition_POINT_SOUP(sNodeAttr, sAttrName)
        pass

    #@flog.trace
    # NOTE: flog.trace can't be used with callback functions, or there will be a runtime error.
    def apply_POINT_SOUP(self, sNodeAttr, sScrollField):
        log.debug('%s.%s(%s)', self.__class__.__name__,   isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )

        sText = cmds.scrollField(sScrollField, q=True, tx=True);
        cmds.setAttr(sNodeAttr, sText, type='string')
        pass

    #@flog.trace
    # NOTE: flog.trace can't be used with callback functions, or there will be a runtime error.
    def reload_POINT_SOUP(self, sNodeAttr, sScrollField):
        log.debug('%s.%s(%s)', self.__class__.__name__,   isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )

        sText = cmds.getAttr(sNodeAttr)
        cmds.scrollField(sScrollField, e=True, tx=sText)
        pass
