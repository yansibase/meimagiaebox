# -*- coding: utf-8 -*-

import maya.cmds                    as cmds
import maya.mel                     as mel
import pymel.core                   as pm
import pymel.tools.py2mel           as py2mel
import mymagicbox.log               as log

log.debug('This is '+__file__)

gDeveloperMode = False
################################################################
def createNode_arrayDataContainer(nodeName=''):
    '''
    test:
        createNode_arrayDataContainer('testnode')
        createNode_arrayDataContainer()
    '''
    log.debug('This is opensoupSetup.createNode_arrayDataContainer(%s)', nodeName)

    # create 'arrayDataContainer' node
    node=''
    if '' == nodeName:
        node = cmds.createNode('os_arrayDataContainer')
    else:
        node = cmds.createNode('os_arrayDataContainer', name='%s'%nodeName)
    return node
#
def createNode_createArray(nodeName=''):
    '''
    test:
        createNode_createArray('testnode')
        createNode_createArray()
    '''
    log.debug('This is opensoupSetup.createNode_createArray(%s)', nodeName)

    # create 'createArray' node
    node=''
    if '' == nodeName:
        node = cmds.createNode('os_createArray')
    else:
        node = cmds.createNode('os_createArray', name='%s'%nodeName)
    return node
#
def createNode_boundingObject(nodeName=''):
    '''
    test:
        createNode_boundingObject('testnode')
        createNode_boundingObject()
    '''
    log.debug('This is opensoupSetup.createNode_boundingObject(%s)', nodeName)

    # create 'boundingObject' node
    node=''
    if '' == nodeName:
        node = cmds.createNode('os_boundingObject')
    else:
        node = cmds.createNode('os_boundingObject', name='%s'%nodeName)

    # connect inParentMatrix
    parents = cmds.listRelatives(node, parent=True)
    cmds.connectAttr(parents[0]+'.worldMatrix[0]', node+'.inParentMatrix')

    return node

def createNode_attributeTransfer(nodeName=''):
    '''
    test:
        createNode_attributeTransfer('testnode')
        createNode_attributeTransfer()
    '''
    log.debug('This is opensoupSetup.createNode_attributeTransfer(%s)', nodeName)

    # create 'attributeTransfer' node
    node=''
    if '' == nodeName:
        node = cmds.createNode('os_attributeTransfer')
    else:
        node = cmds.createNode('os_attributeTransfer', name='%s'%nodeName)

    # connect inParentMatrix
    #parents = cmds.listRelatives(node, parent=True)
    #cmds.connectAttr(parents[0]+'.worldMatrix[0]', node+'.inParentMatrix')

    return node

def createNode_group(nodeName=''):
    '''
    test:
        createNode_group('testnode')
        createNode_group()
    '''
    log.debug('This is opensoupSetup.createNode_group(%s)', nodeName)

    # create 'group' node
    node=''
    if '' == nodeName:
        node = cmds.createNode('os_group')
    else:
        node = cmds.createNode('os_group', name='%s'%nodeName)

    return node

def createNode_point(nodeName=''):
    '''
    test:
        createNode_point('testnode')
        createNode_point()
    '''
    log.debug('This is opensoupSetup.createNode_point(%s)', nodeName)

    # create 'point' node
    node=''
    if '' == nodeName:
        node = cmds.createNode('os_point')
    else:
        node = cmds.createNode('os_point', name='%s'%nodeName)

    return node

def createNode_vertexMapper(nodeName=''):
    '''
    test:
        createNode_vertexMapper('testnode')
        createNode_vertexMapper()
    '''
    log.debug('This is opensoupSetup.createNode_vertexMapper(%s)', nodeName)

    # create 'vertexMapper' node
    node=''
    if '' == nodeName:
        node = cmds.createNode('os_vertexMapper')
    else:
        node = cmds.createNode('os_vertexMapper', name='%s'%nodeName)

    return node
#
def about():
    log.debug('This is opensoupSetup.about()')
    log.info('=================================================')
    log.info('--------------------- NOTE ----------------------')
    log.info('=================================================')

    pass
################################################################
def createMenu(parentMenuPath):
    log.debug('This is opensoupSetup.createMenu(%s)', parentMenuPath)

    #cmds.setParent(parentMenuPath, menu=True)
    menuPath = parentMenuPath +'|opensoup'
    #log.debug('menu name(expected): %s', menuPath);

    if not cmds.menu(menuPath, query=True, exists=True): # create the menu if it doesn't exists
        menuPath = cmds.menuItem('opensoup', parent=parentMenuPath, subMenu=True, tearOff=True)
        #log.debug('menu name(created): %s', menuPath);
        cmds.menuItem('os_arrayDataContainer',  command=lambda *args: createNode_arrayDataContainer(), ann='createNode_arrayDataContainer(nodeName)')
        cmds.menuItem('os_createArray',         command=lambda *args: createNode_createArray(),        ann='createNode_createArray(nodeName)')
        cmds.menuItem('os_boundingObject',      command=lambda *args: createNode_boundingObject(),     ann='createNode_boundingObject(nodeName)')
        cmds.menuItem('os_attributeTransfer',   command=lambda *args: createNode_attributeTransfer(),  ann='createNode_attributeTransfer(nodeName)')
        cmds.menuItem('os_group',               command=lambda *args: createNode_group(),              ann='createNode_group(nodeName)')
        cmds.menuItem('os_point',               command=lambda *args: createNode_point(),              ann='createNode_point(nodeName)')
        cmds.menuItem('os_vertexMapper',        command=lambda *args: createNode_vertexMapper(),       ann='createNode_vertexMapper(nodeName)')
        cmds.menuItem(divider=True)
        cmds.menuItem('About',               command=lambda *args: about(),                      ann='about()')

    return menuPath
#
def deleteMenu(parentMenuPath):
    log.debug('This is opensoupSetup.deleteMenu()')

    menuPath = parentMenuPath +'|opensoup'
    if cmds.menu(menuPath, query=True, exists=True):
        cmds.deleteUI(menuPath, menu=True)
    else:
        log.debug('menu is not found: %s', menuPath)

    pass
#
def createDummyBaseRiggingToolsMenu():
    '''
    Create 'BFX_Rigging' menu if it doesn't exists.
    In my local develop environment, 'BFX_Rigging' menu will not be created,
    so I create a dummy 'BFX_Rigging' menu.
    '''
    menuName = 'riggingToolsMenu'# 'BFX_Rigging', see /PLE/studio/app/maya/baseRigging/utils.py --> createMenu()

    menuPath=''
    if not cmds.menu(menuName, query=True, exists=True):
        gMainWindow = mel.eval('$temp=$gMainWindow') # 'MayaWindow'

        menuPath=cmds.menu(menuName, l='BFX_Rigging', parent=gMainWindow, tearOff=True)
        cmds.setParent(menuName, menu=True)
        cmds.menuItem(subMenu=True, l='Facial Setup', tearOff=True)
        cmds.menuItem(l='Auto Connect Selected Attributes')
        cmds.menuItem(divider=True)
    else:
        menuPath='MayaWindow|riggingToolsMenu'
    return menuPath
################################################################
def loadDependentPlugins():
    log.debug('This is opensoupSetup.loadDependentPlugins()');

    pass

################################################################
def setupUI():
    log.debug('This is opensoupSetup.setupUI() in py');

    loadDependentPlugins()

    baseRigMenuPath = createDummyBaseRiggingToolsMenu()

    myBasePluginsMenuPath = baseRigMenuPath +'|MyBasePlugins'
    #log.debug('menu name(expected): %s', myBasePluginsMenuPath);

    if not cmds.menu(myBasePluginsMenuPath, query=True, exists=True): # create the menu if it doesn't exists
        myBasePluginsMenuPath = cmds.menuItem('MyBasePlugins', parent=baseRigMenuPath, subMenu=True, tearOff=True)
        #log.debug('menu name(created): %s', myBasePluginsMenuPath);

    createMenu(myBasePluginsMenuPath)
    pass

def unsetupUI():
    log.debug('This is opensoupSetup.unsetupUI() in py');

    baseRigMenuPath = 'MayaWindow|riggingToolsMenu'

    myBasePluginsMenuPath = baseRigMenuPath +'|MyBasePlugins'

    deleteMenu(myBasePluginsMenuPath)

    pass

def setup():
    log.debug('This is opensoupSetup.setup() in py');

    loadDependentPlugins()

    pass

def unsetup():
    log.debug('This is opensoupSetup.unsetup() in py');
    pass

# register the above python functions as mel procedures
py2mel.py2melProc(setupUI,   procName='opensoup_setupUI',   evaluateInputs=True);
py2mel.py2melProc(unsetupUI, procName='opensoup_unsetupUI', evaluateInputs=True);
py2mel.py2melProc(setup,     procName='opensoup_setup',     evaluateInputs=True);
py2mel.py2melProc(unsetup,   procName='opensoup_unsetup',   evaluateInputs=True);
