import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class MyTest_vertexMapper(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest_vertexMapper, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('opensoup')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('opensoup')
        
    def test_(self):
        log.debug('---- begin %s.test_() ----', self.m_id);


        #
        mappedVertexSrc = cmds.getAttr('os_vertexMapper1.outMappedVertexSrc');
        self.assertEqual(mappedVertexSrc,     [0, 1, 2, 3, 4, 6, 10, 11, 13])
        
        mappedVertexDst = cmds.getAttr('os_vertexMapper1.outMappedVertexDst');
        self.assertEqual(mappedVertexDst,     [0, 2, 3, 7, 9, 10, 11, 12, 13])


        #
        connectedFaceOfMappedVertexSrc = cmds.getAttr('os_vertexMapper1.outConnectedFaceOfMappedVertexSrc')
        self.assertEqual(connectedFaceOfMappedVertexSrc, [0,1,4,2,3,5,6]); 
        
        connectedFaceOfMappedVertexDst = cmds.getAttr('os_vertexMapper1.outConnectedFaceOfMappedVertexDst')
        self.assertEqual(connectedFaceOfMappedVertexDst, [0,1,4,5,2,6,3]);


        #
        mappedFaceSrc = cmds.getAttr('os_vertexMapper1.outMappedFaceSrc')
        self.assertEqual(mappedFaceSrc, [1,4]);

        mappedFaceDst = cmds.getAttr('os_vertexMapper1.outMappedFaceDst')
        self.assertEqual(mappedFaceDst, [4,5]);


        #
        unmappedVertexSrc = cmds.getAttr('os_vertexMapper1.outUnmappedVertexSrc')
        self.assertEqual(unmappedVertexSrc,     [5, 7, 8, 9, 12]) 

        unmappedVertexDst = cmds.getAttr('os_vertexMapper1.outUnmappedVertexDst')
        self.assertEqual(unmappedVertexDst,     [1, 4, 5, 6, 8]) 


        #
        connectedFaceOfUnmappedVertexSrc = cmds.getAttr('os_vertexMapper1.outConnectedFaceOfUnmappedVertexSrc')
        self.assertEqual(connectedFaceOfUnmappedVertexSrc,     [3, 1, 2, 5, 4, 6])        
        connectedFaceOfUnmappedVertexDst = cmds.getAttr('os_vertexMapper1.outConnectedFaceOfUnmappedVertexDst')
        self.assertEqual(connectedFaceOfUnmappedVertexDst,     [0, 1, 2, 3, 4, 5])

        ############### put the faces into set
        self.addToSet__(connectedFaceOfUnmappedVertexSrc, connectedFaceOfUnmappedVertexDst, 
                    'connectedFaceOfUnmappedVertexName_src', 'connectedFaceOfUnmappedVertexName_dst'
        )
        self.addToSet__(mappedFaceSrc, mappedFaceDst, 
                    'mappedFace_src', 'mappedFace_dst'
        )

        log.debug('---- end   %s.test_() ----', self.m_id);


    def addToSet__(self, indexArraySrc, indexArrayDst, setNameSrc, setNameDst):

        # find the face name
        nameArraySrc = []
        for f in indexArraySrc:
            nameArraySrc.append('pPlaneSrcShape.f[%d]'%(f))
            
        nameArrayDst = []
        for f in indexArrayDst:
            nameArrayDst.append('pPlaneDstShape.f[%d]'%(f))
            
        # add the face into set
        newSet1 = cmds.sets(name=setNameSrc)
        cmds.sets(nameArraySrc, addElement=newSet1)

        newSet2 = cmds.sets(name=setNameDst)
        cmds.sets(nameArrayDst, addElement=newSet2)


'''
# test in maya GUI
def addToSet__( indexArraySrc, indexArrayDst, setNameSrc, setNameDst):
        # find the face name
        nameArraySrc = []
        for f in indexArraySrc:
            nameArraySrc.append('pPlaneSrcShape.f[%d]'%(f))
            
        nameArrayDst = []
        for f in indexArrayDst:
            nameArrayDst.append('pPlaneDstShape.f[%d]'%(f))
            
        # add the face into set
        newSet1 = cmds.sets(name=setNameSrc)
        cmds.sets(nameArraySrc, addElement=newSet1)

        newSet2 = cmds.sets(name=setNameDst)
        cmds.sets(nameArrayDst, addElement=newSet2)

#
mappedFaceSrc = cmds.getAttr('os_vertexMapper1.outMappedFaceSrc')
mappedFaceDst = cmds.getAttr('os_vertexMapper1.outMappedFaceDst')

#
connectedFaceOfUnmappedVertexSrc = cmds.getAttr('os_vertexMapper1.outConnectedFaceOfUnmappedVertexSrc')
connectedFaceOfUnmappedVertexDst = cmds.getAttr('os_vertexMapper1.outConnectedFaceOfUnmappedVertexDst')


addToSet__(connectedFaceOfUnmappedVertexSrc, connectedFaceOfUnmappedVertexDst, 
                    'connectedFaceOfUnmappedVertexName_src', 'connectedFaceOfUnmappedVertexName_dst'
)
addToSet__(mappedFaceSrc, mappedFaceDst, 
                    'mappedFace_src', 'mappedFace_dst'
)
'''



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
