import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('opensoup')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('opensoup')
        
    def __getOutputArray(self, node):
        outArray = cmds.getAttr (node+'.outArray')
        outArrayType = cmds.getAttr (node+'.outArray', type=True)
        return outArrayType, outArray

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        ############### create node
        node = cmds.createNode('os_createArray', name='os_createArray2');
        cmds.setAttr(node+'.defaultValueX', 1);
        cmds.setAttr(node+'.defaultValueY', 2);
        cmds.setAttr(node+'.defaultValueZ', 3);
        cmds.setAttr(node+'.defaultValueW', 4);
        
        ############### length = 0
        outArrayType, outArray = self.__getOutputArray(node)
        self.assertEqual(outArrayType, None)
        self.assertEqual(outArray,     None)

        ############### length = 1
        cmds.setAttr(node+'.length', 1);
        #  outputType = 0
        cmds.setAttr(node+'.outputType', 0);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'Int32Array')
        self.assertEqual(outArray,     [1])

        #  outputType = 1
        cmds.setAttr(node+'.outputType', 1);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0])

        #  outputType = 2
        cmds.setAttr(node+'.outputType', 2);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0])

        #  outputType = 3
        cmds.setAttr(node+'.outputType', 3);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 3.0])
        
        #  outputType = 4
        cmds.setAttr(node+'.outputType', 4);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'vectorArray')
        self.assertEqual(outArray,     [(1.0, 2.0, 3.0)])
        
        #  outputType = 5
        cmds.setAttr(node+'.outputType', 5);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 3.0, 4.0])
        
        ############### length = 2
        cmds.setAttr(node+'.length', 2);
        #  outputType = 0
        cmds.setAttr(node+'.outputType', 0);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'Int32Array')
        self.assertEqual(outArray,     [1, 1])

        #  outputType = 1
        cmds.setAttr(node+'.outputType', 1);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 1.0])

        #  outputType = 2
        cmds.setAttr(node+'.outputType', 2);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 1.0, 2.0])

        #  outputType = 3
        cmds.setAttr(node+'.outputType', 3);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 3.0, 1.0, 2.0, 3.0])
        
        #  outputType = 4
        cmds.setAttr(node+'.outputType', 4);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'vectorArray')
        self.assertEqual(outArray,     [(1.0, 2.0, 3.0), (1.0, 2.0, 3.0)])
        
        #  outputType = 5
        cmds.setAttr(node+'.outputType', 5);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0, 4.0])
                
        ############### length = 3
        cmds.setAttr(node+'.length', 3);
        #  outputType = 0
        cmds.setAttr(node+'.outputType', 0);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'Int32Array')
        self.assertEqual(outArray,     [1, 1, 1])

        #  outputType = 1
        cmds.setAttr(node+'.outputType', 1);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 1.0, 1.0])

        #  outputType = 2
        cmds.setAttr(node+'.outputType', 2);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 1.0, 2.0, 1.0, 2.0])

        #  outputType = 3
        cmds.setAttr(node+'.outputType', 3);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 3.0, 1.0, 2.0, 3.0, 1.0, 2.0, 3.0])
        
        #  outputType = 4
        cmds.setAttr(node+'.outputType', 4);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'vectorArray')
        self.assertEqual(outArray,     [(1.0, 2.0, 3.0), (1.0, 2.0, 3.0), (1.0, 2.0, 3.0)])
        
        #  outputType = 5
        cmds.setAttr(node+'.outputType', 5);
        outArrayType, outArray = self.__getOutputArray(node)
        #print outArrayType, outArray
        self.assertEqual(outArrayType, 'doubleArray')
        self.assertEqual(outArray,     [1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0, 4.0])
                

        cmds.delete(node)
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
