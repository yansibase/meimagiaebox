import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class MyTest_vertexMapper(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest_vertexMapper, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('opensoup')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('opensoup')
        
    def test_(self):
        log.debug('---- begin %s.test_() ----', self.m_id);
        
        # set src mesh
        cmds.setAttr("polyPlane2.subdivisionsWidth",  2);
        cmds.setAttr("polyPlane2.subdivisionsHeight", 2);
        
        outVertexIndexSrc = cmds.getAttr('os_vertexMapper1.outMappedVertexSrc');
        self.assertEqual(outVertexIndexSrc,     [0, 1, 2, 3, 4, 5, 6, 7, 8])
        
        outVertexIndexDst = cmds.getAttr('os_vertexMapper1.outMappedVertexDst');
        self.assertEqual(outVertexIndexDst,     [0, 1, 2, 3, 4, 5, 6, 7, 8])
        
        
        # set src mesh
        cmds.setAttr("polyPlane2.subdivisionsWidth",  1);
        cmds.setAttr("polyPlane2.subdivisionsHeight", 1);
        
        outVertexIndexSrc = cmds.getAttr('os_vertexMapper1.outMappedVertexSrc');
        self.assertEqual(outVertexIndexSrc,     [0, 1, 2, 3])
        
        outVertexIndexDst = cmds.getAttr('os_vertexMapper1.outMappedVertexDst');
        self.assertEqual(outVertexIndexDst,     [0, 2, 6, 8])
        
        log.debug('---- end   %s.test_() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
