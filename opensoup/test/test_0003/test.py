import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('opensoup')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('opensoup')
        
        renderer = rndr.CreateRenderer('mentalRay')
        renderer.render()
        self.m_helper.moveRenderedImages();
        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)
        
    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
