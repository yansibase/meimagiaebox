import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

filepath = ''
if sys.platform == 'win32':
    filepath = 'd:/dev/mybox/opensoup/test/test_0005'
elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
    filepath = '/backup/lhome/dev/mybox/opensoup/test/test_0005'

m_id        = ''
m_helper    = None


m_id       = filepath + '/' + ''
m_helper   = hp.ATHelper()


log.debug('%s.setUp', m_id);
#super(MyTest00, self).setUp()
m_helper.openMayaFile(filepath+'/scenes/test.ma')
hp.ATHelper.tryToLoadPlugin('opensoup')
        

renderer = rndr.CreateRenderer('mayaHardware')
renderer.render()
m_helper.moveRenderedImages();
isEqual = m_helper.compare_image()
if isEqual:
    log.info('Passed. %s', filepath)
else:
    self.assertEqual(isEqual, True, "Failed. "+filepath)
        
