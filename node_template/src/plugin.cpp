#include <maya/MFnPlugin.h>
#include <maya/MStatus.h>

#include <common/log.h>
#include "node_template.h"
#include "version.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya
//
PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('"+node_template::cTypeName()+"')", true, false) );

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

	CHECK_MSTATUS(
		plugin.registerNode(
		node_template::cTypeName(),
		node_template::cTypeId(),
		node_template::creator,
		node_template::initialize,
		node_template::cType(),
		&node_template::classification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += node_template::classification();
	command += "\");}\n";

	CHECK_MSTATUS( MGlobal::executeCommand( command ) );

	// import node_templateSetup
	CHECK_MSTATUS( MGlobal::executePythonCommand("import "+node_template::cTypeName()+"Setup", true, false) );
	CHECK_MSTATUS(plugin.registerUI(
                    node_template::cTypeName()+"_setupUI()",
                    node_template::cTypeName()+"_unsetupUI()",
                    node_template::cTypeName()+"_setup()",
                    node_template::cTypeName()+"_unsetup()"
                    ));

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('"+node_template::cTypeName()+"')", true, false) );
                    
	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('"+node_template::cTypeName()+"')", true, false) );

	MFnPlugin plugin( obj );

	CHECK_MSTATUS( plugin.deregisterNode( node_template::cTypeId() ) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += node_template::classification();
	command += "\");}\n";

	CHECK_MSTATUS( MGlobal::executeCommand( command ) );

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('"+node_template::cTypeName()+"')", true, false) );

	return MS::kSuccess;
}
