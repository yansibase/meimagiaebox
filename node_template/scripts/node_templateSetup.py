# -*- coding: utf-8 -*-

import maya.cmds                    as cmds
import maya.mel                     as mel
import pymel.core                   as pm
import pymel.tools.py2mel           as py2mel
import mymagicbox.log               as log

log.debug('This is '+__file__)

gDeveloperMode = False
################################################################

def createNode(nodeName=''):
    '''
    test:
        createNode('testnode')
        createNode()
    '''
    log.debug('This is node_template.createNode(%s)', nodeName)

    # create 'node_template' node
    node=''
    if '' == nodeName:
        node = pm.createNode('node_template')
    else:
        node = pm.createNode('node_template', name='%s'%nodeName)


    return node
#
def about():
    log.debug('This is node_template.about()')
    log.info('=================================================')
    log.info('--------------------- NOTE ----------------------')
    log.info('=================================================')

    pass
################################################################
def createMenu(parentMenuPath):
    log.debug('This is node_template.createMenu(%s)', parentMenuPath)

    #cmds.setParent(parentMenuPath, menu=True)
    menuPath = parentMenuPath +'|node_template'
    #log.debug('menu name(expected): %s', menuPath);

    if not cmds.menu(menuPath, query=True, exists=True): # create the menu if it doesn't exists
        menuPath = cmds.menuItem('node_template', parent=parentMenuPath, subMenu=True, tearOff=True)
        #log.debug('menu name(created): %s', menuPath);
        cmds.menuItem(    'Create',                    command=lambda *args: createNode(),                 ann='createNode(nodeName)')
        cmds.menuItem(divider=True)
        cmds.menuItem(    'About',                     command=lambda *args: about(),                      ann='about()')

    return menuPath
#
def deleteMenu(parentMenuPath):
    log.debug('This is node_template.deleteMenu()')

    menuPath = parentMenuPath +'|node_template'
    if cmds.menu(menuPath, query=True, exists=True):
        cmds.deleteUI(menuPath, menu=True)
    else:
        log.debug('menu is not found: %s', menuPath)

    pass
#
def createDummyBaseRiggingToolsMenu():
    '''
    Create 'BFX_Rigging' menu if it doesn't exists.
    In my local develop environment, 'BFX_Rigging' menu will not be created,
    so I create a dummy 'BFX_Rigging' menu.
    '''
    menuName = 'riggingToolsMenu'# 'BFX_Rigging', see /PLE/studio/app/maya/baseRigging/utils.py --> createMenu()

    menuPath=''
    if not cmds.menu(menuName, query=True, exists=True):
        gMainWindow = mel.eval('$temp=$gMainWindow') # 'MayaWindow'

        menuPath=cmds.menu(menuName, l='BFX_Rigging', parent=gMainWindow, tearOff=True)
        cmds.setParent(menuName, menu=True)
        cmds.menuItem(subMenu=True, l='Facial Setup', tearOff=True)
        cmds.menuItem(l='Auto Connect Selected Attributes')
        cmds.menuItem(divider=True)
    else:
        menuPath='MayaWindow|riggingToolsMenu'
    return menuPath
################################################################
def loadDependentPlugins():
    log.debug('This is node_template.loadDependentPlugins()');

    pass

################################################################
def setupUI():
    log.debug('This is node_template.setupUI() in py');

    loadDependentPlugins()

    baseRigMenuPath = createDummyBaseRiggingToolsMenu()

    myBasePluginsMenuPath = baseRigMenuPath +'|MyBasePlugins'
    #log.debug('menu name(expected): %s', myBasePluginsMenuPath);

    if not cmds.menu(myBasePluginsMenuPath, query=True, exists=True): # create the menu if it doesn't exists
        myBasePluginsMenuPath = cmds.menuItem('MyBasePlugins', parent=baseRigMenuPath, subMenu=True, tearOff=True)
        #log.debug('menu name(created): %s', myBasePluginsMenuPath);

    createMenu(myBasePluginsMenuPath)
    pass

def unsetupUI():
    log.debug('This is node_template.unsetupUI() in py');

    baseRigMenuPath = 'MayaWindow|riggingToolsMenu'

    myBasePluginsMenuPath = baseRigMenuPath +'|MyBasePlugins'

    deleteMenu(myBasePluginsMenuPath)

    pass

def setup():
    log.debug('This is node_template.setup() in py');

    loadDependentPlugins()

    pass

def unsetup():
    log.debug('This is node_template.unsetup() in py');
    pass

# register the above python functions as mel procedures
py2mel.py2melProc(setupUI,   procName='node_template_setupUI',   evaluateInputs=True);
py2mel.py2melProc(unsetupUI, procName='node_template_unsetupUI', evaluateInputs=True);
py2mel.py2melProc(setup,     procName='node_template_setup',     evaluateInputs=True);
py2mel.py2melProc(unsetup,   procName='node_template_unsetup',   evaluateInputs=True);
