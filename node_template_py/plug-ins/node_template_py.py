# sampleDGNode.py

import sys
import maya.OpenMayaMPx         as OpenMayaMPx
import maya.OpenMaya            as OpenMaya
import mymagicbox.plugin_setup  as plugin_setup
# ... additional imports here ...

# Plug-in information:
kPluginNodeName = 'node_template_py'              # The name of the node.
kPluginNodeClassify = 'utility/general'     # Where this node will be found in the Maya UI.
kPluginNodeId = OpenMaya.MTypeId( 0x87EFE ) # A unique ID associated to this node type.

# Default attribute values
sampleDefaultValue = 1

##########################################################
# Plug-in 
##########################################################
class myNode(OpenMayaMPx.MPxNode):
    # Static variables which will later be replaced by the node's attributes.
    sampleInAttribute = OpenMaya.MObject()
    sampleOutAttribute = OpenMaya.MObject()
    
    def __init__(self):
        ''' Constructor. '''
        OpenMayaMPx.MPxNode.__init__(self)
        
    def compute(self, pPlug, pDataBlock):
        '''
        Node computation method.
            - pPlug: A connection point related to one of our node attributes (could be an input or an output)
            - pDataBlock: Contains the data on which we will base our computations.
        '''
        
        if( pPlug == myNode.sampleOutAttribute ):
            
            # Obtain the data handles for each attribute
            sampleInDataHandle = pDataBlock.inputValue( myNode.sampleInAttribute )
            sampleOutDataHandle = pDataBlock.outputValue( myNode.sampleOutAttribute )
            
            # Extract the actual value associated to our sample input attribute (we have defined it as a float)
            sampleInValue = sampleInDataHandle.asFloat()
            
            
            # ... perform the desired computation ...
            
            # Set the output value.
            sampleOutDataHandle.setFloat( sampleInValue ) #  As an example, we just set the output value to be equal to the input value.
            
            # Mark the output data handle as being clean; it need not be computed given its input.
            sampleOutDataHandle.setClean()
             
        else:
            return OpenMaya.kUnknownParameter

##########################################################
# Plug-in initialization.
##########################################################
def nodeCreator():
    ''' Creates an instance of our node class and delivers it to Maya as a pointer. '''
    return OpenMayaMPx.asMPxPtr( myNode() )

def nodeInitializer():
    ''' Defines the input and output attributes as static variables in our plug-in class. '''
    
    # The following MFnNumericAttribute function set will allow us to create our attributes.
    numericAttributeFn = OpenMaya.MFnNumericAttribute()
    
    #==================================
    # INPUT NODE ATTRIBUTE(S)
    #==================================
    global sampleDefaultValue
    myNode.sampleInAttribute = numericAttributeFn.create( 'myInputAttribute', 'i', 
                                                          OpenMaya.MFnNumericData.kFloat,
                                                          sampleDefaultValue )
    numericAttributeFn.setWritable( True )
    numericAttributeFn.setStorable( True ) 
    numericAttributeFn.setHidden( False )
    myNode.addAttribute( myNode.sampleInAttribute ) # Calls the MPxNode.addAttribute function.
    
    #==================================
    # OUTPUT NODE ATTRIBUTE(S)
    #==================================
    myNode.sampleOutAttribute = numericAttributeFn.create( 'myOutputAttribute', 'o',
                                                           OpenMaya.MFnNumericData.kFloat )
    numericAttributeFn.setStorable( False )
    numericAttributeFn.setWritable( False )
    numericAttributeFn.setReadable( True )
    numericAttributeFn.setHidden( False )
    myNode.addAttribute( myNode.sampleOutAttribute )
    
    #==================================
    # NODE ATTRIBUTE DEPENDENCIES
    #==================================
    # If sampleInAttribute changes, the sampleOutAttribute data must be recomputed.
    myNode.attributeAffects( myNode.sampleInAttribute, myNode.sampleOutAttribute )
    
    
###############################################################################
def initializePlugin( mobject ):

    # callback function
    plugin_setup.initializePlugin_beginCB(kPluginNodeName)
    
    ''' Initialize the plug-in '''
    mplugin = OpenMayaMPx.MFnPlugin( mobject )
    try:
        mplugin.registerNode( kPluginNodeName, kPluginNodeId, nodeCreator,
                              nodeInitializer, OpenMayaMPx.MPxNode.kDependNode, kPluginNodeClassify )
    except:
        sys.stderr.write( 'Failed to register node: ' + kPluginNodeName )
        raise

    # callback function
    plugin_setup.initializePlugin_endCB(kPluginNodeName)
    
    
def uninitializePlugin( mobject ):
    # callback function
    plugin_setup.uninitializePlugin_beginCB(kPluginNodeName)
    
    ''' Uninitializes the plug-in '''
    mplugin = OpenMayaMPx.MFnPlugin( mobject )
    try:
        mplugin.deregisterNode( kPluginNodeId )
    except:
        sys.stderr.write( 'Failed to deregister node: ' + kPluginNodeName )
        raise

    # callback function
    plugin_setup.uninitializePlugin_endCB(kPluginNodeName)
    
##########################################################
# Sample usage.
##########################################################
''' 
# Copy the following lines and run them in Maya's Python Script Editor:

import maya.cmds as cmds

cmds.loadPlugin( 'node_template_py.py' )
cmds.createNode( 'node_template_py' )
# ...

'''
