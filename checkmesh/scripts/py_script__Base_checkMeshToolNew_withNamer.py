# This file is taken from /ibrix3/PLE/studio/app/maya/scripts/Py_Script/Base_checkMeshToolNew_withNamer.py -by yaoyansi
#
from __future__ import division
from maya.cmds import *
import maya.OpenMaya as om
import maya.mel as mel
import math, re

def BMRTools():
    if(window('Base_MRUI',ex=1)):
        deleteUI('Base_MRUI',window=True)
    BMRUI=window('Base_MRUI' , t = 'BMR Tools' , s = True)
    
    formLayout('MainForm')
    checkMeshTab = tabLayout('checkMeshTab' , innerMarginWidth = 5 , innerMarginHeight = 5)
    formLayout( 'MainForm', edit=True, attachForm=((checkMeshTab, 'top', 0), 
                                                   (checkMeshTab, 'left', 0), 
                                                   (checkMeshTab, 'bottom', 0), 
                                                   (checkMeshTab, 'right', 0)
                                                  )
              )
    tabChild1 = formLayout('meshesFrom')
    
    tx_meshes = text('meshTX' , l='meshes:',fn='boldLabelFont')
    TSL_meshes = textScrollList('meshesList' , allowMultiSelection = True)
    
    #
    popupMenu('meshes_check_popmenu')
    menuItem('correctMeshMenuItem' , l = '')
    
    #
    check_Vertices_bt = button('CVertices',label='Check Vertices',command='checkMeshes()',w=190,h=25)
    
    #  Number sides of polygon 
    radioCollection()
    lessFour_rab = radioButton('lessFour' , label='3  Edges in a face.' , align='left'  , onc = 'lessFour_rab_c()')
    moreFour_rab = radioButton('moreFour' , label='More than 4  Edges in a face' , align='left' ,sl = True , onc = 'moreFour_rab_c()')
    check_polygons_bt = button('CPolygons',label='Check Polygons',command='checkPolygons()',w=190,h=25)
    
    #
    UVwindow_bt = button('UVwindow',label='UV Window',command = 'UVtextureUI()' , w=190,h=25)

    #  get current Panel 
    #  if the panel is type "modelPanel" , get the "twoSidedLighting" value .
    currentPanel = getPanel(wf=True)
    try:
        light = modelEditor( currentPanel , q = True , twoSidedLighting= True)
    except:
        light = True
    if light:
        light = 'TwoSide : (ON)'
    else:
        light = 'TwoSide : (OFF)'
    twoSideLight_bt = button('two_Side_Light',label = light , command = 'twoSideLight()' , h=25)

    #
    softEdge_bt = button('soft_edge',label = 'Global soften edge' , command = 'EdgeSoft()' , h=25)
    
    BorderEdges_bt = button('Border_Edges',label = 'BorderEdges' , command = 'BorderEdgesRoutine()' , h=25)
    
    BorderEdges_slider = floatSlider('Border_Edges_slider' , min=3, max=10, value=3, step=0.1 , h=25 , changeCommand = 'BorderWidthChange()')
    
    split_ctlA = separator('split_ctlA' ,  style='doubleDash' , h = 15)

    #  ConversionUnit ctls
    ConversionUnit = optionMenu('Conversion_Unit',label='Conversion Unit :' , cc='ConUnitRoutine()')
    menuItem( label='' )
    menuItem( label='Centimeter To Decimeter' )
    menuItem( label='Decimeter To Centimeter' )
    menuItem( label='' )
    menuItem( label='Centimeter To Foot' )
    menuItem( label='Foot To Centimeter' )
    menuItem( label='' )
    menuItem( label='Decimeter To Foot' )
    menuItem( label='Foot To Decimeter' )
    
    #
    scaleplate_bt = button('scaleplate',label = 'BoundingBox Scaleplate' , command = 'BBScaleplateRoutine()' , h=25)
    setParent('..')

    formLayout('meshesFrom',e=True,
               attachForm=[(tx_meshes,'top',2) , (tx_meshes,'left',5) ,
                           (TSL_meshes,'left',5) , (TSL_meshes,'right',5) ,
                           (lessFour_rab,'left',5) , 
                           (moreFour_rab,'left',5) , 
                           (check_Vertices_bt,'left',5) , (check_Vertices_bt,'right',5) ,
                           (check_polygons_bt,'left',5) , (check_polygons_bt,'right',5) , 
                           (UVwindow_bt,'left',5) , (UVwindow_bt,'right',5) , 
                           (twoSideLight_bt,'left',5) , (twoSideLight_bt,'right',5) , 
                           (BorderEdges_bt,'left',5) , (BorderEdges_bt,'right',5) , 
                           (BorderEdges_slider,'left',5) , (BorderEdges_slider,'right',5) , 
                           (softEdge_bt,'left',5) , (softEdge_bt,'right',5) ,
                           (ConversionUnit,'left',5) , (ConversionUnit,'right',5) ,
                           (scaleplate_bt,'left',5) , (scaleplate_bt,'right',5) ,(scaleplate_bt,'bottom',5)
                          ],
           attachControl=[(TSL_meshes,'top',2,tx_meshes) ,
                          (TSL_meshes, 'bottom' , 2 ,lessFour_rab),
                          (lessFour_rab, 'bottom' , 2 ,moreFour_rab),
                          (moreFour_rab, 'bottom' , 2 ,check_Vertices_bt),
                          (check_Vertices_bt, 'bottom' , 2 ,check_polygons_bt),
                          (check_polygons_bt, 'bottom' , 2 ,UVwindow_bt),
                          (UVwindow_bt, 'bottom' , 2 ,twoSideLight_bt),
                          (twoSideLight_bt, 'bottom' , 2 , BorderEdges_bt),
                          (BorderEdges_bt, 'bottom' , 2 ,BorderEdges_slider),
                          (BorderEdges_slider, 'bottom' , 2 ,softEdge_bt),
                          (softEdge_bt, 'bottom' , 2 ,ConversionUnit),
                          (ConversionUnit, 'bottom' , 2 ,scaleplate_bt)
                         ]
               )
    tabChild2 = formLayout('MeshBuilderForm')
    sourceSkinMesh = textField('Skin_Mesh' , fn = 'boldLabelFont' , h = 25 , tx = 'SkinMeshInThis' , enable = False)
    sourceOrigMesh = textField('Source_Mesh' , fn = 'boldLabelFont' , h = 25 , tx = 'OrigMeshInThis' , enable = False)
    sourceSkinNode = textField('Source_Skin_Node' , fn = 'boldLabelFont' , h = 25 , tx = 'SkinNodeInThis' , enable = False)
    targetMeshText = text('target_mesh_text' ,  label='Target mesh name :', align='left' , fn = 'boldLabelFont' , w = 100 , h = 25)
    targetMeshName = textField('Target_mesh_name' , fn = 'boldLabelFont' , h = 25 )
    BSText = text('BSText' ,  label='BlendNode name :', align='left' , fn = 'boldLabelFont' , w = 100 , h = 25)
    BSName = textField('BS_name' , fn = 'boldLabelFont' , h = 25 )  
    source_bt = button(l=u">>>>>>>>>>Get It ! !<<<<<<<<<<",h=25,c='getOrgSkin()')
    copy_bt =   button(l=u">>>>>>>>>>Hit It ! !<<<<<<<<<<",h=25,c='SelPointsCopy()')
    setParent('..')

    formLayout('MeshBuilderForm',edit=True,
               attachForm=[(sourceSkinMesh ,'top',10),(sourceSkinMesh ,'left',10),(sourceSkinMesh ,'right',10),
                           (sourceOrigMesh ,'left',10),(sourceOrigMesh ,'right',10),
                           (sourceSkinNode,'left',10),(sourceSkinNode,'right',10),
                           (targetMeshText,'left',12),
                           (targetMeshName,'right',10),
                           (BSText,'left',12),
                           (BSName,'right',10),
                           (source_bt,'left',10),(source_bt,'right',10),
                           (copy_bt,'left',10),(copy_bt,'right',10)
                          ],
               attachControl=[(sourceOrigMesh,'top',10,sourceSkinMesh),
                              (sourceSkinNode,'top',10,sourceOrigMesh),
                              (targetMeshText,'top',10,sourceSkinNode),
                              (targetMeshName,'top',10,sourceSkinNode),(targetMeshName,'left',10,targetMeshText),
                              (BSText,'top',10,targetMeshName),
                              (BSName,'top',10,targetMeshName),(BSName,'left',10,BSText),
                              (source_bt,'top',10,BSName),
                              (copy_bt,'top',10,source_bt)
                             ]
               )
             
    # child3 Namerform
    tabChild3 = formLayout('Namerform')
    radioCollection()
    hi_radio=radioButton('hi_radio',label=u'hierarchy',align='left',sl=True,onc='hi_radio_c()')
    glb_radio=radioButton('glb_radio',label=u'global',align='left',onc='glb_radio_c()')
    sel_radio=radioButton('sel_radio',label=u'selected',align='left',onc='sel_radio_c()')
    
    sep0=separator('sep0',height=2,style='none',bgc=[0.2,0.2,0.2])
    
    radioCollection()
    perfix_radio=radioButton('perfix_radio',label=u'prefix',align='left',sl=True,onc='perfix_radio_c()')
    replace_radio=radioButton('replace_radio',label=u'replace',align='left',onc='replace_radio_c()')
    postfix_radio=radioButton('postfix_radio',label=u'Suffix',align='left',onc='postfix_radio_c()')
    
    sep4=separator('sep4',height=2,style='none',bgc=[0.2,0.2,0.2])
    
    find_texGrp=textFieldGrp('find_texGrp',label=u'prefix:',text='',cw=(1,50),adjustableColumn=2,vis=True)
    replace_texGrp2=textFieldGrp('replace_texGrp2',label=u'replace:',text='',cw=(1,50),adjustableColumn=2,vis=False)
    
    sep1=separator('sep1',height=7,style='none',bgc=[0.2,0.2,0.2])
    add_but=button('add_but', l=u'add',c='add()',h=25,en=True)
    
    sep2=separator('sep2',height=7,style='none',bgc=[0.2,0.2,0.2])
    remove_but=button('remove_but',l=u'remove',c='remove()',h=25,en=True)
    
    sep3=separator('sep3',height=7,style='none',bgc=[0.2,0.2,0.2])
    replace_but=button('replace_but', l=u'replace',c='replace()',h=25,en=False)
    
    setParent('..')
    formLayout('Namerform',e=True,
               attachForm=[(hi_radio,'top',5),(hi_radio,'left',40),
                           (glb_radio,'top',5),
                           (sel_radio,'top',5),
                           (sep0,'left',5),(sep0,'right',5),
                           (perfix_radio,'left',40),
                           (sep4,'left',5),(sep4,'right',5),
                           (find_texGrp,'left',5),(find_texGrp,'right',5),
                           (replace_texGrp2,'left',5),(replace_texGrp2,'right',5),
                           (sep1,'left',5),(sep1,'right',5),
                           (add_but,'left',5),(add_but,'right',5),
                           (sep2,'left',5),(sep2,'right',5),
                           (remove_but,'left',5),(remove_but,'right',5),
                           (sep3,'left',5),(sep3,'right',5),
                           (replace_but,'left',5),(replace_but,'right',5)
                          ],
               attachControl=[(glb_radio,'left',10,hi_radio),
                              (sel_radio,'left',18,glb_radio),
                              (sep0,'top',1,sel_radio),
                              (perfix_radio,'top',1,sep0),
                              (replace_radio,'top',1,sep0),(replace_radio,'left',32,perfix_radio),
                              (postfix_radio,'top',1,sep0),(postfix_radio,'left',10,replace_radio),
                              (sep4,'top',3,postfix_radio),
                              (find_texGrp,'top',3,sep4),
                              (replace_texGrp2,'top',3,find_texGrp),
                              (sep1,'top',5,replace_texGrp2),
                              (add_but,'top',3,sep1),
                              (sep2,'top',5,add_but),
                              (remove_but,'top',3,sep2),
                              (sep3,'top',5,remove_but),
                              (replace_but,'top',3,sep3)
                             ]
               )    
                    
               
    tabLayout( 'checkMeshTab' , edit = True, tabLabel = ((tabChild1 , 'checkMeshTool'), 
                                                         (tabChild2 , 'TMeshBuilder'), 
                                                         (tabChild3 , 'NamerTool')
                                                        )
             )
    window('Base_MRUI',e=True,wh=(316,560))
    showWindow('Base_MRUI')
    

def lessFour_rab_c():
    radioButton('lessFour',e=True,sl = True)
    radioButton('moreFour',e=True,sl = False)


def moreFour_rab_c():
    radioButton('lessFour',e=True,sl = False)
    radioButton('moreFour',e=True,sl = True)

def checkMeshes():
    try:
        select(ls( type = 'mesh'))
    except:
        confirmDialog(t='Error',m='This scene have no mesh !')
        raise Exception('This scene have no mesh !')
    mel.eval('ConvertSelectionToFaces')
    mel.eval('ConvertSelectionToVertices')
    mel.eval('InvertSelection')
    allList = ls(sl = True , ap = True)

    if allList == [] or allList == None:
        textScrollList('meshesList',e = True, ra = True)
        menuItem('correctMeshMenuItem' , e = True , c = '()' , l = '')
    else:
        meshes = set()
        for i in allList:
            meshes.add(i.split('.')[0])
        meshes = [i for i in meshes]
        textScrollList('meshesList',e = True, ra = True)
        textScrollList('meshesList',e = True, a = meshes)
        
        menuItem('correctMeshMenuItem' ,e = True , c='correctMesh()' , l = 'correctMesh!')
    
    
def correctMesh():
    meshes = textScrollList('meshesList',q = True, ai = True)
    textScrollList('meshesList',q = True, ra = True)
        
    for obj in meshes:
        polyMergeVertex('%s.vtx[0]' % obj , d = 0.0001 , am = True , ch = False)
        
    select(meshes)
    textScrollList('meshesList',e = True, ra = True)
    menuItem('correctMeshMenuItem' , e = True , c = '()' , l = '')

def checkPolygons():
    menuItem('correctMeshMenuItem' , e = True , c = '()' , l = '')
    
    lessFour = radioButton('lessFour',q = True , sl = True)
    try:
        select(ls( type = 'mesh'))
    except:
        confirmDialog(t='Error',m='This scene have no mesh !')
        raise Exception('This scene have no mesh !')
        
    meshes = ls ( sl = True )
    faces = []
    for m in meshes:
        obj = listRelatives(m , p = True)[0]
        select( m )
        Path = om.MDagPath()
        node = om.MObject()
        list = om.MSelectionList()
        om.MGlobal.getActiveSelectionList( list )

        polygonCounts = om.MIntArray()
        polygonConnects = om.MIntArray()
        
        for i in range(list.length()) :
            list.getDagPath( i , Path , node )
            mesh = om.MFnMesh( Path )
            mesh.getVertices(polygonCounts , polygonConnects)
        if lessFour == 1:
            bufferList = ['%s.f[%d]' % (obj , i) for i in range(len(polygonCounts)) if polygonCounts[i] < 4 ]
        else:
            bufferList = ['%s.f[%d]' % (obj , i) for i in range(len(polygonCounts)) if polygonCounts[i] > 4 ]
        faces += bufferList
    
    if faces == [] or faces == None:
        textScrollList('meshesList',e = True, ra = True)
        select(cl = True)
        
    else:
        select(faces)
        print faces
        meshes = set()
        for i in faces:
            meshes.add(i.split('.')[0])
        meshes = [i for i in meshes]
        
        textScrollList('meshesList',e = True, ra = True)
        textScrollList('meshesList',e = True, a = meshes)



def UVtextureUI():
    texWinName = getPanel(sty='polyTexturePlacementPanel')[0]
    textureWindow(texWinName ,  e = True ,  displaySolidMap = True)
    scriptedPanel(texWinName ,  e = True ,  tearOff = True)

def twoSideLight():
    currentPanel = getPanel(wf=True)
    try:
        light = modelEditor( currentPanel ,  q = True , twoSidedLighting= True)
    except:
        confirmDialog(t='Panel Error',m='Pleas select a "modelPanel" !')
        raise Exception('Pleas select a "modelPanel" !')
    if light:
        modelEditor( currentPanel ,  e = True , twoSidedLighting= False)
        light = 'TwoSide : (OFF)'
        button('two_Side_Light', e = True , label = light )
    else:
        modelEditor( currentPanel ,  e = True , twoSidedLighting= True)
        light = 'TwoSide : (ON)'
        button('two_Side_Light', e = True , label = light )

def BorderEdgesRoutine():
    obj = ls(sl = True)
    if obj != [] and obj != None:
        for o in obj:
            mesh = listRelatives(o , s = True , f = True)[0]
            borderDis = getAttr('%s.displayBorders' % mesh)
            if borderDis:
                setAttr('%s.displayBorders' % mesh , False)
            else:
                setAttr('%s.displayBorders' % mesh , True)
                
def BorderWidthChange():
    obj = ls(sl = True)
    if obj != [] and obj != None:
        for o in obj:
            widthV = floatSlider('Border_Edges_slider' , q = True , v = True)
            mesh = listRelatives(o , s = True , f = True)[0]
            setAttr('%s.borderWidth' % mesh , widthV)



def EdgeSoft():
    try:
        select(ls( type = 'mesh'))
    except:
        confirmDialog(t='Error',m='This scene have no mesh !')
        raise Exception('This scene have no mesh !')
    edgeType = button('soft_edge',q = True ,label = True)
    if edgeType == 'Global soften edge':
        meshes = ls ( sl = True )
        for m in meshes:
            m = listRelatives(m , p = True)[0]
            polySoftEdge(m, a = 180 ,ch = False)
        select(cl = True)
        button('soft_edge', e = True ,label = 'Global harden edge')
    else:
        meshes = ls ( sl = True )
        for m in meshes:
            m = listRelatives(m , p = True)[0]
            polySoftEdge(m, a = 0 ,ch = False)
        select(cl = True)
        button('soft_edge', e = True ,label = 'Global soften edge')

def ConUnitRoutine():
    Conversion_type = optionMenu('Conversion_Unit' , q = True , v = True)
    Objs = ls(sl = True)
    if Objs == [] or Objs == None:
        raise Exception('Please select some transform!')
    if Conversion_type == 'Centimeter To Decimeter':
        ConUnit(Objs , 0.1)
    elif Conversion_type == 'Centimeter To Foot':
        ConUnit(Objs , 0.032808)
    elif Conversion_type == 'Decimeter To Centimeter':
        ConUnit(Objs , 10)
    elif Conversion_type == 'Foot To Centimeter':
        ConUnit(Objs , 30.48)
    elif Conversion_type == 'Decimeter To Foot':
        ConUnit(Objs , 0.32808)
    elif Conversion_type == 'Foot To Decimeter':
        ConUnit(Objs , 3.048)
        

def ConUnit(Objs , arg = 1):
    for o in Objs:
        sxV = getAttr('%s.sx' % o) * arg
        syV = getAttr('%s.sy' % o) * arg
        szV = getAttr('%s.sz' % o) * arg
        setAttr('%s.sx' % o , sxV)
        setAttr('%s.sy' % o , syV)
        setAttr('%s.sz' % o , szV)
        makeIdentity(o , apply = True , t = False , r = False , s = True , n = 0)

        
        
def BBScaleplateRoutine():
    objs = ls(sl = True , ap = True)
    if objs == [] or objs ==None:
        confirmDialog(t='Error',m='Please select some object with mesh!')
        raise Exception('Please select some object with mesh!')
        
    if objExists('ScaleplateGrp') == False:
        group(n = 'ScaleplateGrp' , em = True)
 
    for o in objs:
        childs = listRelatives('ScaleplateGrp' , c = True , f = True)
        try:
            select(childs)
            childs = ls('%s*' % o, sl = True )
        except:
            pass
        if childs != [] and childs != None:
            delete(childs)
            continue
            
        #Max location connection
        MaxLoc = parentLoc('ScaleplateLocMax' , o)
        connectAttr('%s.boundingBox.boundingBoxMax' % o , '%s.translate' % MaxLoc , f = True)
        MaxLoc = xform(MaxLoc , q = True , t = True , ws = True)
        
        #MinX location connection
        MinXLoc = parentLoc('ScaleplateLocMinX' , o)
        connectAttr('%s.boundingBox.boundingBoxMin.boundingBoxMinX' % o , '%s.translate.translateX' % MinXLoc , f = True)
        connectAttr('%s.boundingBox.boundingBoxMax.boundingBoxMaxY' % o , '%s.translate.translateY' % MinXLoc , f = True)
        connectAttr('%s.boundingBox.boundingBoxMax.boundingBoxMaxZ' % o , '%s.translate.translateZ' % MinXLoc , f = True)
        MinXLoc = xform(MinXLoc , q = True , t = True , ws = True)
        
        #MinY location connection
        MinYLoc = parentLoc('ScaleplateLocMinY' , o)
        connectAttr('%s.boundingBox.boundingBoxMax.boundingBoxMaxX' % o , '%s.translate.translateX' % MinYLoc , f = True)
        connectAttr('%s.boundingBox.boundingBoxMin.boundingBoxMinY' % o , '%s.translate.translateY' % MinYLoc , f = True)
        connectAttr('%s.boundingBox.boundingBoxMax.boundingBoxMaxZ' % o , '%s.translate.translateZ' % MinYLoc , f = True)
        MinYLoc = xform(MinYLoc , q = True , t = True , ws = True)
        
        #MinZ location connection
        MinZLoc = parentLoc('ScaleplateLocMinZ' , o)
        connectAttr('%s.boundingBox.boundingBoxMax.boundingBoxMaxX' % o , '%s.translate.translateX' % MinZLoc , f = True)
        connectAttr('%s.boundingBox.boundingBoxMax.boundingBoxMaxY' % o , '%s.translate.translateY' % MinZLoc , f = True)
        connectAttr('%s.boundingBox.boundingBoxMin.boundingBoxMinZ' % o , '%s.translate.translateZ' % MinZLoc , f = True)
        MinZLoc = xform(MinZLoc , q = True , t = True , ws = True)
        
        
        XScaleplate = distanceDimension(sp = MaxLoc , ep = MinXLoc)
        setAttr('%s.template' % XScaleplate, 1)
        XScaleplate = listRelatives(XScaleplate , p = True)[0]
        XScaleplate = rename(XScaleplate , '%s_XScaleplate' % o)
        parent(XScaleplate , 'ScaleplateGrp')
        
        YScaleplate = distanceDimension(sp = MaxLoc , ep = MinYLoc)
        setAttr('%s.template' % YScaleplate, 1)
        YScaleplate = listRelatives(YScaleplate , p = True)[0]
        YScaleplate = rename(YScaleplate , '%s_YScaleplate' % o)
        parent(YScaleplate , 'ScaleplateGrp')
        
        ZScaleplate = distanceDimension(sp = MaxLoc , ep = MinZLoc)
        setAttr('%s.template' % ZScaleplate, 1)
        ZScaleplate = listRelatives(ZScaleplate , p = True)[0]
        ZScaleplate = rename(ZScaleplate , '%s_ZScaleplate' % o)
        parent(ZScaleplate , 'ScaleplateGrp')
    childs = listRelatives('ScaleplateGrp' , c = True , f = True) 
    if childs == None or childs == []:
        delete('ScaleplateGrp')
    select(objs)

def parentLoc(locName , o):
    locaterName = spaceLocator(n = '%s_%s' % (o , locName))[0]
    parent(locaterName , 'ScaleplateGrp')
    setAttr('%s_%s.v' % (o , locName) , 0)
    return locaterName
    
    
##################################################################
########################  Mesh Builder ###########################
##################################################################

    
    
def getOrgSkin():
    selObjs = ls(sl = True , ap = True)
    selObjShape = listRelatives(selObjs , s = True)[0]

    if selObjs==[] or selObjs==None:
        confirmDialog(t='Selection Error',m='Select a skinned mesh !')
        raise Exception('Selection Error')

    if nodeType(selObjs[0])!='transform':
        confirmDialog(t='Selection Error',m='Select a skinned mesh !')
        raise Exception('Selection Error')
        
    allHis = listHistory(selObjs[0])
    meshes = ls(allHis , type = 'mesh')
    for h in meshes:
        if h.find("Orig")>=0 and selObjShape in h:
            textField('Source_Mesh' , e = True , tx = h)
            if objExists('%s.scuplt' % h) == False:
                select(h)
                addAttr(ln = 'scuplt')
                setAttr("%s.scuplt" % h , e = False , keyable = True)
                
                scupltObj = duplicate(selObjs[0] , n = '%s_scupltObj' % selObjs[0])[0]
                scupltShape = listRelatives(scupltObj , c = True)
                delete(scupltShape[1])
            
                setAttr("%s.tx" % scupltObj,lock=False,keyable=True)
                setAttr("%s.ty" % scupltObj,lock=False,keyable=True)
                setAttr("%s.tz" % scupltObj,lock=False,keyable=True)
                setAttr("%s.rx" % scupltObj,lock=False,keyable=True)
                setAttr("%s.ry" % scupltObj,lock=False,keyable=True)
                setAttr("%s.rz" % scupltObj,lock=False,keyable=True)
                setAttr("%s.sx" % scupltObj,lock=False,keyable=True)
                setAttr("%s.sy" % scupltObj,lock=False,keyable=True)
                setAttr("%s.sz" % scupltObj,lock=False,keyable=True)

                
                select(scupltObj)
                addAttr(ln = 'scuplt')
                setAttr("%s.scuplt" % scupltObj , e = False , keyable = True)
                connectAttr('%s.scuplt' % h,'%s.scuplt' % scupltObj)
                #try:
                #    parent(scupltObj , w = True)
                #except:
                #    pass
                setAttr("%s.rx" % scupltObj,lock=True)
                setAttr("%s.ry" % scupltObj,lock=True)
                setAttr("%s.rz" % scupltObj,lock=True)
                setAttr("%s.sx" % scupltObj,lock=True)
                setAttr("%s.sy" % scupltObj,lock=True)
                setAttr("%s.sz" % scupltObj,lock=True)
                
                
                
            else:
                scupltObj = listConnections( '%s.scuplt' % h)
                if scupltObj == [] or scupltObj == None:
                    scupltObj = duplicate(selObjs[0] , n = '%s_scupltObj' % selObjs[0])[0]
                    scupltShape = listRelatives(scupltObj , c = True)
                    delete(scupltShape[1])

                    setAttr("%s.tx" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.ty" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.tz" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.rx" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.ry" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.rz" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.sx" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.sy" % scupltObj,lock=False,keyable=True)
                    setAttr("%s.sz" % scupltObj,lock=False,keyable=True)

                
                    addAttr(ln = 'scuplt')
                    setAttr("%s.scuplt" % scupltObj , e = False , keyable = True)
                    connectAttr('%s.scuplt' % h,'%s.scuplt' % scupltObj)
                    #try:
                    #    parent(scupltObj , w = True)
                    #except:
                    #    pass
                try:
                    setAttr("%s.rx" % scupltObj,lock=True)
                    setAttr("%s.ry" % scupltObj,lock=True)
                    setAttr("%s.rz" % scupltObj,lock=True)
                    setAttr("%s.sx" % scupltObj,lock=True)
                    setAttr("%s.sy" % scupltObj,lock=True)
                    setAttr("%s.sz" % scupltObj,lock=True)
                except:
                    pass
                        
    skinNodes = ls(allHis, type = 'skinCluster')
    if skinNodes==[] or skinNodes==None:
        confirmDialog(t='Selection Error',m='Select a skinned mesh !')
        raise Exception('Selection Error')   
    textField('Source_Skin_Node' , e = True , tx = skinNodes[0])
    textField('Skin_Mesh' , e = True , tx = selObjs[0])
    
def SelPointsCopy():
    targetObjName = textField('Target_mesh_name' , q = True , tx = True)
    BSName = textField('BS_name' , q = True , tx = True)
    skinObj = textField('Skin_Mesh' , q = True , tx = True)
    origMesh = textField('Source_Mesh' , q = True , tx = True)
    
    scupltObj = listConnections( '%s.scuplt' % origMesh)[0]
    
    parentTransform = listRelatives(skinObj, p = True)
    
    skinNode = textField('Source_Skin_Node' , q = True , tx = True)
    mel.eval('ConvertSelectionToVertices')
    Tpoints = ls(sl = True , fl = True)
    
    if Tpoints==[] or Tpoints==None:
        confirmDialog(t='Selection Error',m='Select some points in scupltObj !')
        raise Exception('Selection Error')
    
    TObj = Tpoints[0].split('.')[0]
    if TObj != scupltObj:
        confirmDialog(t='Selection Error',m='Select some points in scupltObj !')
        raise Exception('Selection Error')
        
    if targetObjName == '' or targetObjName == None:
        targetObjName = 'FinalObj'
    else:
        Time = currentTime(q = True)
        targetObjName += ('_Frm_' + `int(Time)`)
        
        
    finalObj = polyCube( n = targetObjName , ch = False , h = 1 , w = 1 , d = 1)[0]

    finalObjShape = listRelatives(finalObj , s = True)[0]
    connectAttr('%s.outMesh' % origMesh,'%s.inMesh' % finalObjShape)
    if parentTransform != None or parentTransform != '':
        parent(finalObj, scupltObj, parentTransform)
    
    scupltObjPos = xform(scupltObj , q = True , t = True , ws = True)
    
    scupltObjMatrix = getAttr('%s.worldInverseMatrix' % scupltObj)
    scupltObjMMatrix = om.MMatrix()
    om.MScriptUtil().createMatrixFromList(scupltObjMatrix , scupltObjMMatrix)

    matrixIndices = getAttr('%s.matrix' % skinNode , multiIndices = True)

    progressWindow( title='waiting~~', progress = 0, status='0%' , isInterruptable = True )
    amount = 100/len(Tpoints)
    amountBuffer = 0
    for p in Tpoints:
    
        if progressWindow( query=True, isCancelled=True ) :
            break
        amountBuffer += amount
        
        tp = xform(p , q = True , t = True , ws = True)
        op = xform(p.replace(scupltObj , skinObj) , q = True , t = True , ws = True)
        
        TVecx = tp[0] - scupltObjPos[0] - op[0]
        TVecy = tp[1] - scupltObjPos[1] - op[1]
        TVecz = tp[2] - scupltObjPos[2] - op[2]
        
        if math.fabs(TVecx)  < 0.0001 and math.fabs(TVecy) < 0.0001 and math.fabs(TVecz) < 0.0001:
            continue
            
        MTVec = om.MVector(TVecx , TVecy , TVecz)
        weightList = skinPercent(skinNode , p.replace(scupltObj , skinObj) , q = True , v = True)
        tishMatrix = om.MMatrix()
        om.MScriptUtil().createMatrixFromList([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] , tishMatrix)
        
        amountW = amount/len(weightList)
        amountBuffer -=amount
        progressWindow( edit=True, progress = amountBuffer , status=('') )
        
        for w in range(len(weightList)):
        
            amountBuffer += amountW
            progressWindow( edit=True, progress = amountBuffer , status=('') )
            
            if weightList[w] < 0.001:
                continue
            bindPreMatrix=getAttr('%s.bindPreMatrix[%d]' % (skinNode , matrixIndices[w]))
            drivingMatrix=getAttr('%s.matrix[%d]' % (skinNode , matrixIndices[w]))
            bindPreMMatrix=om.MMatrix()
            drivingMMatrix=om.MMatrix()
            
            om.MScriptUtil().createMatrixFromList(bindPreMatrix , bindPreMMatrix)
            om.MScriptUtil().createMatrixFromList(drivingMatrix , drivingMMatrix)
            
            bufferMatirx = bindPreMMatrix * drivingMMatrix  * weightList[w]
            
            tishMatrix += bufferMatirx
            
        tishMatrix = tishMatrix.inverse()
        
        #MTVec *= tishMatrix
        MTVec *= (tishMatrix * scupltObjMMatrix)
        
        move ( MTVec.x , MTVec.y, MTVec.z, p.replace(scupltObj , finalObj) , r = True)
        
    progressWindow(endProgress = True)
    
    parent(finalObj, scupltObj, w = True)
    
    if BSName != '' and BSName != None:
        NumCount = blendShape(BSName , q = True , weightCount = True)
        
        blendShape(BSName , e = True , t=(skinObj , NumCount , targetObjName , 1.0))

##################################################################
########################    Namer    #############################
##################################################################


def hi_radio_c():
    radioButton('hi_radio',e=True,sl=True)
    radioButton('glb_radio',e=True,sl=False)
    radioButton('sel_radio',e=True,sl=False)


def glb_radio_c():
    radioButton('hi_radio',e=True,sl=False)
    radioButton('glb_radio',e=True,sl=True)
    radioButton('sel_radio',e=True,sl=False)

def sel_radio_c():
    radioButton('hi_radio',e=True,sl=False)
    radioButton('glb_radio',e=True,sl=False)
    radioButton('sel_radio',e=True,sl=True)
    




def perfix_radio_c():
    radioButton('perfix_radio',e=True,sl=True)
    radioButton('replace_radio',e=True,sl=False)
    radioButton('postfix_radio',e=True,sl=False)
    textFieldGrp('find_texGrp',e=True,label=u'prefix:')
    textFieldGrp('replace_texGrp2',e=True,label=u'replace:',vis=False)
    button('add_but',e=True,en=True)
    button('remove_but',e=True,en=True)
    button('replace_but',e=True,en=False)

def replace_radio_c():
    radioButton('perfix_radio',e=True,sl=False)
    radioButton('replace_radio',e=True,sl=True)
    radioButton('postfix_radio',e=True,sl=False)
    textFieldGrp('find_texGrp',e=True,label=u'find:',vis=True)
    textFieldGrp('replace_texGrp2',e=True,label=u'replace:',vis=True)
    button('add_but',e=True,en=False)
    button('remove_but',e=True,en=False)
    button('replace_but',e=True,en=True)

def postfix_radio_c():
    perfix_radio=radioButton('perfix_radio',e=True,sl=False)
    replace_radio=radioButton('replace_radio',e=True,sl=False)
    postfix_radio=radioButton('postfix_radio',e=True,sl=True)
    textFieldGrp('find_texGrp',e=True,label=u'suffix:')
    textFieldGrp('replace_texGrp2',e=True,label=u'replace:',vis=False)
    button('add_but',e=True,en=True)
    button('remove_but',e=True,en=True)
    button('replace_but',e=True,en=False)
    

def optimizeword(eachNode):
    if '|' in eachNode:
        buffer=eachNode.split('|')
        eachNode=buffer[len(buffer)-1]
    return eachNode

def add():
    
    condition1_1=radioButton('hi_radio',q=True,sl=True)
    condition1_2=radioButton('glb_radio',q=True,sl=True)
    condition1_3=radioButton('sel_radio',q=True,sl=True)
    
    condition2_1=radioButton('perfix_radio',q=True,sl=True)
    condition2_2=radioButton('replace_radio',q=True,sl=True)
    condition2_3=radioButton('postfix_radio',q=True,sl=True)
    
    prefixText=textFieldGrp('find_texGrp',q=True,tx=True)
    
    #
    if condition1_1==1 and condition1_2==0 and condition1_3==0:
        select(hi=True)
        allNodes=ls(sl=True)
        allNodes.reverse()
        #
        if condition2_1==1 and condition2_2==0 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,(prefixText+ReachNode))
                except:
                    pass
        #
        if condition2_1==0 and condition2_2==0 and condition2_3==1:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,(ReachNode+prefixText))
                except:
                    pass
    # 
    elif condition1_1==0 and condition1_2==1 and condition1_3==0:
        allNodes=ls(dep=True)
        allNodes.reverse()
        #
        if condition2_1==1 and condition2_2==0 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,(prefixText+ReachNode))
                except:
                    pass
        #
        if condition2_1==0 and condition2_2==0 and condition2_3==1:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,(ReachNode+prefixText))
                except:
                    pass
    #
    elif condition1_1==0 and condition1_2==0 and condition1_3==1:
        allNodes=ls(sl=True)
        allNodes.reverse()
        #
        if condition2_1==1 and condition2_2==0 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,(prefixText+ReachNode))
                except:
                    pass
        #
        if condition2_1==0 and condition2_2==0 and condition2_3==1:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,(ReachNode+prefixText))
                except:
                    pass
                


def remove():
    
    condition1_1=radioButton('hi_radio',q=True,sl=True)
    condition1_2=radioButton('glb_radio',q=True,sl=True)
    condition1_3=radioButton('sel_radio',q=True,sl=True)
    
    condition2_1=radioButton('perfix_radio',q=True,sl=True)
    condition2_2=radioButton('replace_radio',q=True,sl=True)
    condition2_3=radioButton('postfix_radio',q=True,sl=True)
    
    prefixText=textFieldGrp('find_texGrp',q=True,tx=True)
    
    #
    if condition1_1==1 and condition1_2==0 and condition1_3==0:
        select(hi=True)
        allNodes=ls(sl=True)
        allNodes.reverse()
        #
        if condition2_1==1 and condition2_2==0 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.partition(prefixText)[2])
                except:
                    pass
        #
        if condition2_1==0 and condition2_2==0 and condition2_3==1:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.partition(prefixText)[0] + ReachNode.partition(prefixText)[2])
                except:
                    pass
    #
    elif condition1_1==0 and condition1_2==1 and condition1_3==0:
        allNodes=ls(dep=True)
        allNodes.reverse()
        #
        if condition2_1==1 and condition2_2==0 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.partition(prefixText)[2])
                except:
                    pass
        #
        if condition2_1==0 and condition2_2==0 and condition2_3==1:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.partition(prefixText)[0] + ReachNode.partition(prefixText)[2])
                except:
                    pass
    #
    elif condition1_1==0 and condition1_2==0 and condition1_3==1:
        allNodes=ls(sl=True)
        allNodes.reverse()
        #
        if condition2_1==1 and condition2_2==0 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.partition(prefixText)[2])
                except:
                    pass
        #
        if condition2_1==0 and condition2_2==0 and condition2_3==1:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.partition(prefixText)[0] + ReachNode.partition(prefixText)[2])
                except:
                    pass

def replace():
    condition1_1=radioButton('hi_radio',q=True,sl=True)
    condition1_2=radioButton('glb_radio',q=True,sl=True)
    condition1_3=radioButton('sel_radio',q=True,sl=True)
    
    condition2_1=radioButton('perfix_radio',q=True,sl=True)
    condition2_2=radioButton('replace_radio',q=True,sl=True)
    condition2_3=radioButton('postfix_radio',q=True,sl=True)
    
    searchStr=textFieldGrp('find_texGrp',q=True,tx=True)
    replaceStr=textFieldGrp('replace_texGrp2',q=True,tx=True)
    
    
    #
    if condition1_1==1 and condition1_2==0 and condition1_3==0:
        select(hi=True)
        allNodes=ls(sl=True)
        allNodes.reverse()
        #
        if condition2_1==0 and condition2_2==1 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.replace(searchStr,replaceStr))
                except:
                    pass

    #
    elif condition1_1==0 and condition1_2==1 and condition1_3==0:
        allNodes=ls(dep=True)
        allNodes.reverse()
        #
        if condition2_1==0 and condition2_2==1 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.replace(searchStr,replaceStr))
                except:
                    pass

    #
    elif condition1_1==0 and condition1_2==0 and condition1_3==1:
        allNodes=ls(sl=True)
        allNodes.reverse()
        #
        if condition2_1==0 and condition2_2==1 and condition2_3==0:
            for eachNode in allNodes:
                try:
                    ReachNode=optimizeword(eachNode)
                    rename(eachNode,ReachNode.replace(searchStr,replaceStr))
                except:
                    pass

BMRTools()
