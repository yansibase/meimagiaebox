import os
import sys
import unittest
import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

if os.path.isfile(     os.path.dirname(os.path.abspath(__file__)) + '/../../scripts/Base_checkMeshTool.py'):
    sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../../scripts/');
elif os.path.isfile(   '/ibrix3/PLE/studio/app/maya/scripts/Py_Script/Base_checkMeshTool.py'):
    sys.path.insert(0, '/ibrix3/PLE/studio/app/maya/scripts/Py_Script');
elif os.path.isfile(   'Y:/PLE/studio/app/maya/scripts/Py_Script/Base_checkMeshTool.py'):
    sys.path.insert(0, 'Y:/PLE/studio/app/maya/scripts/Py_Script');

import Base_checkMeshTool

#-------------------------------------------------------------------------------
class MyTest00(unittest.TestCase):
    m_id           = ''
    m_helper       = None
    m_driven_li    = ''
    m_sculpt_li    = ''
    m_driver_li    = ''
    m_driven_dq    = ''
    m_sculpt_dq    = ''
    m_driver_dq    = ''

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('skinClusterDq')
        self.m_driven_li = '|linear|pPlane1'
        self.m_driven_dq = '|dual_quaternion|pPlane1'

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)

    def assertAlmostEqual_list(self, list0=[], list1=[], msg=None, delta=None):
        self.assertEqual(len(list0), len(list1))

        for i in range(len(list0)):
            self.assertAlmostEqual_tuple(list0[i], list1[i], msg=msg, delta=delta)

    def assertAlmostEqual_tuple(self, tuple0=(0,0,0), tuple1=(0,0,0), msg=None, delta=None):
        self.assertAlmostEqual(tuple0[0], tuple1[0], msg=msg, delta=delta)
        self.assertAlmostEqual(tuple0[1], tuple1[1], msg=msg, delta=delta)
        self.assertAlmostEqual(tuple0[2], tuple1[2], msg=msg, delta=delta)

    def test00(self):
        '''
        import maya.cmds
        maya.cmds.loadPlugin("automation")

        import unittest
        suite = unittest.defaultTestLoader.discover(start_dir="/backup/lhome/dev/mybox/checkMesh/test/test_0001")
        unittest.TextTestRunner(verbosity=3).run(suite)
        '''
        log.debug('---- begin %s.test00() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        #log.debug('hello world 2');

        Base_checkMeshTool.BMRTools()             # show window

        # ---------------- linear mesh --------------------------
        cmds.select(self.m_driven_li, replace=True)
        Base_checkMeshTool.getOrgSkin('') # press 'Get It' button
        self.m_sculpt_li = 'linear_pPlane1_scupltObj'
        cmds.move(-1.5+3, 5, 0, self.m_sculpt_li, r=True) # move it

        # sculpt mesh
        cmds.move(0, 0, -2, self.m_sculpt_li+'.vtx[38:39]', r=True)

        cmds.select(self.m_sculpt_li, replace=True)
        Base_checkMeshTool._SelPointsCopy(1) # press 'Hit It' button
        self.m_driver_li = 'FinalObj'
        cmds.move(0, 9+3, 0, self.m_driver_li, r=True) # move it

        # check
        #driver_pos_os_li = cmds.getAttr(self.m_driver_li+'.vtx[38:39]') # mesh's vertex position in object space
        #excepted_pos_li  = [(0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (-1.0, 0.0, 0.0), (0.0, 0.0, 0.0)]
        #self.assertAlmostEqual_list(driver_pos_os_li, excepted_pos_li, delta = 0.002)

        # add blendshape
        cmds.select(self.m_driver_li, replace=True)
        cmds.select(self.m_driven_li, tgl=True)
        bsNode_li = cmds.blendShape(frontOfChain=True)

        # set blend weight to 1.0
        cmds.setAttr(bsNode_li[0]+'.'+self.m_driver_li, 1.0)
        driven_pos_ws_li = cmds.getAttr(self.m_driven_li+'.vrts[38:39]')# world space
        #excepted_pos_li  = [(0.5, 0, 2.0), (1.5, 0, 2.0), (0.5, 0, 0), (1.5, 0, 0), (-0.5, 1.0, -1.73), (1.5, 1.0, -1.73)]
        #self.assertAlmostEqual_list(driven_pos_ws_li, excepted_pos_li, delta = 0.015)

        # ---------------- dual quaternion mesh --------------------------
        cmds.select(self.m_driven_dq, replace=True)
        Base_checkMeshTool.getOrgSkin('') # press 'Get It' button
        self.m_sculpt_dq = 'dual_quaternion_pPlane1_scupltObj'
        cmds.move(-1.5+3, 5, 0, self.m_sculpt_dq, r=True) # move it

        # sculpt mesh
        cmds.move(0, 0, -2, self.m_sculpt_dq+'.vtx[38:39]', r=True)

        cmds.select(self.m_sculpt_dq, replace=True)
        Base_checkMeshTool._SelPointsCopy(2) # press 'Hit It' button
        self.m_driver_dq = 'FinalObj1'
        cmds.move(5.0, 9+3, 0, self.m_driver_dq, r=True) # move it

        # check
        #driver_pos_os_dq = cmds.getAttr(self.m_driver_dq+'.vtx[38:39]') # mesh's vertex position in object space
        #excepted_pos_dq  = [(0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (-1.0, 0.0, 0.0), (0.0, 0.0, 0.0)]
        #self.assertAlmostEqual_list(driver_pos_os_dq, excepted_pos_dq, delta = 0.002)

        # add blendshape
        cmds.select(self.m_driver_dq, replace=True)
        cmds.select(self.m_driven_dq, tgl=True)
        bsNode_dq = cmds.blendShape(frontOfChain=True)

        # set blend weight to 1.0
        cmds.setAttr(bsNode_dq[0]+'.'+self.m_driver_dq, 1.0)
        driven_pos_ws_dq = cmds.getAttr(self.m_driven_dq+'.vrts[38:39]')# world space
        #excepted_pos_dq  = [(0.5, 0, 2.0), (1.5, 0, 2.0), (0.5, 0, 0), (1.5, 0, 0), (-0.5, 1.0, -1.73), (1.5, 1.0, -1.73)]

        #self.assertAlmostEqual_list(driven_pos_ws_dq, driven_pos_ws_li, delta = 0.015)

        # -------------------------------------------------------

        #mel.eval('setNamedPanelLayout("Single Perspective View");// updateToolbox();')
        mel.eval('setNamedPanelLayout("Four View"); //updateToolbox();')

        cmds.deleteUI('Base_MRUI', window=True) # close window

        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
