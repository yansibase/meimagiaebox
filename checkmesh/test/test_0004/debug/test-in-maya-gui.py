import os
import sys
import time
import unittest
import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

filepath = ''
if sys.platform == 'win32':
    filepath = 'd:/dev/mybox/checkmesh/test/test_0004'
elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
    filepath = '/backup/lhome/dev/mybox/checkmesh/test/test_0004'


sys.path.insert(0, filepath + '/../../scripts');
import Base_checkMeshTool

#-------------------------------------------------------------------------------

m_id           = ''
m_helper       = None
m_driven_dq    = 'basic_man:body'
m_sculpt_dq    = 'basic_man:body_scupltObj'
m_driver_dq    = 'FinalObj'

# init
m_id       = filepath + '/test_0004'
m_helper   = hp.ATHelper()

#setup
#super(MyTest00, self).setUp()
m_helper.openMayaFile(filepath+'/scenes/test.ma')
hp.ATHelper.tryToLoadPlugin('skinClusterDq')



# test00(self):
'''
import maya.cmds
maya.cmds.loadPlugin("automation")

import unittest
suite = unittest.defaultTestLoader.discover(start_dir="/backup/lhome/dev/mybox/checkMesh/test/test_0001")
unittest.TextTestRunner(verbosity=3).run(suite)
'''
log.debug('---- begin %s.test00() ----', m_id);
#super(MyTest00, self).test_animation()

#log.debug('hello world 2');

Base_checkMeshTool.BMRTools()             # show window

# ----------------  --------------------------
cmds.select(m_driven_dq, replace=True)
Base_checkMeshTool.getOrgSkin('') # press 'Get It' button
cmds.move(3, 0, 0, m_sculpt_dq, r=True) # try to move the generated mesh

# sculpt mesh
# select some vertexs and scale them
mel.eval('select -r basic_man:body_scupltObj.vtx[7876:7881] basic_man:body_scupltObj.vtx[8571:8572] basic_man:body_scupltObj.vtx[8574:8583] basic_man:body_scupltObj.vtx[8586] basic_man:body_scupltObj.vtx[8598:8602] basic_man:body_scupltObj.vtx[9247:9482] basic_man:body_scupltObj.vtx[9791:9956] basic_man:body_scupltObj.vtx[10155:11369] basic_man:body_scupltObj.vtx[11451:11643] basic_man:body_scupltObj.vtx[11646:11647] basic_man:body_scupltObj.vtx[11649:11650] basic_man:body_scupltObj.vtx[11652] basic_man:body_scupltObj.vtx[11654:11679] basic_man:body_scupltObj.vtx[11690:11692] basic_man:body_scupltObj.vtx[11696] basic_man:body_scupltObj.vtx[11698] basic_man:body_scupltObj.vtx[11701:11709] basic_man:body_scupltObj.vtx[11730] basic_man:body_scupltObj.vtx[11750] basic_man:body_scupltObj.vtx[11752:11756] basic_man:body_scupltObj.vtx[11758:11891] basic_man:body_scupltObj.vtx[13499] basic_man:body_scupltObj.vtx[13501] basic_man:body_scupltObj.vtx[13503:13518] basic_man:body_scupltObj.vtx[13520:13573] basic_man:body_scupltObj.vtx[15520:15575] basic_man:body_scupltObj.vtx[15777:15869] basic_man:body_scupltObj.vtx[15877:15968] basic_man:body_scupltObj.vtx[15971:16062] basic_man:body_scupltObj.vtx[16072:16074] basic_man:body_scupltObj.vtx[16076:16197] basic_man:body_scupltObj.vtx[16200:16755] basic_man:body_scupltObj.vtx[16757:17563] basic_man:body_scupltObj.vtx[17576:17761];')
mel.eval('scale -r -p 4.474571cm 5.34027cm 0.0987038cm 1.491506 1.491506 1.491506;')


cmds.select(m_sculpt_dq, replace=True)
log.info('Timer begins')
start = time.time()
Base_checkMeshTool._SelPointsCopy(2) # press 'Hit It' button
end   = time.time()
log.info('Timer ends')
elipsed_time = end - start
log.info('Elipsed time: %d(s)', elipsed_time)


cmds.move(0, 3, 0, m_driver_dq, r=True)# try to move the generated mesh
        
# -------------------------------------------------------

#mel.eval('setNamedPanelLayout("Single Perspective View");// updateToolbox();')
mel.eval('setNamedPanelLayout("Four View"); //updateToolbox();')

cmds.deleteUI('Base_MRUI', window=True) # close window

log.debug('---- end   %s.test00() ----', m_id);

 #----------------------------
renderer = rndr.CreateRenderer('arnold')
renderer.render()

m_helper.moveRenderedImages();

isEqual = m_helper.compare_image()
if isEqual:
    log.info('Passed. ')
else:
    log.info('images not equal')


 
