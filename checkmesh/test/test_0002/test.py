import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

if os.path.isfile(     os.path.dirname(os.path.abspath(__file__)) + '/../../scripts/Base_checkMeshTool.py'):
    sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../../scripts/');
elif os.path.isfile(   '/ibrix3/PLE/studio/app/maya/scripts/Py_Script/Base_checkMeshTool.py'):
    sys.path.insert(0, '/ibrix3/PLE/studio/app/maya/scripts/Py_Script');
elif os.path.isfile(   'Y:/PLE/studio/app/maya/scripts/Py_Script/Base_checkMeshTool.py'):
    sys.path.insert(0, 'Y:/PLE/studio/app/maya/scripts/Py_Script');

import Base_checkMeshTool

#-------------------------------------------------------------------------------
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None
    m_driven    = ''
    m_sculpt    = ''
    m_driver    = ''

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        self.m_driven = 'pPlane1'

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()

    def assertAlmostEqual_list(self, list0=[], list1=[], msg=None, delta=None):
        self.assertEqual(len(list0), len(list1))

        for i in range(len(list0)):
            self.assertAlmostEqual_tuple(list0[i], list1[i], msg=msg, delta=delta)

    def assertAlmostEqual_tuple(self, tuple0=(0,0,0), tuple1=(0,0,0), msg=None, delta=None):
        self.assertAlmostEqual(tuple0[0], tuple1[0], msg=msg, delta=delta)
        self.assertAlmostEqual(tuple0[1], tuple1[1], msg=msg, delta=delta)
        self.assertAlmostEqual(tuple0[2], tuple1[2], msg=msg, delta=delta)

    def test00(self):
        '''
        import maya.cmds
        maya.cmds.loadPlugin("automation")

        import unittest
        suite = unittest.defaultTestLoader.discover(start_dir="/backup/lhome/dev/mybox/Base_checkMeshTool/test/test_0001")
        unittest.TextTestRunner(verbosity=3).run(suite)
        '''
        log.debug('---- begin %s.test00() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        #log.debug('hello world 2');

        Base_checkMeshTool.BMRTools()             # show window

        cmds.select(self.m_driven, replace=True)
        Base_checkMeshTool.getOrgSkin('') # press 'Get It' button
        self.m_sculpt = 'pPlane1_scupltObj'

        # sculpt mesh
        cmds.move(-1, 0, 0, self.m_sculpt+'.vtx[4]', r=True)

        cmds.select(self.m_sculpt, replace=True)
        Base_checkMeshTool.SelPointsCopy('') # press 'Hit It' button
        self.m_driver = 'FinalObj'

        # check
        driver_pos_os = cmds.getAttr(self.m_driver+'.vtx[:]') # mesh's vertex position in object space
        excepted_pos  = [(0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (-1.0, 0.0, 0.0), (0.0, 0.0, 0.0)]
        self.assertAlmostEqual_list(driver_pos_os, excepted_pos, delta = 0.002)
        '''
        self.assertAlmostEqual_tuple(driver_pos_os[0], ( 0.0,  0.0,  0.0))
        self.assertAlmostEqual_tuple(driver_pos_os[1], ( 0.0,  0.0,  0.0))
        self.assertAlmostEqual_tuple(driver_pos_os[2], ( 0.0,  0.0,  0.0))
        self.assertAlmostEqual_tuple(driver_pos_os[3], ( 0.0,  0.0,  0.0))
        self.assertAlmostEqual_tuple(driver_pos_os[4], (-1.0,  0.0,  0.0), delta = 0.002)
        self.assertAlmostEqual_tuple(driver_pos_os[5], ( 0.0,  0.0,  0.0))
        '''

        # add blendshape
        cmds.select(self.m_driver, replace=True)
        cmds.select(self.m_driven, tgl=True)
        bsNode = cmds.blendShape(frontOfChain=True)

        # set blend weight to 1.0
        cmds.setAttr(bsNode[0]+'.'+self.m_driver, 1.0)
        driven_pos_ws = cmds.getAttr(self.m_driven+'.vrts[:]')# world space
        excepted_pos  = [(0.5, 0, 2.0), (1.5, 0, 2.0), (0.5, 0, 0), (1.5, 0, 0), (-0.5, 1.0, -1.73), (1.5, 1.0, -1.73)]
        self.assertAlmostEqual_list(driven_pos_ws, excepted_pos, delta = 0.015)

        # set blend weight to 0.0
        cmds.setAttr(bsNode[0]+'.'+self.m_driver, 0.0)
        driven_pos_ws = cmds.getAttr(self.m_driven+'.vrts[:]')# world space
        excepted_pos  = [(0.5, 0, 2.0), (1.5, 0, 2.0), (0.5, 0, 0), (1.5, 0, 0), (0.5, 1.0, -1.73), (1.5, 1.0, -1.73)]
        self.assertAlmostEqual_list(driven_pos_ws, excepted_pos, delta = 0.015)



        cmds.deleteUI('Base_MRUI', window=True) # close window

        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
