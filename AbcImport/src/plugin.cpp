#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MStatus.h>

#include <common/log.h>
#include "version.h"
#include "AlembicNode.h"
#include "AbcImport.h"
#include "AlembicImportFileTranslator.h"
// These methods load and unload the plugin, registerNode registers the
// new node type with maya

PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('AbcImport')", true, false) );

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );


    CHECK_MSTATUS(plugin.registerCommand("AbcImport",
                                AbcImport::creator,
                                AbcImport::createSyntax));

    CHECK_MSTATUS(plugin.registerNode("AlembicNode",
                                AlembicNode::mMayaNodeId,
                                &AlembicNode::creator,
                                &AlembicNode::initialize));

    CHECK_MSTATUS(plugin.registerFileTranslator("Alembic",
                                NULL,
                                AlembicImportFileTranslator::creator,
                                NULL,
                                NULL,
                                true));

    MGlobal::executeCommandOnIdle("AlembicCreateUI");

    MString info = "AbcImport v";
    info += "1.0";
    info += " using ";
    info += Alembic::AbcCoreAbstract::GetLibraryVersion().c_str();
    MGlobal::displayInfo(info);

//	// import node_templateSetup
//	CHECK_MSTATUS( MGlobal::executePythonCommand("import opensoupSetup", true, false) );
//	CHECK_MSTATUS(plugin.registerUI(
//                    "opensoup_setupUI()",
//                    "opensoup_unsetupUI()",
//                    "opensoup_setup()",
//                    "opensoup_unsetup()"
//                    ));
                    
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('AbcImport')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
    // callback functions
    CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('AbcImport')", true, false) );

	MFnPlugin plugin( obj );

    CHECK_MSTATUS(plugin.deregisterFileTranslator("Alembic"));

    CHECK_MSTATUS(plugin.deregisterNode(AlembicNode::mMayaNodeId));

    CHECK_MSTATUS(plugin.deregisterCommand("AbcImport"));

    MGlobal::executeCommandOnIdle("AlembicDeleteUI");

    // callback functions
    CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('AbcImport')", true, false) );

	return MS::kSuccess;
}

