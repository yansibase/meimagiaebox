set(HDRS
	version.h
	cmd_template.h

	"../../common/node_ids.h"
	"../../common/log.h"
	"../../common/version_helper.h"
)

set(SRCS
	cmd_template.cpp
	plugin.cpp
	
	"../../common/node_ids.cpp"
	"../../common/log.cpp"
	"../../common/version_helper.cpp"
)

set(SCRIPTS
	"../scripts/__init__.py"
	"../scripts/userSetup.mel"
	"../scripts/userSetup.py"
	"../scripts/cmd_templateSetup.py"
)


ADD_LIBRARY( ${MY_PROJECT_NAME}
	SHARED
		${HDRS}
		${SRCS}
		${SCRIPTS}
)

include_directories(
	${MAYA_INCLUDE_DIR}
	"../../"
)

TARGET_LINK_LIBRARIES( ${MY_PROJECT_NAME}
	${MAYA_Foundation_LIBRARY}
	${MAYA_OpenMaya_LIBRARY} 
	${MAYA_OpenMayaAnim_LIBRARY}
	${MAYA_OpenMayaFX_LIBRARY}
	${MAYA_OpenMayaRender_LIBRARY}
	${MAYA_OpenMayaUI_LIBRARY}
	${MAYA_Image_LIBRARY}
	${MAYA_IMFbase_LIBRARY}
)
if(UNIX)
	set(MAYA_EXTENSION ".so")
elseif(WIN32)
	set(MAYA_EXTENSION ".mll")
endif()

if(UNIX)
	set(MAYA_COMPILE_DEFINITIONS
		REQUIRE_IOSTREAM _BOOL LINUX _LINUX LINUX_64)
elseif(WIN32)
	set(MAYA_COMPILE_DEFINITIONS
		REQUIRE_IOSTREAM NT_PLUGIN _BOOL)
endif()

if (CMAKE_COMPILER_IS_GNUCC)
	set(MAYA_COMPILE_FLAGS
		"-m64 -g -pthread -pipe -fPIC -Wno-deprecated -fno-gnu-keywords" )
elseif(MSVC)
	if ("${CMAKE_BUILD_TYPE}" MATCHES "Debug")
		set(MAYA_COMPILE_FLAGS "/MDd /Wall /W3" )
	elseif ("${CMAKE_BUILD_TYPE}" MATCHES "Release")
		set(MAYA_COMPILE_FLAGS "/MD /Wall /W3" )
	endif()
endif()

if (CMAKE_COMPILER_IS_GNUCC)
	set(MAYA_LINK_FLAGS
		"-shared -m64 -g -pthread -pipe -fPIC -Wno-deprecated -fno-gnu-keywords -Wl -Bsymbolic" )
elseif(MSVC)
	set(MAYA_LINK_FLAGS
		" /export:initializePlugin /export:uninitializePlugin " )
endif()


if (CMAKE_COMPILER_IS_GNUCC)
	SET_TARGET_PROPERTIES( ${MY_PROJECT_NAME}
		PROPERTIES
			COMPILE_DEFINITIONS		"${MAYA_COMPILE_DEFINITIONS}"
			COMPILE_FLAGS			"${MAYA_COMPILE_FLAGS}"
			LINK_FLAGS			"${MAYA_LINK_FLAGS}"
			PREFIX				""
			SUFFIX				${MAYA_EXTENSION}

			OUTPUT_NAME			${MY_PROJECT_NAME}

#			LIBRARY_OUTPUT_DIRECTORY	${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
	#		RUNTIME_OUTPUT_DIRECTORY	${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
	#		EXECUTABLE_OUTPUT_PATH		${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
	)
elseif(MSVC)
	SET_TARGET_PROPERTIES( ${MY_PROJECT_NAME}
		PROPERTIES
			COMPILE_DEFINITIONS		"${MAYA_COMPILE_DEFINITIONS}"
			COMPILE_FLAGS			"${MAYA_COMPILE_FLAGS}"
			LINK_FLAGS			"${MAYA_LINK_FLAGS}"
			PREFIX				""
			SUFFIX				${MAYA_EXTENSION}

			OUTPUT_NAME			${MY_PROJECT_NAME}

	#		LIBRARY_OUTPUT_DIRECTORY		${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
	#		LIBRARY_OUTPUT_DIRECTORY_DEBUG		${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
	#		LIBRARY_OUTPUT_DIRECTORY_RELEASE	${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins

#			RUNTIME_OUTPUT_DIRECTORY		${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins  # .mll .pdb .ilk
#			RUNTIME_OUTPUT_DIRECTORY_DEBUG		${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
#			RUNTIME_OUTPUT_DIRECTORY_RELEASE	${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins

	#		EXECUTABLE_OUTPUT_PATH			${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
	#		EXECUTABLE_OUTPUT_PATH_DEBUG		${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
	#		EXECUTABLE_OUTPUT_PATH_RELEASE		${CMAKE_CURRENT_SOURCE_DIR}/../plug-ins
)
endif()

# Install
install(TARGETS   ${MY_PROJECT_NAME}    DESTINATION ${CMAKE_INSTALL_PREFIX}/plug-ins )

