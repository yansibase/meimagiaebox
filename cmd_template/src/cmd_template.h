
#pragma once

#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <common/IncludeMFnPluginClass.h>

class CmdTemplate : public MPxCommand
{
public:
    CmdTemplate() {};
    ~CmdTemplate() {};

    virtual MStatus doIt( const MArgList &args );
    virtual MStatus undoIt();
    virtual MStatus redoIt();

    virtual bool isUndoable() const;

    virtual bool hasSyntax() const;

    bool isHistoryOn() const;



    static MString name();
    static MSyntax mySyntax();

    static void* creator() {return new CmdTemplate();};

    static MStatus registerCmd(MFnPlugin &plugin);
    static MStatus deregisterCmd(MFnPlugin &plugin);


    static const MString getUsageString();
};


