#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MStatus.h>

#include <common/log.h>
#include "cmd_template.h"
#include "version.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya
//
PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
	OPEN_CONSOLE_WINDOW_FOR_DEBUG();
    
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('"+CmdTemplate::name()+"')", true, false) );

    LInf("\nplugin is loaded: %s-----------------------\n", CmdTemplate::name().asChar());

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

	CHECK_MSTATUS(CmdTemplate::registerCmd(plugin));

	// import CmdTemplate
	CHECK_MSTATUS( MGlobal::executePythonCommand("import "+CmdTemplate::name()+"Setup", true, false) );
	CHECK_MSTATUS(plugin.registerUI(
                    CmdTemplate::name()+"_setupUI()",
                    CmdTemplate::name()+"_unsetupUI()",
                    CmdTemplate::name()+"_setup()",
                    CmdTemplate::name()+"_unsetup()"
                    ));

	/// unit test
	//unittest_main();
	
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('"+CmdTemplate::name()+"')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('"+CmdTemplate::name()+"')", true, false) );

	MFnPlugin plugin( obj );

	CHECK_MSTATUS(CmdTemplate::deregisterCmd(plugin));

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('"+CmdTemplate::name()+"')", true, false) );

	return MS::kSuccess;
}
