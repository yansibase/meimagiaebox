#include "cmd_template.h"

#include <cassert>

#include <maya/MArgList.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MGlobal.h>
#include <maya/MIntArray.h>
#include <maya/MMatrix.h>
#include <maya/MSelectionList.h>

#include <common/log.h>
#include <common/node_ids.h>

#define kHelp_s     ("-h")
#define kHelp_l     ("-help")
// int
#define kInteger_s  ("-i")
#define kInteger_l  ("-integer")
#define kInteger2_s ("-i2")
#define kInteger2_l ("-integer2")
#define kIMatrix_s  ("-im")
#define kIMatrix_l  ("-imatrix")
#define kIArray_s   ("-ia")
#define kIArray_l   ("-iarray")
// float
#define kDouble_s   ("-d")
#define kDouble_l   ("-double")
#define kDouble2_s  ("-d2")
#define kDouble2_l  ("-double2")
#define kDMatrix_s  ("-dm")
#define kDMatrix_l  ("-dmatrix")
#define kDArray_s   ("-da")
#define kDArray_l   ("-darray")
// string
#define kString_s   ("-s")
#define kString_l   ("-string")
#define kString2_s  ("-s2")
#define kString2_l  ("-string2")
#define kSArray_s   ("-sa")
#define kSArray_l   ("-sarray")

//
// CmdTemplate::doIt
//
//  This is the function used to invoke the command. The
// command is not undoable and it does not change any state,
// so it does not use the method to call back throught redoIt.


//////////////////////////////////////////////////////////////////////
const MString CmdTemplate::getUsageString()
{
    MString usage;

    usage += "\n ";
    usage += "\n "+name()+"  [options]";
    usage += "\n Options:";
    usage += "\n "+MString(kHelp_s)    +" / "+MString(kHelp_l)    +"\t\t\t"+"print this message ";

    usage += "\n "+MString(kInteger_s) +" / "+MString(kInteger_l) +"\t\t\t"+"int parameter ";
    usage += "\n "+MString(kInteger2_s)+" / "+MString(kInteger2_l)+"\t\t\t"+"two int parameters";
    usage += "\n "+MString(kIMatrix_s) +" / "+MString(kIMatrix_l) +"\t\t\t"+"int matrix parameter ";
    usage += "\n "+MString(kIArray_s)  +" / "+MString(kIArray_l)  +"\t\t\t"+"int array parameter ";

    usage += "\n "+MString(kDouble_s)   +" / "+MString(kDouble_l)   +"\t\t\t"+"double parameter ";
    usage += "\n "+MString(kDouble2_s)  +" / "+MString(kDouble2_l)  +"\t\t\t"+"two double parameters";
    usage += "\n "+MString(kDMatrix_s) +" / "+MString(kDMatrix_l) +"\t\t\t"+"double matrix parameter ";
    usage += "\n "+MString(kDArray_s)  +" / "+MString(kDArray_l)  +"\t\t\t"+"double array parameter ";

    usage += "\n "+MString(kString_s)  +" / "+MString(kString_l)  +"\t\t\t"+"string parameter ";
    usage += "\n "+MString(kString2_s) +" / "+MString(kString2_l) +"\t\t\t"+"two string parameters";
    usage += "\n "+MString(kSArray_s)  +" / "+MString(kSArray_l)  +"\t\t\t"+"string array parameter ";

    usage += "\n ";
    usage += "\n Example:";
    usage += "\n "+name()+" "+MString(kHelp_s)+";";

    usage += "\n "+name()+" "+MString(kInteger_s) +" 12;";
    usage += "\n "+name()+" "+MString(kInteger2_s)+" 12 13;";
    usage += "\n cmd_templateFun(im=[0, 1, 2, 3,  4, 5, 6, 7,  8, 9, 10, 11,  12, 13, 14, 15])";
    usage += "\n cmd_templateFun(ia=[0, 1, 2, 3,  4])";

    usage += "\n "+name()+" "+MString(kDouble_s) +" 12.0;";
    usage += "\n "+name()+" "+MString(kDouble2_s)+" 12.0 13.0;";
    usage += "\n cmd_templateFun(dm=[0.0, 1.0, 2.0, 3.0,  4.0, 5.0, 6.0, 7.0,  8.0, 9.0, 10.0, 11.0,  12.0, 13.0, 14.0, 15.0])";
    usage += "\n cmd_templateFun(da=[0.0, 1.0, 2.0, 3.0,  4.0])";

    usage += "\n "+name()+" "+MString(kString_s) +" \"AA\";";
    usage += "\n "+name()+" "+MString(kString2_s)+" \"BB\" \"CC\";";
    usage += "\n cmd_templateFun(sa=[\"BB\", \"CC\", \"EE\" \"FF\"])";
    usage += "\n ";

    return usage;
}
//
MSyntax CmdTemplate::mySyntax()
{
    MSyntax syntax;

    // NOTE:
    // Short flags are three letters or less;
    // Long flags are four letters or more.
    CHECK_MSTATUS(syntax.addFlag( kHelp_s, kHelp_l,                 MSyntax::kNoArg));
    // int
    CHECK_MSTATUS(syntax.addFlag( kInteger_s,   kInteger_l,         MSyntax::kLong));
    CHECK_MSTATUS(syntax.addFlag( kInteger2_s,  kInteger2_l,        MSyntax::kLong, MSyntax::kLong));
    CHECK_MSTATUS(syntax.addFlag( kIMatrix_s,   kIMatrix_l,         MSyntax::kString));
    CHECK_MSTATUS(syntax.addFlag( kIArray_s,    kIArray_l,          MSyntax::kString));
    // float
    CHECK_MSTATUS(syntax.addFlag( kDouble_s,    kDouble_l,          MSyntax::kDouble));
    CHECK_MSTATUS(syntax.addFlag( kDouble2_s,   kDouble2_l,         MSyntax::kDouble, MSyntax::kDouble));
    CHECK_MSTATUS(syntax.addFlag( kDMatrix_s,   kDMatrix_l,         MSyntax::kString));
    CHECK_MSTATUS(syntax.addFlag( kDArray_s,    kDArray_l,          MSyntax::kString));
    // string
    CHECK_MSTATUS(syntax.addFlag( kString_s,    kString_l,          MSyntax::kString));
    CHECK_MSTATUS(syntax.addFlag( kString2_s,   kString2_l,         MSyntax::kString, MSyntax::kString));
    CHECK_MSTATUS(syntax.addFlag( kSArray_s,    kSArray_l,          MSyntax::kString));

    syntax.enableQuery(true);
    syntax.enableEdit(true);

    return syntax;
}
//////////////////////////////////////////////////////////////////////
MStatus CmdTemplate::doIt( const MArgList &args)
{
    MStatus status;

    MArgDatabase argData( syntax(), args, &status);
    CHECK_MSTATUS(status);

    //const bool queryUsed = argData.isQuery();

    // help
    if (argData.isFlagSet(kHelp_l))
    {
        MGlobal::displayInfo(getUsageString());
        return MS::kSuccess;
    }
    /////////////////////////// int ///////////////////////////////////////
    // int
    if (argData.isFlagSet(kInteger_l))
    {
        int intParam;
        status = argData.getFlagArgument(kInteger_l, 0, intParam);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("integer flag parsing failed"));
            return MStatus::kFailure;
        }
        // set result
        MPxCommand::setResult(intParam);
    }
    // int2
    if (argData.isFlagSet(kInteger2_s))
    {
        int intParam0, intParam1;
        status = argData.getFlagArgument(kInteger2_l, 0, intParam0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("integer0 flag parsing failed"));
            return MStatus::kFailure;
        }
        status = argData.getFlagArgument(kInteger2_l, 1, intParam1);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("integer1 flag parsing failed"));
            return MStatus::kFailure;
        }
        // set result
        MPxCommand::clearResult ();
        MPxCommand::appendToResult(intParam0);
        MPxCommand::appendToResult(intParam1);
    }
    // int matrix
    if (argData.isFlagSet(kIMatrix_l))
    {
        MString strParam0;
        status = argData.getFlagArgument( kIMatrix_l, 0, strParam0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("matrix flag parsing failed"));
            return MStatus::kFailure;
        }
        // get values from plug
        MIntArray intArray;
        MGlobal::executeCommand("getAttr "+strParam0, intArray);
        // translate values to matrix
        MMatrix matrix;
        for(int i=0; i<16; ++i)
        {
            matrix(i/4, i%4) = intArray[i];
        }
        // set result
        MPxCommand::clearResult ();
        for(int i=0; i<16; ++i)
        {
            MPxCommand::appendToResult(matrix(i/4, i%4));
        }
    }
    // int array
    if (argData.isFlagSet(kIArray_l))
    {
        MString strParam0;
        status = argData.getFlagArgument( kIArray_l, 0, strParam0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("int array flag parsing failed"));
            return MStatus::kFailure;
        }
        // get values from plug
        MIntArray intArray;
        MGlobal::executeCommand("getAttr "+strParam0, intArray);

        // set result
        MPxCommand::clearResult ();
        for(unsigned int i=0; i<intArray.length(); ++i)
        {
            MPxCommand::appendToResult(intArray[i]);
        }
    }
    /////////////////////////// double ///////////////////////////////////////
    // double
    if (argData.isFlagSet(kDouble_l))
    {
        double param;
        status = argData.getFlagArgument(kDouble_l, 0, param);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("double flag parsing failed"));
            return MStatus::kFailure;
        }
        // set result
        MPxCommand::setResult(param);
    }
    // double2
    if (argData.isFlagSet(kDouble2_s))
    {
        double param0, param1;
        status = argData.getFlagArgument(kDouble2_l, 0, param0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("double0 flag parsing failed"));
            return MStatus::kFailure;
        }
        status = argData.getFlagArgument(kDouble2_l, 1, param1);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("double1 flag parsing failed"));
            return MStatus::kFailure;
        }
        // set result
        MPxCommand::clearResult ();
        MPxCommand::appendToResult(param0);
        MPxCommand::appendToResult(param1);
    }
    // double matrix
    if (argData.isFlagSet(kDMatrix_l))
    {
        MString strParam0;
        status = argData.getFlagArgument( kDMatrix_l, 0, strParam0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("double matrix flag parsing failed"));
            return MStatus::kFailure;
        }
        // get values from plug
        MDoubleArray doubleArray;
        MGlobal::executeCommand("getAttr "+strParam0, doubleArray);
        // translate values to matrix
        MMatrix matrix;
        for(int i=0; i<16; ++i)
        {
            matrix(i/4, i%4) = doubleArray[i];
        }
        // set result
        MPxCommand::clearResult ();
        for(int i=0; i<16; ++i)
        {
            MPxCommand::appendToResult(matrix(i/4, i%4));
        }
    }
    // double array
    if (argData.isFlagSet(kDArray_l))
    {
        MString strParam0;
        status = argData.getFlagArgument( kDArray_l, 0, strParam0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("double array flag parsing failed"));
            return MStatus::kFailure;
        }
        // get values from plug
        MDoubleArray doubleArray;
        MGlobal::executeCommand("getAttr "+strParam0, doubleArray);

        // set result
        MPxCommand::clearResult ();
        for(unsigned int i=0; i<doubleArray.length(); ++i)
        {
            MPxCommand::appendToResult(doubleArray[i]);
        }
    }
   /////////////////////////// string ///////////////////////////////////////
    // string
    if (argData.isFlagSet(kString_l))
    {
        MString param;
        status = argData.getFlagArgument(kString_l, 0, param);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("string flag parsing failed"));
            return MStatus::kFailure;
        }
        // set result
        MPxCommand::setResult(param);
    }
    // String2
    if (argData.isFlagSet(kString2_s))
    {
        MString param0, param1;
        status = argData.getFlagArgument(kString2_l, 0, param0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("string0 flag parsing failed"));
            return MStatus::kFailure;
        }
        status = argData.getFlagArgument(kString2_l, 1, param1);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("string1 flag parsing failed"));
            return MStatus::kFailure;
        }
        // set result
        MPxCommand::clearResult ();
        MPxCommand::appendToResult(param0);
        MPxCommand::appendToResult(param1);
    }
    // string array
    if (argData.isFlagSet(kSArray_l))
    {
        MString strParam0;
        status = argData.getFlagArgument( kSArray_l, 0, strParam0);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("string array flag parsing failed"));
            return MStatus::kFailure;
        }
        // get values from plug
        MStringArray strArray;
        MGlobal::executeCommand("getAttr "+strParam0, strArray);

        // set result
        MPxCommand::clearResult ();
        for(unsigned int i=0; i<strArray.length(); ++i)
        {
            MPxCommand::appendToResult(strArray[i]);
        }
    }
    return MStatus::kSuccess;
}

//
//  There is never anything to undo.
//////////////////////////////////////////////////////////////////////
MStatus CmdTemplate::undoIt()
{
    return MStatus::kSuccess;
}

//
//  There is never really anything to redo.
//////////////////////////////////////////////////////////////////////
MStatus CmdTemplate::redoIt()
{
    return MStatus::kSuccess;
}
//////////////////////////////////////////////////////////////////////
bool CmdTemplate::isUndoable() const
{
    return false;
}
//////////////////////////////////////////////////////////////////////
bool CmdTemplate::hasSyntax() const
{
    return true;
}
//////////////////////////////////////////////////////////////////////
bool CmdTemplate::isHistoryOn() const
{
    //what is this supposed to do?
    return false;
}
//////////////////////////////////////////////////////////////////////
MString CmdTemplate::name()
{
    return CmdName_CmdTemplate;
}
//
MStatus CmdTemplate::registerCmd(MFnPlugin &plugin)
{
    MStatus status;

    //register a helper command for querying shader information
    CHECK_MSTATUS(plugin.registerCommand(
                           CmdTemplate::name(),
                           CmdTemplate::creator,
                           CmdTemplate::mySyntax)
                  );
    return MS::kSuccess;
}
//
MStatus CmdTemplate::deregisterCmd(MFnPlugin &plugin)
{
    CHECK_MSTATUS(
                  plugin.deregisterCommand( CmdTemplate::name() )
                  );

    return MS::kSuccess;
}
