import os
import sys
import unittest
import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import at_helper                as hp

class MyTest00(unittest.TestCase):
    m_id        = ''

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        hp.ATHelper.tryToLoadPlugin('cmd_template')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        hp.ATHelper.tryToUnloadPlugin('cmd_template')

    def test_basic_parameters(self):
        log.debug('---- begin %s.basic_parameters() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        #=================== int ===================================
        #--------test integer-----------------
        # py
        iv = cmds.cmd_template(i=78)
        self.assertEqual(iv, 78)
        # mel
        iv = mel.eval('cmd_template -i 90;');
        self.assertEqual(iv, 90)

        #--------test integer2-----------------
        # py
        i2v = cmds.cmd_template(i2=[11, 22])
        self.assertEqual(i2v, [11, 22])
        # mel
        i2v = mel.eval('cmd_template -i2 33 44;');
        self.assertEqual(i2v, [33, 44])

        #--------test int matrix-----------------
        import utils #import cmd_template.scripts.utils
        # py
        imv = utils.cmd_templateFun(im=[0, 1, 2, 3,  4, 5, 6, 7,  8, 9, 10, 11,  12, 13, 14, 15])
        self.assertEqual(imv, [0, 1, 2, 3,  4, 5, 6, 7,  8, 9, 10, 11,  12, 13, 14, 15])
        # mel
        #imv = mel.eval('cmd_templateFun -im 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0;');
        #self.assertEqual(imv, [15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0])

        #--------test int array-----------------
        # py
        iav = utils.cmd_templateFun(ia=[12, 13, 14, 15])
        self.assertEqual(iav, [12, 13, 14, 15])
        # mel
        #iav = mel.eval('cmd_templateFun -ia 15 14 13 12;');
        #self.assertEqual(iav, [15, 14, 13, 12])
        #=================== double ===================================
        #--------test double-----------------
        # py
        dv = cmds.cmd_template(d=78)
        self.assertEqual(dv, 78)
        # mel
        dv = mel.eval('cmd_template -d 90;');
        self.assertEqual(dv, 90)

        #--------test double2-----------------
        # py
        d2v = cmds.cmd_template(d2=[11, 22])
        self.assertEqual(d2v, [11, 22])
        # mel
        d2v = mel.eval('cmd_template -d2 33 44;');
        self.assertEqual(d2v, [33, 44])

        #--------test double matrix-----------------
        # py
        dmv = utils.cmd_templateFun(im=[0.0, 1.0, 2.0, 3.0,  4.0, 5.0, 6.0, 7.0,  8.0, 9.0, 10.0, 11.0,  12.0, 13.0, 14.0, 15.0])
        self.assertEqual(dmv, [0.0, 1.0, 2.0, 3.0,  4.0, 5.0, 6.0, 7.0,  8.0, 9.0, 10.0, 11.0,  12.0, 13.0, 14.0, 15.0])
        # mel
        #dmv = mel.eval('cmd_templateFun -dm 15.0 14.0 13.0 12.0 11.0 10.0 9.0 8.0 7.0 6.0 5.0 4.0 3.0 2.0 1.0 0.0;');
        #self.assertEqual(dmv, [15.0, 14.0, 13.0, 12.0, 11.0, 10.0, 9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0, 0.0])

        #--------test double array-----------------
        # py
        dav = utils.cmd_templateFun(da=[12.0, 13.0, 14.0, 15.0])
        self.assertEqual(dav, [12.0, 13.0, 14.0, 15.0])
        # mel
        #dav = mel.eval('cmd_templateFun -da 15.0 14.0 13.0 12.0;');
        #self.assertEqual(dav, [15.0, 14.0, 13.0, 12.0])
        #=================== string ===================================
        #--------test string-----------------
        # py
        sv = cmds.cmd_template(s='XYZ')
        self.assertEqual(sv, 'XYZ')
        # mel
        sv = mel.eval('cmd_template -s "ABC";');
        self.assertEqual(sv, 'ABC')

        #--------test string2-----------------
        # py
        s2v = cmds.cmd_template(s2=['11', '22'])
        self.assertEqual(s2v, ['11', '22'])
        # mel
        s2v = mel.eval('cmd_template -s2 "33" "44";');
        self.assertEqual(s2v, ['33', '44'])

        #--------test string array-----------------
        # py
        sav = utils.cmd_templateFun(sa=['12', '13', '14', '15'])
        self.assertEqual(sav, ['12', '13', '14', '15'])
        # mel
        #sav = mel.eval('cmd_templateFun -sa "15.0" "14.0" "13.0" "12.0";');
        #self.assertEqual(sav, ['15.0', '14.0', '13.0', '12.0'])

        log.debug('---- end   %s.basic_parameters() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
