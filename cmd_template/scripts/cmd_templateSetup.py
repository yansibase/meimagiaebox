# -*- coding: utf-8 -*-

import maya.cmds                    as cmds
import maya.mel                     as mel
import pymel.core                   as pm
import pymel.tools.py2mel           as py2mel
import mymagicbox.log               as log

log.debug('This is '+__file__)

gDeveloperMode = False
################################################################################
def setupUI():
    log.debug('This is VecToQuatAccSetup.setupUI() in py');
    pass

def unsetupUI():
    log.debug('This is VecToQuatAccSetup.unsetupUI() in py');
    pass

def setup():
    log.debug('This is VecToQuatAccSetup.setup() in py');
    pass

def unsetup():
    log.debug('This is VecToQuatAccSetup.unsetup() in py');
    pass

# register the above python functions as mel procedures
py2mel.py2melProc(setupUI,   procName='cmd_template_setupUI',   evaluateInputs=True);
py2mel.py2melProc(unsetupUI, procName='cmd_template_unsetupUI', evaluateInputs=True);
py2mel.py2melProc(setup,     procName='cmd_template_setup',     evaluateInputs=True);
py2mel.py2melProc(unsetup,   procName='cmd_template_unsetup',   evaluateInputs=True);
