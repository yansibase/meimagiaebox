import maya.cmds                as cmds
import pymel.tools.py2mel       as py2mel

def cmd_templateFun(im=None, ia=None, dm=None, da=None, sa=None):
    '''
    cmd_templateFun(im=[1, 1, 1, 1,  2, 2, 2, 2,  8, 9, 10, 11,  12, 13, 14, 16])
    cmd_templateFun(ia=[1, 2, 3, 4])
    cmd_templateFun(dm=[1.0, 1.0, 1.0, 1.0,  2.0, 2.0, 2.0, 2.0,  8.0, 9.0, 10.0, 11.0,  12.0, 13.0, 14.0, 16.0])
    cmd_templateFun(da=[1.0, 2.0, 3.0, 4.0])
    cmd_templateFun(sa=['AA', 'BB', 'CC', 'DD'])
    '''

    longname    = ''
    shortname   = ''
    datatype    = ''


    if im is not None:
        longname    = 'imatrix'
        shortname   = 'im'
        datatype    = 'matrix'
    elif ia is not None:
        longname    = 'iarray'
        shortname   = 'ia'
        datatype    = 'Int32Array'
    elif dm is not None:
        longname    = 'dmatrix'
        shortname   = 'dm'
        datatype    = 'matrix'
    elif da is not None:
        longname    = 'darray'
        shortname   = 'da'
        datatype    = 'doubleArray'
    elif sa is not None:
        longname    = 'sarray'
        shortname   = 'sa'
        datatype    = 'stringArray'

    #----------------------------------------------
    # create a temp node for holding the data
    nodeName = cmds.createNode('choice', name='cmd_templateAuxiliary', skipSelect=True);
    # save data to a plug
    cmds.addAttr(nodeName, ln=longname,   sn=shortname, dt=datatype,      r=True, w=True, s=True, k=True)
    #----------------------------------------------


    if im is not None:
        # set value
        cmds.setAttr(nodeName+'.'+longname,  im, type=datatype) # <--- im
        # we can fetch the data in cmd_template
        ret = cmds.cmd_template(im=(nodeName+'.'+longname))     # <--- im
        # delete the node
        cmds.delete(nodeName)
        return ret
    elif ia is not None:
        # set value
        cmds.setAttr(nodeName+'.'+longname,  ia, type=datatype) # <--- ia
        # we can fetch the data in cmd_template
        ret = cmds.cmd_template(ia=(nodeName+'.'+longname))     # <--- ia
        # delete the node
        cmds.delete(nodeName)
        return ret
    elif dm is not None:
        # set value
        cmds.setAttr(nodeName+'.'+longname,  dm, type=datatype) # <--- dm
        # we can fetch the data in cmd_template
        ret = cmds.cmd_template(dm=(nodeName+'.'+longname))     # <--- dm
        # delete the node
        cmds.delete(nodeName)
        return ret
    elif da is not None:
        # set value
        cmds.setAttr(nodeName+'.'+longname,  da, type=datatype) # <--- da
        # we can fetch the data in cmd_template
        ret = cmds.cmd_template(da=(nodeName+'.'+longname))     # <--- da
        # delete the node
        cmds.delete(nodeName)
        return ret
    elif sa is not None:
        # set value
        cmds.setAttr(nodeName+'.'+longname,  sa, type=datatype) # <--- sa
        # we can fetch the data in cmd_template
        ret = cmds.cmd_template(sa=(nodeName+'.'+longname))     # <--- sa
        # delete the node
        cmds.delete(nodeName)
        return ret

py2mel.py2melProc(cmd_templateFun,   procName='cmd_templateFun',   evaluateInputs=True);

