#pragma once

#include <string>
#include <vector>
#include <maya/MItGeometry.h>
#include "driver_mesh.h"

class DriverMeshMgr
{
public:
    DriverMeshMgr();
    ~DriverMeshMgr();

    void clearAllDriverMesh();

    // construct DriverMesh data from MObject and save them to m_driverMesh
    void addDriverMesh(const MObject& driverShape, /*const*/ MObject& dvnGeo, const short methodOfMappingVertexIndex);

    const DriverMesh& getDriverMesh(const std::size_t i)const;

    // init driven mesh shape origional node id
    // Why we need the orig node?
    // Because we use the distance to map the vertex from driver mesh to driven mesh,
    // and the driven mesh shape node is deformed by driver mesh.
    MStatus setupDrivenMeshOrig();

    void getDrivenMeshOrigPos(MPointArray& out)const;

    MObject getDrivenMeshOrig(){ return m_drivenShapeOrig; }

protected:
    std::string             m_drivenMeshID; // driver mesh id
    std::vector<DriverMesh> m_driverMesh;   // driver meshes

    MObject                 m_drivenShapeOrig;
    //MObject                 m_drivenMObject;
    //MItGeometry&            m_drivenMItGeometry;

private:


protected:
    DriverMeshMgr(const DriverMeshMgr& o);
    DriverMeshMgr& operator=(const DriverMeshMgr& o);

};
