#include "driver_mesh_mgr_test.h"
#include <cassert>

#include <maya/MDagPath.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>

#include <common/log.h>
#include "../driver_mesh_mgr.h"

static int DriverMeshMgr_setup()
{
    MStatus status;

    MSelectionList sList;
    MString toMatch("|driven|drivenShape");/// you can use "/test/test_0002/data/test.ma"
    MGlobal::getSelectionListByName(toMatch, sList);

    unsigned int nMatches = sList.length();
    assert(nMatches>0 && "driven mesh is not found");

    MDagPath drivenDP;
    CHECK_MSTATUS(sList.getDagPath(0, drivenDP));

    MObject drivenObj(drivenDP.node(&status)); CHECK_MSTATUS(status);
    printMeshInfo(__FUNCTION__, drivenObj);


    const MItGeometry itDvnGeo(drivenObj, &status); CHECK_MSTATUS(status);
    //DriverMeshMgr driverMeshMgr;
    //driverMeshMgr.addDriverMesh(drivenObj, "|driver0|driver0Shape", itDvnGeo);

    return 0;
}

//
int test_DriverMeshMgr()
{
    DriverMeshMgr_setup();

    return 0;
}
