#include "unittest_facade.h"

#include <common/log.h>
#include "driver_mesh_test.h"
#include "driver_mesh_mgr_test.h"

int unittest_main()
{
    LInf("----------------------------------\n");

    test_DriverMesh();

    test_DriverMeshMgr();

    LInf("----------------------------------\n");
    return 0;
}
