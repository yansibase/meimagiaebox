#include "driver_mesh_test.h"

#include <cassert>

#include "../driver_mesh.h"
//
static int DriverMesh_init()
{
    int ret = 0;

    DriverMesh emptyDM0;
    DriverMesh emptyDM1;
    DriverMesh driver0("driver0", 0, 1, 2);
    DriverMesh driver1("driver1", 1, 2, 3);

    /// compare
    assert(emptyDM0.compare(emptyDM1));

    /// print
    driver0.print(std::cout);
    driver1.print(std::cout);

    /// swap
    swap(driver0, driver1);
    assert(driver0.id() == "driver1");
    assert(driver0.v().at(0) == 1);

    /// operator=
    DriverMesh tmp0("tmp0", -1, 0, 1);
    DriverMesh tmp1;
    tmp1 = tmp0;
    assert(tmp1.id() == "tmp0");
    assert(tmp1.v().at(0) == -1);


    return ret;
}
//
int test_DriverMesh()
{
    int ret = 0;

    ret = DriverMesh_init();

    return ret;
}
