#include <maya/MFnPlugin.h>
#include <maya/MStatus.h>
#include <common/log.h>
#include "version.h"
#include "partial_blendshape.h"
#include "unittest/unittest_facade.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya
//
PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('"+TestDeformer::cTypeName()+"')", true, false) );

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif


	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

	CHECK_MSTATUS(
		plugin.registerNode(
            TestDeformer::cTypeName(),
            TestDeformer::cTypeId(),
            TestDeformer::creator,
            TestDeformer::initialize,
            TestDeformer::cType(),
            &TestDeformer::cClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += TestDeformer::cClassification();
	command += "\");}\n";

	CHECK_MSTATUS( MGlobal::executeCommand( command ) );

	// import pbsSetup
	CHECK_MSTATUS( MGlobal::executePythonCommand("import pbsSetup", true, false) );
	CHECK_MSTATUS(plugin.registerUI(
                    "pbs_setupUI()",
                    "pbs_unsetupUI()",
                    "pbs_setup()",
                    "pbs_unsetup()"
                    ));
	/// unit test
	//unittest_main();

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('"+TestDeformer::cTypeName()+"')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );

    // callback functions
    CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('"+TestDeformer::cTypeName()+"')", true, false) );

	CHECK_MSTATUS( plugin.deregisterNode( TestDeformer::cTypeId() ) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += TestDeformer::cClassification();
	command += "\");}\n";

	CHECK_MSTATUS( MGlobal::executeCommand( command ) );

    // callback functions
    CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('"+TestDeformer::cTypeName()+"')", true, false) );

	return MS::kSuccess;
}
