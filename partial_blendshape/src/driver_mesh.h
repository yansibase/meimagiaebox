#pragma once

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <iterator>
#include <maya/MArrayDataBuilder.h>
#include <maya/MItGeometry.h>

class MFloatArray;

enum MethodOfMappingVertexIndex
{
    MMVI_ObjectSpace = 0,
    MMVI_UVSpace     = 1,
    NUM_MMVI
};

class DriverMesh
{
public:
    DriverMesh();
    DriverMesh(const std::string& id, int i0, int i1, int i2);
    DriverMesh(const DriverMesh& o);

    // construct data from MObject
    DriverMesh(const MObject& mobjId, /*const*/ MObject& dvnGeo, const short methodOfMappingVertexIndex);
    ~DriverMesh();

    DriverMesh& operator=(const DriverMesh& o);

    // getter & setter
    const std::string&      id() const{ return m_id; }
    const std::vector<int>& v()  const{ return m_v;  }
    //  dvrVtxIdx --> dvnVtxIdx
    // \param   dvrVtxIdx   the vertex index on driver mesh
    //  \ret                the vertex index on driven mesh
    std::size_t getDvnVtxIdx(const std::size_t dvrVtxIdx)const;

    // set this->m_v to PBS.drivers[*].vtxIdx
    void setVtxIdxToPlug(MArrayDataHandle& vtxIdxHandle)const;

    // utility
    void swap(DriverMesh& o);
    //
    std::ostream& print(std::ostream &os) const;
    //
    bool compare(const DriverMesh& o) const;

protected:
    // param pointOnDriver  map this point to a vertex on the driven mesh
    // param itDvnGeo       be used to walk through all the vertice on driven mesh
    std::size_t getIndexOfClosestVertexOnDrivenMesh(const MPoint& pointOnDriver, MItGeometry& itDvnGeo)const;

    int getIndexOfClosestUVOnDrivenMesh(const MFloatArray &uBuffer, const MFloatArray &vBuffer, const float2 uv) const;
    int mapUVIdxToVertexIdx(int uvIdx, const std::map<int, std::vector<int> > &vtxIdxToUVIdx);

    static MStatus getOrigNode(const MObject& mobjId, MObject& origMObjId);

protected:
    MObject             m_mobjId;
    std::string         m_id; // mesh id
    std::vector<int>    m_v; // "m_v[i]=j" means dvrVtxIdx[i] is mapped to dvnVtxIdx[j]

protected:


};
//
void swap(DriverMesh& a, DriverMesh& b);
