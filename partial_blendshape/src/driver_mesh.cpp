#include "driver_mesh.h"
#include <stdio.h>
#include <cassert>

#include <maya/MArrayDataHandle.h>
#include <maya/MDagPath.h>
#include <maya/MDataHandle.h>
#include <maya/MFnDagNode.h>
#include <maya/MGlobal.h>
#include <maya/MPoint.h>
#include <maya/MSelectionList.h>
#include <maya/MStatus.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshVertex.h>

#include <common/log.h>

DriverMesh::DriverMesh()
:m_mobjId(MObject::kNullObj), m_id("empty")
{

}
//
DriverMesh::DriverMesh(const std::string& id, int i0, int i1, int i2)
:m_mobjId(MObject::kNullObj), m_id(id)
{
    m_v.push_back(i0);
    m_v.push_back(i1);
    m_v.push_back(i2);
}
//
DriverMesh::DriverMesh(const MObject& mobjId, /*const*/ MObject& dvnGeo, const short methodOfMappingVertexIndex)
:m_mobjId(mobjId), m_id("empty")
{
    MStatus status;

    // Debug
    MFnDagNode fnDagNode(m_mobjId, &status);            CHECK_MSTATUS(status);
    MDagPath path; status = fnDagNode.getPath(path);    CHECK_MSTATUS(status);
    LDbg("%s", path.fullPathName().asChar());

    MObject whichMObjIdIsUsedtoMapVtxId;
    MObject origMObjId;
    if(MS::kSuccess == getOrigNode(m_mobjId, origMObjId))
    {
        // orig node of ... is found
        LDbg("orig node is found. %s", path.fullPathName().asChar());
        whichMObjIdIsUsedtoMapVtxId = origMObjId;
    }else{
        //
        LErr("orig node is not found! So I have to use shape node to map the vertex index. %s", path.fullPathName().asChar());
        whichMObjIdIsUsedtoMapVtxId = m_mobjId;
    }

    // map vertex index from driver mesh to driven mesh, and construct this->m_v
    MItGeometry itDvrGeo(whichMObjIdIsUsedtoMapVtxId, &status);        CHECK_MSTATUS(status);
    m_v.reserve(itDvrGeo.count());

    MString strCount; strCount.set(itDvrGeo.count());
    CHECK_MSTATUS(MGlobal::executePythonCommand("import pbs_progressbar as pbar; pbar.pbs_BeginProgressBar("+strCount+", 'Map vertex index begins:"+path.fullPathName()+"')"));

    MString strIndex;
    if(methodOfMappingVertexIndex == MMVI_ObjectSpace)
    {
        MItGeometry itDvnGeo(dvnGeo,  &status);            CHECK_MSTATUS(status);
        while( !itDvrGeo.isDone(&status) )
        {
            CHECK_MSTATUS(status);

            // map 'from'(vertex index on driver mesh)
            std::size_t from = itDvrGeo.index();
            // map 'to'  (vertex index on driven mesh)
            std::size_t to   = getIndexOfClosestVertexOnDrivenMesh(itDvrGeo.position(MSpace::kObject, &status), itDvnGeo);

            assert(m_v.size() == from);
            m_v.push_back(to);// m_v[from] = to

            CHECK_MSTATUS(itDvrGeo.next());

            strIndex.set(from);
            CHECK_MSTATUS(MGlobal::executePythonCommand("import pbs_progressbar as pbar; pbar.pbs_UpdateProgressBar('"+strIndex+"/"+strCount+",   "+path.fullPathName()+"')"));
        }
    }else if(methodOfMappingVertexIndex == MMVI_UVSpace)
    {
/*
Given a cube and the vertex index (see doc/vertex-and-uv.png)

   4____________5
   /.          /|
  / .         / |
2/___________/3 |
 |  .       |   |
 |  ........|...|7
 | . 6      |  /
 |.         | /
 |__________|/
 0          1

 Its uv index could be:

           8 +----------+ 9
             |          |
             |          |
             |          |
             |          |
           6 +----------+ 7
             |          |
             |          |
             |          |
             |          |
           4 +----------+ 5
             |          |
             |          |
             |          |
 13        2 |          | 3        11
  +----------+----------+----------+
  |          |          |          |
  |          |          |          |
  |          |          |          |
  |          |          |          |
  +----------+----------+----------+
  12         0          1          10

The following code will get the uv values:
        MFloatArray u, v;
        CHECK_MSTATUS(fnDvnMesh.getUVs(u, v, &uvSet));
It could be:
    uv index:     0      1      2      3      4      5      6      7      8      9     10     11     12     13
           u: 0.375, 0.625, 0.375, 0.625, 0.375, 0.625, 0.375, 0.625, 0.375, 0.625, 0.875, 0.875, 0.125, 0.125,
           v:     0,     0,  0.25,  0.25,   0.5,   0.5,  0.75,  0.75,     1,     1,     0,  0.25,     0,  0.25,

The cube has 8 vertex and 13 uv values. So one vertex may have multiple uv values. The function MItMeshVertex::getUVIndices()
can get this relationship. For example, it could be:

vertex index ~ uv index   Description
           0   0, 0, 8,   vertex 0 has 2 uv index: 0, 8
           1   9, 1, 1,   vertex 1 has 2 uv index: 9, 1
           2   2, 2, 2,   vertex 2 has 1 uv index: 2
           3   3, 3, 3,   vertex 3 has 1 uv index: 3
           4   4, 4, 13,  vertex 4 has 2 uv index: 4, 13
           5   11,5, 5,   vertex 5 has 2 uv index: 11, 5
           6   6, 6, 12,  vertex 6 has 2 uv index: 6, 12
           7   10,7, 7,   vertex 7 has 2 uv index: 10, 7

I call it the vtxIdxToUVIdx table.


*/
        /// driven mesh
        //printMeshInfo("dvn mesh", dvnGeo);// DEBUG

        //
        std::map<int, std::vector<int> > vtxIdxToUVIdxOnDvnMesh;// vtxIdxToUVIdx table of the driven mesh
        MFnMesh         fnDvnMesh(dvnGeo,  &status);                        CHECK_MSTATUS(status);
        MItMeshVertex   itDvnMeshVtx(dvnGeo,  &status);                     CHECK_MSTATUS(status);
        const MString uvSet("map1");
        MFloatArray uDvnMesh, vDvnMesh;
        CHECK_MSTATUS(fnDvnMesh.getUVs(uDvnMesh, vDvnMesh, &uvSet));

        // DEBUG
        //::print("u=", uDvnMesh);
        //::print("v=", vDvnMesh);

        while( !itDvnMeshVtx.isDone(&status) )
        {
            CHECK_MSTATUS(status);

            // for each vertex on the driven mesh ...

            /// the index of the current vertex in the vertex list for this polygonal object.
            // It could be 0~7 for a cube
            int vtxIdx = itDvnMeshVtx.index(&status);

            /// how many uv index does this vertex has?
            MIntArray uvIndices;
            CHECK_MSTATUS(itDvnMeshVtx.getUVIndices(uvIndices, &uvSet));

            /// build vtxIdxToUVIdx table
            std::vector<int> tmp;// assign tmp to uvIndices
            tmp.reserve(uvIndices.length());
            for(std::size_t i=0; i<uvIndices.length(); ++i)
            {
                tmp.push_back(uvIndices[i]);
            }
            vtxIdxToUVIdxOnDvnMesh[vtxIdx] = tmp;

            // DEBUG. print vtxIdxToUVIdx table
            //LDbg("vtxIdx[%d], uvIndices:", vtxIdx);
            //std::copy(tmp.begin(), tmp.end(), std::ostream_iterator<int>(std::cout, ","));
            //std::cout<< std::endl;

            CHECK_MSTATUS(itDvnMeshVtx.next());// turn to next vertex
        }

/*
Now, how we map the vertex index from driver mesh to driven mesh with uv?

(driver mesh)                               (driven mesh)

for each vertex,                get the vertex index from uv index: 'to'
get uv: 'uv'                                   /\
    and                                        ||
vertex index: 'from'                           || mapUVIdxToVertexIdx()
   ||                                          ||
   ||                           get the uv index on driven mesh
   ||                                          /\
   ||                                          ||
   ||                                          ||
    \===========================================/
     Given uv value on driver mesh, which uv index
      is the closest uv value on driven mesh?
     The function getIndexOfClosestUVOnDrivenMesh() gives the answer.

Now we map the vertex index 'from' on driver mesh to vertex index 'to' on driven mesh.
We save the relationship to this->m_v.
*/
        /// driver mesh
        //printMeshInfo("dvr mesh", whichMObjIdIsUsedtoMapVtxId);// DEBUG

        MFnMesh         fnDvrMesh(whichMObjIdIsUsedtoMapVtxId,     &status);   CHECK_MSTATUS(status);
        MItMeshVertex   itDvrMeshVtx(whichMObjIdIsUsedtoMapVtxId,  &status);    CHECK_MSTATUS(status);

        while( !itDvrMeshVtx.isDone(&status) )
        {
            CHECK_MSTATUS(status);

            float2 uv;
            CHECK_MSTATUS(itDvrMeshVtx.getUV(uv, &uvSet));

            // Given uv value on driver mesh, which uv index is the closest uv value on driven mesh?
            const int theClosestUVIdxOnDvnMesh = getIndexOfClosestUVOnDrivenMesh(uDvnMesh, vDvnMesh, uv);

            // map 'from'(vertex index on driver mesh)
            std::size_t from = itDvrMeshVtx.index(&status);// the index of the current vertex in the vertex list for this polygonal object.
            // map 'to'  (vertex index on driven mesh)
            std::size_t to   = mapUVIdxToVertexIdx(theClosestUVIdxOnDvnMesh, vtxIdxToUVIdxOnDvnMesh);

            assert(m_v.size() == from);
            m_v.push_back(to);// m_v[from] = to

            CHECK_MSTATUS(itDvrMeshVtx.next());


        }

    }else{
        LFat("Only MMVI_ObjectSpace(%d) and MMVI_UVSpace(%d) are supported for vertex mapping method, the current map method is %d", MMVI_ObjectSpace, MMVI_UVSpace, methodOfMappingVertexIndex);
        assert(0 && "the map method is not supported");
    }



    CHECK_MSTATUS(MGlobal::executePythonCommand("import pbs_progressbar as pbar; pbar.pbs_EndProgressBar('Map vertex index ends:"+path.fullPathName()+"')"));

    //Debug
    //print(std::cout);
}
//
DriverMesh::DriverMesh(const DriverMesh& o)
{
    m_id    = o.m_id;
    m_v     = o.m_v;
}
//
DriverMesh::~DriverMesh()
{

}
//
DriverMesh& DriverMesh::operator=(const DriverMesh& o)
{
    DriverMesh tmp(o);
    swap(tmp);
    return (*this);
}
//
void DriverMesh::swap(DriverMesh& o)
{
    std::swap(m_id,   o.m_id);
    std::swap(m_v,    o.m_v);
}
//
std::ostream& DriverMesh::print(std::ostream &os) const
{
    os<< std::endl;
    os<< "DriverMesh:   " << m_id<<":"<< std::endl;
    std::copy(m_v.begin(), m_v.end(), std::ostream_iterator<int>(os, ","));
    os<< std::endl;
    return os;
}
//
bool DriverMesh::compare(const DriverMesh& o) const
{
    if (m_id != o.m_id)
        return false;

    if (m_v != o.m_v)
        return false;

    return true;
}
//
std::size_t DriverMesh::getDvnVtxIdx(const std::size_t dvrVtxIdx)const
{
    assert(dvrVtxIdx < m_v.size());
    return m_v[dvrVtxIdx];
}
//
void DriverMesh::setVtxIdxToPlug(MArrayDataHandle& vtxIdxHandle)const
{
    MStatus status;

    MArrayDataBuilder builder(vtxIdxHandle.builder(&status)); CHECK_MSTATUS(status);

    for(std::size_t i=0; i<m_v.size(); ++i)
    {
        MDataHandle h = builder.addElement(i, &status);CHECK_MSTATUS(status);
        h.setInt(m_v[i]);
        h.setClean();
    }

    CHECK_MSTATUS(vtxIdxHandle.set(builder));
    CHECK_MSTATUS(vtxIdxHandle.setAllClean());
}
//void DriverMesh::accumulateDeformTo(MPointArray& output, const MPointArray& dvnOriPos)const
//{
//    const std::size_t dvrVtxCnt = m_v.size();
//    for(std::size_t dvrVtxIdx = 0; dvrVtxIdx<dvrVtxCnt; ++dvrVtxIdx)
//    {
//        //output[] += dvrPos[i] - dvnOriPos[i];
//        assert(dvrVtxIdx < output.length());
//
//        const std::size_t dvnVtxIdx = m_v.at(dvrVtxIdx);// driver vtx idx --> driven vtx idx
//
//        output[dvnVtxIdx] += dvrPos[dvnVtxIdx] - dvnOriPos[dvnVtxIdx];
//    }
//}
//
std::size_t DriverMesh::getIndexOfClosestVertexOnDrivenMesh(const MPoint& pointOnDriver, MItGeometry& itDvnGeo)const
{
    MStatus status;
    int     ptIndex = -1;
    float   nearestDistanceSoFar = 9e99;

    itDvnGeo.reset();
    while( !itDvnGeo.isDone(&status) )
    {
        CHECK_MSTATUS( status );

        float dist = pointOnDriver.distanceTo(itDvnGeo.position(MSpace::kObject, &status)); CHECK_MSTATUS(status);
        if(dist < nearestDistanceSoFar )
        {
            ptIndex = itDvnGeo.index();
            nearestDistanceSoFar = dist;
        }

        itDvnGeo.next();
    }

    assert(ptIndex != -1 && "can't find the nearest point on the driven mesh");
    return ptIndex;
}

MStatus DriverMesh::getOrigNode(const MObject& mobjId, MObject& origMObjId)
{
    MStatus status;

    MFnDependencyNode fnDNode(mobjId, &status);           CHECK_MSTATUS(status);

    MString origNodeName;
    origNodeName = MGlobal::executePythonCommandStringResult("import testDeformer as pbs; pbs.getOrigNode(\""+fnDNode.name()+"\")", true, false, &status);
    CHECK_MSTATUS(status);

    if(origNodeName == "")// if the orig node is not found in DAG, try to find orig node with name $(shape)Orig
    {
        origNodeName = MGlobal::executePythonCommandStringResult("import pbs_protocol as pbs_prot; pbs_prot.getDriverShapeOrigName(\""+fnDNode.name()+"\")", true, false, &status);
        CHECK_MSTATUS(status);
    }

    // in order to print the message in script editor window, I call checkShapeOrigName()
    CHECK_MSTATUS(MGlobal::executePythonCommand("import testDeformer as pbs; pbs.checkShapeOrigName(\""+origNodeName+"\")", true, false));


    MSelectionList sList;
    MString toMatch(origNodeName);
    MGlobal::getSelectionListByName(toMatch, sList);

    unsigned int nMatches = sList.length();
    if (nMatches<=0)
    {
        //assert(nMatches>0 && "original shape node of driven mesh is not found");
        LWrn("original shape node of driver mesh is not found: %s", toMatch.asChar());
        origMObjId = MObject::kNullObj;
        return MS::kNotFound;
    }else if(nMatches>1){
        LWrn("Too many original shape node of driver mesh: %s. The orig node names are shown in script window.", toMatch.asChar());
        origMObjId = MObject::kNullObj;
        return MS::kInvalidParameter;
    }

    MDagPath origdagpath;
    CHECK_MSTATUS(sList.getDagPath(0, origdagpath));

    origMObjId = origdagpath.node(&status); CHECK_MSTATUS(status);
    return MS::kSuccess;
}

int DriverMesh::getIndexOfClosestUVOnDrivenMesh(const MFloatArray &uBuffer, const MFloatArray &vBuffer, const float2 uv) const
{
    MStatus status;
    int     ptIndex = -1;
    float   nearestDistanceSoFar = 9e99;

    const std::size_t BUFFER_SIZE = uBuffer.length();
    const MPoint pt(uv[0], uv[1], 0.0);

    for(std::size_t i = 0; i < BUFFER_SIZE; ++i)
    {
        const MPoint pt_in_buffer(uBuffer[i], vBuffer[i], 0.0);

        float dist = pt.distanceTo(pt_in_buffer); CHECK_MSTATUS(status);
        if(dist < nearestDistanceSoFar )
        {
            ptIndex = i;
            nearestDistanceSoFar = dist;
        }
    }

    assert(ptIndex != -1 && "can't find the nearest point on the driven mesh");
    return ptIndex;
}

int DriverMesh::mapUVIdxToVertexIdx(int uvIdx, const std::map<int, std::vector<int> > &vtxIdxToUVIdx)
{
    std::map<int, std::vector<int> >::const_iterator b = vtxIdxToUVIdx.begin();
    std::map<int, std::vector<int> >::const_iterator e = vtxIdxToUVIdx.end();
    for(std::map<int, std::vector<int> >::const_iterator i = b; i != e; ++i)
    {
        std::vector<int>::const_iterator valuebegin = i->second.begin();
        std::vector<int>::const_iterator valueend   = i->second.end();
        for(std::vector<int>::const_iterator j = valuebegin; j != valueend; ++j)
        {
            if(*j == uvIdx)
            {
                // found
                return i->first;
            }
        }
/*        if(std::find(valuebegin, valueend, uvIdx) != valueend)
        {
            // found
            return i->first;
        }*/
    }
    return -1; // not found
}
//
void swap(DriverMesh& a, DriverMesh& b)
{
    printf("::%s()\n", __FUNCTION__);
    a.swap(b);
}
