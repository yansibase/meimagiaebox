//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+

// Example Plugin: lambertShader.cpp
//
// Produces dependency graph node LambertShader
// This node is an example of a Lambert shader and how to build a
// dependency node as a surface shader in Maya. The inputs for this node
// are many, and can be found in the Maya UI on the Attribute Editor for
// the node. The output attributes for the node are "outColor" and
// "outTransparency". To use this shader, create a lambertShader with
// Shading Group or connect the outputs to a Shading Group's
// "SurfaceShader" attribute.
//
#pragma once

#include <maya/MIOStream.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MGlobal.h>
#include <maya/MPointArray.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MFnMesh.h>
#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnDagNode.h>

#include <common/version_helper.h>
#include "driver_mesh_mgr.h"

//#define _DEBUG

class TestDeformer : public MPxDeformerNode
{
public:
	TestDeformer();
	virtual			~TestDeformer();

	static  void *	creator();

	static  MStatus	initialize();

	// this function can be removed later.
	// add the attribues of maya BlendShape node into my node
	static  MStatus initialize_mayaBlendshape();

	virtual void	postConstructor();
    virtual MStatus deform(MDataBlock& data,
                          MItGeometry& itDvnGeo,
                          const MMatrix& localToWorldMatrix,
                          unsigned int multiIndex);

	static MString          cTypeName();
	static MTypeId          cTypeId();
	static MPxNode::Type    cType();
	static const MString&   cClassification();

private:
//    void initVertMapping(MDataBlock& data,
//                          MItGeometry& itDvnGeo,
//                          const MMatrix& localToWorldMatrix,
//                          unsigned int mIndex);
//    int getClosestPt(const MPoint &pt, const MPointArray &points);

    // accumulate the deform values of one driver mesh to dvrPos[]
    void _deform_on_one_mesh(MDataBlock& data,
                        const std::size_t dvrIdx,
                        const MPointArray &dvnOriPos,
                        MPointArray &accDeform
                        );

    // gather connected driver meshes to a MObjectArray
    void gatherDriverMeshes(MDataBlock& data, MObjectArray& srcDriverMeshs);
    void setVtxIdxToPlug(MDataBlock& data, const MObjectArray& srcDriverMeshs);


protected:
	//static MTypeId   m_id;
	static MString   m_classification;
	static VersionHelper    m_version_helper;

	static MObject  aMethodOfMappingVertexIndex;// how to map vertex index? use UV or point position to map vertex index
	static MObject  aInitializedData;
//	static MObject  aVertMap;

/**
drivers
|
+---drivers[0]
|   +---mesh            <-------(connect to)---  driverShape1(mesh)
|   +---objectWeight    0.8
|   +---vtxIdx          [4, 6]
|   +---vtxWeight       [0.0, 0.2]
|
+---drivers[1]
|   +---mesh            <-------(connect to)---  driverShape2(mesh)
|   +---objectWeight    0.9
|   +---vtxIdx          [5, 7, 9]
|   +---vtxWeight       [0.1, 0.3, 0.5]
|
+---drivers[2]
    + ...

How to caculate the vertex position of driven mesh:
DvnPos[0] = 0.8*Dvr0Pos[4]*0.0 + 0.9*Dvr1Pos[5]*0.1
DvnPos[1] = 0.8*Dvr0Pos[6]*0.2 + 0.9*Dvr1Pos[7]*0.3
DvnPos[2] =                      0.9*Dvr1Pos[9]*0.5

(where
DvnPos  stands for Driven Mesh's  Vertex Position Buffer
Dvr0Pos stands for Driver Mesh0's Vertex Position Buffer
Dvr1Pos stands for Driver Mesh1's Vertex Position Buffer)
*/
	static MObject  aDrivers;
	static MObject      aMesh;// this attribute will be connected with a mesh node which is considered as a deform driver.
	static MObject      aVtxIdx;// "aVtxIdx[i]=j" means that i-th vertex of the driven mesh is mapped to j-th of the driver mesh
	static MObject      aObjectWeight;// weight for each driver object. this weight will be added to every vertex
	static MObject      aVtxWeight;// painted weight for each vertex

	DriverMeshMgr m_driverMeshMgr;

	// maya blendshape attributes
	// These attribues are not used, and can be removed.
	static MObject  aTopologyCheck;
	static MObject  aWeight;
	static MObject  aInputTarget;
    static MObject    	  aInputTargetGroup;
    static MObject              aInputTargetItem;
    static MObject                  aInputGeomTarget;
    static MObject                  aInputPointsTarget;
    static MObject                  aInputComponentsTarget;
    static MObject              aTargetWeights;
    static MObject              aNormalizationId;
    static MObject    	  aBaseWeights;
    static MObject    	  aNormalizationGroup;
    static MObject              aNormalizationUseWeights;
    static MObject              aNormalizationWeights;
    static MObject    	  aPaintTargetWeights;
    static MObject    	  aPaintTargetIndex;
	static MObject  aOrigin;
	static MObject  aBaseOrigin;
	static MObject  aTargetOrigin;
	static MObject  aParallelBlender;
	static MObject  aUseTargetCompWeights;
	static MObject  aSupportNegativeWeights;
	static MObject  aPaintWeights;




};
