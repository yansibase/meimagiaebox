
#include "partial_blendshape.h"

#include <cfloat>
#include <cassert>
#include <vector>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MPlugArray.h>
#include <common/log.h>
#include <common/node_ids.h>
#include "version.h"

#include "driver_mesh.h"

// For local testing of nodes you can use any identifier between
// 0x00000000 and 0x0007ffff, but for any node that you plan to use for
// more permanent purposes, you should get a universally unique id from
// Autodesk Support. You will be assigned a unique range that you
// can manage on your own.
//
//MTypeId TestDeformer::m_id( 0x7269b );
MString TestDeformer::m_classification("utility/general");
VersionHelper   TestDeformer::m_version_helper;
///////////////////////////////////////////////////////
// DESCRIPTION: attribute information
///////////////////////////////////////////////////////
//
//MObject  TestDeformer::aDriverMesh;
MObject  TestDeformer::aMethodOfMappingVertexIndex;
MObject  TestDeformer::aInitializedData;
//MObject  TestDeformer::aVertMap;
MObject  TestDeformer::aDrivers;
MObject  TestDeformer::aMesh;
MObject  TestDeformer::aVtxIdx;
MObject  TestDeformer::aObjectWeight;
MObject  TestDeformer::aVtxWeight;
// maya blendshape attributes
// These attribues are not used, and can be removed.
MObject  TestDeformer::aTopologyCheck;
MObject  TestDeformer::aWeight;
MObject  TestDeformer::aInputTarget;
    MObject  TestDeformer::aInputTargetGroup;
        MObject  TestDeformer::aInputTargetItem;
            MObject  TestDeformer::aInputGeomTarget;
            MObject  TestDeformer::aInputPointsTarget;
            MObject  TestDeformer::aInputComponentsTarget;
        MObject  TestDeformer::aTargetWeights;
        MObject  TestDeformer::aNormalizationId;
        MObject  TestDeformer::aBaseWeights;
        MObject  TestDeformer::aNormalizationGroup;
            MObject  TestDeformer::aNormalizationUseWeights;
            MObject  TestDeformer::aNormalizationWeights;
        MObject  TestDeformer::aPaintTargetWeights;
        MObject  TestDeformer::aPaintTargetIndex;
MObject  TestDeformer::aOrigin;
MObject  TestDeformer::aBaseOrigin;
MObject  TestDeformer::aTargetOrigin;
MObject  TestDeformer::aParallelBlender;
MObject  TestDeformer::aUseTargetCompWeights;
MObject  TestDeformer::aSupportNegativeWeights;
MObject  TestDeformer::aPaintWeights;
//
void TestDeformer::postConstructor( )
{
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}
//
MString TestDeformer::cTypeName()
{
	return NodeTypeName_PartialBlendShape;
}
//
MTypeId TestDeformer::cTypeId()
{
	return NodeID_PartialBlendShape;
}
//
MPxNode::Type TestDeformer::cType()
{
	return MPxNode::kDeformerNode;
}
//
const MString& TestDeformer::cClassification()
{
	return m_classification;
}
//
TestDeformer::TestDeformer()
: MPxDeformerNode()
{
}
//
TestDeformer::~TestDeformer()
{
}
//
void* TestDeformer::creator()
{
	return new TestDeformer();
}
//
MStatus TestDeformer::initialize()
{
	MFnNumericAttribute numericAttr;
	MFnTypedAttribute polyMeshAttr;
	MFnEnumAttribute enumAttr;

	MStatus status; // Status will be used to hold the MStatus value

    // use UV or point position to map vertex index
	aMethodOfMappingVertexIndex = enumAttr.create( "mapmethod", "mmvi", MMVI_ObjectSpace/*default*/, &status );
	CHECK_MSTATUS( status );
	CHECK_MSTATUS( enumAttr.addField("point position in object space", MMVI_ObjectSpace) );
	CHECK_MSTATUS( enumAttr.addField("uv",                             MMVI_UVSpace) );
	CHECK_MSTATUS( enumAttr.setKeyable(true) );
	CHECK_MSTATUS( enumAttr.setStorable(true) );
	CHECK_MSTATUS( enumAttr.setReadable(true) );
	CHECK_MSTATUS( enumAttr.setWritable(true) );
	CHECK_MSTATUS( enumAttr.setDefault(MMVI_ObjectSpace) );
	CHECK_MSTATUS( addAttribute( aMethodOfMappingVertexIndex ) );
	CHECK_MSTATUS( attributeAffects( aMethodOfMappingVertexIndex, outputGeom ) );

	// initialize is used to mark this node's state
	aInitializedData = enumAttr.create( "initialize", "inl", 0/*default*/, &status );
	CHECK_MSTATUS( status );
	CHECK_MSTATUS( enumAttr.addField("no deform effect",          0) );
	CHECK_MSTATUS( enumAttr.addField("init vertex index mapping", 1) );
	CHECK_MSTATUS( enumAttr.addField("show deform effect",        2) );
	CHECK_MSTATUS( enumAttr.setKeyable(true) );
	CHECK_MSTATUS( enumAttr.setStorable(true) );
	CHECK_MSTATUS( enumAttr.setReadable(true) );
	CHECK_MSTATUS( enumAttr.setWritable(true) );
	CHECK_MSTATUS( enumAttr.setDefault(0) );
	CHECK_MSTATUS( addAttribute( aInitializedData ) );
	CHECK_MSTATUS( attributeAffects( aInitializedData, outputGeom ) );

    // hold the vertex index mapping
//	aVertMap = numericAttr.create( "vtxIndexMap", "vtximp", MFnNumericData::kLong, 0/*default*/, &status );
//	CHECK_MSTATUS( status );
//    CHECK_MSTATUS( numericAttr.setKeyable(false) );
//	CHECK_MSTATUS( numericAttr.setArray(true) );
//	CHECK_MSTATUS( numericAttr.setStorable(true) );
//	CHECK_MSTATUS( numericAttr.setReadable(true) );
//	CHECK_MSTATUS( numericAttr.setWritable(true) );
//	CHECK_MSTATUS( addAttribute( aVertMap ) );
//	CHECK_MSTATUS( attributeAffects( aVertMap, outputGeom ) );

    {
        {
            MFnTypedAttribute tAttr;
            aMesh = tAttr.create( "mesh", "msh", MFnData::kMesh, &status ); CHECK_MSTATUS(status);
            CHECK_MSTATUS(tAttr.setArray(false));
            CHECK_MSTATUS(tAttr.setConnectable(true));
            CHECK_MSTATUS(tAttr.setKeyable(false));
            CHECK_MSTATUS(tAttr.setStorable(true));
            CHECK_MSTATUS(tAttr.setReadable(true));
            CHECK_MSTATUS(tAttr.setWritable(true));
            CHECK_MSTATUS(tAttr.setDisconnectBehavior(MFnAttribute::kDelete));
        }
        {
            MFnNumericAttribute nAttr;
            // why we change the default value from 0 to -1?
            // If we use the 0 as the default value, 0 will not be saved to vtxidx[] in *.ma file.
            // e.g.
            //     setAttr -s 4 ".dvr[1].vtxidx[0:3]"  0 1 2 3
            // will be saved as
            //     setAttr -s 4 ".dvr[1].vtxidx[1:3]"  1 2 3;
            // Then when we reopen *.ma file, the default 0 values in vtxidx[] will not be accessable in C++. See ticket #826.
            // And, why we choose -1 as the default value? Because vertex index can never be a negetive number,
            // all vertex index values will be saved into *.ma file.
            aVtxIdx = nAttr.create( "vertexIndex", "vtxidx", MFnNumericData::kLong, -1/*default*/, &status ); CHECK_MSTATUS( status );
            CHECK_MSTATUS(nAttr.setArray(true));
            CHECK_MSTATUS(nAttr.setConnectable(false));
            CHECK_MSTATUS(nAttr.setKeyable(false));
            CHECK_MSTATUS(nAttr.setStorable(true));
            CHECK_MSTATUS(nAttr.setReadable(true));
            CHECK_MSTATUS(nAttr.setWritable(true));
            CHECK_MSTATUS(nAttr.setUsesArrayDataBuilder(true));
        }
        {
            MFnNumericAttribute nAttr;
            aObjectWeight = nAttr.create( "objectWeight", "ow", MFnNumericData::kFloat, 1.0, &status ); CHECK_MSTATUS( status );
            //CHECK_MSTATUS(nAttr.setArray(false));
            //CHECK_MSTATUS(nAttr.setConnectable(true));
            CHECK_MSTATUS(nAttr.setKeyable(true));
            //CHECK_MSTATUS(nAttr.setStorable(true));
            //CHECK_MSTATUS(nAttr.setReadable(true));
            //CHECK_MSTATUS(nAttr.setWritable(true));
        }
        {
            MFnNumericAttribute nAttr;
            aVtxWeight = nAttr.create("vertexWeight", "vtxw", MFnNumericData::kFloat, 1.0, &status); CHECK_MSTATUS( status );
            CHECK_MSTATUS(nAttr.setArray(true));
            CHECK_MSTATUS(nAttr.setConnectable(true));
            CHECK_MSTATUS(nAttr.setKeyable(true));
            CHECK_MSTATUS(nAttr.setStorable(true));
            CHECK_MSTATUS(nAttr.setReadable(true));
            CHECK_MSTATUS(nAttr.setWritable(true));
            CHECK_MSTATUS(nAttr.setUsesArrayDataBuilder(true));
            // setIndexMatters() will only affect array attributes with setReadable set to false,
            // i.e. destination attributes. We have set the default value to an unlikely value
            // to guarantee an entry is created regardless of its value.
            // nAttr.setIndexMatters(true);
        }

        // Compound target(geometry,weight): array, delete on disconnect
        MFnCompoundAttribute cAttr;
        aDrivers = cAttr.create( "drivers", "dvr",&status ); CHECK_MSTATUS(status);
        CHECK_MSTATUS(cAttr.addChild(aMesh));
        CHECK_MSTATUS(cAttr.addChild(aVtxIdx));
        CHECK_MSTATUS(cAttr.addChild(aObjectWeight));
        CHECK_MSTATUS(cAttr.addChild(aVtxWeight));

        CHECK_MSTATUS(cAttr.setArray(true));
        CHECK_MSTATUS(cAttr.setConnectable(true));
        CHECK_MSTATUS(cAttr.setKeyable(false));
        CHECK_MSTATUS(cAttr.setStorable(true));
        CHECK_MSTATUS(cAttr.setReadable(true));
        CHECK_MSTATUS(cAttr.setWritable(true));
        CHECK_MSTATUS(cAttr.setUsesArrayDataBuilder(true));
        CHECK_MSTATUS(cAttr.setDisconnectBehavior(MFnAttribute::kDelete));
    }
    CHECK_MSTATUS(addAttribute(aDrivers));
    CHECK_MSTATUS(attributeAffects(aDrivers,        outputGeom));
    CHECK_MSTATUS(attributeAffects(aMesh,           outputGeom));
    CHECK_MSTATUS(attributeAffects(aVtxIdx,         outputGeom));
    CHECK_MSTATUS(attributeAffects(aObjectWeight,   outputGeom));
    CHECK_MSTATUS(attributeAffects(aVtxWeight,      outputGeom));

	initialize_mayaBlendshape();

	m_version_helper.initialize();

	// make some attributes paintalbe.
	// makePaintable can be run in the initialize function of the node itself or from the command that
    // creates the node and sets its connections. We choose the second option.(see doc/skinningAtILM.pdf)
	CHECK_MSTATUS( MGlobal::executePythonCommand("import maya.cmds; maya.cmds.makePaintable('"+TestDeformer::cTypeName()+"', 'weights',             attrType='multiFloat', sm='deformer')") );
	CHECK_MSTATUS( MGlobal::executePythonCommand("import maya.cmds; maya.cmds.makePaintable('"+TestDeformer::cTypeName()+"', 'baseWeights',         attrType='multiFloat', sm='deformer')") );
	CHECK_MSTATUS( MGlobal::executePythonCommand("import maya.cmds; maya.cmds.makePaintable('"+TestDeformer::cTypeName()+"', 'paintTargetWeights',  attrType='multiFloat', sm='deformer')") );


	return( MS::kSuccess );
}
// this function can be removed later
// add the attribues of maya BlendShape node into my node
MStatus TestDeformer::initialize_mayaBlendshape()
{
    MStatus status;

    //{// topologyCheck
        MFnNumericAttribute nAttr;
        aTopologyCheck = nAttr.create( "topologyCheck", "tc", MFnNumericData::kBoolean, true, &status); CHECK_MSTATUS( status );
        CHECK_MSTATUS(nAttr.setArray(false));
        CHECK_MSTATUS(nAttr.setReadable(true));
        CHECK_MSTATUS(nAttr.setWritable(true));
        CHECK_MSTATUS(nAttr.setConnectable(true));
        CHECK_MSTATUS(nAttr.setStorable(true));
        CHECK_MSTATUS(nAttr.setKeyable(false));

        CHECK_MSTATUS(addAttribute(aTopologyCheck));
        CHECK_MSTATUS(attributeAffects(aTopologyCheck,        outputGeom));
    //}// topologyCheck

//    {// weight(but MPxDeformerNode already has attribute "w")
//        MFnNumericAttribute nAttr;
//        aWeight = nAttr.create( "weight", "w", MFnNumericData::kFloat, 0.0f, &status ); CHECK_MSTATUS( status );
//        CHECK_MSTATUS(nAttr.setArray(true));
//        CHECK_MSTATUS(nAttr.setReadable(true));
//        CHECK_MSTATUS(nAttr.setWritable(true));
//        CHECK_MSTATUS(nAttr.setConnectable(true));
//        CHECK_MSTATUS(nAttr.setStorable(true));
//        CHECK_MSTATUS(nAttr.setKeyable(true));
//        CHECK_MSTATUS(nAttr.setUsesArrayDataBuilder(true));
//        CHECK_MSTATUS(nAttr.setDisconnectBehavior(MFnAttribute::kDelete));
//
//        CHECK_MSTATUS(addAttribute(aWeight));
//        CHECK_MSTATUS(attributeAffects(aWeight,        outputGeom));
//    }// weight

    {// inputTarget
        {// inputTargetGroup
            {// inputTargetItem
                {// inputGeomTarget
                    MFnTypedAttribute tAttr;
                    aInputGeomTarget = tAttr.create( "inputGeomTarget", "igt", MFnData::kMesh, &status); CHECK_MSTATUS(status);
                    CHECK_MSTATUS(tAttr.setArray(false));
                    CHECK_MSTATUS(tAttr.setReadable(true));
                    CHECK_MSTATUS(tAttr.setWritable(true));
                    CHECK_MSTATUS(tAttr.setConnectable(true));
                    CHECK_MSTATUS(tAttr.setStorable(false));
                    CHECK_MSTATUS(tAttr.setKeyable(false));

                    CHECK_MSTATUS(addAttribute(aInputGeomTarget));
                    CHECK_MSTATUS(attributeAffects(aInputGeomTarget,  outputGeom));
                }// inputGeomTarget
                {// inputPointsTarget
                    MFnTypedAttribute tAttr;
                    aInputPointsTarget = tAttr.create( "inputPointsTarget", "ipt", MFnData::kPointArray, &status); CHECK_MSTATUS(status);
                    CHECK_MSTATUS(tAttr.setArray(false));
                    CHECK_MSTATUS(tAttr.setReadable(true));
                    CHECK_MSTATUS(tAttr.setWritable(true));
                    CHECK_MSTATUS(tAttr.setConnectable(true));
                    CHECK_MSTATUS(tAttr.setStorable(true));
                    CHECK_MSTATUS(tAttr.setKeyable(false));

                    CHECK_MSTATUS(addAttribute(aInputPointsTarget));
                    CHECK_MSTATUS(attributeAffects(aInputPointsTarget,  outputGeom));
                }// inputPointsTarget
                {// inputComponentsTarget
                    MFnTypedAttribute tAttr;
                    aInputComponentsTarget = tAttr.create( "inputComponentsTarget", "ict", MFnData:: kComponentList, &status); CHECK_MSTATUS(status);
                    CHECK_MSTATUS(tAttr.setArray(false));
                    CHECK_MSTATUS(tAttr.setReadable(true));
                    CHECK_MSTATUS(tAttr.setWritable(true));
                    CHECK_MSTATUS(tAttr.setConnectable(true));
                    CHECK_MSTATUS(tAttr.setStorable(true));
                    CHECK_MSTATUS(tAttr.setKeyable(false));

                    CHECK_MSTATUS(addAttribute(aInputComponentsTarget));
                    CHECK_MSTATUS(attributeAffects(aInputComponentsTarget,  outputGeom));
                }// inputComponentsTarget
                MFnCompoundAttribute cAttr;
                aInputTargetItem = cAttr.create( "inputTargetItem", "iti", &status ); CHECK_MSTATUS(status);
                CHECK_MSTATUS(cAttr.setArray(true));
                CHECK_MSTATUS(cAttr.setReadable(true));
                CHECK_MSTATUS(cAttr.setWritable(true));
                CHECK_MSTATUS(cAttr.setConnectable(true));
                CHECK_MSTATUS(cAttr.setStorable(true));
                CHECK_MSTATUS(cAttr.setKeyable(false));
                CHECK_MSTATUS(cAttr.setUsesArrayDataBuilder(true));
                CHECK_MSTATUS(cAttr.setDisconnectBehavior(MFnAttribute::kDelete));

                CHECK_MSTATUS(cAttr.addChild(aInputGeomTarget));
                CHECK_MSTATUS(cAttr.addChild(aInputPointsTarget));
                CHECK_MSTATUS(cAttr.addChild(aInputComponentsTarget));

                CHECK_MSTATUS(addAttribute(aInputTargetItem));
                CHECK_MSTATUS(attributeAffects(aInputTargetItem,  outputGeom));
            }// inputTargetItem
            {// targetWeights
                MFnNumericAttribute nAttr;
                aTargetWeights = nAttr.create( "targetWeights", "tw", MFnNumericData::kFloat, 1.0f, &status ); CHECK_MSTATUS( status );
                CHECK_MSTATUS(nAttr.setArray(true));
                CHECK_MSTATUS(nAttr.setReadable(true));
                CHECK_MSTATUS(nAttr.setWritable(true));
                CHECK_MSTATUS(nAttr.setConnectable(true));
                CHECK_MSTATUS(nAttr.setStorable(true));
                CHECK_MSTATUS(nAttr.setKeyable(true));
                CHECK_MSTATUS(nAttr.setUsesArrayDataBuilder(true));
                CHECK_MSTATUS(nAttr.setDisconnectBehavior(MFnAttribute::kDelete));

                CHECK_MSTATUS(addAttribute(aTargetWeights));
                CHECK_MSTATUS(attributeAffects(aTargetWeights,        outputGeom));
            }// targetWeights
            {// normalizationId
                MFnNumericAttribute nAttr;
                aNormalizationId = nAttr.create( "normalizationId", "nid", MFnNumericData::kInt, 0, &status ); CHECK_MSTATUS( status );
                CHECK_MSTATUS(nAttr.setArray(false));
                CHECK_MSTATUS(nAttr.setReadable(true));
                CHECK_MSTATUS(nAttr.setWritable(true));
                CHECK_MSTATUS(nAttr.setConnectable(true));
                CHECK_MSTATUS(nAttr.setStorable(true));
                CHECK_MSTATUS(nAttr.setKeyable(false));

                CHECK_MSTATUS(addAttribute(aNormalizationId));
                CHECK_MSTATUS(attributeAffects(aNormalizationId,        outputGeom));
            }// normalizationId
            MFnCompoundAttribute cAttr;
            aInputTargetGroup = cAttr.create( "inputTargetGroup", "itg", &status ); CHECK_MSTATUS(status);
            CHECK_MSTATUS(cAttr.setArray(true));
            CHECK_MSTATUS(cAttr.setReadable(true));
            CHECK_MSTATUS(cAttr.setWritable(true));
            CHECK_MSTATUS(cAttr.setConnectable(true));
            CHECK_MSTATUS(cAttr.setStorable(true));
            CHECK_MSTATUS(cAttr.setKeyable(false));
            CHECK_MSTATUS(cAttr.setUsesArrayDataBuilder(true));
            CHECK_MSTATUS(cAttr.setDisconnectBehavior(MFnAttribute::kDelete));

            CHECK_MSTATUS(cAttr.addChild(aInputTargetItem));
            CHECK_MSTATUS(cAttr.addChild(aTargetWeights));
            CHECK_MSTATUS(cAttr.addChild(aNormalizationId));

            CHECK_MSTATUS(addAttribute(aInputTargetGroup));
            CHECK_MSTATUS(attributeAffects(aInputTargetGroup,  outputGeom));
        }// inputTargetGroup
        {// baseWeights
            MFnNumericAttribute nAttr;
            aBaseWeights = nAttr.create( "baseWeights", "bw", MFnNumericData::kFloat, 1.0f, &status ); CHECK_MSTATUS( status );
            CHECK_MSTATUS(nAttr.setArray(true));
            CHECK_MSTATUS(nAttr.setReadable(true));
            CHECK_MSTATUS(nAttr.setWritable(true));
            CHECK_MSTATUS(nAttr.setConnectable(true));
            CHECK_MSTATUS(nAttr.setStorable(true));
            CHECK_MSTATUS(nAttr.setKeyable(true));
            CHECK_MSTATUS(nAttr.setUsesArrayDataBuilder(true));
            CHECK_MSTATUS(nAttr.setDisconnectBehavior(MFnAttribute::kDelete));

            CHECK_MSTATUS(addAttribute(aBaseWeights));
            CHECK_MSTATUS(attributeAffects(aBaseWeights,        outputGeom));
        }// baseWeights
        {// normalizationGroup
            {// normalizationUseWeights
                MFnNumericAttribute nAttr;
                aNormalizationUseWeights = nAttr.create( "normalizationUseWeights", "nuw", MFnNumericData::kBoolean, false, &status ); CHECK_MSTATUS( status );
                CHECK_MSTATUS(nAttr.setArray(false));
                CHECK_MSTATUS(nAttr.setReadable(true));
                CHECK_MSTATUS(nAttr.setWritable(true));
                CHECK_MSTATUS(nAttr.setConnectable(true));
                CHECK_MSTATUS(nAttr.setStorable(true));
                CHECK_MSTATUS(nAttr.setKeyable(false));

                CHECK_MSTATUS(addAttribute(aNormalizationUseWeights));
                CHECK_MSTATUS(attributeAffects(aNormalizationUseWeights,        outputGeom));
            }// normalizationUseWeights
            {// normalizationWeights
                MFnNumericAttribute nAttr;
                aNormalizationWeights = nAttr.create( "normalizationWeights", "nw", MFnNumericData::kFloat, 1.0f, &status ); CHECK_MSTATUS( status );
                CHECK_MSTATUS(nAttr.setArray(true));
                CHECK_MSTATUS(nAttr.setReadable(true));
                CHECK_MSTATUS(nAttr.setWritable(true));
                CHECK_MSTATUS(nAttr.setConnectable(true));
                CHECK_MSTATUS(nAttr.setStorable(true));
                CHECK_MSTATUS(nAttr.setKeyable(true));
                CHECK_MSTATUS(nAttr.setUsesArrayDataBuilder(true));
                CHECK_MSTATUS(nAttr.setDisconnectBehavior(MFnAttribute::kDelete));

                CHECK_MSTATUS(addAttribute(aNormalizationWeights));
                CHECK_MSTATUS(attributeAffects(aNormalizationWeights,        outputGeom));
            }// normalizationWeights
            MFnCompoundAttribute cAttr;
            aNormalizationGroup = cAttr.create( "normalizationGroup", "ng", &status ); CHECK_MSTATUS(status);
            CHECK_MSTATUS(cAttr.setArray(true));
            CHECK_MSTATUS(cAttr.setReadable(true));
            CHECK_MSTATUS(cAttr.setWritable(true));
            CHECK_MSTATUS(cAttr.setConnectable(true));
            CHECK_MSTATUS(cAttr.setStorable(true));
            CHECK_MSTATUS(cAttr.setKeyable(false));
            CHECK_MSTATUS(cAttr.setUsesArrayDataBuilder(true));
            CHECK_MSTATUS(cAttr.setDisconnectBehavior(MFnAttribute::kDelete));

            CHECK_MSTATUS(cAttr.addChild(aNormalizationUseWeights));
            CHECK_MSTATUS(cAttr.addChild(aNormalizationWeights));

            CHECK_MSTATUS(addAttribute(aNormalizationGroup));
            CHECK_MSTATUS(attributeAffects(aNormalizationGroup,  outputGeom));
        }// normalizationGroup
        {// paintTargetWeights
            MFnNumericAttribute nAttr;
            aPaintTargetWeights = nAttr.create( "paintTargetWeights", "pwt", MFnNumericData::kFloat, 1.0f, &status ); CHECK_MSTATUS( status );
            CHECK_MSTATUS(nAttr.setArray(true));
            CHECK_MSTATUS(nAttr.setReadable(true));
            CHECK_MSTATUS(nAttr.setWritable(true));
            CHECK_MSTATUS(nAttr.setConnectable(true));
            CHECK_MSTATUS(nAttr.setStorable(false));
            CHECK_MSTATUS(nAttr.setKeyable(false));
            CHECK_MSTATUS(nAttr.setHidden(true));
            CHECK_MSTATUS(nAttr.setUsesArrayDataBuilder(true));
            CHECK_MSTATUS(nAttr.setDisconnectBehavior(MFnAttribute::kDelete));

            CHECK_MSTATUS(addAttribute(aPaintTargetWeights));
            CHECK_MSTATUS(attributeAffects(aPaintTargetWeights,        outputGeom));
        }// paintTargetWeights
        {// paintTargetIndex
            MFnNumericAttribute nAttr;
            aPaintTargetIndex = nAttr.create( "paintTargetIndex", "pti", MFnNumericData::kInt, 0, &status ); CHECK_MSTATUS( status );
            CHECK_MSTATUS(nAttr.setArray(false));
            CHECK_MSTATUS(nAttr.setReadable(true));
            CHECK_MSTATUS(nAttr.setWritable(true));
            CHECK_MSTATUS(nAttr.setConnectable(true));
            CHECK_MSTATUS(nAttr.setStorable(false));
            CHECK_MSTATUS(nAttr.setKeyable(false));
            CHECK_MSTATUS(nAttr.setHidden(true));

            CHECK_MSTATUS(addAttribute(aPaintTargetIndex));
            CHECK_MSTATUS(attributeAffects(aPaintTargetIndex,        outputGeom));
        }// paintTargetIndex
        MFnCompoundAttribute cAttr;
        aInputTarget = cAttr.create( "inputTarget", "it", &status ); CHECK_MSTATUS(status);
        CHECK_MSTATUS(cAttr.setArray(true));
        CHECK_MSTATUS(cAttr.setReadable(true));
        CHECK_MSTATUS(cAttr.setWritable(true));
        CHECK_MSTATUS(cAttr.setConnectable(true));
        CHECK_MSTATUS(cAttr.setStorable(true));
        CHECK_MSTATUS(cAttr.setKeyable(false));
        CHECK_MSTATUS(cAttr.setUsesArrayDataBuilder(true));
        CHECK_MSTATUS(cAttr.setDisconnectBehavior(MFnAttribute::kDelete));

        CHECK_MSTATUS(cAttr.addChild(aInputTargetGroup));
        CHECK_MSTATUS(cAttr.addChild(aBaseWeights));
        CHECK_MSTATUS(cAttr.addChild(aNormalizationGroup));
        CHECK_MSTATUS(cAttr.addChild(aPaintTargetWeights));
        CHECK_MSTATUS(cAttr.addChild(aPaintTargetIndex));

        CHECK_MSTATUS(addAttribute(aInputTarget));
        CHECK_MSTATUS(attributeAffects(aInputTarget,  outputGeom));
    }// inputTarget
    {// origin
        MFnEnumAttribute eAttr;
        aOrigin = eAttr.create( "origin", "or", 1, &status );   CHECK_MSTATUS( status );
        CHECK_MSTATUS(eAttr.addField("world",  0));
        CHECK_MSTATUS(eAttr.addField("local",  1));
        CHECK_MSTATUS(eAttr.addField("user",   2));

        CHECK_MSTATUS(eAttr.setArray(false));
        CHECK_MSTATUS(eAttr.setReadable(true));
        CHECK_MSTATUS(eAttr.setWritable(true));
        CHECK_MSTATUS(eAttr.setConnectable(true));
        CHECK_MSTATUS(eAttr.setStorable(true));
        CHECK_MSTATUS(eAttr.setKeyable(false));

        CHECK_MSTATUS(addAttribute(aOrigin));
        CHECK_MSTATUS(attributeAffects(aOrigin,   outputGeom));
    }// origin
    {// baseOrigin
        MFnNumericAttribute nAttr;
        aBaseOrigin = nAttr.createPoint( "baseOrigin", "bo", &status ); CHECK_MSTATUS( status );
        CHECK_MSTATUS(nAttr.setArray(false));
        CHECK_MSTATUS(nAttr.setReadable(true));
        CHECK_MSTATUS(nAttr.setWritable(true));
        CHECK_MSTATUS(nAttr.setConnectable(true));
        CHECK_MSTATUS(nAttr.setStorable(true));
        CHECK_MSTATUS(nAttr.setKeyable(false));
        CHECK_MSTATUS(nAttr.setDefault(0.0f, 0.0f, 0.0f));

        CHECK_MSTATUS(addAttribute(aBaseOrigin));
        CHECK_MSTATUS(attributeAffects(aBaseOrigin,        outputGeom));
    }// baseOrigin
    {// targetOrigin
        MFnNumericAttribute nAttr;
        aTargetOrigin = nAttr.createPoint( "targetOrigin", "to", &status ); CHECK_MSTATUS( status );
        CHECK_MSTATUS(nAttr.setArray(false));
        CHECK_MSTATUS(nAttr.setReadable(true));
        CHECK_MSTATUS(nAttr.setWritable(true));
        CHECK_MSTATUS(nAttr.setConnectable(true));
        CHECK_MSTATUS(nAttr.setStorable(true));
        CHECK_MSTATUS(nAttr.setKeyable(false));
        CHECK_MSTATUS(nAttr.setDefault(0.0f, 0.0f, 0.0f));

        CHECK_MSTATUS(addAttribute(aTargetOrigin));
        CHECK_MSTATUS(attributeAffects(aTargetOrigin,        outputGeom));
    }// targetOrigin
    {// parallelBlender
        MFnNumericAttribute nAttr;
        aParallelBlender = nAttr.create("parallelBlender", "pb", MFnNumericData::kBoolean, false, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setArray(false));
        CHECK_MSTATUS(nAttr.setReadable(true));
        CHECK_MSTATUS(nAttr.setWritable(true));
        CHECK_MSTATUS(nAttr.setConnectable(false));
        CHECK_MSTATUS(nAttr.setStorable(true));
        CHECK_MSTATUS(nAttr.setKeyable(false));
        CHECK_MSTATUS(nAttr.setHidden(true));

        CHECK_MSTATUS(addAttribute(aParallelBlender));
        CHECK_MSTATUS(attributeAffects(aParallelBlender,        outputGeom));
    }// parallelBlender
    {// useTargetCompWeights
        MFnNumericAttribute nAttr;
        aUseTargetCompWeights = nAttr.create("useTargetCompWeights", "itcw", MFnNumericData::kBoolean, true, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setArray(false));
        CHECK_MSTATUS(nAttr.setReadable(true));
        CHECK_MSTATUS(nAttr.setWritable(true));
        CHECK_MSTATUS(nAttr.setConnectable(false));
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setKeyable(false));
        CHECK_MSTATUS(nAttr.setHidden(true));

        CHECK_MSTATUS(addAttribute(aUseTargetCompWeights));
        CHECK_MSTATUS(attributeAffects(aUseTargetCompWeights,        outputGeom));
    }// useTargetCompWeights
    {// supportNegativeWeights
        MFnNumericAttribute nAttr;
        aSupportNegativeWeights = nAttr.create("supportNegativeWeights", "sn", MFnNumericData::kBoolean, false, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setArray(false));
        CHECK_MSTATUS(nAttr.setReadable(true));
        CHECK_MSTATUS(nAttr.setWritable(true));
        CHECK_MSTATUS(nAttr.setConnectable(false));
        CHECK_MSTATUS(nAttr.setStorable(true));
        CHECK_MSTATUS(nAttr.setKeyable(false));
        CHECK_MSTATUS(nAttr.setHidden(true));

        CHECK_MSTATUS(addAttribute(aSupportNegativeWeights));
        CHECK_MSTATUS(attributeAffects(aSupportNegativeWeights,        outputGeom));
    }// supportNegativeWeights
    {// paintWeights
        MFnTypedAttribute tAttr;
        aPaintWeights = tAttr.create( "paintWeights", "ptw", MFnData::kDoubleArray, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(tAttr.setArray(false));
        CHECK_MSTATUS(tAttr.setReadable(true));
        CHECK_MSTATUS(tAttr.setWritable(true));
        CHECK_MSTATUS(tAttr.setConnectable(true));
        CHECK_MSTATUS(tAttr.setStorable(false));
        CHECK_MSTATUS(tAttr.setKeyable(false));
        CHECK_MSTATUS(tAttr.setHidden(true));

        CHECK_MSTATUS(addAttribute(aPaintWeights));
        CHECK_MSTATUS(attributeAffects(aPaintWeights,  outputGeom));
    }// paintWeights

	return( MS::kSuccess );
}
//
MStatus TestDeformer::deform(MDataBlock& data,
                          MItGeometry& itDvnGeo,// iterator of the driven geometry
                          const MMatrix& localToWorldMatrix,
                          unsigned int multiIndex)
{
    LDbg("---------------------------------------------");
    MStatus status;

    // how to map vertex index? use UV or point position to map vertex index
    short methodOfMappingVertexIndex = data.inputValue( aMethodOfMappingVertexIndex, &status).asShort();
    CHECK_MSTATUS(status);

    // get the current node state
    short initialized_mapping = data.inputValue( aInitializedData, &status).asShort();
    CHECK_MSTATUS(status);

    // DEBUG
    //LDbg("methodOfMappingVertexIndex=%d, initialized_mapping=%d, multiIndex=%d", methodOfMappingVertexIndex, initialized_mapping, multiIndex);

    if( initialized_mapping == 0 )
    {
        // todo ...
    }// if( initialized_mapping == 0 )
    else if( initialized_mapping == 1 )
    {
        // see /test/test_0006/test.py.
        // After you add driver mesh(es) and set PBS state to 1,
        // the program will go here.

        /// 1. init
        // Why we need the orig node?
        // Because we use the distance to map the vertex from driver mesh to driven mesh,
        // and the driven mesh shape node is deformed by driver mesh.
        m_driverMeshMgr.setupDrivenMeshOrig();

        /// 1.1 gather driver meshes
        // Since you add driver meshes, let's gather these driver meshes into srcDriverMeshs.
        MObjectArray srcDriverMeshs;
        gatherDriverMeshes(data, srcDriverMeshs);

        /// 1.2 get driven mesh node
        MObject dvnMesh;

        MPlug pOutputGeom(thisMObject(), outputGeom);
        //LDbg("pOutputGeom.numElements()=%d", pOutputGeom.numElements());
        if(pOutputGeom.numElements() > 0)
        {
            MPlugArray dvnMeshesPlug;
            if(pOutputGeom[0].connectedTo(dvnMeshesPlug, false/*asDst*/, true/*asSrc*/, &status))// PBS.outputGeom[0] ----> ?
            {
                assert(dvnMeshesPlug.length() >= 1); // it is connected to driven mesh

                MPlug dvnMeshPlug(dvnMeshesPlug[0]);
                dvnMesh = dvnMeshPlug.node(&status);     CHECK_MSTATUS(status);
                //printMeshInfo("dvn mesh", dvnMesh);
            }
        }

        /// 1.3 init vtxindex data to DriverMesh::m_v[]
        // construct DriverMesh data from MObject and save them to m_driverMeshMgr
        m_driverMeshMgr.clearAllDriverMesh();
        for(std::size_t i=0; i<srcDriverMeshs.length(); ++i)
        {
            m_driverMeshMgr.addDriverMesh(srcDriverMeshs[i], dvnMesh, methodOfMappingVertexIndex);
        }

        /// 1.4 transfer DriverMesh::m_v[] to this node's plug:vertexIndex
        setVtxIdxToPlug(data, srcDriverMeshs);
    }// if( initialized_mapping == 1 )
    else if( initialized_mapping == 2 )
    {
        // see /test/test_0006/test.py.
        // After you add driver mesh(es) and set PBS state to 2,
        // the program will go here.

        // Debug:
//        LDbg("buffer begin----------------------");
//        for(int objIdx=0; objIdx<3; ++objIdx)
//        {
//            for(int vtxIdx=0; vtxIdx<7; ++vtxIdx)
//            {
//                float vtxWeight = weightValue(data, objIdx, vtxIdx); //painted weight
//                LDbg("w(%d, %d)=%f", objIdx,  vtxIdx, vtxWeight);
//            }
//        }
//        LDbg("buffer end  ----------------------");


        envelope = MPxDeformerNode::envelope;
        MDataHandle envelopeHandle = data.inputValue( envelope, &status );
        CHECK_MSTATUS( status );

        //MArrayDataHandle vertMapArrayData  = data.inputArrayValue( aVertMap, &status  );
        //CHECK_MSTATUS( status );



        /// 0. init dvnOriPos[] to driven mesh's original position
        // set mesh orig node if it is not ready
        if(m_driverMeshMgr.getDrivenMeshOrig() == MObject::kNullObj)
        {
            LErr("DriverMeshMgr::m_drivenShapeOrig is not ready. call setupDrivenMeshOrig().\n");
            if (MS::kNotFound == m_driverMeshMgr.setupDrivenMeshOrig())
            {
                return MS::kFailure;
            }
        }
        MPointArray dvnOriPos;// driven mesh's original position (object space)
        m_driverMeshMgr.getDrivenMeshOrigPos(dvnOriPos);
        // Debug
        //std::cout << "dvnOriPos:\n"<< dvnOriPos << std::endl;

        /// 1. init accDeform[] to zero
        // accumulated deformed values from each driver mesh (object space),
        // and the driven mesh will be deformed by this value
        MPointArray accDeform(itDvnGeo.count(), MPoint::origin);


        itDvnGeo.reset();

        MArrayDataHandle driversHandle = data.inputArrayValue( aDrivers, &status );
        CHECK_MSTATUS( status );
        int numDrivers = driversHandle.elementCount();
        //LDbg("numDrivers=%d", numDrivers);

        //CHECK_MSTATUS(meshAttrHandle.jumpToElement(0));
        /// 2. accumulate deform value of each driver mesh to accDeform[]
        for( int dvrIdx=0; dvrIdx < numDrivers; ++dvrIdx )
        {
            // the index of the driver mesh
            //LDbg("dvrIdx=%d", dvrIdx);

            _deform_on_one_mesh(data, dvrIdx, dvnOriPos, accDeform);

        }// for each driver mesh

        // Debug
        //std::cout << "accDeform:\n"<< accDeform << std::endl;

        /// 3. use accDeform[] to deform the driven mesh
        int i = 0;
        itDvnGeo.reset();
        while( !itDvnGeo.isDone(&status) )
        {
            MPoint p = itDvnGeo.position(MSpace::kObject, &status);
            CHECK_MSTATUS(status);

            // add the deform value to this vertex
            CHECK_MSTATUS(itDvnGeo.setPosition( dvnOriPos[i] + accDeform[i] ));

            CHECK_MSTATUS(itDvnGeo.next());
            ++i;
        }
    }// if( initialized_mapping == 2 )

	return( MS::kSuccess );
}
//
void TestDeformer::_deform_on_one_mesh(MDataBlock& data,
                        const std::size_t dvrIdx,
                        const MPointArray &dvnOriPos,
                        MPointArray &accDeform
                        )
{
    MStatus status;

    MArrayDataHandle driversHandle = data.inputArrayValue( aDrivers, &status ); CHECK_MSTATUS(status);
    //LDbg("srcDriverMeshs.length()(%d) == driversHandle.elementCount()(%d)", srcDriverMeshs.length(), driversHandle.elementCount());
    /**
        drivers
        |
        +---drivers[0]
        |   +---mesh            <-------(connect to)---  driverShape1(mesh)
        |   +---objectWeight    0.8
        |   +---vtxIdx          [4, 6]
        |   +---vtxWeight       [0.0, 0.2]
        |
        +---drivers[1]
        |   +---mesh            <-------(connect to)---  driverShape2(mesh)
        |   +---objectWeight    0.9
        |   +---vtxIdx          [5, 7, 9]
        |   +---vtxWeight       [0.1, 0.3, 0.5]
        |
        +---drivers[2]
            + ...

        How to caculate the vertex position of driven mesh:
        DvnPos[0] = 0.8*Dvr0Pos[4]*0.0 + 0.9*Dvr1Pos[5]*0.1
        DvnPos[1] = 0.8*Dvr0Pos[6]*0.2 + 0.9*Dvr1Pos[7]*0.3
        DvnPos[2] =                      0.9*Dvr1Pos[9]*0.5

        (where
        DvnPos  stands for Driven Mesh's  Vertex Position Buffer
        Dvr0Pos stands for Driver Mesh0's Vertex Position Buffer
        Dvr1Pos stands for Driver Mesh1's Vertex Position Buffer)
    */
    // drivers[i]
    CHECK_MSTATUS(driversHandle.jumpToArrayElement(dvrIdx));
    MDataHandle driver_i = driversHandle.inputValue(&status);
    CHECK_MSTATUS(status);
    // drivers[i].mesh
    MObject driver_mesh = driver_i.child(aMesh).asMesh(); CHECK_MSTATUS(status);
    MItMeshVertex driver_meshVertIter( driver_mesh, &status ); CHECK_MSTATUS( status );

    // drivers[i].objectWeight
    const float objectWeight = driver_i.child(aObjectWeight).asFloat();

    // drivers[i].vtxidx
    MArrayDataHandle vtxidxHandle(driver_i.child(aVtxIdx), &status);  CHECK_MSTATUS(status);
    std::size_t dvrVtxCnt = vtxidxHandle.elementCount();
    for(std::size_t dvrVtxIdx=0; dvrVtxIdx<dvrVtxCnt; ++dvrVtxIdx)
    {
        /// get one driver vertex(dvrVtxIdx) position
        int dvrVtxIdxBeforeChanged;
        CHECK_MSTATUS(driver_meshVertIter.setIndex(dvrVtxIdx, dvrVtxIdxBeforeChanged));
        //LDbg("dvrVtxIdx(%d) == dvrVtxIdxBeforeChanged(%d)", dvrVtxIdx, dvrVtxIdxBeforeChanged);//assert(dvnVtxIdx == dvnVtxIdxBeforeChanged);

        MPoint dvrVtxPos(driver_meshVertIter.position( MSpace::kObject, &status ));
        CHECK_MSTATUS(status);

        // driver vtx idx --> driven vtx idx
        CHECK_MSTATUS(vtxidxHandle.jumpToArrayElement(dvrVtxIdx));
        const std::size_t dvnVtxIdx =  vtxidxHandle.inputValue(&status).asLong();
        //LDbg("drivers[%d].vertexIndex[%d]=%d", dvrIdx, dvrVtxIdx, dvnVtxIdx);

        // Now, I don't create weightList[dvrIdx+1] anymore. I use weightList[0]
        // access .weightList[0].weights[dvnVtxIdx]
        float vtxWeight = weightValue(data, 0, dvnVtxIdx); //painted weight
        //LDbg("dvrIdx=%d,    (%d,%d),    w=%f", dvrIdx+1,  dvrVtxIdx,dvnVtxIdx,  vtxWeight);

        /// add deform value of the current driver mesh to accDeform[]
        accDeform[dvnVtxIdx] += (dvrVtxPos - dvnOriPos[dvnVtxIdx]) * objectWeight * vtxWeight;
    }

}
//
void TestDeformer::gatherDriverMeshes(MDataBlock& data, MObjectArray& srcDriverMeshs)
{
    MStatus status;
    /*

    The driver meshes are connected to PBS.drivers[*].mesh, like this:

    drivers
    |
    +---drivers[0]
    |   +---mesh            <-------(connect to)---  driverShape1(mesh)
    |   ...
    |
    +---drivers[1]
    |   +---mesh            <-------(connect to)---  driverShape2(mesh)
    |   ...
    |
    +---drivers[2]
        + ...

    */
    MPlug arrayDriversPlug(thisMObject(), aDrivers);
    //MString arrayPlugName(arrayDriversPlug.name());// Debug
    for(unsigned int i=0; i<arrayDriversPlug.numElements(); ++i)
    {
        //MString name(arrayDriversPlug[i].name());// debug
        MPlug meshPlug(arrayDriversPlug[i].child(aMesh, &status));   CHECK_MSTATUS(status);
        //LDbg("%s, %s", arrayDriversPlug[i].name().asChar(), meshPlug.name().asChar());

        MPlugArray srcPlug;
        if(meshPlug.connectedTo(srcPlug, true/*asDst*/, false/*asSrc*/, &status))
        {
            CHECK_MSTATUS(status);
            assert(srcPlug.length() == 1);// srcPlug[0] --> driver[i].mesh

            // Debug
            //printMeshInfo(__FUNCTION__, srcPlug[0].node(&status));   CHECK_MSTATUS(status);

            srcDriverMeshs.append(srcPlug[0].node(&status));
        }
    }
}
//
void TestDeformer::setVtxIdxToPlug(MDataBlock& data, const MObjectArray& srcDriverMeshs)
{
    MStatus status;

    MArrayDataHandle driversHandle = data.inputArrayValue( aDrivers, &status ); CHECK_MSTATUS(status);
    LDbg("srcDriverMeshs.length()(%d) == driversHandle.elementCount()(%d)", srcDriverMeshs.length(), driversHandle.elementCount());
    //assert(srcDriverMeshs.length() == driversHandle.elementCount());
    CHECK_MSTATUS(driversHandle.jumpToElement(0));
    for(std::size_t i=0; i<driversHandle.elementCount(); ++i)
    {
        // driver mesh i
        MDataHandle driver_i = driversHandle.inputValue(&status);
        CHECK_MSTATUS( status );
        // drivers[i].mesh
        //MFnMesh fnMesh(driver_i.child(aMesh).asMesh(), &status); CHECK_MSTATUS(status);
        // drivers[i].vtxidx
        MArrayDataHandle vtxidxHandle(driver_i.child(aVtxIdx), &status);  CHECK_MSTATUS(status);
        m_driverMeshMgr.getDriverMesh(i).setVtxIdxToPlug(vtxidxHandle);

        if( !driversHandle.next() ){
            break;
        }
    }
}
