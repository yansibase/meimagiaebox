#include "driver_mesh_mgr.h"
#include <cassert>
#include <maya/MDagPath.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MObject.h>
#include <maya/MPointArray.h>
#include <maya/MSelectionList.h>
#include <maya/MStatus.h>
#include <common/log.h>
#include "partial_blendshape.h"

DriverMeshMgr::DriverMeshMgr()
{
    m_drivenShapeOrig = MObject::kNullObj;
}
//
DriverMeshMgr::~DriverMeshMgr()
{

}
//
void DriverMeshMgr::clearAllDriverMesh()
{
    m_driverMesh.clear();
}
//
void DriverMeshMgr::addDriverMesh(const MObject& driverShape, /*const*/ MObject& dvnGeo, const short methodOfMappingVertexIndex)
{
    //m_drivenMObject     = drivenObj;
    //m_drivenMItGeometry = itDvnGeo;

    //printMeshInfo(__FUNCTION__, m_drivenMObject);
    m_driverMesh.push_back(DriverMesh(driverShape, dvnGeo, methodOfMappingVertexIndex));

}
//
const DriverMesh& DriverMeshMgr::getDriverMesh(const std::size_t i)const
{
    assert(i<m_driverMesh.size());

    return m_driverMesh[i];
}
//
MStatus DriverMeshMgr::setupDrivenMeshOrig()
{
    MStatus status;

    MString origNodeName;
    origNodeName = MGlobal::executePythonCommandStringResult("import testDeformer as pbs; pbs.getDrivenShapeOrigName(\""+TestDeformer::cTypeName()+"\")", true, false, &status);
    CHECK_MSTATUS(status);

    MSelectionList sList;
    MString toMatch(origNodeName/*"|driven|drivenShapeOrig"*/);/// you can use "/test/test_0002/data/test.ma"
    MGlobal::getSelectionListByName(toMatch, sList);

    unsigned int nMatches = sList.length();
    if (nMatches<=0)
    {
        //assert(nMatches>0 && "original shape node of driven mesh is not found");
        LWrn("original shape node of driven mesh is not found: %s", toMatch.asChar());
        return MS::kNotFound;
    }

    MDagPath dagpath;
    CHECK_MSTATUS(sList.getDagPath(0, dagpath));

    MObject mobj(dagpath.node(&status)); CHECK_MSTATUS(status);
    // Debug
    //printMeshInfo(__FUNCTION__, drivenObj);

    m_drivenShapeOrig = mobj;

    return MS::kSuccess;
}
//
void DriverMeshMgr::getDrivenMeshOrigPos(MPointArray& output)const
{
    MStatus status;

    assert(m_drivenShapeOrig != MObject::kNullObj && "m_drivenShapeOrig should not be null");

    MObject drivenShapeOrig(m_drivenShapeOrig);
    MItGeometry i(drivenShapeOrig, &status); CHECK_MSTATUS(status);
    i.reset();

    output.clear();
    while( !i.isDone(&status) )
    {
        CHECK_MSTATUS(output.append(i.position()));
        CHECK_MSTATUS(i.next());
    }
    assert(int(output.length()) == i.count());
}
//

