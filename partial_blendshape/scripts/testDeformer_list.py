import maya.cmds            as cmds
import mymagicbox.log       as log
import mymagicbox.mmb_flog	as flog
import testDeformer_ui      as pbs_ui

@flog.trace
def getDriverMesh_tmp():
    drivermesh = cmds.listConnections(pbs_ui.g_testDeformerNodeName+'.drivers', s=True, d=False, shapes=False, exactType=True, type='mesh')
    #print drivermesh

    if drivermesh is not None:
        for i in range(0, len(drivermesh)) :
            drivermesh[i] = cmds.ls(drivermesh[i], long=True)[0] # set the values to 'full dag path'

    #print drivermesh

    return drivermesh

@flog.trace
def list_update(*args):
    '''
    update the driver mesh list UI
    '''
    drivermesh = getDriverMesh_tmp();

    cmds.textScrollList(pbs_ui.g_listUI, e=True, removeAll=True)
    #print pbs_ui.g_listUI;

    if drivermesh is None:
        log.debug('no driver mesh at all')
    else:
        for mesh in drivermesh:
            cmds.textScrollList(pbs_ui.g_listUI, e=True, append=mesh)



if '__main__' == __name__:
    pass

