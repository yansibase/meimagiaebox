import maya.cmds            as cmds
import mymagicbox.log       as log
import mymagicbox.mmb_flog	as flog
import testDeformer         as pbs
import testDeformer_ui      as pbs_ui
import testDeformer_list    as pbs_list

global g_oldPartId
g_oldPartId = 0
global g_newPartId
g_newPartId = 0

@flog.trace
def test_initWeightListToZero():
    dnode=pbs_ui.g_testDeformerNodeName;
    # Now, I don't create weightList[dvrIdx+1] anymore. I use weightList[0]
    log.debug("initialize testDeformer.weightList[0] to 0.0");
    length = pbs.getVertexCountInShape( pbs.getDrivenShapeName() );

    for i in range(0, length):
        cmds.setAttr(dnode+".weightList[0].weights["+str(i)+"]", 0.0);
    #log.debug('init object weights: ');print cmds.getAttr(dnode+".weightList["+str(objId)+"].weights")

@flog.trace
def test_BreakConnectionFromWeightListWeights():
    dnode=pbs_ui.g_testDeformerNodeName;
    weightsSize = cmds.getAttr(dnode+".weightList[0].weights", size=True)
    for i in range(0, weightsSize):
        srcPlug  = dnode+'.weightList[0].weights['+str(i)+']'
        desPlugs = cmds.listConnections(srcPlug, d=True, plugs=True)
        #log.debug('desPlugs:'+','.join(desPlugs))
        if desPlugs is not None:
            for desPlug in desPlugs:
                cmds.disconnectAttr(srcPlug, desPlug);


@flog.trace
def test_PreparePaintOnDriver(dvrIdx):
    '''
    # In painting mode, Maya can modify the 'weights' attribute of the deformer node only.
    # So, how to save the painted weight?
    # The answer is when we select a driver node, the $PBS.weights is connected to $PBS.drivers[i].vertexWeight.
    # (You can see doc/attr-connection-when-painting.png).
    # This is done in this function.
    # So, when you paint the driven mesh, the paint weight will be updated to $PBS.drivers[i].vertexWeight automatically.
    '''
    dnode=pbs_ui.g_testDeformerNodeName; objId=0;

    # 1. Set pbs.weightList[0].weights[:] to 0.0
    test_BreakConnectionFromWeightListWeights()
    test_initWeightListToZero()

    # 2. how many vertex on driver mesh?
    dvrVtxCnt = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers['+str(dvrIdx)+'].vertexIndex', size=True)
    log.debug('driver %d has %d vertex(s)', dvrIdx, dvrVtxCnt)

    for dvrVtxIdx in range(0, dvrVtxCnt):
        # for each vertex on driver mesh ...
        dvnVtxIdx = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers['+str(dvrIdx)+'].vertexIndex['+str(dvrVtxIdx)+']')
        # dvrVtxIdx is mapped to dvnVtxIdx
        srcPlug = pbs_ui.g_testDeformerNodeName+'.drivers['+str(dvrIdx)+'].vertexWeight['+str(dvrVtxIdx)+']'
        desPlug = pbs_ui.g_testDeformerNodeName+'.weightList[0].weights['+str(dvnVtxIdx)+']'
        srcPlugValue = cmds.getAttr(srcPlug)
        desPlugValue = cmds.getAttr(desPlug)

        # set testDeformer1.weightList[0].weights[*]
        cmds.setAttr(desPlug, srcPlugValue)

        # prepare painting on driver mesh
        cmds.connectAttr(desPlug, srcPlug, force=True);




if '__main__' == __name__:
    pass

