import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import mymagicbox.mmb_flog      as flog

@flog.trace
def createMainWindow1():
    deleteMainWindow1()

    pbsWindow = cmds.window('PartialBlendShape1', width=150)

    #log.debug('window=%s', pbsWindow);
    cmds.columnLayout(adjustableColumn=True)
    cmds.button(
        label="Create Deformer Node",
        annotation="Select the driven mesh and press this button to create the deform node",
    )

    cmds.showWindow(pbsWindow)

@flog.trace
def deleteMainWindow1():
    if cmds.window('PartialBlendShape1', exists = True):
        cmds.deleteUI('PartialBlendShape1', window=True)
