'''
Update the progress bar when mapping vertex index from driver mesh to driven mesh.
Because this step is time consuming.
'''
import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import mymagicbox.mmb_flog      as flog

@flog.trace
def pbs_BeginProgressBar(vertexCount, msg):
    gMainProgressBar = mel.eval('$tmp = $gMainProgressBar')
    cmds.progressBar( gMainProgressBar,
                    edit            = True,
                    beginProgress   = True,
                    isInterruptable = False,
                    status          = msg,
                    maxValue        = vertexCount )

#@flog.trace
def pbs_UpdateProgressBar(msg):
    gMainProgressBar = mel.eval('$tmp = $gMainProgressBar')
    cmds.progressBar(gMainProgressBar, edit=True, step=1, status=msg)

@flog.trace
def pbs_EndProgressBar(msg):
    gMainProgressBar = mel.eval('$tmp = $gMainProgressBar')
    cmds.progressBar(gMainProgressBar, edit=True, status=msg)
    cmds.progressBar(gMainProgressBar, edit=True, endProgress=True)

if '__main__' == __name__:
    pbs_BeginProgressBar(500, 'test pbs_progressbar begin')
    for i in range(500) :
        #if cmds.progressBar(gMainProgressBar, query=True, isCancelled=True ) :
        #        break
        pbs_UpdateProgressBar('test pbs_progressbar updating ... ('+str(i)+'/'+str(500)+')')

    pbs_EndProgressBar('test pbs_progressbar end')
