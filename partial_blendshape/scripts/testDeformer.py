import maya.cmds                as cmds
import maya.OpenMaya            as om
import mymagicbox.log           as log
import mymagicbox.mmb_flog      as flog
import testDeformer_dummyWeight as pbs_weight
import testDeformer_list        as pbs_list
import testDeformer_ui          as pbs_ui
import pbs_protocol             as pbs_prot

# utility functions
@flog.trace
def xxu_getShapes( xform_ ):
    '''
    print xxu_getShapes('pSphere1')
    '''
    shapes=[xform_]

    if  "transform" == cmds.nodeType(xform_):
        # If given node is not a transform, assume it is a shape
        # and pass it through
        shapes = cmds.listRelatives(xform_, fullPath=True, shapes=True, ni=True)

    return shapes;

@flog.trace
def xxu_getShape(xform_):
    '''
    print xxu_getShape('pSphere1')
    '''
    shapes = xxu_getShapes(xform_);
    return shapes[0];
############################################################
'''
What happend when I delete a driver mesh:
    drivers
    |
    +---drivers[0]
        +---mesh            <-------(connect to)---  driver.worldMesh[0]
        + ...
    +---drivers[1]
        +---mesh
        + ...

    When you delete driver mesh node, this function will delete drivers[0].
    I use cmds.removeMultiInstance() to delete this plug.


'''
@flog.trace
def testDeformer_getDeformerNodeFromSelection():
    '''
    If you select a transform node,
    this function will return the PBS node which is connected with the transform node.
    '''

    selected = cmds.ls(sl=True);# select an mesh node(transform node)
    shape = xxu_getShape(selected[0]);# extend to shape node
    deformNode = cmds.listConnections(shape+".inMesh", s=True)

    pbs_ui.g_testDeformerNodeName = deformNode[0];
    #print pbs_ui.g_testDeformerNodeName;

@flog.trace
def WhichPlugIsGoingTobeRemoved(plugName):
    '''
    WhichPlugIsGoingTobeRemoved('testDeformer1.drivers[1].mesh')
    >>>'testDeformer1.drivers[1]'
    '''
    if plugName.endswith('.mesh'):
        ret = plugName[:-5]
    else:
        log.err('Dont know how to get the parent plug name of %s', plugName);
    return ret

# delete driver mesh
@flog.trace
def testDeformer_deleteDriverMesh(xform_):
    '''

    '''
    shape = xxu_getShape(xform_);# extend to shape node
    driver_mesh_plug = cmds.listConnections(shape+".worldMesh[0]", d=True, plugs=True)
    assert len(driver_mesh_plug)==1, shape+'.worldMesh[0] is connected to many plugs, removeMultiInstance() doesnt know which one is going to be removed'

    pbs_drivers_i = WhichPlugIsGoingTobeRemoved(driver_mesh_plug[0])

    # break the connection, and delete the driver mesh element
    # form deform node plug(it is an array plug)
    cmds.removeMultiInstance(pbs_drivers_i, b=True)

    pbs_list.list_update();

@flog.trace
def testDeformer_deleteSelectedDriverMesh(*args):
    pbs_ui.testDeformer_StopPainting()

    selected = cmds.ls(sl=True)
    for sel in selected:
        testDeformer_deleteDriverMesh(sel)
    trimDriversPlug()

@flog.trace
def trimDriversPlug():
    '''
    Confider the followint case.
    Before we delete driver1:

    drivers
    |
    +---drivers[0]
        +---mesh            <-------(connect to)---  driver0.worldMesh[0]
        + ...
    +---drivers[1]
        +---mesh            <-------(connect to)---  driver1.worldMesh[0]
        + ...
    +---drivers[2]
        +---mesh            <-------(connect to)---  driver2.worldMesh[0]
        + ...
    +---drivers[3]


    After we delete driver1, we re-connect the driver mesh to PBS.drivers
    drivers
    |
    +---drivers[0]
        +---mesh            <-------(connect to)---  driver0.worldMesh[0]
        + ...
    +---drivers[1]
        +---mesh            <-------(connect to)---  driver2.worldMesh[0]
        + ...
    +---drivers[2]
        +---mesh
        + ...
    +---drivers[3]

    Now, drivers[2] and drivers[3] are not connected anymore. we have to delete them.
    '''
    srcXforms = pbs_list.getDriverMesh_tmp()
    if srcXforms is None:
        return
    #log.debug('len(srcXforms)=%d', len(srcXforms))

    i=0
    for i in range(0, len(srcXforms)):
        srcShape = xxu_getShape(srcXforms[i])
        #print i,   srcShape
        if not cmds.isConnected(srcShape+".worldMesh[0]", pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].mesh'):
            cmds.connectAttr(srcShape+".worldMesh[0]", pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].mesh', force=True);

    drivers_plugarray_size = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers', size=True)
    #print drivers_plugarray_size
    for j in range(i+1, drivers_plugarray_size):
        #print j
        cmds.removeMultiInstance(pbs_ui.g_testDeformerNodeName+'.drivers['+str(j)+']', b=True)

@flog.trace
def setPBSState(state):
    cmds.setAttr (pbs_ui.g_testDeformerNodeName+'.initialize', state);
    cmds.refresh(force=True)

@flog.trace
def getPBSState():
    ret = cmds.getAttr (pbs_ui.g_testDeformerNodeName+'.initialize');
    return ret;

@flog.trace
def setPBSMapMethod(state):
    cmds.setAttr (pbs_ui.g_testDeformerNodeName+'.mapmethod', state);
    cmds.refresh(force=True)

@flog.trace
def getPBSMapMethod(state):
    cmds.setAttr (pbs_ui.g_testDeformerNodeName+'.mapmethod', state);
    cmds.refresh(force=True)
############################################################
#///////////////////////////////////////////////////
@flog.trace
def testDeformer_getNextAvailableIndex():
    '''
    Consider the following case:

    drivers
    |
    +---drivers[0]
        +---mesh            <-------(connect to)---  driver0.worldMesh[0]
        + ...
    +---drivers[1]
        +---mesh            <-------(connect to)---  driver2.worldMesh[0]
        + ...
    +---drivers[2]
        +---mesh
        + ...
    +---drivers[3]

    if we are going to add a new driver mesh. This function will return 2

    '''
    size_ = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers', size=True)
    for i in range(0, size_):
        srcXforms=cmds.listConnections(pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].mesh', s=True, d=False, shapes=False)
        if srcXforms is None:
            return i
    return size_

# add driver mesh
@flog.trace
def testDeformer_addDriverMesh(xform):
    #global pbs_ui.g_testDeformerNodeName

    shape = xxu_getShape(xform) # extend to shape node
    i = testDeformer_getNextAvailableIndex()
    log.debug('testDeformer_getNextAvailableIndex() return %d', i)

    # connect driver mesh to deform node
    log.debug(shape+".worldMesh[0]  -->  "+pbs_ui.g_testDeformerNodeName+".drivers["+str(i)+"].mesh")
    cmds.connectAttr(shape+".worldMesh[0]", pbs_ui.g_testDeformerNodeName+".drivers["+str(i)+"].mesh");
    #cmds.connectAttr(shape+".worldMesh[0]", pbs_ui.g_testDeformerNodeName+".it[0].itg["+str(i)+"].iti[0].igt");

#	pbs_list.list_addItem($xform);
    pbs_list.list_update();

@flog.trace
def testDeformer_addSelectedDriverMesh(*args):
    pbs_ui.testDeformer_StopPainting()

    oldstate = getPBSState();
    setPBSState(0)

    trimDriversPlug()

    selected = cmds.ls(sl=True)
    for sel in selected:
        testDeformer_addDriverMesh(sel);

    #pbs_weight.test_initWeight()

    setPBSState(1)
    setPBSState(oldstate)

    testDeformer_initVertexWeight()

#################################################################3
# get the name of driven shape's node
@flog.trace
def getDrivenShapeName():
    '''
    Given the current PBS node,
    return the name of driven shape node which is connected from the current PBS node.
    '''
    myDeformer = pbs_ui.g_testDeformerNodeName

    #
    drivenShape = cmds.listConnections(myDeformer+".outputGeometry", d=True, s=False, shapes=True)
    if len(drivenShape)<=0:
        log.err(myDeformer +".outputGeometry is not connected to a driven mesh");
        return "";

    if len(drivenShape)>1:
        log.err(myDeformer +".outputGeometry is connected to many driven meshes. Which one is the choice?");
        return "";

    longName = cmds.ls(drivenShape[0], long=True)

    return longName[0];

@flog.trace
def getDrivenShapeOrigName(deformerType):
    '''
    get the name of driven shape's original node
    '''
    return getDrivenShapeName()+"Orig";

#///////////////////////////////////////////////////
@flog.trace
def getVertexCountInShape(node):
    '''getVertexCountInShape("pCubeShape1"); getVertexCountInShape("pPlaneShape1");
    '''
    return len(cmds.getAttr(node+".vtx[:]", size=True));




@flog.trace
def testDeformer_initVertexWeight():
    '''
    initialize the vertex weight to 1.0
    '''
    driversCnt = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers', size=True)
    for i in range(0, driversCnt):
        #log.debug('driver %d', i)
        vtxCnt = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].vertexIndex', size=True)
        #log.debug('vertex count=%d', vtxCnt)

        for vi in range(0, vtxCnt):
            cmds.setAttr(pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].vertexWeight['+str(vi)+']', 1.0)
        log.debug('drivers['+str(i)+'].vertexWeight[:]=1.0');

@flog.trace
def testDeformer_driversInfo(msg=''):
    '''
    Call this function to show the data of driver mesh(es) for debug
    '''
    drivers = pbs_list.getDriverMesh_tmp();

    log.debug('---------- Drivers Info ---------- %s', msg)
    for i in range(0, len(drivers)):
        vertexIndex  = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].vertexIndex')
        objectWeight = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].objectWeight')
        vertexWeight = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.drivers['+str(i)+'].vertexWeight')

        log.debug('driver[%d]=%s', i, drivers[i])
        log.debug('     vertexIndex[] = %s', str(vertexIndex));
        log.debug('     objectWeight  = %d', objectWeight);
        log.debug('     vertexWeight[]= %s', str(vertexWeight));

    paintWeights = cmds.getAttr(pbs_ui.g_testDeformerNodeName+'.weightList[0].weights');
    log.debug('weightList[0].weights[] = %s', str(paintWeights));

    log.debug('')

@flog.trace
def createOneBackupNode(driver_i, srcShape, pbsNodeName, msg=''):
    vi = cmds.getAttr(pbsNodeName+'.drivers['+str(driver_i)+'].vertexIndex')  # print vi
    ow = cmds.getAttr(pbsNodeName+'.drivers['+str(driver_i)+'].objectWeight') # print ow
    vw = cmds.getAttr(pbsNodeName+'.drivers['+str(driver_i)+'].vertexWeight') # print vw

    # trying to create a backup node, make sure the node does not exists.
    bkNodeName_expected = pbs_prot.getBackupNodeName(pbsNodeName, driver_i)
    if cmds.objExists(bkNodeName_expected):
        log.err('Trying to create a backup node, but it already exists. There MUST be something wrong! Please find out the reason. [%s]', bkNodeName_expected)
        return

    # create a backup node.
    bkNodeName = cmds.createNode('transform', name=bkNodeName_expected, skipSelect=True);
    cmds.addAttr(bkNodeName, ln='mesh',           sn='msh',    dt='mesh',        r=True, w=True, s=True, k=True)
    cmds.addAttr(bkNodeName, ln='vertexIndex',    sn='vtxidx', dt='Int32Array',  r=True, w=True, s=True, k=True)
    cmds.addAttr(bkNodeName, ln='objectWeight',   sn='ow',     at='double',      r=True, w=True, s=True, k=True)
    cmds.addAttr(bkNodeName, ln='vertexWeight',   sn='vtxw',   dt='doubleArray', r=True, w=True, s=True, k=True)

    # connect driver mesh node to backup node
    # $srcShape.worldMesh[0] ---> $bkNodeName.mesh
    cmds.connectAttr(srcShape+".worldMesh[0]", bkNodeName+'.mesh', force=True);

    # init the backup node attributes
    cmds.setAttr(bkNodeName+'.vertexIndex',  vi[0], type='Int32Array')
    cmds.setAttr(bkNodeName+'.objectWeight', ow)
    cmds.setAttr(bkNodeName+'.vertexWeight', vw[0], type='doubleArray')

    # print
    log.debug('---------- Backup Node Info ---------- %s, %s', bkNodeName, msg)
    log.debug('backup node: %s', bkNodeName)
    log.debug('     vertexIndex[] = %s', str(cmds.getAttr(bkNodeName+'.vertexIndex')));
    log.debug('     objectWeight  = %d',     cmds.getAttr(bkNodeName+'.objectWeight'));
    log.debug('     vertexWeight[]= %s', str(cmds.getAttr(bkNodeName+'.vertexWeight')));
    log.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')

@flog.trace
def createAllBackupNodes(pbsNodeName):
    '''
    create backup nodes for all driver nodes
    '''
    srcXforms = pbs_list.getDriverMesh_tmp()
    if srcXforms is None:
        return
    #log.debug('len(srcXforms)=%d', len(srcXforms))

    i=0
    for i in range(0, len(srcXforms)):
        srcShape = xxu_getShape(srcXforms[i])
        #print i,   srcShape

        # create a backup node for one driver mesh
        if cmds.isConnected(srcShape+".worldMesh[0]", pbsNodeName+'.drivers['+str(i)+'].mesh'):
            createOneBackupNode(i, srcShape, pbsNodeName) #cmds.connectAttr(srcShape+".worldMesh[0]", pbsNodeName+'.drivers['+str(i)+'].mesh', force=True);

@flog.trace
def gatherBackupNodes(pbsNodeName):
    '''
    Which backup nodes we have in the current maya scence?
    '''
    pattern = pbs_prot.getBackupNodePrefix(pbsNodeName) + '*'
    nodes   = cmds.ls(pattern, type='transform')
    return nodes

@flog.trace
def deleteBackupNodes(pbsNodeName):
    backupNodes = gatherBackupNodes(pbsNodeName)
    if len(backupNodes) != 0:
        cmds.delete(backupNodes)


@flog.trace
def setMultiValue(arrayPlug, newValues):
    '''
    set arrayPlug with newValues
    '''
    #print 'setMultiValue('+arrayPlug+', '+str(newValues)+')'

    oldValues = cmds.getAttr(arrayPlug)[0]; #log.debug('oldValues=%s', str(oldValues))
    oldLength = len(oldValues);             #log.debug('oldLength=%s', str(oldLength))
    newLength = len(newValues);             #log.debug('newLength=%s', str(newLength))

    for i in range(0, newLength):
        cmds.setAttr(arrayPlug+'['+str(i)+']', newValues[i])

    # if newLength < oldLength then remove the redundant plugs in reverted order
    for i in reversed(range(newLength, oldLength)):
        cmds.removeMultiInstance(arrayPlug+'['+str(i)+']', b=True)

@flog.trace
def restoreOneDriverData(driver_i, srcShape, pbsNodeName, msg=''):
    '''
    Since driver mesh node is connected to backup node: $srcShape.worldMesh[0] ---> $bkNodeName.mesh
    We can get the backup node data from the driver node,
    and set the data to PBS.driver[i]
    '''
    BACKUP_NODE_TYPE = 'transform'
    srcPlugs = cmds.listConnections(srcShape+'.worldMesh[0]', d=True, t=BACKUP_NODE_TYPE)
    # assert
    if srcPlugs is None:
        log.err('There is no backup data about mesh, return. %s', srcShape)
        return
    if len(srcPlugs) != 1:
        log.debug('<%s.worldMesh[0]> is connected to backup nodes: %s', srcShape, str(srcPlugs))
        assert len(srcPlugs)==1, 'Why <'+srcShape+'.worldMesh[0]> is not connected to ONE backup node?'

    desPlugs = cmds.listConnections(srcShape+'.worldMesh[0]', d=True, t='testDeformer', plugs=True)
    # assert
    if len(desPlugs) != 1:
        log.debug('<%s.worldMesh[0]> is connected to pbs nodes: %s', srcShape, str(desPlugs))
        assert len(desPlugs)==1, 'Why <'+srcShape+'.worldMesh[0]> is not connected to ONE pbs node?'

    # assert
    if pbs_ui.g_testDeformerNodeName+'.drivers['+str(driver_i)+'].mesh' != desPlugs[0]:
        log.debug('Backup node and restore plug doesnt match with each other!')
        assert pbs_ui.g_testDeformerNodeName+'.drivers['+str(driver_i)+'].mesh' == desPlugs[0]


    desPlug = pbsNodeName + '.drivers['+str(driver_i)+'].vertexWeight'
    srcPlug = srcPlugs[0] + '.vertexWeight'
    log.debug('srcPlug --(restore)--> desPlug: %s --(restore)--> %s', srcPlug, desPlug)

    srcValue = cmds.getAttr(srcPlug);
    #log.debug('srcValue=%s', str(srcValue))

    #desValue = cmds.getAttr(desPlug);
    #log.debug('desValue=%s', str(desValue))

    setMultiValue(desPlug, srcValue);

    #desValue = cmds.getAttr(desPlug);
    #log.debug('desValue=%s', str(desValue))

@flog.trace
def restoreAllDriversData(pbsNodeName):
    srcXforms = pbs_list.getDriverMesh_tmp()
    if srcXforms is None:
        return
    #log.debug('len(srcXforms)=%d', len(srcXforms))

    i=0
    for i in range(0, len(srcXforms)):
        srcShape = xxu_getShape(srcXforms[i])
        if cmds.isConnected(srcShape+".worldMesh[0]", pbsNodeName+'.drivers['+str(i)+'].mesh'):
            restoreOneDriverData(i, srcShape, pbsNodeName)


@flog.trace
def createDriverShapeOrigNode(fullPath):
    '''
    nodeType = cmds.nodeType(fullPath)
    parent   = cmds.listRelatives(fullPath, fullPath=True, parent=True)[0] # get parent

    origFullPath = pbs_prot.getDriverShapeOrigName(fullPath)
    #cmds.duplicate(fullPath, name=origFullPath, smartTransform=False, upstreamNodes=False, inputConnections=True, instanceLeaf=False, parentOnly=False)
    cmds.createNode(nodeType, name=origFullPath, parent=parent)
    cmds.connectAttr   (fullPath+'.outMesh', origFullPath+'.inMesh', force=True)
    cmds.disconnectAttr(fullPath+'.outMesh', origFullPath+'.inMesh')
    cmds.setAttr(origFullPath+'.intermediateObject', 1)
    '''
    log.err('Please create the Orig node manually, cause the procedure is not implemented yet. %s', fullPath)

@flog.trace
def checkShapeOrigName(shapeOrig):
    '''
    checkShapeOrigName('pPlaneShape1Orig')
    '''
    result = cmds.ls(shapeOrig)
    resultLength = len(result)

    if resultLength <= 0 :
        log.err('Original shape node of driver mesh is not found: [%s]', shapeOrig);
    elif resultLength > 1:
        log.err('Too many original shape node of driver mesh: %s. orig nodes:[%s]', shapeOrig, str(result));
    else:
        log.debug('Original shape node is found: [%s]', shapeOrig);

@flog.trace
def getAllDGNodes(inNode,direction,nodeMfnType):
    '''
    getAllDGNodes('body2Shape', om.MItDependencyGraph.kUpstream, om.MFn.kMesh)
    '''

    nodes = []
    # Create a MSelectionList with our selected items:
    selList = om.MSelectionList()
    selList.add(inNode)
    mObject = om.MObject()  # The current object
    selList.getDependNode( 0, mObject )

    # Create a dependency graph iterator for our current object:
    mItDependencyGraph = om.MItDependencyGraph(mObject,direction,om.MItDependencyGraph.kPlugLevel)
    while not mItDependencyGraph.isDone():
        currentItem = mItDependencyGraph.currentItem()
        dependNodeFunc = om.MFnDependencyNode(currentItem)
        # See if the current item is an animCurve:
        if currentItem.hasFn(nodeMfnType):
            name = dependNodeFunc.name()
            nodes.append(name)
        mItDependencyGraph.next()

    # See what we found:
    #for n in sorted(nodes):
    #   print n

    return nodes

@flog.trace
def getOrigNode(shapeNode):
    '''
    Only the used orig node of shapeNode will be returned,
    the unused orig node of shapeNode will be excluded and will not be returned.
    '''
    candidates_origs = getAllDGNodes(shapeNode, om.MItDependencyGraph.kUpstream, om.MFn.kMesh)
    #print '0.candidates_origs:'
    #print candidates_origs

    # exclude the unused orig node
    if not cmds.objExists(shapeNode+'.inMesh'):
        log.warn('Attribute inMesh doesnt exist on node %s. Please dont select a non-mesh node', shapeNode)

    srcDeformers = cmds.listConnections(shapeNode+'.inMesh', s=True, d=False);
    if (srcDeformers is None) or (0 == len(srcDeformers)):
        log.err('Dont know how to get the orig node, cause $(shape).inMesh is not connected with a deformer node. [%s]', shapeNode)
        return ''

    srcDeformer = srcDeformers[0]

    if cmds.nodeType(srcDeformer)=='blendShape':
        inputTargets = cmds.listConnections(srcDeformer+'.inputTarget', s=True, d=False, shapes=True);
        #print '1.inputTargets:'
        #print inputTargets

        inputTargets_upstream = [] # upstream nodes of srcTargets
        for inputTarget in inputTargets:
            t = getAllDGNodes(inputTarget, om.MItDependencyGraph.kUpstream, om.MFn.kMesh)
            inputTargets_upstream += t
        inputTargets_upstream = list( set(inputTargets_upstream) )

        candidates_origs = [x for x in candidates_origs if x not in inputTargets]         # candidates_origs -= inputTargets
        candidates_origs = [x for x in candidates_origs if x not in inputTargets_upstream]# candidates_origs -= inputTargets_upstream

        #print '2.inputTargets_upstream:'
        #print inputTargets_upstream

    elif cmds.nodeType(srcDeformer)=='skinCluster':
        pass
    else:
        pass

    #print '3.candidates_origs:'
    #print candidates_origs

    if len(candidates_origs) == 0:
        log.err('Does not find orig node for %s' % (shapeNode) )
        return ''
    elif len(candidates_origs) > 1:
        log.err('There are so many orig nodes for %s. Which is the right one? [%s]' % (shapeNode, str(candidates_origs)) )
        return ''
    else: # len(candidates_origs) == 1
        log.debug('Find the orig node for %s. [%s]' % (shapeNode, candidates_origs[0]) )
        return candidates_origs[0]




if '__main__' == __name__:
    pass

