import maya.cmds                as cmds
import mymagicbox.log           as log
import mymagicbox.mmb_flog      as flog

@flog.trace
def getBackupNodePrefix(pbsNodeName):
    return ('backup_'+pbsNodeName+'_driver')

@flog.trace
def getBackupNodeName(pbsNodeName, driver_i):
    return getBackupNodePrefix(pbsNodeName)+str(driver_i)

@flog.trace
def getDriverShapeOrigName(driverShape):
    return (driverShape+'Orig')

