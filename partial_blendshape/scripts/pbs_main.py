import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import mymagicbox.mmb_flog      as flog
import pbs_ui

@flog.trace
def loadPlugin():
    if not cmds.pluginInfo('partial_blendshape', query=True, loaded=True):
        cmds.loadPlugin('partial_blendshape', quiet=True)
    else:
        log.debug('partial_blendshape is already loaded')

@flog.trace
def unloadPlugin():
    if cmds.pluginInfo('partial_blendshape', query=True, loaded=True):
        cmds.unloadPlugin('partial_blendshape')
    else:
        log.debug('partial_blendshape is already unloaded')

##################################################################
pbs_g_events = []

@flog.trace
def registerEvents():
    global pbs_g_events
    '''
    events:
    deleteAll:
        when a file new occurs
    NewSceneOpened:
        when a new scene has been opened.
    PostSceneRead:
        after a scene has been read.
        Specifically after a file open,
        import or all child references have been read.
    quitApplication:
        when the user has chosen to quit, either through the quit MEL command, or through the Exit menu item.
    SceneOpened:
        when a scene has been opened.
    SceneSaved:
        when a scene has been saved.
    ToolChanged:
        when the user changes the tool/context.
    PostToolChanged:
        after the user changes the tool/context.



    -uiDeleted flag
    '''
    # clear old events
    pbs_g_events = []

    jobNum = cmds.scriptJob(event=['deleteAll',         pbs_ui.deleteMainWindow1])
    pbs_g_events.append(jobNum)

    jobNum = cmds.scriptJob(event=['NewSceneOpened',    pbs_ui.deleteMainWindow1])
    pbs_g_events.append(jobNum)

    jobNum = cmds.scriptJob(event=['PostSceneRead',     pbs_ui.deleteMainWindow1])
    pbs_g_events.append(jobNum)

    jobNum = cmds.scriptJob(event=['quitApplication',   pbs_ui.deleteMainWindow1])
    pbs_g_events.append(jobNum)

    jobNum = cmds.scriptJob(event=['SceneOpened',       pbs_ui.deleteMainWindow1])
    pbs_g_events.append(jobNum)


    pass

@flog.trace
def deregisterEvents():
    global pbs_g_events

    for e in pbs_g_events:
        cmds.scriptJob(kill=e, force=True)

    pbs_g_events = []

    pass
##################################################################

@flog.trace
def setup():
    loadPlugin()
    registerEvents()

    pass

@flog.trace
def teardown():
    unloadPlugin()

    deregisterEvents()
    pass
