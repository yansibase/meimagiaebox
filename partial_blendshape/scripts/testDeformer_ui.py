'''
 See doc/ui.png
'''

import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import mymagicbox.mmb_flog      as flog
import testDeformer             as pbs
import testDeformer_list        as pbs_list
import testDeformer_dummyWeight as pbs_weight

# --------------- Global Variables ----------------
global g_listUI;
g_listUI = ''

global g_IsInPaintingMode;
g_IsInPaintingMode = False;

global g_testDeformerNodeName;
g_testDeformerNodeName = ""; # print($g_testDeformerNodeName);
#--------------------------------------------------

#
@flog.trace
def testDeformer_loadPlugin():
    if not cmds.pluginInfo('partial_blendshape', query=True, loaded=True):
        cmds.loadPlugin('partial_blendshape', quiet=True)

#
@flog.trace
def SetDeformerNode(pbsNode):
    global g_testDeformerNodeName;
    g_testDeformerNodeName = pbsNode
    log.debug('set new pbs node: %s', g_testDeformerNodeName)

    return g_testDeformerNodeName
#
@flog.trace
def CreateDeformerNode(drivenXform):
    global g_testDeformerNodeName;

    defr= cmds.deformer(drivenXform, type='testDeformer')
    ret = SetDeformerNode(defr[0]) #g_testDeformerNodeName = defr[0]

    return ret

#
@flog.trace
def testDeformer_CreateDeformerNode(*args):
    selectedNodes = cmds.ls(sl=True)
    selectedNode  = selectedNodes[0]

    selectedNodeType = cmds.objectType(selectedNode)
    selectedShape = ''
    if selectedNodeType == 'transform':
        selectedShape = pbs.xxu_getShape(selectedNode)
    else:
        selectedShape = selectedNode

    ret = ''
    pbsNodes = cmds.listConnections(selectedShape+'.inMesh', d=False, s=True)
    if (pbsNodes is None) or (len(pbsNodes)==0):# the shape node is NOT connected to a PBS node
        ret = CreateDeformerNode(selectedNode)
    else:# the shape node is connected to a PBS node
        pbsNode  = pbsNodes[0]
        sourceNodeType = cmds.objectType(pbsNode)
        if sourceNodeType == 'testDeformer':
            #ret = SetDeformerNode(pbsNode)
            log.warn('The deformer node already exists. (%s ---> %s.inMesh)' % (pbsNode, selectedShape) )
            log.warn('You could: ')
            log.warn('   - set the deformer node(Prss the "Set Defomer Node" button); ')
            log.warn('   - or, delete the deformer node at first and then press "Create Defomer Node" button')
        else:
            ret = CreateDeformerNode(selectedNode) # it could be a polyCube

    pbs_list.list_update()

    return ret

#
@flog.trace
def testDeformer_SetDeformerNode(*args):
    selectedNodes = cmds.ls(sl=True)
    selectedNode  = selectedNodes[0]

    selectedNodeType = cmds.objectType(selectedNode)
    selectedShape = ''
    if selectedNodeType == 'transform':
        selectedShape = pbs.xxu_getShape(selectedNode)
    else:
        selectedShape = selectedNode

    ret = ''
    pbsNodes = cmds.listConnections(selectedShape+'.inMesh', d=False, s=True)
    if (pbsNodes is None) or (len(pbsNodes)==0):# the selected shape node is NOT connected to a PBS node
        #ret = CreateDeformerNode(selectedNode)
        log.warn('You are going to set the deformer node, but the shape node(%s.inMesh) is NOT connected to a PBS node at all. You could press "Create Defomer Node" button'%(selectedShape))
    else:# the selected shape node is connected to a PBS node
        pbsNode  = pbsNodes[0]
        sourceNodeType = cmds.objectType(pbsNode)
        if sourceNodeType == 'testDeformer':
            ret = SetDeformerNode(pbsNode)
        else:
            #ret = CreateDeformerNode(selectedNode) # it could be a polyCube
            log.warn('You are going to set the deformer node, but the source node(%s, %s) is NOT a PBS node. You could press "Create Defomer Node" button'%(pbsNode, sourceNodeType) )

    pbs_list.list_update()

    return ret
#
@flog.trace
def AddSelectedDriverMesh(*args):
    global g_testDeformerNodeName;

    # 1.1 delete the backup nodes at first
    pbs.deleteBackupNodes(g_testDeformerNodeName)
    # 1.2 backup pbs node
    pbs.createAllBackupNodes(g_testDeformerNodeName)

    # 2.0
    pbs.testDeformer_addSelectedDriverMesh(args)

    # 3.0 restore pbs node
    pbs.restoreAllDriversData(g_testDeformerNodeName)

@flog.trace
def DeleteSelectedDriverMesh(*args):
    pbs.testDeformer_deleteSelectedDriverMesh(args)


#
@flog.trace
def testDeformer_selectDriverCB():
    global g_listUI;
    selectedIndex = cmds.textScrollList(g_listUI, q=True, selectIndexedItem=True);

    if len(selectedIndex) != 1:
        log.err("One and only one driver mesh should be selected.");
        return;
    else:
        selectedItem  = cmds.textScrollList(g_listUI, q=True, selectItem=True);
        log.debug(str(selectedIndex[0])+", "+selectedItem[0]+"\n");

    testDeformer_selectDriver(selectedIndex[0])
#
@flog.trace
def testDeformer_selectDriver(selectedIndex):
    global g_testDeformerNodeName;
    global g_IsInPaintingMode;

    #cmds.select(pbs.getDrivenShapeName(), r=True );
    #mel.eval("artAttrToolScript 3 \"testDeformer\"");

    if g_IsInPaintingMode:
        pbs_weight.test_PreparePaintOnDriver(selectedIndex-1);

        cmds.select(pbs.getDrivenShapeName(), r=True );
        mel.eval("artAttrToolScript 3 \"testDeformer\"");
    else:
        global g_listUI;
        selectedItem  = cmds.textScrollList(g_listUI, q=True, selectItem=True);
        cmds.select(selectedItem, r=True)

#
@flog.trace
def testDeformer_ShowDeformEffect(*args):
    pbs.setPBSState(2)
#
@flog.trace
def testDeformer_StartPainting(*args):
    global g_IsInPaintingMode;

    if g_IsInPaintingMode:
        log.warn('It is already in painting mode, do nothing and return')
        return # early return

    g_IsInPaintingMode = True
    pbs_weight.test_BreakConnectionFromWeightListWeights()
    pbs_weight.test_initWeightListToZero()

#
@flog.trace
def testDeformer_StopPainting(*args):
    global g_IsInPaintingMode;

    if not g_IsInPaintingMode:
        log.warn('It is already not in painting mode, do nothing and return')
        return # early return

    g_IsInPaintingMode = False

    pbs_weight.test_BreakConnectionFromWeightListWeights()

    cmds.setToolTo('selectSuperContext') # exit paint mode


@flog.trace
def testDeformer_BackupDriversData(*args):
    global g_testDeformerNodeName;

    # delete the backup nodes at first
    pbs.deleteBackupNodes(g_testDeformerNodeName)

    # create the backup nodes
    pbs.createAllBackupNodes(g_testDeformerNodeName)

@flog.trace
def testDeformer_PrintOrigNode(*args):
    selectedNodes = cmds.ls(sl=True)

    node_orig_pair = {}
    for node in selectedNodes:
        shapeNode = pbs.xxu_getShape(node)
        origNode = pbs.getOrigNode(shapeNode)
        node_orig_pair[shapeNode] = origNode

    log.info('--------- Orig nodes -----------')
    for node, orig in node_orig_pair.iteritems():
        log.info('(node --> orig): %s --> %s'%(node, orig) )
    log.info('--------------------------------')

@flog.trace
def testDeformer_ui():
    '''
     See doc/ui.png
    '''
    global g_listUI;

    testDeformer_loadPlugin()

    if cmds.window('PartialBlendShape', exists = True):
        testDeformer_deleteUI()

    pbsWindow = cmds.window('PartialBlendShape', width=150)

    #log.debug('window=%s', pbsWindow);
    cmds.columnLayout(adjustableColumn=True)

    cmds.rowLayout(numberOfColumns=2, adjustableColumn=2)
    cmds.button(
        label="Create Deformer Node",
        annotation="Select the driven mesh and press this button to create the deform node",
        command=testDeformer_CreateDeformerNode
    )
    cmds.button(
        label="Set Deformer Node",
        annotation="Select the driven mesh and press this button to set the deform node",
        command=testDeformer_SetDeformerNode
    )
    cmds.setParent('..')

    cmds.button(
        label="Add Driver Mesh(es)",
        annotation="Select the driver mesh and press this button to connect to the deform node",
        command=AddSelectedDriverMesh
    )

    cmds.button(
        label="Remove Driver Mesh(es)",
        annotation="Select the driver mesh and press this button to disconnect from the deform node",
        command=DeleteSelectedDriverMesh
    )

    cmds.button(
        label="Update Drivers in UI",
        annotation="Update the driver mesh in textScrollList",
        command=pbs_list.list_update
    )

    # driver list UI
    g_listUI = cmds.textScrollList(numberOfRows=8, allowMultiSelection=True, showIndexedItem=4, selectCommand=testDeformer_selectDriverCB);

    cmds.button(
        label="Show Deform Effect",
        annotation="Show Deform Effect",
        command=testDeformer_ShowDeformEffect
    )
    cmds.button(
        label="Start Painting",
        annotation="Paint",
        command=testDeformer_StartPainting
    )
    cmds.button(
        label="Stop  Painting",
        annotation="Stop Painting",
        command=testDeformer_StopPainting
    )

    cmds.button(
        label='Backup Drivers Data',
        annotation='Backup Drivers Data',
        command=testDeformer_BackupDriversData
    )
    cmds.button(
        label='Print the Orig Node of Selected Node',
        annotation='Select a node and press this button to print its orig node',
        command=testDeformer_PrintOrigNode
    )
    cmds.showWindow(pbsWindow)

    #---------------------

@flog.trace
def testDeformer_deleteUI():
    cmds.deleteUI('PartialBlendShape', window=True)


if '__main__' == __name__:
    pass
