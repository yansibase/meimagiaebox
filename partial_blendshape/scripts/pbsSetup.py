# -*- coding: utf-8 -*-

import maya.cmds                    as cmds
import maya.mel                     as mel
#import pymel.core                   as pm
import pymel.tools.py2mel           as py2mel
import mymagicbox.log               as log
import mymagicbox.mmb_flog          as flog


log.debug('This is '+__file__)

gDeveloperMode = False
################################################################
@flog.trace
def about():
    log.info('=================================================')
    log.info('--------------------- NOTE ----------------------')
    log.info('=================================================')
    pass

################################################################
@flog.trace
def setupUI():
    import testDeformer_ui      as ui
    ui.testDeformer_ui()
    pass

@flog.trace
def unsetupUI():
    import testDeformer_ui      as ui
    ui.testDeformer_deleteUI()
    pass

@flog.trace
def setup():
    pass

@flog.trace
def unsetup():
    pass

# register the above python functions as mel procedures
py2mel.py2melProc(setupUI,   procName='pbs_setupUI',   evaluateInputs=True);
py2mel.py2melProc(unsetupUI, procName='pbs_unsetupUI', evaluateInputs=True);
py2mel.py2melProc(setup,     procName='pbs_setup',     evaluateInputs=True);
py2mel.py2melProc(unsetup,   procName='pbs_unsetup',   evaluateInputs=True);

