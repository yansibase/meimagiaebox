import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

import testDeformer_ui
import testDeformer    as pbs

#-------------------------------------------------------------------------------
class TestPBS1(object):
    m_group     = '|NoOrig'
    m_driven    = m_group+'|driven'
    m_drivers   = [m_group+'|driver0']
    m_unittest  = None
    m_pbs       = ''

    def __init__(self, unittest):
        log.debug('This is partial_blendshape/test/test_0008/test.py (orig node)')
        self.m_unittest = unittest

    def createPBSNode(self):
        cmds.select(self.m_driven, r=True)
        self.m_pbs = testDeformer_ui.testDeformer_CreateDeformerNode()

    def _switchToThisPBSNode(self):
        cmds.select(self.m_driven, r=True)
        testDeformer_ui.testDeformer_SetDeformerNode()

    def addDrivers(self):
        self._switchToThisPBSNode()

        cmds.select([ self.m_drivers[0] ], r=True)
        testDeformer_ui.AddSelectedDriverMesh()
        pbs.setPBSState(1)
        '''
mybox| Dbg| ---------- Backup Node Info ---------- backup_testDeformer1_driver0,
mybox| Dbg| backup node: backup_testDeformer1_driver0
mybox| Dbg|      vertexIndex[] = [1, 0, 2, 3]
mybox| Dbg|      objectWeight  = 1
mybox| Dbg|      vertexWeight[]= [0.30000001192092896, 1.0, 1.0, 1.0]
mybox| Dbg| ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        '''
        attrName0 = self.m_pbs+'.drivers[0].vertexIndex'
        self.m_unittest.assertEqual(cmds.getAttr(attrName0), [(1, 0, 2, 3)])# it is NOT (0, 1, 2, 3), cause it uses the deformed driver node to map the vertex index

#-------------------------------------------------------------------------------
class TestPBS2(object):
    m_group     = '|HasOrig'
    m_driven    = m_group+'|driven'
    m_drivers   = [m_group+'|driver1']
    m_unittest  = None
    m_pbs       = ''

    def __init__(self, unittest):
        log.debug('Hello, this is partial_blendshape/test/test_0008/test.py TestPBS2')
        self.m_unittest = unittest

    def createPBSNode(self):
        cmds.select(self.m_driven, r=True)
        self.m_pbs = testDeformer_ui.testDeformer_CreateDeformerNode()

    def _switchToThisPBSNode(self):
        cmds.select(self.m_driven, r=True)
        testDeformer_ui.testDeformer_SetDeformerNode()

    def addDrivers(self):
        self._switchToThisPBSNode()

        cmds.select([ self.m_drivers[0] ], r=True)
        testDeformer_ui.AddSelectedDriverMesh()
        pbs.setPBSState(1)
        '''
mybox| Dbg| ---------- Backup Node Info ---------- backup_testDeformer2_driver0,
mybox| Dbg| backup node: backup_testDeformer2_driver0
mybox| Dbg|      vertexIndex[] = [0, 1, 2, 3]
mybox| Dbg|      objectWeight  = 1
mybox| Dbg|      vertexWeight[]= [0.30000001192092896, 1.0, 1.0, 1.0]
mybox| Dbg| ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        '''
        attrName0 = self.m_pbs+'.drivers[0].vertexIndex'
        self.m_unittest.assertEqual(cmds.getAttr(attrName0), [(0, 1, 2, 3)]) # it is (0, 1, 2, 3), cause it uses the orig node to map the vertex index

#-------------------------------------------------------------------------------
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()

    def test00(self):
        '''
        import maya.cmds
        maya.cmds.loadPlugin("automation")

        import unittest
        suite = unittest.defaultTestLoader.discover(start_dir="/backup/lhome/dev/mybox/partial_blendshape/test/test_0008")
        unittest.TextTestRunner(verbosity=3).run(suite)
        '''
        log.debug('---- begin %s.test00() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        testPBS1 = TestPBS1(self)
        testPBS2 = TestPBS2(self)
        #----------------------------------

        #- show Main UI window
        testDeformer_ui.testDeformer_ui()

        #- create deformer node
        testPBS1.createPBSNode()
        testPBS2.createPBSNode()

        #- add drivers
        testPBS1.addDrivers()
        testPBS2.addDrivers()

        #-------------------test deform----------------------
        #testPBS1.testDeform()
        #testPBS2.testDeform()

        #------------------ test painting------begin-------------
        #testPBS1.testPaint()
        #testPBS2.testPaint()


        #---------------------save as -------------------------
        cmds.file(rename=os.path.expanduser('~')+'/test_saveAs.ma')
        cmds.file(save=True, type='mayaAscii', options="v=0;")
        #------------------------------------------------------

        #- close Main UI window
        testDeformer_ui.testDeformer_deleteUI()


        #----------------------------------

        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
