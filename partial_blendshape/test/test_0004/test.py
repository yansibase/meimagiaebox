import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()

    def test00(self):
        log.debug('---- begin %s.test00() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        log.debug('Hello, this is partial_blendshape/test/test_0004/test.py')
        #----------------------------------
        import testDeformer_ui
        import testDeformer    as pbs
        #- show Main UI window
        testDeformer_ui.testDeformer_ui()

        #- create deformer node
        cmds.select('driven', r=True)
        testDeformer_ui.testDeformer_CreateDeformerNode()

        #- add drivers
        cmds.select(['driver0','driver1'], r=True)
        testDeformer_ui.AddSelectedDriverMesh()

        #- set testDeformer1.initialize=1
        cmds.setAttr('testDeformer1.initialize', 1)

        cmds.refresh(force=True)

        #- set testDeformer1.initialize=2
        cmds.setAttr('testDeformer1.initialize', 2)

        #cmds.getAttr('driver0Shape.vtx[0]')

        #-------------------test deform----------------------
        #- deform on driver0
        cmds.select('driver0.vtx[0]', r=True)
        cmds.move(0, 0, 1, r=True) #cmds.setAttr('driver0.vtx[0]', 0, 1, 0)
        self.assertEqual(cmds.getAttr('driven.vrts[0]'), [(-0.5, -0.5, 1.5)])

        #- deform on driver1
        cmds.select('driver1.vtx[0]', r=True)
        cmds.move(0, 0, 1, r=True) #cmds.setAttr('driver1.vtx[0]', 0, 1, 0)
        self.assertEqual(cmds.getAttr('driven.vrts[0]'), [(-0.5, -0.5, 1.5)])

        #------------------ test painting------begin-------------
        testDeformer_ui.testDeformer_StartPainting()

        # paint to driver0
        testDeformer_ui.testDeformer_selectDriver(1)# 'driver0'
        attrName = 'testDeformer1.drivers[0].vertexWeight'
        self.assertEqual(cmds.getAttr(attrName), [(1.0, 1.0, 1.0, 1.0)])
        # paint driver0's vtx0's weight to 0.3
        cmds.setAttr('testDeformer1.weightList[0].weights[0]', 0.3)
        self.assertAlmostEqual(cmds.getAttr(attrName+'[0]'), 0.3)

        # paint to driver1
        testDeformer_ui.testDeformer_selectDriver(2)# 'driver1'
        attrName = 'testDeformer1.drivers[1].vertexWeight'
        self.assertEqual(cmds.getAttr(attrName), [(1.0, 1.0, 1.0, 1.0, 1.0, 1.0)])
        # paint driver1's vtx0's weight to 0.5
        cmds.setAttr('testDeformer1.weightList[0].weights[1]', 0.5)
        self.assertAlmostEqual(cmds.getAttr(attrName+'[0]'), 0.5)

        testDeformer_ui.testDeformer_StopPainting()
        #------------------ test painting----end---------------

        #---------------------save as -------------------------
        cmds.file(rename=os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR']+'/test_saveAs.ma')
        cmds.file(save=True, type='mayaAscii', options="v=0;")
        #------------------------------------------------------

        #- close Main UI window
        testDeformer_ui.testDeformer_deleteUI()


        #----------------------------------

        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
