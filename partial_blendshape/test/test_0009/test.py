import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

import testDeformer_ui
import testDeformer    as pbs


#-------------------------------------------------------------------------------
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()

    def test00(self):
        '''
        import maya.cmds
        maya.cmds.loadPlugin("automation")

        import unittest
        suite = unittest.defaultTestLoader.discover(start_dir="/backup/lhome/dev/mybox/partial_blendshape/test/test_0009")
        unittest.TextTestRunner(verbosity=3).run(suite)
        '''
        log.debug('---- begin %s.test00() ----', self.m_id);
        #super(MyTest00, self).test_animation()



        self.assertEqual(pbs.getOrigNode('|pCube1|pCubeShape1'),                  '')
        
        self.assertEqual(pbs.getOrigNode('|pCube2_dvr|pCube2_dvrShape'),          '')
        self.assertEqual(pbs.getOrigNode('|pCube2|pCubeShape2'),                  'pCubeShape2Orig')

        self.assertEqual(pbs.getOrigNode('|pCube3|pCubeShape3'),                  'pCubeShape3Orig')
        self.assertEqual(pbs.getOrigNode('|pCube3_dvr|pCube3_dvrShape'),          'pCube3_dvrShapeOrig')
        self.assertEqual(pbs.getOrigNode('|pCube3_dvr_dvr|pCube3_dvr_dvrShape'),  '')  

        self.assertEqual(pbs.getOrigNode('|skin|skinShape'),                      'skinShapeOrig')

        self.assertEqual(pbs.getOrigNode('|pCube5|pCubeShape5'),                  'pCubeShape5Orig')

        #----------------------------------

        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
