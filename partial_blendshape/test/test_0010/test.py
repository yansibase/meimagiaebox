import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()

    def test00(self):
        '''
        import maya.cmds
        maya.cmds.loadPlugin("automation")

        import unittest
        suite = unittest.defaultTestLoader.discover(start_dir="/backup/lhome/dev/mybox/partial_blendshape/test/test_0006")
        unittest.TextTestRunner(verbosity=3).run(suite)
        '''
        log.debug('---- begin %s.test00() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        log.debug('Hello, this is partial_blendshape/test/test_0006/test.py')
        #=======================================================================
        import testDeformer_ui
        import testDeformer    as pbs
        #-----------------------------------
        # show PartialBlendShape window which is the main UI
        #-----------------------------------
        testDeformer_ui.testDeformer_ui()
        
        
        #-----------------------------------
        # create deformer node for node 'driven'
        #-----------------------------------
        # select the driven node
        cmds.select('driven', r=True)
        # press "Create Deformer Node" button  in PartialBlendShape window to create deformer node
        testDeformer_ui.testDeformer_CreateDeformerNode()

        # map method:
        # 0 - "point position in object space"
        # 1 - "uv"
        pbs.setPBSMapMethod(1)

        #-----------------------------------
        # add driver node(s)
        #-----------------------------------
        # select the driver node(s)
        cmds.select(['driver0','driver1'], r=True)
        # press "Add Driver Mesh(es)" button in PartialBlendShape window to add driver node(s)
        testDeformer_ui.AddSelectedDriverMesh()


        #-----------------------------------
        # initilize the vertex index mapping 
        # between driver node(s) and driven node
        #-----------------------------------
        # How to do this in Maya:
        #  - Select PBS node(testDeformer)
        #  - In Attribute Editor, set Initialize to 'init the vertex index mapping'
        pbs.setPBSState(1)
        # After you set PBS state to 1, the program will run into TestDeformer::deform()
        # TestDeformer::deform(){
        #     ...
        #     else if( initialized_mapping == 1 )
        #     {
        #          // the program will run into here
        #     }
        #     ...
        # }

        # Maya will not refresh editor window until the following command is executed
        cmds.refresh(force=True)


        #-----------------------------------
        # show the deform effect on driven node
        #-----------------------------------
        # How to do this in Maya:
        #  - Select PBS node(e.g. testDeformer)
        #  - In Attribute Editor, set the value of attribute 'Initialize' to 'show the deform effect'
        #
        # After attribute 'initialize' being set to 'show the deform effect', 
        # the driven node will be deformed along with the driver node.
        #
        # If you don't set attribute 'initialize' to 'show the deform effect', 
        # the driven node will NOT be deformed at all, even the driver is deformed.
        pbs.setPBSState(2)
        # After you set PBS state to 2, the program will run into TestDeformer::deform()
        # TestDeformer::deform(){
        #     ...
        #     else if( initialized_mapping == 2 )
        #     {
        #          // the program will run into here
        #     }
        #     ...
        # }
        
        
        #-------------------test deform----------------------
        # This is unit test, I will check if the deform value is correct.
        
        # move a vertex on driver0 to make a deformation on driver0
        # Since we set 'show the deform effect' on the PBS node of the driven node,
        # the driven node will be deformed synchronously
        cmds.select('driver0.vtx[0]', r=True)
        cmds.move(0, 0, 1, r=True)
        # So, let's check the deformation value on the driven node.
        self.assertEqual(cmds.getAttr('driven.vrts[0]'), [(-0.5, -0.5, 1.5)])

        # move a vertex on driver1 to make a deformation on driver1
        # Since we set 'show the deform effect' on the PBS node of the driven node,
        # the driven node will be deformed synchronously
        cmds.select('driver1.vtx[0]', r=True)
        cmds.move(0, 0, 1, r=True)
        # So, let's check the deformation value on the driven node.
        self.assertEqual(cmds.getAttr('driven.vrts[0]'), [(-0.5, -0.5, 1.5)])

        #------------------ test painting------begin-------------
        # press "Start Painting" in PartialBlendShape window 
        testDeformer_ui.testDeformer_StartPainting()
    
        # NOTE:
        # In painting mode, Maya can modify the 'weights' attribute of the deformer node only.
        # So, how to save the painted weight?
        # The answer is when we select a driver node, the $PBS.weights is connected to $PBS.drivers[i].vertexWeight. 
        # (You can see doc/attr-connection-when-painting.png)
        # This is done in testDeformer_dummyWeight.test_PreparePaintOnDriver()
        # So, when you paint the driven mesh, the paint weight will be updated to $PBS.drivers[i].vertexWeight automatically.
        
        #------------------------------
        # paint to driver0
        #------------------------------
        # select driver0, and testDeformer.weights is connected to testDeformer.drivers[0].vertexWeight
        testDeformer_ui.testDeformer_selectDriver(1)#, 'driver0'
        attrName0 = 'testDeformer1.drivers[0].vertexWeight'
        self.assertEqual(cmds.getAttr(attrName0), [(1.0, 1.0, 1.0, 1.0)]) # before painting driver0
        # paint driver0's vtx0's weight to 0.3
        cmds.setAttr('testDeformer1.weightList[0].weights[0]', 0.3)      # paint a vertex of driver0
        self.assertAlmostEqual(cmds.getAttr(attrName0+'[0]'), 0.3)       # after painting driver0

        #------------------------------
        # paint to driver1
        #------------------------------
        # select driver1, and testDeformer.weights is connected to testDeformer.drivers[1].vertexWeight
        testDeformer_ui.testDeformer_selectDriver(2)#, 'driver1'
        attrName1 = 'testDeformer1.drivers[1].vertexWeight'
        self.assertEqual(cmds.getAttr(attrName1), [(1.0, 1.0, 1.0, 1.0, 1.0, 1.0)])# before painting driver1
        # paint driver1's vtx0's weight to 0.5
        cmds.setAttr('testDeformer1.weightList[0].weights[1]', 0.5)       # paint a vertex of driver1
        self.assertAlmostEqual(cmds.getAttr(attrName1+'[0]'), 0.5)        # after painting driver1
        
        # If we don't paint anything else, press "Stop Painting" in PartialBlendShape window to complete the painting.
        testDeformer_ui.testDeformer_StopPainting()
        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        #-- test adding a new driver, make sure the vertex weight of previous drivers is not reset -- begin --
        cmds.select('driver2', r=True)
        testDeformer_ui.AddSelectedDriverMesh()

        self.assertAlmostEqual(cmds.getAttr(attrName0+'[0]'), 0.3) # make sure the vertex weight of driver0 is not reset
        self.assertAlmostEqual(cmds.getAttr(attrName1+'[0]'), 0.5) # make sure the vertex weight of driver1 is not reset
        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        #---------------------save as -------------------------
        cmds.file(rename=os.path.expanduser('~')+'/test_saveAs.ma')
        cmds.file(save=True, type='mayaAscii', options="v=0;")
        #------------------------------------------------------

        #- close Main UI window
        testDeformer_ui.testDeformer_deleteUI()


        #----------------------------------

        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
