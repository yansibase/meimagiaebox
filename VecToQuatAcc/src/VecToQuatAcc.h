
#pragma once

#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MGlobal.h>
#include <maya/MPointArray.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MFnMesh.h>
#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnDagNode.h>
#include <maya/MQuaternion.h>
#include <maya/MFloatVector.h>

#include <common/version_helper.h>
//#define _DEBUG

class VecToQuatAccNode : public MPxNode
{
public:
                            VecToQuatAccNode();
	virtual			        ~VecToQuatAccNode();

	static  MString         cTypeName();
	static  MTypeId         cTypeId();
	static  MPxNode::Type   cType();
	static  const MString&  cClassification();

	static  void *	        creator();
	static  MStatus	        initialize();
	virtual void	        postConstructor();
    virtual MStatus         compute(const MPlug &plug, MDataBlock &dataBlock);

private:
    MFloatVector  old_vector;
    MQuaternion quaternionAcc;

protected:
	static MString  m_classification;
	static VersionHelper    m_version_helper;

    static MObject  aInVector;
    static MObject  aUpVector;
    static MObject  aOutDeltaVector;

    static MObject  aInRadius;
    static MObject  aInCurrentTime;
    static MObject  aInStartTime;
    static MObject  aInEndTime;
    static MObject  aInRevert;

    static MObject  aInOffsetEulerRot;
    static MObject  aInInjectTime;

    static MObject  aOutQuat;
    static MObject      aOutQuatX;
    static MObject      aOutQuatY;
    static MObject      aOutQuatZ;
    static MObject      aOutQuatW;
    static MObject  aOutQuatAcc;
    static MObject      aOutQuatAccX;
    static MObject      aOutQuatAccY;
    static MObject      aOutQuatAccZ;
    static MObject      aOutQuatAccW;

};
