#include <maya/MFnPlugin.h>
#include <maya/MStatus.h>
#include <common/log.h>
#include "VecToQuatAcc.h"
#include "version.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya
//
PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
	OPEN_CONSOLE_WINDOW_FOR_DEBUG();

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('"+VecToQuatAccNode::cTypeName()+"')", true, false) );

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

	CHECK_MSTATUS(
		plugin.registerNode(
            VecToQuatAccNode::cTypeName(),
            VecToQuatAccNode::cTypeId(),
            VecToQuatAccNode::creator,
            VecToQuatAccNode::initialize,
            VecToQuatAccNode::cType(),
            &VecToQuatAccNode::cClassification()
		) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += VecToQuatAccNode::cClassification();
	command += "\");}\n";

	CHECK_MSTATUS( MGlobal::executeCommand( command ) );

	// import VecToQuatAccSetup
	CHECK_MSTATUS( MGlobal::executePythonCommand("import "+VecToQuatAccNode::cTypeName()+"Setup", true, false) );
	CHECK_MSTATUS(plugin.registerUI(
                    VecToQuatAccNode::cTypeName()+"_setupUI()",
                    VecToQuatAccNode::cTypeName()+"_unsetupUI()",
                    VecToQuatAccNode::cTypeName()+"_setup()",
                    VecToQuatAccNode::cTypeName()+"_unsetup()"
                    ));

	/// unit test
	//unittest_main();

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('"+VecToQuatAccNode::cTypeName()+"')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('"+VecToQuatAccNode::cTypeName()+"')", true, false) );

	MFnPlugin plugin( obj );

	CHECK_MSTATUS( plugin.deregisterNode( VecToQuatAccNode::cTypeId() ) );

	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += VecToQuatAccNode::cClassification();
	command += "\");}\n";

	CHECK_MSTATUS( MGlobal::executeCommand( command ) );

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('"+VecToQuatAccNode::cTypeName()+"')", true, false) );

	return MS::kSuccess;
}
