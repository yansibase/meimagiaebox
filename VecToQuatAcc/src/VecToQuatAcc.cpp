
#include "VecToQuatAcc.h"

#include <cfloat>
#include <cassert>
#include <vector>
#include <maya/MEulerRotation.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MPlugArray.h>
#include <maya/MTime.h>
#include <common/log.h>
#include <common/node_ids.h>
#include "version.h"


// For local testing of nodes you can use any identifier between
// 0x00000000 and 0x0007ffff, but for any node that you plan to use for
// more permanent purposes, you should get a universally unique id from
// Autodesk Support. You will be assigned a unique range that you
// can manage on your own.
//
MString         VecToQuatAccNode::m_classification("utility/general");
VersionHelper   VecToQuatAccNode::m_version_helper;

///////////////////////////////////////////////////////
// DESCRIPTION: attribute information
///////////////////////////////////////////////////////
MObject  VecToQuatAccNode::aInVector;
MObject  VecToQuatAccNode::aUpVector;
MObject  VecToQuatAccNode::aOutDeltaVector;
MObject  VecToQuatAccNode::aInRadius;
MObject  VecToQuatAccNode::aInCurrentTime;
MObject  VecToQuatAccNode::aInStartTime;
MObject  VecToQuatAccNode::aInEndTime;
MObject  VecToQuatAccNode::aInRevert;
MObject  VecToQuatAccNode::aInOffsetEulerRot;
MObject  VecToQuatAccNode::aInInjectTime;
MObject  VecToQuatAccNode::aOutQuat;
MObject  VecToQuatAccNode::    aOutQuatX;
MObject  VecToQuatAccNode::    aOutQuatY;
MObject  VecToQuatAccNode::    aOutQuatZ;
MObject  VecToQuatAccNode::    aOutQuatW;
MObject  VecToQuatAccNode::aOutQuatAcc;
MObject  VecToQuatAccNode::    aOutQuatAccX;
MObject  VecToQuatAccNode::    aOutQuatAccY;
MObject  VecToQuatAccNode::    aOutQuatAccZ;
MObject  VecToQuatAccNode::    aOutQuatAccW;
//
VecToQuatAccNode::VecToQuatAccNode()
: MPxNode()
{
//    old_infloat0    = 0.0f;
//    old_infloat1    = 0.0f;
    old_vector      = MVector::zero;
    quaternionAcc   = MQuaternion::identity;
}
//
VecToQuatAccNode::~VecToQuatAccNode()
{
}
////
MString VecToQuatAccNode::cTypeName()
{
	return "VecToQuatAcc";
}
//
MTypeId VecToQuatAccNode::cTypeId()
{
	return (NodeID_VecToQuatAcc);
}
//
MPxNode::Type VecToQuatAccNode::cType()
{
	return MPxNode::kDependNode;
}
//
const MString& VecToQuatAccNode::cClassification()
{
	return m_classification;
}
//
void* VecToQuatAccNode::creator()
{
	return new VecToQuatAccNode();
}
//
MStatus VecToQuatAccNode::initialize()
{
	MFnNumericAttribute nAttr;
    MFnMatrixAttribute mAttr;
    MFnCompoundAttribute cAttr;
    MFnUnitAttribute uAttr;

	MStatus status; // Status will be used to hold the MStatus value

    // aInVector
    aInVector = nAttr.createPoint("vector", "v", &status); CHECK_MSTATUS(status);
    CHECK_MSTATUS(nAttr.setDefault(0.0f, 0.0f, 0.0f));
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInVector));

    // aUpVector
    aUpVector = nAttr.createPoint("up", "u", &status); CHECK_MSTATUS(status);
    CHECK_MSTATUS(nAttr.setDefault(0.0f, 1.0f, 0.0f));
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aUpVector));

    // aOutDeltaVector
    aOutDeltaVector = nAttr.createPoint("deltavector", "dv", &status); CHECK_MSTATUS(status);
    CHECK_MSTATUS(nAttr.setStorable(false));
    CHECK_MSTATUS(nAttr.setWritable(false));
    //CHECK_MSTATUS(nAttr.setReadable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aOutDeltaVector));

    // aInRadius
    aInRadius = nAttr.create("radius", "r", MFnNumericData::kFloat, 1.0f, &status); CHECK_MSTATUS(status);
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInRadius));

    // aInCurrentTime
    aInCurrentTime = uAttr.create("currentTime", "ct", MFnUnitAttribute::kTime, 1.0, &status); CHECK_MSTATUS(status);
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInCurrentTime));

    // aInStartTime
    aInStartTime = uAttr.create("startTime", "st", MFnUnitAttribute::kTime, 1.0, &status); CHECK_MSTATUS(status);
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInStartTime));

    // aInEndTime
    aInEndTime = uAttr.create("endTime", "et", MFnUnitAttribute::kTime, 99999.0, &status); CHECK_MSTATUS(status);
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInEndTime));

    // aInRevert
    aInRevert = nAttr.create( "revert", "rvt", MFnNumericData::kBoolean, false, &status); CHECK_MSTATUS(status);
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInRevert));

    // aInOffsetEulerRot
    aInOffsetEulerRot = nAttr.createPoint("offsetEulerRot", "oer", &status); CHECK_MSTATUS(status);
    CHECK_MSTATUS(nAttr.setDefault(0.0f, 0.0f, 0.0f));
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInOffsetEulerRot));

    // aInInjectTime
    aInInjectTime = uAttr.create("injectTime", "it", MFnUnitAttribute::kTime, 99999.0, &status); CHECK_MSTATUS(status);
    //CHECK_MSTATUS(nAttr.setWritable(true));
    //CHECK_MSTATUS(nAttr.setStorable(true));
    //CHECK_MSTATUS(nAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aInInjectTime));

    // aOutQuatX, aOutQuatY, aOutQuatZ, aOutQuatW
    {
        aOutQuatX = nAttr.create( "outqx", "oqx", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatX));

        aOutQuatY = nAttr.create( "outqy", "oqy", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatY));

        aOutQuatZ = nAttr.create( "outqz", "oqz", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatZ));

        aOutQuatW = nAttr.create( "outqw", "oqw", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatW));
    }
    // aOutQuat
    aOutQuat = cAttr.create( "outq", "oq", &status); CHECK_MSTATUS(status);
    CHECK_MSTATUS(cAttr.addChild(aOutQuatX));
    CHECK_MSTATUS(cAttr.addChild(aOutQuatY));
    CHECK_MSTATUS(cAttr.addChild(aOutQuatZ));
    CHECK_MSTATUS(cAttr.addChild(aOutQuatW));
    CHECK_MSTATUS(cAttr.setStorable(false));
    CHECK_MSTATUS(cAttr.setWritable(false));
    //CHECK_MSTATUS(cAttr.setReadable(true));
    //CHECK_MSTATUS(cAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aOutQuat));

    // aOutQuatAccX, aOutQuatAccY, aOutQuatAccZ, aOutQuatAccW
    {
        aOutQuatAccX = nAttr.create( "outqax", "oqax", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatAccX));

        aOutQuatAccY = nAttr.create( "outqay", "oqay", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatAccY));

        aOutQuatAccZ = nAttr.create( "outqaz", "oqaz", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatAccZ));

        aOutQuatAccW = nAttr.create( "outqaw", "oqaw", MFnNumericData::kDouble, 0.0f, &status); CHECK_MSTATUS(status);
        CHECK_MSTATUS(nAttr.setStorable(false));
        CHECK_MSTATUS(nAttr.setWritable(false));
        //CHECK_MSTATUS(nAttr.setReadable(true));
        //CHECK_MSTATUS(nAttr.setHidden(false));
        CHECK_MSTATUS(addAttribute(aOutQuatAccW));
    }
    // aOutQuatAcc
    aOutQuatAcc = cAttr.create( "outqa", "oqa", &status); CHECK_MSTATUS(status);
    CHECK_MSTATUS(cAttr.addChild(aOutQuatAccX));
    CHECK_MSTATUS(cAttr.addChild(aOutQuatAccY));
    CHECK_MSTATUS(cAttr.addChild(aOutQuatAccZ));
    CHECK_MSTATUS(cAttr.addChild(aOutQuatAccW));
    CHECK_MSTATUS(cAttr.setStorable(false));
    CHECK_MSTATUS(cAttr.setWritable(false));
    //CHECK_MSTATUS(cAttr.setReadable(true));
    //CHECK_MSTATUS(cAttr.setHidden(false));
    CHECK_MSTATUS(addAttribute(aOutQuatAcc));


    //==================================
    // NODE ATTRIBUTE DEPENDENCIES
    //==================================
    // If aInFloat changes, the aOutDeltaFloat data must be recomputed.
    CHECK_MSTATUS(attributeAffects(aInVector, aOutDeltaVector));
	CHECK_MSTATUS(attributeAffects(aInVector, aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInVector, aOutQuatAcc));

    CHECK_MSTATUS(attributeAffects(aUpVector, aOutDeltaVector));
	CHECK_MSTATUS(attributeAffects(aUpVector, aOutQuat));
    CHECK_MSTATUS(attributeAffects(aUpVector, aOutQuatAcc));

	CHECK_MSTATUS(attributeAffects(aInRadius, aOutDeltaVector));
    CHECK_MSTATUS(attributeAffects(aInRadius, aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInRadius, aOutQuatAcc));

    CHECK_MSTATUS(attributeAffects(aInCurrentTime, aOutDeltaVector));
    CHECK_MSTATUS(attributeAffects(aInCurrentTime, aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInCurrentTime, aOutQuatAcc));

    CHECK_MSTATUS(attributeAffects(aInStartTime, aOutDeltaVector));
    CHECK_MSTATUS(attributeAffects(aInStartTime, aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInStartTime, aOutQuatAcc));

    CHECK_MSTATUS(attributeAffects(aInEndTime, aOutDeltaVector));
    CHECK_MSTATUS(attributeAffects(aInEndTime, aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInEndTime, aOutQuatAcc));

    CHECK_MSTATUS(attributeAffects(aInRevert,  aOutDeltaVector));
    CHECK_MSTATUS(attributeAffects(aInRevert,  aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInRevert,  aOutQuatAcc));

    CHECK_MSTATUS(attributeAffects(aInOffsetEulerRot,  aOutDeltaVector));
    CHECK_MSTATUS(attributeAffects(aInOffsetEulerRot,  aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInOffsetEulerRot,  aOutQuatAcc));

    CHECK_MSTATUS(attributeAffects(aInInjectTime,  aOutDeltaVector));
    CHECK_MSTATUS(attributeAffects(aInInjectTime,  aOutQuat));
    CHECK_MSTATUS(attributeAffects(aInInjectTime,  aOutQuatAcc));

	m_version_helper.initialize();

	return MS::kSuccess;
}
//
void VecToQuatAccNode::postConstructor( )
{
	// setMPSafe indicates that this shader can be used for multiprocessor
	// rendering. For a shading node to be MP safe, it cannot access any
	// shared global data and should only use attributes in the datablock
	// to get input data and store output data.
	//
	setMPSafe( true );

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}
//
MStatus VecToQuatAccNode::compute(const MPlug &plug, MDataBlock &dataBlock)
{
    MStatus status;

#ifdef DEBUG
	MString plugname(plug.name(&status));
	CHECK_MSTATUS(status);
#endif

    if( (plug == VecToQuatAccNode::aOutDeltaVector)
       || (plug == VecToQuatAccNode::aOutQuat)      || (plug.parent(&status) == VecToQuatAccNode::aOutQuat)
       || (plug == VecToQuatAccNode::aOutQuatAcc)   || (plug.parent(&status) == VecToQuatAccNode::aOutQuatAcc)
       )
    {
		//CHECK_MSTATUS(status);
        //--------------------------------------------
        MDataHandle dhInVector = dataBlock.inputValue(VecToQuatAccNode::aInVector, &status);  CHECK_MSTATUS(status);
        const MFloatVector& invector = dhInVector.asFloatVector();
#ifdef DEBUG
        LDbg("invector=[%f, %f, %f] old_invector=[%f, %f, %f]", invector[0], invector[1], invector[2],  old_vector[0], old_vector[1], old_vector[2]);
#endif
        const MFloatVector delta_vector(invector - old_vector);

        MDataHandle dhOutDeltaVector = dataBlock.outputValue(VecToQuatAccNode::aOutDeltaVector,   &status);  CHECK_MSTATUS(status);
        dhOutDeltaVector.set(delta_vector);
        dhOutDeltaVector.setClean();

         // update the old value
        old_vector = invector;

#ifdef DEBUG
        LDbg("delta_vector=[%f, %f, %f]", delta_vector[0], delta_vector[1], delta_vector[2]);
#endif
        //--------------------------------------------
        MDataHandle dhUpVector = dataBlock.inputValue(VecToQuatAccNode::aUpVector, &status);  CHECK_MSTATUS(status);
        const MFloatVector& U = dhUpVector.asFloatVector();

        MDataHandle dhInRadius = dataBlock.inputValue(VecToQuatAccNode::aInRadius, &status);  CHECK_MSTATUS(status);
        const float inRadius = dhInRadius.asFloat();

        //--------------------------------------------
        const MVector V(delta_vector[0], delta_vector[1], delta_vector[2]);
        const MVector Vn(V.normal());   // normalized vector of V
        const MVector Un(U.normal());   // normalized vector of Up
        const MVector Rn(Un ^ Vn);      // normalized vector of Rotation direction

        const double rotate_angle_in_radian = V.length()/inRadius;
        const MQuaternion Q(rotate_angle_in_radian, Rn);
#ifdef DEBUG
        LDbg("Q=[%f, %f, %f,   %f]", Q.x, Q.y, Q.z, Q.w);
#endif
        MDataHandle dhInCurrentTime     = dataBlock.inputValue(VecToQuatAccNode::aInCurrentTime, &status);  CHECK_MSTATUS(status);
        MDataHandle dhInStartTime       = dataBlock.inputValue(VecToQuatAccNode::aInStartTime,   &status);  CHECK_MSTATUS(status);
        MDataHandle dhInEndTime         = dataBlock.inputValue(VecToQuatAccNode::aInEndTime,     &status);  CHECK_MSTATUS(status);
        MDataHandle dhInRevert          = dataBlock.inputValue(VecToQuatAccNode::aInRevert,      &status);  CHECK_MSTATUS(status);
        MDataHandle dhInOffsetEulerRot  = dataBlock.inputValue(VecToQuatAccNode::aInOffsetEulerRot, &status);  CHECK_MSTATUS(status);
        MDataHandle dhInInjectTime      = dataBlock.inputValue(VecToQuatAccNode::aInInjectTime,  &status);  CHECK_MSTATUS(status);


        MTime currentTime                   = dhInCurrentTime.asTime();
        MTime startTime                     = dhInStartTime.asTime();
        MTime endTime                       = dhInEndTime.asTime();
        bool  revert                        = dhInRevert.asBool();
        const MFloatVector& offsetEulerRot  = dhInOffsetEulerRot.asFloatVector();
        MTime injectTime                    = dhInInjectTime.asTime();

#ifdef DEBUG
        LDbg("currentTime:%f, startTime:%f", currentTime.value (), startTime.value ());
#endif



        if(currentTime <= startTime)// reset to default gesture
        {
#ifdef DEBUG
            LDbg("reset ...");
#endif
            if(!revert){
                quaternionAcc   = MQuaternion::identity;
            }else{
                // do nothing(stay to current gesture)
            }
        }
        else if(endTime <= currentTime)
        {
            if(!revert){
                // do nothing(stay to current gesture)
            }else{
                quaternionAcc   = MQuaternion::identity;
            }
        }
        else
        {
            // accumulate quaternion
            quaternionAcc = quaternionAcc * Q;
        }
#ifdef DEBUG
        LDbg("Qa=[%f, %f, %f,   %f]", quaternionAcc.x, quaternionAcc.y, quaternionAcc.z, quaternionAcc.w);
#endif
        if(currentTime == injectTime)
        {
            MEulerRotation eulerRot(offsetEulerRot * (M_PI/180.0), MEulerRotation::kXYZ);
            MQuaternion OffsetQuatRot(eulerRot.asQuaternion());
            quaternionAcc = OffsetQuatRot;
#ifdef DEBUG
            LDbg("eulerRot=[%f, %f, %f,   %d]", eulerRot.x, eulerRot.y, eulerRot.z, eulerRot.order);
            LDbg(" offsetQ=[%f, %f, %f,   %f]", OffsetQuatRot.x, OffsetQuatRot.y, OffsetQuatRot.z, OffsetQuatRot.w);
            LDbg("      Qa=[%f, %f, %f,   %f]", quaternionAcc.x, quaternionAcc.y, quaternionAcc.z, quaternionAcc.w);
#endif
        }
        //--------------------------------------------
        // aOutQuat
        MDataHandle dhOutQuatX = dataBlock.outputValue(VecToQuatAccNode::aOutQuatX,   &status);  CHECK_MSTATUS(status);
        MDataHandle dhOutQuatY = dataBlock.outputValue(VecToQuatAccNode::aOutQuatY,   &status);  CHECK_MSTATUS(status);
        MDataHandle dhOutQuatZ = dataBlock.outputValue(VecToQuatAccNode::aOutQuatZ,   &status);  CHECK_MSTATUS(status);
        MDataHandle dhOutQuatW = dataBlock.outputValue(VecToQuatAccNode::aOutQuatW,   &status);  CHECK_MSTATUS(status);
        dhOutQuatX.set(Q.x);dhOutQuatX.setClean();
        dhOutQuatY.set(Q.y);dhOutQuatY.setClean();
        dhOutQuatZ.set(Q.z);dhOutQuatZ.setClean();
        dhOutQuatW.set(Q.w);dhOutQuatW.setClean();

        // aOutQuatAcc
        MDataHandle dhOutQuatAccX = dataBlock.outputValue(VecToQuatAccNode::aOutQuatAccX,   &status);  CHECK_MSTATUS(status);
        MDataHandle dhOutQuatAccY = dataBlock.outputValue(VecToQuatAccNode::aOutQuatAccY,   &status);  CHECK_MSTATUS(status);
        MDataHandle dhOutQuatAccZ = dataBlock.outputValue(VecToQuatAccNode::aOutQuatAccZ,   &status);  CHECK_MSTATUS(status);
        MDataHandle dhOutQuatAccW = dataBlock.outputValue(VecToQuatAccNode::aOutQuatAccW,   &status);  CHECK_MSTATUS(status);
        dhOutQuatAccX.set(quaternionAcc.x);dhOutQuatAccX.setClean();
        dhOutQuatAccY.set(quaternionAcc.y);dhOutQuatAccY.setClean();
        dhOutQuatAccZ.set(quaternionAcc.z);dhOutQuatAccZ.setClean();
        dhOutQuatAccW.set(quaternionAcc.w);dhOutQuatAccW.setClean();

        return MS::kSuccess;
    }
    else
    {
        return MS::kUnknownParameter;
    }

    return MS::kSuccess;
}
