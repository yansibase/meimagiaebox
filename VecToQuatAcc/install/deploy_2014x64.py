
import os, sys, errno

#
def ensure_dir(dirname):
    """
    Ensure that a named directory exists; if it does not, attempt to create it.
    """
    try:
        os.makedirs(dirname)
    except OSError, e:
        if e.errno != errno.EEXIST:
            raise
#
def mycopy(source_dir, dest_dir, filters=[]):
    #print('mycopy('+source_dir+', '+dest_dir+', '); print(filters); print(')')
    ensure_dir(dest_dir)

    import glob, os, shutil
    files = []
    for filter_ in filters:
        files += glob.iglob(os.path.join(source_dir, filter_))
    #print files

    # copy files
    i = 0
    for f in files:
        if os.path.isfile(f):
            print('copy %s to %s'%(f, dest_dir));
            shutil.copy2(f, dest_dir)
            i=i+1;
    print("%d file(s) copied"%(i))

    # copy subdirectories
    subdirs = os.listdir(source_dir)
    for d in subdirs:
        if os.path.isdir(source_dir+'/'+d):
            mycopy(source_dir+'/'+d, dest_dir+'/'+d, filters)

#
def mydeploy(mybox_root_dir, plugin_name, des_root_dir, maya_version):
    plugin_root_dir    = mybox_root_dir +'/'+plugin_name
    #source_dir = plugin_root_dir+'/scripts/AETemplates'

    # 1.
    mycopy(plugin_root_dir+'/scripts',
           des_root_dir+'/scripts/'+plugin_name+'/scripts',
           ['*.py'])
    # 2.
    mycopy(plugin_root_dir+'/../common',
           des_root_dir+'/scripts/common',
           ['*.h', '*.py'])
    # 3.
    mycopy(plugin_root_dir+'/plug-ins',
           des_root_dir+'/'+maya_version+'/plugins',
           [plugin_name+'.*'])
    # 4.
    mycopy(plugin_root_dir+'/docs',
           des_root_dir+'/scripts/'+plugin_name+'/docs',
           ['*.txt'])




if __name__ == '__main__':
    usage  ='\n---------------------------------------------------------'
    usage +='\nThis is deploy_2014x64.py, and it is intended to be run under linux platform only, '
    usage +='\ne.g. $python ./deploy_2014x64.py ';
    usage +='\n---------------------------------------------------------'
    print(usage);

    mybox_root_dir = '/backup/lhome/dev/mybox'
    plugin_name   = 'VecToQuatAcc'

    des_root_dir   = '/job/PLE/studio/app/maya'
    #des_root_dir   = '/job/HOME/yaoys/mytmp/maya' # for test only
    print('NOTE: des_root_dir = %s \nMake sure this is exactly the destination root directory to which you`re going to copy.'%(des_root_dir));
    print('\n')

    maya_version   = '2014'

    mydeploy(mybox_root_dir, plugin_name, des_root_dir, maya_version);
