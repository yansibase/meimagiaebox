----------------------------------
Note
----------------------------------
- 使用该插件的时候, 在Maya-->Preferences-->Settings-->TimeSlider-->Playback里面,
Playback speed一定要设置为Player every frame, 否则每次播放的速度可能不一致,导致deltax和deltay不一致, 最终导致每次解算的结果不一致;
- 因为该插件是解算性质的, 所以使用该插件的时候, 每次播放动画请从第一帧播放;
- 使用该插件制作的shot, 每次提交shot之前, 请bake driver控制器. 这样的话, 如果之后要修改shot, (从第一帧播放的话)每一帧driven的状态是和之前是一致的;

----------------------------------
 What's new
----------------------------------
1.3.3
add attributes: endTime, revert
1.3.2
add automation test
1.3.1
add menu and menuItem(CreateWithDriverAndDriven) for this plugin
modify install scripts
1.2.8
VecToQuatAcc_setupUI(), VecToQuatAcc_unsetupUI(), VecToQuatAcc_setup(), VecToQuatAcc_unsetup()


----------------------------------
 How to deploy this plugin
----------------------------------
- backup the current plugin:
    - remove the following directories if it exists:
        /job/PLE/studio/app/maya/2014/plugins/VectToQuatAcc_old
        /job/PLE/studio/app/maya/scripts/common_old
        /job/PLE/studio/app/maya/scripts/VecToQuatAcc_old
    - move the current plugin to old directories:
        /job/PLE/studio/app/maya/2014/plugins/VectToQuatAcc.so  --> /job/PLE/studio/app/maya/2014/plugins/VectToQuatAcc_old/
        /job/PLE/studio/app/maya/2014/plugins/VectToQuatAcc.mll --> /job/PLE/studio/app/maya/2014/plugins/VectToQuatAcc_old/
        /job/PLE/studio/app/maya/scripts/common/*.*             --> /job/PLE/studio/app/maya/scripts/common_old
        /job/PLE/studio/app/maya/scripts/VecToQuatAcc/*.*       --> /job/PLE/studio/app/maya/scripts/VecToQuatAcc_old

- python /VecToQuatAcc/install/deploy_2014x64.py
- append some code to /job/PLE/studio/app/maya/scripts/userSetup.py
    if not cmds.about(batch=True):
        ...

        # load AETemplate for VecToQuatAcc node
        _ScriptRootDir = '';
        # we have different application root directory on each platform
        if cmds.about(linux=True):
            _ScriptRootDir = '/job/PLE/studio/app/maya/scripts'
        elif cmds.about(windows=True):
            _ScriptRootDir = 'Y:/PLE/studio/app/maya/scripts'
        else:
            print "userSetup.py doesn't know how to set _ScriptRootDir on this platform."

        # add my plugins's script directory to python sys.path
        path=_ScriptRootDir+'/VecToQuatAcc/scripts'
        if not(path in sys.path):
            sys.path.insert(0, path);

        import imp
        imp.load_source('AEVecToQuatAccTemplate', _ScriptRootDir+'/VecToQuatAcc/scripts/AETemplates/AEVecToQuatAccTemplate.py')

----------------------------------
 How to run automation test for this plugin
----------------------------------
use the new method to run the automation test
