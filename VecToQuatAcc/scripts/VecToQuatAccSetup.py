# -*- coding: utf-8 -*-

import maya.cmds                    as cmds
import maya.mel                     as mel
import pymel.core                   as pm
import pymel.tools.py2mel           as py2mel
import mymagicbox.log               as log

log.debug('This is '+__file__)

gDeveloperMode = False
################################################################

def createNode(nodeName=''):
    '''
    test:
        createNode('testnode')
        createNode()
    '''
    log.debug('This is VecToQuatAccSetup.createNode(%s)', nodeName)

    # create 'VecToQuatAcc' node
    v2qa=''
    if '' == nodeName:
        v2qa = pm.createNode('VecToQuatAcc')
    else:
        v2qa = pm.createNode('VecToQuatAcc', name='%s'%nodeName)

    # create 'quatToEuler' node
    q2eu = pm.createNode('quatToEuler', name='%s_quatToEuler'%v2qa)

    # connect attributes
    cmds.connectAttr('time1.outTime', v2qa+'.currentTime')
    cmds.connectAttr(v2qa+'.outqa',   q2eu+'.inputQuat')

    return (v2qa, q2eu) # VecToQuatAcc, quatToEuler
#
def createNode_DriverAndDriven(nodeName=''):
    '''
    create node with driver and driven nodes

    test:
        createNode_DriverAndDriven('testnode')
        createNode_DriverAndDriven()
    '''
    log.debug('This is VecToQuatAccSetup.createNode_DriverAndDriven(%s)', nodeName)

    # create nodes: VecToQuatAcc, quatToEuler
    v2qa, q2eu = createNode(nodeName)

    # create driver node
    # I use spaceLocator(...) instead of createNode('locator',...),
    # because createNode('locator',...) creates the xform node which doesn't match our naming pattern.
    # e.g. pm.createNode('locator', name='%s_dvr'%'AAA') will create the following nodes:
    #   locator2       (transform node)
    #     |___AAA_dvr  (locator shape node)
    dvr = cmds.spaceLocator(name='%s_dvr'%v2qa, p=(0, 0, 0))[0]
    cmds.setAttr((dvr+'.scale'), 2, 2, 2, type="double3") # scale the driver node for being seclected easily

    # create driven node
    #dvn = pm.createNode('transform', name='%s_dvn'%v2qa)
    dvn = cmds.spaceLocator(name='%s_dvn'%v2qa, p=(0, 0, 0))[0]

    # connect attributes
    cmds.connectAttr(dvn+'.rotateOrder',   q2eu+'.inputRotateOrder')
    cmds.connectAttr(q2eu+'.outputRotate', dvn+'.rotate')

    cmds.connectAttr(dvr+'.translate',     v2qa+'.vector')
    cmds.connectAttr(dvr+'.translate',     dvn+'.translate')
#
def about():
    log.debug('This is VecToQuatAccSetup.about()')
    log.info('=================================================')
    log.info('--------------------- NOTE ----------------------')
    log.info('- 使用该插件的时候, 在Maya-->Preferences-->Settings-->TimeSlider-->Playback里面, Playback speed一定要设置为Player every frame, 否则每次播放的速度可能不一致,导致deltax和deltay不一致, 最终导致每次解算的结果不一致;')
    log.info('- 因为该插件是解算性质的, 所以使用该插件的时候, 每次播放动画请从第一帧播放;')
    log.info('- 使用该插件制作的shot, 每次提交shot之前, 请bake driver控制器. 这样的话, 如果之后要修改shot, (从第一帧播放的话)每一帧driven的状态是和之前是一致的;')
    log.info('')
    log.info('--------------------- About ---------------------')
    log.info('- Sometimes I need to switch to developer mode for debugging,')
    log.info('  e.g. more UIs will be shown in developer mode.')
    log.info('')
    log.info('- How to change to developer mode:')
    log.info('  run the following python command in Maya script editor and reload this plugin again:')
    log.info('    VecToQuatAccSetup.gDeveloperMode = True')
    log.info('=================================================')

    pass
################################################################
def createMenu(parentMenuPath):
    log.debug('This is VecToQuatAccSetup.createMenu(%s)', parentMenuPath)

    #cmds.setParent(parentMenuPath, menu=True)
    menuPath = parentMenuPath +'|VecToQuatAcc'
    #log.debug('menu name(expected): %s', menuPath);

    if not cmds.menu(menuPath, query=True, exists=True): # create the menu if it doesn't exists
        menuPath = cmds.menuItem('VecToQuatAcc', parent=parentMenuPath, subMenu=True, tearOff=True)
        #log.debug('menu name(created): %s', menuPath);
        cmds.menuItem(    'Create',                    command=lambda *args: createNode(),                 ann='createNode(nodeName)')
        if gDeveloperMode:
            cmds.menuItem('CreateWithDriverAndDriven', command=lambda *args: createNode_DriverAndDriven(), ann='createNode_DriverAndDriven(nodeName)')
        cmds.menuItem(divider=True)
        cmds.menuItem(    'About',                     command=lambda *args: about(),                      ann='about()')

    return menuPath
#
def deleteMenu(parentMenuPath):
    log.debug('This is VecToQuatAccSetup.deleteMenu()')

    menuPath = parentMenuPath +'|VecToQuatAcc'
    if cmds.menu(menuPath, query=True, exists=True):
        cmds.deleteUI(menuPath, menu=True)
    else:
        log.debug('menu is not found: %s', menuPath)

    pass
#
def createDummyBaseRiggingToolsMenu():
    '''
    Create 'BFX_Rigging' menu if it doesn't exists.
    In my local develop environment, 'BFX_Rigging' menu will not be created,
    so I create a dummy 'BFX_Rigging' menu.
    '''
    menuName = 'riggingToolsMenu'# 'BFX_Rigging', see /PLE/studio/app/maya/baseRigging/utils.py --> createMenu()

    menuPath=''
    if not cmds.menu(menuName, query=True, exists=True):
        gMainWindow = mel.eval('$temp=$gMainWindow') # 'MayaWindow'

        menuPath=cmds.menu(menuName, l='BFX_Rigging', parent=gMainWindow, tearOff=True)
        cmds.setParent(menuName, menu=True)
        cmds.menuItem(subMenu=True, l='Facial Setup', tearOff=True)
        cmds.menuItem(l='Auto Connect Selected Attributes')
        cmds.menuItem(divider=True)
    else:
        menuPath='MayaWindow|riggingToolsMenu'
    return menuPath
################################################################
def loadDependentPlugins():
    log.debug('This is VecToQuatAccSetup.loadDependentPlugins()');

    dependentPlugins = ['quatNodes']

    for plugin in dependentPlugins:
        if not cmds.pluginInfo(plugin, q=True, loaded=True):
            msg = cmds.loadPlugin(plugin, qt=True)
            log.debug('load dependent plugin: %s', ','.join(msg))
        else:
            log.debug('plugin is already loaded: %s', plugin);

################################################################
def setupUI():
    log.debug('This is VecToQuatAccSetup.setupUI() in py');

    loadDependentPlugins()

    baseRigMenuPath = createDummyBaseRiggingToolsMenu()

    myBasePluginsMenuPath = baseRigMenuPath +'|MyBasePlugins'
    #log.debug('menu name(expected): %s', myBasePluginsMenuPath);

    if not cmds.menu(myBasePluginsMenuPath, query=True, exists=True): # create the menu if it doesn't exists
        myBasePluginsMenuPath = cmds.menuItem('MyBasePlugins', parent=baseRigMenuPath, subMenu=True, tearOff=True)
        #log.debug('menu name(created): %s', myBasePluginsMenuPath);

    createMenu(myBasePluginsMenuPath)
    pass

def unsetupUI():
    log.debug('This is VecToQuatAccSetup.unsetupUI() in py');

    baseRigMenuPath = 'MayaWindow|riggingToolsMenu'

    myBasePluginsMenuPath = baseRigMenuPath +'|MyBasePlugins'

    deleteMenu(myBasePluginsMenuPath)

    pass

def setup():
    log.debug('This is VecToQuatAccSetup.setup() in py');

    loadDependentPlugins()

    pass

def unsetup():
    log.debug('This is VecToQuatAccSetup.unsetup() in py');
    pass

# register the above python functions as mel procedures
py2mel.py2melProc(setupUI,   procName='VecToQuatAcc_setupUI',   evaluateInputs=True);
py2mel.py2melProc(unsetupUI, procName='VecToQuatAcc_unsetupUI', evaluateInputs=True);
py2mel.py2melProc(setup,     procName='VecToQuatAcc_setup',     evaluateInputs=True);
py2mel.py2melProc(unsetup,   procName='VecToQuatAcc_unsetup',   evaluateInputs=True);
