import os
import sys
import unittest
import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

#-------------------------------------------------------------------------------
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        
        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)
            
    def __tryToLoadPlugin (self, plugin):
        if not cmds.pluginInfo(plugin, loaded=True, q=True):
            cmds.loadPlugin(plugin, quiet=True)
            log.info('plugin loaded: %s', plugin);
        else:
            log.info('plugin is already loaded, skip it: %s', plugin);
            
    def test00(self):
        '''
        import maya.cmds
        maya.cmds.loadPlugin("automation")

        import unittest
        suite = unittest.defaultTestLoader.discover(start_dir="/backup/lhome/dev/mybox/skinClusterDq/test/test_0002")
        unittest.TextTestRunner(verbosity=3).run(suite)
        '''
        log.debug('---- begin %s.test00() ----', self.m_id);
        
        # select joint and mesh
        cmds.select('linear|joint1', r=True);
        cmds.select('linear|pCube1', tgl=True);
        
        # bind skin(skinMethod=Classic Linear)
        mel.eval('newSkinCluster("-bindMethod 0 -skinMethod 0 -normalizeWeights 1 -weightDistribution 0 -mi 5 -omi true -dr 4 -rui true,multipleBindPose,1");');

        # deform
        cmds.setAttr("linear|joint1|joint2.rotateX",180);
        
        self.__tryToLoadPlugin('skinClusterDq')
        
        mel.eval('source "skinClusterDqUtils.mel";')
        log.debug('create skinClusterDq');
        mel.eval('createSceneSkinClusterDq();')
        
        

        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
