
cmake_minimum_required(VERSION 2.6)

#-******************************************************************************
# set CMAKE_BUILD_TYPE
#-******************************************************************************
#
# If the user specifies -DCMAKE_BUILD_TYPE on the command line, take their definition
# and dump it in the cache along with proper documentation, otherwise set CMAKE_BUILD_TYPE
# to Debug prior to calling PROJECT()
# source: http://www.cmake.org/pipermail/cmake/2008-September/023808.html
IF(DEFINED CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} 
        CACHE STRING 
        "Choose the type of build, options are: None(CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel."
    )
ELSE()
    set(CMAKE_BUILD_TYPE Debug 
        CACHE STRING 
        "Choose the type of build, options are: None(CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel."
    )
ENDIF()


set(MY_PROJECT_NAME skinClusterDq)

project(${MY_PROJECT_NAME})

add_definitions(-DPLUGIN_VERSION_MAJOR=0)
add_definitions(-DPLUGIN_VERSION_MINOR=1)
add_definitions(-DPLUGIN_VERSION_PATCH=0)


if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/build/cmake/Modules/")
  set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/build/cmake/Modules/")
else ()
  message(SEND_ERROR "Failed to find module path: ${CMAKE_CURRENT_SOURCE_DIR}/build/cmake/Modules/")
endif()

#-******************************************************************************
# get git commit version.
#-******************************************************************************
# http://stackoverflow.com/questions/1435953/how-can-i-pass-git-sha1-to-compiler-as-definition-using-cmake
include(GetGitRevisionDescription)
get_git_head_revision(GIT_REFSPEC GIT_SHA1)
add_definitions(-DGIT_SHA1="${GIT_SHA1}")

#-******************************************************************************
# OPTIONS (set with -D<option>=<value>)
#-******************************************************************************

# Cmake system specific flags
SET(WINDOWS FALSE)
IF ("${CMAKE_SYSTEM_NAME}" MATCHES "Windows")
    SET(WINDOWS TRUE)
ENDIF()

SET(DARWIN FALSE)
IF ("${CMAKE_SYSTEM_NAME}" MATCHES "Darwin")
    SET(DARWIN TRUE)
    # suppress rpath warning
    IF (POLICY CMP0042)
        CMAKE_POLICY(SET CMP0042 OLD)
    ENDIF()
ENDIF()

SET(LINUX FALSE)
IF ("${CMAKE_SYSTEM_NAME}" MATCHES "Linux")
    SET(LINUX TRUE)
ENDIF()

MESSAGE(STATUS "CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}")

# Set some debug vs opt flags
IF ("${CMAKE_BUILD_TYPE}" MATCHES "Debug")
    add_definitions(-DDEBUG=1 -UNDEBUG)
    IF (NOT WINDOWS)
       add_definitions(-Wall -Werror -Wextra -Wno-unused-parameter)
    ENDIF()
ELSEIF ("${CMAKE_BUILD_TYPE}" MATCHES "Release")
    add_definitions(-DNDEBUG=1 -UDEBUG)
    IF (NOT WINDOWS)
        add_definitions(-O3)
    ENDIF()
ENDIF()

#set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/dist")

find_package(Maya REQUIRED)

#
add_subdirectory(src)


# Install
install(DIRECTORY bin/                  DESTINATION ${CMAKE_INSTALL_PREFIX}/bin      )
install(DIRECTORY docs/                 DESTINATION ${CMAKE_INSTALL_PREFIX}/docs     )
install(DIRECTORY icons/                DESTINATION ${CMAKE_INSTALL_PREFIX}/icons    )
install(DIRECTORY scripts/              DESTINATION ${CMAKE_INSTALL_PREFIX}/scripts    FILES_MATCHING  PATTERN "*.py"  )
install(DIRECTORY scripts/              DESTINATION ${CMAKE_INSTALL_PREFIX}/scripts    FILES_MATCHING  PATTERN "*.mel"  )
install(FILES ${MY_PROJECT_NAME}.mod    DESTINATION ${CMAKE_INSTALL_PREFIX}/ )
