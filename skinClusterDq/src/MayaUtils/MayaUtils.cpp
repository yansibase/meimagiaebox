#include "MayaUtils.h"

#include <assert.h>

// Needed for Maya 5.0 iostream
using std::cout;
using std::cerr;
using std::endl;

// General Maya API includes
#include <maya/MString.h>
#include <maya/MFloatArray.h>
#include <maya/MSelectionList.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MGlobal.h>

// Function sets
#include <maya/MFnMesh.h>
#include <maya/MFnAttribute.h>

typedef unsigned int	uint;


//---------------------------------------------------------------------------------------------------------------------
// Maya utils
//---------------------------------------------------------------------------------------------------------------------

namespace MayaUtils
{

//
// Expand a MFnMesh uvset called 'inUVSetName' by UV coords in <inNewUs> and <inNewVs> arrays
//
MStatus gExpandUVSet(MObject inMeshObject, const MFloatArray& inNewUs, const MFloatArray& inNewVs, const MString& inUVSetName)
{
	MStatus stat;
	MFnMesh fn_mesh(inMeshObject, &stat);
	if (stat!=MS::kSuccess)
		return stat;

	MFloatArray us;
	MFloatArray vs;
	fn_mesh.getUVs(us, vs, &inUVSetName);

	gMAppend(us, inNewUs);
	gMAppend(vs, inNewVs);

	return fn_mesh.setUVs(us, vs, &inUVSetName);
}




//
// Check if plug for attribute <inAttributeName> of <inObject> is connected
//
bool gIsPlugConnected(pcTChar inAttributeName, const MObject& inObject)
{
	MStatus	status;
	MFnDependencyNode node(inObject);

	MPlug plug = node.findPlug(MString(inAttributeName), &status);
	if (status==MS::kSuccess)
	{
		return plug.isConnected();
	}
	return false;
}

//
// Check if plug is an output plug (readable)
//
bool gIsOutputPlug(MPlug inPlug)
{
	MFnAttribute fn_attr(inPlug.attribute());
	return !fn_attr.isWritable();
}

//
// Check if plug is an input plug (writable)
//
bool gIsInputPlug(MPlug inPlug)
{
	MFnAttribute fn_attr(inPlug.attribute());
	return fn_attr.isWritable();
}



//
// Get a dependency node by name
//
MObject gNodeFromName(const MString& inName)
{
	MSelectionList tmp_list;
	tmp_list.add(inName);
	if (tmp_list.length()>0)
	{
		MObject out_dep_node;
		if (MFAIL(tmp_list.getDependNode(0, out_dep_node)))
			return MObject::kNullObj;
		return out_dep_node;
	}

	return MObject::kNullObj;
}



//
// Get a MPlug from a dependency node by attribute name
//
MPlug gGetPlugByName(rcMObject inDepNodeObject, rcMString inAttrName)
{
	MFnDependencyNode node(inDepNodeObject);
	return node.findPlug(inAttrName);
}


//
// Split full attribute name in object name and attribute name part
// Splits on first '.' separator. Returns index of separator.
//
int gSplitFullAttributeName(rcMString inFullAttrName, rMString outNodeName, rMString outAttrName)
{
	outNodeName.clear();
	outAttrName.clear();

	int separator_idx = inFullAttrName.index('.');
	if (separator_idx<=0)
	{
		return -1;
	}

	outNodeName = inFullAttrName.substring(0, separator_idx-1);
	outAttrName = inFullAttrName.substring(separator_idx+1, ((int)inFullAttrName.length())-1);

	return separator_idx;
}



//
// Get a MPlug by full attribute name (i.e. including dependency node name)
//
MPlug gGetPlugByName(rcMString inFullAttrName)
{
	MString node_name, attr_name;
	gSplitFullAttributeName(inFullAttrName, node_name, attr_name);
	MObject obj = gNodeFromName(node_name);
	return gGetPlugByName(obj, attr_name);
}



//
// Find child plug by long attribute name for a given <inCompoundPlug>
//
MPlug gFindChildPlugOfCompoundPlug(rcMPlug inCompoundPlug, rcMString inLongAttrName)
{
	assert(!inCompoundPlug.isNull());
	assert(inCompoundPlug.isCompound());

	for (uint i=0, n=inCompoundPlug.numChildren(); i!=n; ++i)
	{
		MPlug child_plug = inCompoundPlug.child(i);
		if (child_plug.isNull())
			continue;

		if (inLongAttrName==child_plug.partialName(false, false, false, false, false, true))
			return child_plug;
	}

	// Return NULL plug
	return MPlug();
}




class PlugHierarchyVisitor
{
public:
	virtual MStatus		Visit(rMPlug ioPlug) const = 0;
};


class DisconnectInputPlugVisitor : public PlugHierarchyVisitor
{
public:
	DisconnectInputPlugVisitor(rMDGModifier ioDgModifier) : mDgModifier(ioDgModifier)	{ }

	virtual MStatus		Visit(rMPlug ioPlug) const
	{
		// Get plugs using <ioPlug> as destination
		MPlugArray connected_plugs;
		ioPlug.connectedTo(connected_plugs, true, false);

		// Disconnect <ioPlug>'s connections
		// NB: This should only be 1 connection at most as an input (i.e.
		//     destination) plug can only have 1 source where the data is coming from.
		for (uint i=0, n=connected_plugs.length(); i!=n; ++i)
			mDgModifier.disconnect(connected_plugs[i], ioPlug);

		return MS::kSuccess;
	}

protected:
	rMDGModifier		mDgModifier;
};



//
// Walk plug hierarchy in depth-first order. Visitor is called
// for each child plug.
//
MStatus gWalkPlugHierarchy(MPlug ioPlug, rcPlugHierarchyVisitor inVisitor)
{
	if (ioPlug.isNull())
		return MS::kSuccess;

	inVisitor.Visit(ioPlug);

	if (ioPlug.isArray())
	{
		for (uint i=0, n=ioPlug.numElements(); i!=n; ++i)
		{
			MStatus status = gWalkPlugHierarchy(ioPlug.elementByPhysicalIndex(i), inVisitor);
			if (MFAIL(status))
				return status;
		}
	}
	else if (ioPlug.isCompound())
	{
		for (uint i=0, n=ioPlug.numChildren(); i!=n; ++i)
		{
			MStatus status = gWalkPlugHierarchy(ioPlug.child(i), inVisitor);
			if (MFAIL(status))
				return status;
		}
	}

	return MS::kSuccess;
}



//
// Disconnect all connections to <ioPlug>
//
void gDisconnectInputPlug(rMPlug ioPlug, rMDGModifier ioDgModifier)
{
	DisconnectInputPlugVisitor disconnector(ioDgModifier);
	gWalkPlugHierarchy(ioPlug, disconnector);
}



//
// Copy the value of a plug. Plug must not be an array or compound plug
//
MStatus gCopyPlugValue(rcMPlug inSrcPlug, rMPlug ioDstPlug)
{
	assert(!inSrcPlug.isNull() && !ioDstPlug.isNull());
	assert(!inSrcPlug.isArray() && !ioDstPlug.isArray());
	assert(!inSrcPlug.isCompound() && !ioDstPlug.isCompound());
	MStatus status;

	// String copy failed. Copy as object
	MObject copied_value_obj;
	status = inSrcPlug.getValue(copied_value_obj);
	if (!MFAIL(status))
		status = ioDstPlug.setValue(copied_value_obj);
	else
	{
		// Try to copy as string
		MString copied_string;
		status = inSrcPlug.getValue(copied_string);
		if (!MFAIL(status))
			status = ioDstPlug.setValue(copied_string);
		else
		{
			// Try to copy As numeric value
			double dv;
			status = inSrcPlug.getValue(dv);
			if (!MFAIL(status))
				status = ioDstPlug.setValue(dv);
		}
	}

	return status;
}

//
// Same as a forced "connectAttr"
//
MStatus gConnectPlug(rcMPlug inSrcPlug, rMPlug ioDstPlug, MDGModifier* ioDgModifier)
{
	if (inSrcPlug.isNull() || ioDstPlug.isNull())
		return MS::kFailure;

	// Get DG modifier
	MDGModifier local_dg_modifier;
	rMDGModifier dg_modifier = (ioDgModifier==NULL) ? local_dg_modifier : *ioDgModifier;

	MStatus status = dg_modifier.connect(inSrcPlug, ioDstPlug);
	CHECK_MSTATUS(status);

	// Finish it for local modifier only
	if (ioDgModifier==NULL)
		status = dg_modifier.doIt();

	return status;
}

//
// Same as a forced "connectAttr"
//
MStatus gConnectPlug(rcMObject inSrcObject,	rcMString inSrcAttrName,
					 rMObject ioDstObject,	rcMString inDstAttrName,
					 MDGModifier* ioDgModifier)
{
    MPlug inSrcPlug(gGetPlugByName(inSrcObject, inSrcAttrName));
    MPlug  ioDstPlug(gGetPlugByName(ioDstObject, inDstAttrName));

	return gConnectPlug(inSrcPlug, ioDstPlug, ioDgModifier);
}



//
// Clear plug for array attribute
//
MStatus gClearArrayPlug(rMPlug ioArrayPlug, MDGModifier* inDgModifier)
{
	assert(ioArrayPlug.isArray());

	uint elem_count = ioArrayPlug.numElements();
	if (elem_count==0)
		return MS::kSuccess;

	// Magic 'removeMultiInstance' MEL command to actually remove
	// array elements.
	MString cmd_str_prefix = "removeMultiInstance -b true " + ioArrayPlug.partialName(true);
	MString maya_cmd_str;

	// Backwards, hoping only length has to be adjusted
	for (uint i=elem_count; i>0; )
	{
		--i;

		uint elem_idx;
		{
			// Scoped, so we don't keep MPlug which will be deleted
			MPlug elem = ioArrayPlug.elementByPhysicalIndex(i);
			elem_idx = elem.logicalIndex();
		}

		maya_cmd_str = cmd_str_prefix;
		maya_cmd_str += "[";
		maya_cmd_str += (int)elem_idx;
		maya_cmd_str += "]";

		MStatus status;
		if (inDgModifier==NULL)
			status = MGlobal::executeCommand(maya_cmd_str, false, false);
		else
			status = inDgModifier->commandToExecute(maya_cmd_str);
		CHECK_MSTATUS(status);
	}

	// Execute now, otherwise it will not be empty yet
	if (inDgModifier!=NULL)
		inDgModifier->doIt();

	assert(ioArrayPlug.numElements()==0);
	return MS::kSuccess;
}




static void sCopyInputPlugStructureRecursive(rcMPlug inSrcPlug, rMPlug ioDstPlug, rMDGModifier ioDgModifier)
{
	assert(!inSrcPlug.isNull() && !ioDstPlug.isNull());
	MStatus status;

	// Get plugs using <src_plug> as destination
	MPlugArray connected_plugs;
	inSrcPlug.connectedTo(connected_plugs, true, false);

	// Connect <inSrcPlug>'s connections to <ioDstPlug>
	// NB: Should only be 0 or 1 connections.
	bool src_is_connected = connected_plugs.length()>0;
	if (src_is_connected)
		ioDgModifier.connect(connected_plugs[0], ioDstPlug);

	// *********************************************************************
	// NB: First check if it's an array, then for compound!
	// Otherwise arrays are reported as compounds and child
	// enumeration fails miserably: child is called "arrayname[-1]".
	// Stupid Maya. Yuck.
	// *********************************************************************

	if (inSrcPlug.isArray() && ioDstPlug.isArray())
	{
		uint src_array_len = inSrcPlug.numElements();

		// Make sure we clear all existing elements of <ioDstPlug>, as we'll only
		// process existing elements of <inSrcPlug>. If it's not empty, all existing elements
		// in <ioDstPlug> will still be present after the "copy".
		gClearArrayPlug(ioDstPlug, &ioDgModifier);

		// Reserve space
#if	(MAYA_API_VERSION>=700)
		ioDstPlug.setNumElements(src_array_len);
#endif

		for (uint i=0; i<src_array_len; ++i)
		{
			MPlug src_child_plug = inSrcPlug.elementByPhysicalIndex(i);
			assert(!src_child_plug.isNull());
			uint src_logical_elem_idx = src_child_plug.logicalIndex();

			// NB: We use elementByLogicalIndex() here to ensure the array element
			//     is actually exists.
			MPlug dst_child_plug = ioDstPlug.elementByLogicalIndex(src_logical_elem_idx);
			assert(!dst_child_plug.isNull());

			sCopyInputPlugStructureRecursive(src_child_plug, dst_child_plug, ioDgModifier);
		}
	}
	else if (inSrcPlug.isCompound() && ioDstPlug.isCompound())
	{
		// Plugs are compound, connect its common child plugs as well.
		for (uint i=0, n=inSrcPlug.numChildren(); i!=n; ++i)
		{
			MPlug src_child_plug = inSrcPlug.child(i);
			assert(!src_child_plug.isNull());
			MString src_child_plug_name = src_child_plug.partialName(false, false, false, false, false, true);

			MPlug dst_child_plug = MayaUtils::gFindChildPlugOfCompoundPlug(ioDstPlug, src_child_plug_name);
			if (!dst_child_plug.isNull())
				sCopyInputPlugStructureRecursive(src_child_plug, dst_child_plug, ioDgModifier);
		}
	}
	else if (!src_is_connected)
	{
		// Not connected and not an array or compound attribute.
		// Try copy attribute value
		status = MayaUtils::gCopyPlugValue(inSrcPlug, ioDstPlug);
		MCheckStatus(status, "Could not copy attribute value");
	}
}



//
// Duplicate plug connections for input plug <inSrcPlug> to <ioDstPlug>
//
MStatus gCopyInputPlugStructure(rcMPlug inSrcPlug, rMPlug ioDstPlug, MDGModifier* ioDgModifier)
{
	if (inSrcPlug.isNull())
		return MReturnFailure("Source plug not found");

	if (ioDstPlug.isNull())
		return MReturnFailure("Destination plug not found");

	// Get DG modifier
	MDGModifier local_dg_modifier;
	rMDGModifier dg_modifier = (ioDgModifier==NULL) ? local_dg_modifier : *ioDgModifier;

	// Ensure all existing elements in <ioDstPlug> are disconnected before (re)connecting it
	MayaUtils::gDisconnectInputPlug(ioDstPlug, dg_modifier);

	sCopyInputPlugStructureRecursive(inSrcPlug, ioDstPlug, dg_modifier);

	// Finish it for local modifier only
	if (ioDgModifier==NULL)
		dg_modifier.doIt();

	return MS::kSuccess;
}



//
// Duplicate all connections to input plug <inSrcAttrName> of <inSrcObject>
// and link connections to input <inDstAttrName> of <ioDstObject>
// All connections of <inDstAttrName> are disconnected.
// When <inSrcAttrName> is not connected, the attribute value is copied.
//
MStatus gCopyInputPlugStructure(rcMObject inSrcObject,	rcMString inSrcAttrName,
								rMObject ioDstObject,	rcMString inDstAttrName,
								MDGModifier* ioDgModifier)
{
    MPlug inSrcPlug(gGetPlugByName(inSrcObject, inSrcAttrName));
    MPlug ioDstPlug(gGetPlugByName(ioDstObject, inDstAttrName));
	return gCopyInputPlugStructure(inSrcPlug, ioDstPlug, ioDgModifier);
}





//
// Recursively reconnect plug hierarchy
//
static void sMoveOutputPlugConnectionsRecursive(rMPlug ioSrcPlug, rMPlug ioDstPlug, rMDGModifier ioDgModifier)
{
	assert(!ioSrcPlug.isNull() && !ioDstPlug.isNull());
	MStatus status;

	// Get plugs using <src_plug> as source
	MPlugArray connected_plugs;
	ioSrcPlug.connectedTo(connected_plugs, false, true);

	// Connect <inSrcPlug>'s connections to <ioDstPlug>
	for (uint i=0, n=connected_plugs.length(); i<n; ++i)
	{
		// Forced reconnect
		ioDgModifier.disconnect(ioSrcPlug, connected_plugs[i]);
//		ioDgModifier.disconnect(ioDstPlug, connected_plugs[i]);
		ioDgModifier.connect(ioDstPlug, connected_plugs[i]);
	}

	// *********************************************************************
	// NB: First check if it's an array, then for compound!
	// Otherwise arrays are reported as compounds and child
	// enumeration fails miserably: child is called "arrayname[-1]".
	// Stupid Maya. Yuck.
	// *********************************************************************

	if (ioSrcPlug.isArray() && ioDstPlug.isArray())
	{
		uint src_array_len = ioSrcPlug.numElements();

		// Make sure we clear all existing elements of <ioDstPlug>, as we'll only
		// process existing elements of <inSrcPlug>
		gClearArrayPlug(ioDstPlug, &ioDgModifier);

		// Reserve space
#if	(MAYA_API_VERSION>=700)
		ioDstPlug.setNumElements(src_array_len);
#endif
		for (uint i=0; i<src_array_len; ++i)
		{
			MPlug src_child_plug = ioSrcPlug.elementByPhysicalIndex(i);
			assert(!src_child_plug.isNull());
			uint src_logical_elem_idx = src_child_plug.logicalIndex();

			// NB: We use elementByLogicalIndex() here to ensure the array element
			//     is actually exists.
			MPlug dst_child_plug = ioDstPlug.elementByLogicalIndex(src_logical_elem_idx);
			assert(!dst_child_plug.isNull());

			sMoveOutputPlugConnectionsRecursive(src_child_plug, dst_child_plug, ioDgModifier);
		}
	}
	else if (ioSrcPlug.isCompound() && ioDstPlug.isCompound())
	{
		// Plugs are compound, connect its common child plugs as well.
		for (uint i=0, n=ioSrcPlug.numChildren(); i!=n; ++i)
		{
			MPlug src_child_plug = ioSrcPlug.child(i);
			assert(!src_child_plug.isNull());
			MString src_child_plug_name = src_child_plug.partialName(false, false, false, false, false, true);

			MPlug dst_child_plug = MayaUtils::gFindChildPlugOfCompoundPlug(ioDstPlug, src_child_plug_name);
			if (!dst_child_plug.isNull())
				sMoveOutputPlugConnectionsRecursive(src_child_plug, dst_child_plug, ioDgModifier);
		}
	}
}

//
// When the output attribute of <ioSrcPlug> is connected, it will
// reconnect its destination to <ioDstPlug>. Reconnects child plugs as well.
//
MStatus gMoveOutputPlugConnections(rMPlug ioSrcPlug, rMPlug ioDstPlug, MDGModifier* ioDgModifier)
{
	if (ioSrcPlug.isNull() || ioDstPlug.isNull())
		return MS::kFailure;

	// Get DG modifier
	MDGModifier local_dg_modifier;
	rMDGModifier dg_modifier = (ioDgModifier==NULL) ? local_dg_modifier : *ioDgModifier;

	sMoveOutputPlugConnectionsRecursive(ioSrcPlug, ioDstPlug, dg_modifier);

	// Finish it for local modifier only
	if (ioDgModifier==NULL)
		dg_modifier.doIt();

	return MS::kSuccess;
}

//
// When the output attribute of <inSrcAttrName> is connected, it will
// reconnect its destination to <inDstAttrName> of <ioDstObject>
// Reconnects child plugs as well.
//
//   BEFORE                           AFTER
//
// +---------+                     +---------+
// | SRC OBJ |					   | SRC OBJ |
// +---------+					   +---------+
// | src attr>-->-->destination	   | src attr=       destination
// |         |					   |         |          ^
// +---------+					   +---------+          |
//								                        |
// +---------+					   +---------+          ^
// | DST OBJ |					   | DST OBJ |          |
// +---------+					   +---------+          |
// | dst attr=					   | dst attr>---->-----+
// |         |					   |         |
// +---------+					   +---------+
//
MStatus gMoveOutputPlugConnections(rMObject ioSrcObject,	rcMString inSrcAttrName,
								   rMObject ioDstObject,	rcMString inDstAttrName,
								   MDGModifier* ioDgModifier)
{
    MPlug ioSrcPlug(gGetPlugByName(ioSrcObject, inSrcAttrName));
    MPlug ioDstPlug(gGetPlugByName(ioDstObject, inDstAttrName));
	return gMoveOutputPlugConnections(ioSrcPlug, ioDstPlug, ioDgModifier);
}







} // namespace MayaUtils
