// MayaUtils.h - Copyright (C) Guido de Haan 2005-2007. All rights reserved. 
#ifndef INCLUDED_MAYAUTILS_H
#define INCLUDED_MAYAUTILS_H
#pragma once

#include <iostream>
#include <maya/MObject.h>
#include <maya/MStatus.h>
class MFloatArray;
class MString;
class MPlug;
class MDGModifier;
class MString;
class MItGeometry;



// Status Checking Macro - MReturnCheckStatus (Debugging tool)
#define MReturnCheckStatus(status,message)	\
	if( MS::kSuccess != status ) {		\
	cerr << message << "\n";		\
	return status;					\
	}

#define MCheckStatus(status,message)	\
	if (MS::kSuccess != status) {		\
	cerr << message << " (" << status.errorString().asChar() << ")\n";		\
	}

inline const MStatus& MReturnStatus(MStatus& ioStatus, const char* inErrorTxt=NULL)
{
	if (inErrorTxt!=NULL)
		ioStatus.perror(inErrorTxt);
	return ioStatus;
}

inline const MStatus& MReturnSetFailure(MStatus& outStatus, const char* inErrorTxt=NULL)
{
	outStatus = MStatus::kFailure;
	if (inErrorTxt!=NULL)
		outStatus.perror(inErrorTxt);
	return outStatus;
}

inline MStatus MReturnFailure(const char* inErrorTxt=NULL)
{
	MStatus status = MStatus::kFailure;
	if (inErrorTxt!=NULL)
		status.perror(inErrorTxt);
	return status;
}

// Type savers
typedef MString &			rMString;
typedef const MString&		rcMString;
typedef MObject&			rMObject;
typedef const MObject&		rcMObject;
typedef MDGModifier&		rMDGModifier;
typedef MPlug&				rMPlug;
typedef const MPlug&		rcMPlug;



namespace MayaUtils
{

typedef const char*		pcTChar;

//
// Append Maya array to another Maya array
//
template <typename taMAYAARRAY>
const taMAYAARRAY& gMAppend(taMAYAARRAY& ioDestArray, const taMAYAARRAY& inSourceToAppend)
{
	unsigned int start_idx	= ioDestArray.length();
	unsigned int count		= inSourceToAppend.length();
	ioDestArray.setLength(start_idx+count);
	for (unsigned int i=0; i!=count; ++i)
		ioDestArray[start_idx+i] = inSourceToAppend[i];

	return ioDestArray;
}


//
// Returns index to the first position in array where <inElementToFind> may be inserted
// while maintaining the sorting order
//
template <typename taMAYAARRAY, typename taMAYATYPE, typename taCOMPAREPREDICATE>
int gMLowerBound(const taMAYAARRAY& inArray, const taMAYATYPE& inElementToFind, const taCOMPAREPREDICATE& inCompare)
{
	int begin	= 0;
	int n		= (int)inArray.length();
	for ( ; 0<n; )
	{
		int half_n=n/2;
		int m = begin+half_n;
		if (inCompare(inArray[m], inElementToFind))
			begin=++m, n-=half_n+1;
		else
			n=half_n;
	}
	return begin;
}

//
// gMLowerBound() with less-than predicate
//
template <typename taMAYAARRAY, typename taMAYATYPE>
int gMLowerBound(const taMAYAARRAY& inArray, const taMAYATYPE& inElementToFind)
{
	return gMLowerBound(inArray, inElementToFind, std::less<taMAYATYPE>());
}


//
// Find element in Maya array. Returns -1 when not found
//
template <typename taMAYAARRAY, typename taMAYATYPE>
int gMFind(const taMAYAARRAY& inArray, const taMAYATYPE& inElem)
{
	for (int i=0, n=(int)inArray.length(); i<n; ++i)
		if (inArray[i]==inElem)
			return i;
	return -1;
}

//
// Find element in sorted Maya array. Ordering must adhere to <taCOMPAREPREDICATE>. Returns -1 when not found
//
template <typename taMAYAARRAY, typename taMAYATYPE, typename taCOMPAREPREDICATE>
int gMSortedFind(const taMAYAARRAY& inArray, const taMAYATYPE& inElementToFind, const taCOMPAREPREDICATE& inCompare)
{
	int n = (int)inArray.length();

	// For small arrays, do linear search
	if (n<=8)
	{
		for (int i=0; i<n; ++i)
			if (inArray[(uint)i]==inElementToFind)
				return i;
	}
	else
	{
		// Do binary search. Find lower bound index first
		int lb_idx = gMLowerBound(inArray, inElementToFind, inCompare);

		// If index in range and element at <lb_idx> is indeed the one we're looking for, success
		if (lb_idx<n && !inCompare(inElementToFind, inArray[(uint)lb_idx]))
			return lb_idx;
	}
	return -1;
}

// Expand a MFnMesh uvset called 'inUVSetName' by UV coords in <inNewUs> and <inNewVs> arrays
MStatus gExpandUVSet(MObject inMeshObject, const MFloatArray& inNewUs, const MFloatArray& inNewVs, const MString& inUVSetName);

// Split full attribute name in object name and attribute name part
// Splits on first '.' separator. Returns index of separator.
int gSplitFullAttributeName(rcMString inFullAttrName, rMString outNodeName, rMString outAttrName);

// Get a dependency node by name. Returns NULL when not found.
MObject gNodeFromName(const MString& inName);

// Get a MPlug from a dependency node by attribute name
MPlug gGetPlugByName(rcMObject inDepNodeObject, rcMString inAttrName);

// Get a MPlug by full attribute name (i.e. including dependency node name)
MPlug gGetPlugByName(rcMString inFullAttrName);

// Check if plug for attribute <inAttributeName> of <inObject> is connected
bool gIsPlugConnected(pcTChar inAttributeName, const MObject& inObject);

// Check if plug is an output plug (readable)
bool gIsOutputPlug(const MPlug& inPlug);

// Check if plug is an input plug (writable)
bool gIsInputPlug(const MPlug& inPlug);

// Find child plug by long attribute name for a given <inCompoundPlug>
MPlug gFindChildPlugOfCompoundPlug(rcMPlug inCompoundPlug, rcMString inLongAttrName);

// Disconnect all connections to <ioPlug>
void gDisconnectInputPlug(rMPlug ioPlug, rMDGModifier ioDgModifier);

// Same as a forced "connectAttr"
MStatus gConnectPlug(rcMPlug inSrcPlug, rMPlug ioDstPlug, MDGModifier* ioDgModifier);

// Same as a forced "connectAttr"
MStatus gConnectPlug(rcMObject inSrcObject,	rcMString inSrcAttrName, 
					 rMObject ioDstObject,	rcMString inDstAttrName,
					 MDGModifier* ioDgModifier);

// Clear plug for array attribute
MStatus gClearArrayPlug(rMPlug ioArrayPlug, MDGModifier* inDgModifier);

MStatus gCopyPlugValue(rcMPlug inSrcPlug, rMPlug ioDstPlug);

// Duplicate all connections to input plug <inSrcAttrName> of <inSrcObject>
// and link connections to input <inDstAttrName> of <ioDstObject>
// All connections of <inDstAttrName> are disconnected. 
// When <inSrcAttrName> is not connected, the attribute value is copied.
MStatus gCopyInputPlugStructure(rcMObject inSrcObject,	rcMString inSrcAttrName, 
								rMObject ioDstObject,	rcMString inDstAttrName,
								MDGModifier* ioDgModifier);

// Duplicate plug connections for input plug <inSrcPlug> to <ioDstPlug>
MStatus gCopyInputPlugStructure(rcMPlug inSrcPlug, rMPlug ioDstPlug, MDGModifier* ioDgModifier);

//
// When the output attribute of <inSrcAttrName> is connected, it will
// reconnect its destination to <inDstAttrName> of <ioDstObject>
// Reconnects child plugs as well.
//
//   BEFORE                           AFTER
//
// +---------+                     +---------+
// | SRC OBJ |					   | SRC OBJ |
// +---------+					   +---------+
// | src attr>-->-->destination	   | src attr=       destination
// |         |					   |         |          ^
// +---------+					   +---------+          |  
//								                        |
// +---------+					   +---------+          ^
// | DST OBJ |					   | DST OBJ |          |
// +---------+					   +---------+          |
// | dst attr=					   | dst attr>---->-----+
// |         |					   |         |
// +---------+					   +---------+  
//
MStatus gMoveOutputPlugConnections(rMObject ioSrcObject,	rcMString inSrcAttrName, 
								   rMObject ioDstObject,	rcMString inDstAttrName,
								   MDGModifier* ioDgModifier);

// When the output attribute of <ioSrcPlug> is connected, it will
// reconnect its destination to <ioDstPlug>. Reconnects child plugs as well.
MStatus gMoveOutputPlugConnections(rMPlug ioSrcPlug, rMPlug ioDstPlug, MDGModifier* ioDgModifier);

class PlugHierarchyVisitor;
typedef PlugHierarchyVisitor&		rPlugHierarchyVisitor;
typedef const PlugHierarchyVisitor&	rcPlugHierarchyVisitor;

// Walk plug hierarchy in depth-first order. Visitor is called for each child plug.
MStatus gWalkPlugHierarchy(MPlug ioPlug, rcPlugHierarchyVisitor inVisitor);

} // namespace MayaUtils



#endif
