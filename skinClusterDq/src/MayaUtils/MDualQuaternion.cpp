// MDualQuaternion.cpp - Copyright (C) Guido de Haan 2006-2007. All rights reserved. 
#include "MDualQuaternion.h"




cMDualQuaternion MDualQuaternion::zero(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
cMDualQuaternion MDualQuaternion::identity(0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0);

rcMDualQuaternion MDualQuaternion::operator=(rcMMatrix inRigidTransform)
{
	// Use MTransformationMatrix instead of MMatrix in case the transform
	// contains scaling. Otherwise the rotation quaternion will not be of unit length.
	MTransformationMatrix trans_mat(inRigidTransform);

	MQuaternion rot = trans_mat.rotation();
	MVector pos		= trans_mat.translation(MSpace::kTransform);

	MDualQuaternion dq(pos, rot);
	mReal = dq.mReal;
	mDual = dq.mDual;

	return *this;
}

MMatrix MDualQuaternion::asMatrix() const
{
	// Fill in rotation part
	MMatrix mat			= getRotation().asMatrix();

	// Fill in translation part
	MVector translation = getTranslation();
	mat[3][0]			= translation.x;
	mat[3][1]			= translation.y;
	mat[3][2]			= translation.z;

	return mat;
}

MMatrix MDualQuaternion::asMatrixFromUnit() const
{
	assert(isUnit());

	// Fill in rotation part
	MMatrix mat			= getRotation().asMatrix();

	// Fill in translation part
	MVector translation = getTranslationFromUnit();
	mat[3][0]			= translation.x;
	mat[3][1]			= translation.y;
	mat[3][2]			= translation.z;

	return mat;
}

MPoint MDualQuaternion::transformFromUnit(rcMPoint inPoint) const
{
	// Instead of getting the transformation matrix, it can be computed directly:
	// P' = (Qr.P.Qr~)/S + (Qd.Qr~ - Qr.Qd~))/S
	// Where:
	//   Qr is real quaternion part
	//   Qd is dual quaternion part
	//   ~ means conjugate
	//   S is dot(Qr,Qr). This is 1.0 for unit dual quaternions.
	// The first part of the equation is the "ordinary" rotate-vector-by-quaternion
	// equation. The second part is the same as calling MDualQuaternion::getTranslation().
	//

	// Here is a sample implementation using vector math, as a possible implementation
	// for use with SIMD instructions. It's here for demonstration purposes, not
	// a truly optimized version.

	assert(isUnit());

	rcMQuaternion q = mReal;
	MVector p(inPoint);
	MVector qxyz(mReal.x,mReal.y,mReal.z);

	// Transforming a vector by a unit quaternion is done by: P' = Q.P.Q~
	// This boils down to vector math as: P' = P + 2*(Q.xyz � (Q.xyz � P + Q.w*P))
	// Do rotation of <inPoint> using vector math
	MVector qxyzcrossp_plus_qwdotp = (qxyz^p) + q.w*p;
	p+=2.0*(qxyz^qxyzcrossp_plus_qwdotp);

	// Add translation part

	// Vector math for quaternion multiply for two quaternions P and Q where
	// P(w,<xyz>) and Q(w,<xyz>), w is scalar part and xyz is vector part. Then:
	// P*Q = (P.w*Q.w + dot(P.xyz, Q.xyz), < P.w*Q.xyz + Q.w*P.xyz + P.xyz � Q.xyz >)
	//
	// For the translation, we need 2.0*mDual*mReal.conjugate() and only the vector part
	// =>  2.0 * (mDual.w*-mReal.xyz + mReal.w*mDual.xyz + mDual.xyz � -mReal.xyz)
	// => -2.0 * (mDual.w* mReal.xyz - mReal.w*mDual.xyz - mDual.xyz �  mReal.xyz)
	// ... to avoid taking the conjugate
	MVector dxyz(mDual.x,mDual.y,mDual.z);
	MVector t = dxyz^qxyz;
	t+= mDual.w*qxyz;
	t+=-mReal.w*dxyz;
	p+=-2.0*t;

	return p;
}

rcMDualQuaternion MDualQuaternion::invertIt()
{
	double sqr_len_0 = dotQuat(mReal, mReal);
	double sqr_len_e = 2.0*dotQuat(mReal, mDual);

	if (sqr_len_0>0.0)
	{
		double inv_sqr_len_0 = 1.0/sqr_len_0;
		double inv_sqr_len_e = -sqr_len_e/(sqr_len_0*sqr_len_0);

		MDualQuaternion conj = conjugate();
		mReal = inv_sqr_len_0*conj.mReal;
		mDual = inv_sqr_len_0*conj.mDual + inv_sqr_len_e*conj.mReal;
	}
	else
		(*this) = MDualQuaternion::zero;

	return *this;
}

rcMDualQuaternion MDualQuaternion::setFromScrew(double inAngle, double inPitch, rcMVector inDir, rcMVector inMoment)
{
	double sin_half_angle = sin(inAngle*0.5);
	double cos_half_angle = cos(inAngle*0.5);
	
	mReal.w = cos_half_angle;
	mReal.x = sin_half_angle*inDir.x;
	mReal.y = sin_half_angle*inDir.y;
	mReal.z = sin_half_angle*inDir.z;

	mDual.w = -inPitch*sin_half_angle*0.5;
	mDual.x = sin_half_angle*inMoment.x + 0.5*inPitch*cos_half_angle*inDir.x;
	mDual.y = sin_half_angle*inMoment.y + 0.5*inPitch*cos_half_angle*inDir.y;
	mDual.z = sin_half_angle*inMoment.z + 0.5*inPitch*cos_half_angle*inDir.z;

	return *this;
}

void MDualQuaternion::toScrew(double& outAngle, double& outPitch, rMVector outDir, rMVector outMoment) const
{
	assert(isUnit());

	// See if it's a pure translation:
	if (isPureTranslation())
	{
		outAngle			= 0.0;
		outDir.x			= mDual.x; 
		outDir.y			= mDual.y; 
		outDir.z			= mDual.z;

		double dir_sq_len	= outDir.x*outDir.x + outDir.y*outDir.y + outDir.z*outDir.z;
		
		// If a translation is nonzero, normalize is
		// else leave <outDir> zero vector (no motion at all)
		if (dir_sq_len>1e-6)
		{
			double dir_len	= sqrt(dir_sq_len);
			outPitch		= 2.0*dir_len;
			outDir			/= dir_len;
		}
		else
			outPitch		= 0.0;
		
		// Moment can be arbitrary
		outMoment			= MVector::zero;
	}
	else
	{ 
		// Rigid transformation with a nonzero rotation
		outAngle			= 2.0*acos(mReal.w);

		double s			= mReal.x*mReal.x + mReal.y*mReal.y + mReal.z*mReal.z;
		if (s<1e-6)
		{
			outDir			= MVector::zero;
			outPitch		= 0.0;
			outMoment		= MVector::zero;
		}
		else
		{
			double oos		= 1.0/sqrt(s);
			outDir.x		= mReal.x * oos;
			outDir.y		= mReal.y * oos;
			outDir.z		= mReal.z * oos;

			outPitch		= -2.0*mDual.w*oos;

			outMoment.x 	= mDual.x; 
			outMoment.y 	= mDual.y; 
			outMoment.z 	= mDual.z;

			outMoment		= (outMoment - outDir*outPitch*mReal.w*0.5) * oos;
		}
	}
}

MDualQuaternion MDualQuaternion::log() const
{
	double angle, pitch;
	MVector direction, moment;
	toScrew(angle, pitch, direction, moment);

	MDualQuaternion res;
	res.mReal.x = direction.x*angle*0.5;
	res.mReal.y = direction.y*angle*0.5;
	res.mReal.z = direction.z*angle*0.5;
	res.mReal.w = 0.0;

	res.mDual.x = moment.x*angle*0.5 + direction.x*pitch*0.5;
	res.mDual.y = moment.y*angle*0.5 + direction.y*pitch*0.5;
	res.mDual.z = moment.z*angle*0.5 + direction.z*pitch*0.5;
	res.mDual.w = 0.0;

	return res;
}

MDualQuaternion MDualQuaternion::exp() const
{
	MDualQuaternion res;
	MVector n(mReal.x, mReal.y, mReal.z);

	double half_angle = n.length();

	// Pure translation?
	if (half_angle<1e-5)
		return MDualQuaternion(MQuaternion::identity, mDual);

	// Get normalized dir
	MVector dir = (1.0/half_angle) * n;

	MVector d(mDual.x, mDual.y, mDual.z);
	double half_pitch = d*dir;
	MVector mom = (d - dir*half_pitch) / half_angle;

	return res.setFromScrew(half_angle*2.0, half_pitch*2.0, dir, mom);
}



//
// Do screw linear interpolation (the "slerp" for dual quaternions) for two unit dual quaternions
//
MDualQuaternion MDualQuaternion::sScLERP(double inT, rcMDualQuaternion inFrom, rcMDualQuaternion inTo)
{
	assert(0.0<=inT && inT<=1.0);	
	assert(inFrom.isUnit() && inTo.isUnit());

	// Make sure dot product is >= 0
	double quat_dot = inFrom.dotReal(inTo);
	MDualQuaternion to_sign_corrected = (quat_dot>=0.0) ? inTo : -inTo;

	MDualQuaternion dif_dq = inFrom.inverse() * to_sign_corrected;
	
	double  angle, pitch;
	MVector direction, moment;
	dif_dq.toScrew(angle, pitch, direction, moment);

	angle *= inT; 
	pitch *= inT;
	dif_dq.setFromScrew(angle, pitch, direction, moment);

	return inFrom * dif_dq;
}



//
// Do dual quaternion linear interpolation. Result is normalized afterwards.
//
MDualQuaternion MDualQuaternion::sDLB(int inCount, const double inWeightList[], const MDualQuaternion inDualQuatList[])
{
	assert(inCount>0);

	// Find max weight index for pivoting to that quaternion, so shortest arc is taken
	int pivot_idx = 0;
	for (int n=1; n<inCount; n++)
		if (inWeightList[pivot_idx] < inWeightList[n])
			pivot_idx = n;
	rcMDualQuaternion dq_pivot = inDualQuatList[pivot_idx];

	MDualQuaternion res(MDualQuaternion::zero);
	for (int n=0; n<inCount; n++)
	{
		rcMDualQuaternion dq = inDualQuatList[n];
		double weight = inWeightList[n];

		// Make sure dot product is >= 0
		if (dq.dotReal(dq_pivot)<0.0)
			weight = -weight;

		res += weight*dq;
	}

	res.normalizeIt();

	return res;
}



//
// Do dual quaternion iterative intrinsic interpolation up to given precision. Result is always normalized.
//
MDualQuaternion MDualQuaternion::sDIB(int inCount, const double inWeightList[], const MDualQuaternion inDualQuatList[], double inPrecision)
{
	assert(inCount>0);
	MDualQuaternion b = sDLB(inCount, inWeightList, inDualQuatList);

	// Find max weight index for pivoting to that quaternion, so shortest arc is taken
	int pivot_idx = 0;
	for (int n=1; n<inCount; n++)
		if (inWeightList[pivot_idx] < inWeightList[n])
			pivot_idx = n;
	rcMDualQuaternion dq_pivot = inDualQuatList[pivot_idx];

	// Iteratively refine
	enum { MAX_DIB_ITERS = 20 };
	for (int iter=0; iter<MAX_DIB_ITERS; ++iter)
	{
		cMDualQuaternion inv_b = b.inverse();
		MDualQuaternion x(MDualQuaternion::zero);
		for (int i=0; i<inCount; i++)
		{
			MDualQuaternion dq		= inDualQuatList[i];
			double weight			= inWeightList[i];

			// Make sure dot product is >= 0
			if (dq.dotReal(dq_pivot)<0.0)
				dq.negateIt();

			MDualQuaternion xi		= inv_b * dq;
			x += weight*xi.log();
		}

		double norm = x.dot(x);

		b = b * x.exp();

		if (norm < inPrecision) 
			return b;				
	}

	// Failed to converge. At least normalize.
	b.normalizeIt();

	return b;
}
