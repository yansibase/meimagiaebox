// MDualQuaternion.h - Copyright (C) Guido de Haan 2006-2007. All rights reserved.
#ifndef INCLUDED_MDUALQUATERNION_H
#define INCLUDED_MDUALQUATERNION_H
#ifdef _WIN32
#pragma once
#endif // _WIN32

#include <math.h>
#include <assert.h>
#include <iostream>
#include <maya/MPoint.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MEulerRotation.h>
#include <maya/MQuaternion.h>

typedef const MEulerRotation&	rcMEulerRotation;
typedef const MVector&			rcMVector;
typedef MVector&				rMVector;
typedef const MPoint&			rcMPoint;
typedef MPoint&					rMPoint;
typedef const MQuaternion&		rcMQuaternion;
typedef MQuaternion&			rMQuaternion;
typedef const MMatrix&			rcMMatrix;
typedef MMatrix&				rMMatrix;
typedef MMatrix*				pMMatrix;
typedef const MMatrix*			pcMMatrix;

class MDualQuaternion;
typedef const MDualQuaternion	cMDualQuaternion;
typedef const MDualQuaternion&	rcMDualQuaternion;
typedef MDualQuaternion&		rMDualQuaternion;

inline double dotQuat(rcMQuaternion inLHS, rcMQuaternion inRHS)
{
	return inLHS.x*inRHS.x + inLHS.y*inRHS.y + inLHS.z*inRHS.z + inLHS.w*inRHS.w;
}

inline MQuaternion mulQuat(rcMQuaternion inLHS, rcMQuaternion inRHS)
{
	// NB: Maya-style (i.e. DirectX) transform. Swap quaternion multiplier arguments for OpenGL-style transforms.
	//     I.e. using OpenGL transforms, A*B means first transform by B, then by A.
	//     Using DirectX transforms, A*B means first transform by A, then by B.
	//     This code was written with OpenGL transforms in mind, so the quaternion multiply was abstracted
	//     in this single function to hide Maya's DirectX style transforms.
	return inRHS*inLHS;
}

//---------------------------------------------------------------------------------------------------------------------
//	MDualQuaternion
//---------------------------------------------------------------------------------------------------------------------

//
// A dual quaternion can do rigid transformations, i.e. rotation and translation.
// It does not support scaling. Based on MQuaternion.
//
// Unless noted otherwise, the <rotation,translation> pair is defined as
// first a rotation, followed by a translation.
//
// Notes:
// - Inspired by dual quaternion C++ code from Ladislav Kavan (kavanl1 AT fel.cvut.cz)
// - This is a reference implementation and not optimized for speed
//
class MDualQuaternion
{
public:
						MDualQuaternion();
						MDualQuaternion(rcMDualQuaternion inOther);
						MDualQuaternion(rcMQuaternion inReal, rcMQuaternion inDual);
						MDualQuaternion(double inRx, double inRy, double inRz, double inRw,
									    double inDx, double inDy, double inDz, double inDw);
	explicit			MDualQuaternion(rcMQuaternion inRotation);
	explicit			MDualQuaternion(rcMVector inTranslation);
						MDualQuaternion(rcMVector inTranslation, rcMQuaternion inRotation);
	explicit			MDualQuaternion(rcMMatrix inRigidTransform);
	rcMDualQuaternion	operator=(rcMDualQuaternion inRHS);
	rcMDualQuaternion	operator=(rcMMatrix inRigidTransform);
	rcMDualQuaternion	operator=(rcMEulerRotation inRotationMatrix);

	// MQuaternion interface
	bool				operator==(rcMDualQuaternion inRHS) const;
	bool				operator!=(rcMDualQuaternion inRHS) const;
	MDualQuaternion		operator+(rcMDualQuaternion inRHS) const;
	MDualQuaternion		operator-(rcMDualQuaternion inRHS) const;
	MDualQuaternion		operator-() const;
	friend MDualQuaternion operator*(double inScale, rcMDualQuaternion inRHS);
	rcMDualQuaternion	negateIt();
	bool	         	isEquivalent(rcMDualQuaternion inOther, double inTolerance = kQuaternionEpsilon) const;
	rcMDualQuaternion	scaleIt(double inScale);
	MDualQuaternion 	normal() const;
	rcMDualQuaternion	normalizeIt();
	MDualQuaternion 	conjugate() const;
	rcMDualQuaternion	conjugateIt();
	MDualQuaternion 	inverse() const;
	rcMDualQuaternion	invertIt();
	MDualQuaternion		log() const;
	MDualQuaternion		exp() const;
	MMatrix				asMatrix() const;
	operator			MMatrix() const;

	// Misc
	rcMDualQuaternion	operator+=(rcMDualQuaternion inRHS);
	rcMDualQuaternion	operator*=(rcMDualQuaternion inLHS);
	rcMDualQuaternion	operator*=(double inScalar);
	MDualQuaternion		operator*(rcMDualQuaternion inRHS) const;
	double				dotReal(rcMDualQuaternion inRHS) const;
	double				dotDual(rcMDualQuaternion inRHS) const;
	double				dot(rcMDualQuaternion inRHS) const;
	MDualQuaternion		rotationNormalized() const;
	MDualQuaternion		pluckerNormalized() const;
	MQuaternion			getRotation() const;
	MVector				getTranslation() const;
	MVector				getTranslationFromUnit() const;
	MMatrix				asMatrixFromUnit() const;
	bool				checkPlucker() const;
	bool				isUnit() const;
	bool				hasRotation() const;
	bool				isPureTranslation() const;
	MPoint				transform(rcMPoint inPoint) const;
	MPoint				transformFromUnit(rcMPoint inPoint) const;
	rcMDualQuaternion	setFromScrew(double inAngle, double inPitch, rcMVector inDir, rcMVector inMoment);
	void				toScrew(double& outAngle, double& outPitch, rMVector outDir, rMVector outMoment) const;

	// Do screw linear interpolation (the "slerp" for dual quaternions) for two unit dual quaternions
	static MDualQuaternion		sScLERP(double inT, rcMDualQuaternion inFrom, rcMDualQuaternion inTo);

	// Do dual quaternion linear interpolation. Result is normalized afterwards.
	static MDualQuaternion		sDLB(int inCount, const double inWeightList[], const MDualQuaternion inDualQuatList[]);

	// Do dual quaternion iterative intrinsic interpolation up to given precision. Result is always normalized.
	static MDualQuaternion		sDIB(int inCount, const double inWeightList[], const MDualQuaternion inDualQuatList[], double inPrecision);

	// Static DualQuaternions
	static cMDualQuaternion		zero;											///< R(0, 0, 0, 0), D(0,0,0,0)
	static cMDualQuaternion		identity;										///< R(0, 0, 0, 1), D(0,0,0,0)

	MQuaternion			mReal;													///< Real quaternion part
	MQuaternion			mDual;													///< Dual quaternion part
};


//---------------------------------------------------------------------------------------------------------------------
//	MDualQuaternion .inl
//---------------------------------------------------------------------------------------------------------------------

inline MDualQuaternion::MDualQuaternion()
{
}

inline MDualQuaternion::MDualQuaternion(rcMDualQuaternion inOther) :
	mReal(inOther.mReal),
	mDual(inOther.mDual)
{
}

inline MDualQuaternion::MDualQuaternion(rcMQuaternion inReal, rcMQuaternion inDual) :
	mReal(inReal),
	mDual(inDual)
{
}

inline MDualQuaternion::MDualQuaternion(double inRx, double inRy, double inRz, double inRw,
										double inDx, double inDy, double inDz, double inDw)
{
	mReal.x = inRx;	mReal.y = inRy;	mReal.z = inRz;	mReal.w = inRw;
	mDual.x = inDx;	mDual.y = inDy;	mDual.z = inDz;	mDual.w = inDw;
}

inline MDualQuaternion::MDualQuaternion(rcMQuaternion inRotation) :
	mReal(inRotation),
	mDual(0.0,0.0,0.0,0.0)
{
}

inline MDualQuaternion::MDualQuaternion(rcMVector inTranslation) :
	mReal(MQuaternion::identity),
	mDual(0.5*inTranslation.x, 0.5*inTranslation.y, 0.5*inTranslation.z, 0.0)
{
}

inline MDualQuaternion::MDualQuaternion(rcMVector inTranslation, rcMQuaternion inRotation) :
	mReal(inRotation),
	mDual(0.5*inTranslation.x, 0.5*inTranslation.y, 0.5*inTranslation.z, 0.0)
{
	mDual = mulQuat(mDual, mReal);
}

inline MDualQuaternion::MDualQuaternion(rcMMatrix inRigidTransform)
{
	*this = inRigidTransform;
}

inline rcMDualQuaternion MDualQuaternion::operator=(rcMDualQuaternion inRHS)
{
	mReal = inRHS.mReal;
	mDual = inRHS.mDual;
	return *this;
}

inline rcMDualQuaternion MDualQuaternion::operator=(rcMEulerRotation inRotationMatrix)
{
	mReal = inRotationMatrix;
	mDual = MQuaternion(0.0,0.0,0.0,0.0);
    return *this;
}

inline bool MDualQuaternion::operator==(rcMDualQuaternion inRHS) const
{
	return mReal==inRHS.mReal && mDual==inRHS.mDual;
}

inline bool MDualQuaternion::operator!=(rcMDualQuaternion inRHS) const
{
	return !((*this)==inRHS);
}

inline double MDualQuaternion::dotReal(rcMDualQuaternion inRHS) const
{
	return dotQuat(mReal, inRHS.mReal);
}

inline double MDualQuaternion::dotDual(rcMDualQuaternion inRHS) const
{
	return dotQuat(mDual, inRHS.mDual);
}

inline double MDualQuaternion::dot(rcMDualQuaternion inRHS) const
{
	return dotReal(inRHS) + dotDual(inRHS);
}

inline MDualQuaternion MDualQuaternion::operator-() const
{
	return MDualQuaternion(-mReal, -mDual);
}

inline rcMDualQuaternion MDualQuaternion::negateIt()
{
	mReal.negateIt();
	mDual.negateIt();
	return *this;
}

inline MDualQuaternion MDualQuaternion::operator+(rcMDualQuaternion inRHS) const
{
	return MDualQuaternion(mReal+inRHS.mReal, mDual+inRHS.mDual);
}

inline MDualQuaternion MDualQuaternion::operator-(rcMDualQuaternion inRHS) const
{
	return MDualQuaternion(mReal-inRHS.mReal, mDual-inRHS.mDual);
}

inline bool MDualQuaternion::isEquivalent(rcMDualQuaternion inOther, double inTolerance) const
{
	return mReal.isEquivalent(inOther.mReal, inTolerance) && mDual.isEquivalent(inOther.mDual, inTolerance);
}

inline rcMDualQuaternion MDualQuaternion::scaleIt(double inScale)
{
	mReal.scaleIt(inScale);
	mDual.scaleIt(inScale);
	return *this;
}

inline MDualQuaternion MDualQuaternion::rotationNormalized() const
{
	double oo_magn = 1.0/sqrt(dotQuat(mReal, mReal));
	return MDualQuaternion(oo_magn*mReal, oo_magn*mDual);
}

inline MDualQuaternion MDualQuaternion::pluckerNormalized() const
{
	double oo_magn_sqr = 1.0/dotQuat(mReal, mReal);
	return MDualQuaternion(mReal, mDual - (dotQuat(mReal, mDual)*oo_magn_sqr)*mReal);
}

inline bool MDualQuaternion::checkPlucker() const
{
	// Test for Pl�cker condition. Dot between real and dual part must be 0
	return fabs(dotQuat(mReal,mDual))<1e-5;
}

inline bool MDualQuaternion::isUnit() const
{
	// Real must be unit and plucker condition must hold
	return (fabs(dotQuat(mReal,mReal)-1.0)<1e-5) && checkPlucker();
}

inline bool MDualQuaternion::hasRotation() const
{
	assert(isUnit());
	return fabs(mReal.w)<0.999999f;
}

inline bool MDualQuaternion::isPureTranslation() const
{
	return !hasRotation();
}

inline MQuaternion MDualQuaternion::getRotation() const
{
	assert(isUnit());
	return mReal;
}

inline MVector MDualQuaternion::getTranslationFromUnit() const
{
	assert(isUnit());
	MQuaternion qeq0 = mulQuat(mDual, mReal.conjugate());
	return MVector(2.0*qeq0.x, 2.0*qeq0.y, 2.0*qeq0.z);
}

inline MVector MDualQuaternion::getTranslation() const
{
	if (isUnit())
	{
		return getTranslationFromUnit();
	}
	else
	{
		double nq		= dotQuat(mReal, mReal);
		double scale	= -2.0/nq;

		double rw		= mReal.w; double rx = mReal.x; double ry = mReal.y; double rz = mReal.z;
		double dw		= mDual.w; double dx = mDual.x; double dy = mDual.y; double dz = mDual.z;

		double tx		= (dw*rx - rw*dx + dy*rz - ry*dz)*scale;
		double ty		= (dw*ry - dx*rz + rx*dz - rw*dy)*scale;
		double tz		= (dw*rz - rx*dy - rw*dz + dx*ry)*scale;

		// This can also be written as:
		//	MQuaternion qeq0= mulQuat(mDual, mReal.conjugate()) - mulQuat(mReal, mDual.conjugate());
		//	double tx		= qeq0.x/nq;
		//	double ty		= qeq0.y/nq;
		//	double tz		= qeq0.z/nq;

		return MVector(tx,ty,tz);
	}
}

inline MDualQuaternion MDualQuaternion::normal() const
{
	MDualQuaternion dq(*this);
	return dq.normalizeIt();
}

inline rcMDualQuaternion MDualQuaternion::normalizeIt()
{
	// NB: the order of normalizations does not matter
	*this = rotationNormalized().pluckerNormalized();
	return *this;
}

inline MDualQuaternion MDualQuaternion::conjugate() const
{
	MDualQuaternion dq(*this);
	return dq.conjugateIt();
}

inline rcMDualQuaternion MDualQuaternion::conjugateIt()
{
	mReal.conjugateIt();
	mDual.conjugateIt();
	return *this;
}

inline MDualQuaternion MDualQuaternion::inverse() const
{
	MDualQuaternion dq(*this);
	return dq.invertIt();
}

inline MDualQuaternion operator*(double inScale, rcMDualQuaternion inRHS)
{
	return MDualQuaternion(inScale*inRHS.mReal, inScale*inRHS.mDual);
}

inline rcMDualQuaternion MDualQuaternion::operator+=(rcMDualQuaternion inRHS)
{
	mReal = mReal + inRHS.mReal;
	mDual = mDual + inRHS.mDual;
	return *this;
}

inline rcMDualQuaternion MDualQuaternion::operator*=(double inScalar)
{
	return scaleIt(inScalar);
}

inline rcMDualQuaternion MDualQuaternion::operator*=(rcMDualQuaternion inLHS)
{
	// NB: Maya style transform multiplication: this = inLHS * this
	*this = inLHS * (*this);
	return *this;
}

inline MDualQuaternion MDualQuaternion::operator*(rcMDualQuaternion inRHS) const
{
	return MDualQuaternion(mReal*inRHS.mReal, mReal*inRHS.mDual + mDual*inRHS.mReal);
}

inline MPoint MDualQuaternion::transform(rcMPoint inPoint) const
{
	MMatrix mat = asMatrix();
	return inPoint * mat;
}

inline MDualQuaternion::operator MMatrix() const
{
	return asMatrix();
}

#endif // INCLUDED_MDUALQUATERNION_H


