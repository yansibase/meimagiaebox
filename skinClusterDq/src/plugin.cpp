#include "common/IncludeMFnPluginClass.h"
#include <maya/MGlobal.h>
#include <maya/MStatus.h>

#include <common/log.h>
#include "version.h"
#include "skinClusterDq/skinClusterDqCmds.h"
#include "skinClusterDq/skinClusterDq.h"
#include "skinClusterDq/dualQuaternionCmd.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya
//
PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('skinClusterDq')", true, false) );

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

	CHECK_MSTATUS( SkinClusterDq::registerNode(plugin) );

	CHECK_MSTATUS( DualQuaternionCmd::registerCmd(plugin) );

	CHECK_MSTATUS( CopyInputAttrCmd::sRegisterCommand(plugin) );
	CHECK_MSTATUS( CopyOutputAttrCmd::sRegisterCommand(plugin) );

	//MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	//command += TestDeformer::cClassification();
	//command += "\");}\n";
	//CHECK_MSTATUS( MGlobal::executeCommand( command ) );

	/// unit test
	//unittest_main();

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('skinClusterDq')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('skinClusterDq')", true, false) );

	MFnPlugin plugin( obj );

	CHECK_MSTATUS( CopyOutputAttrCmd::sDeregisterCommand(plugin) );
	CHECK_MSTATUS( CopyInputAttrCmd::sDeregisterCommand(plugin) );

	CHECK_MSTATUS( DualQuaternionCmd::deregisterCmd(plugin) );

	CHECK_MSTATUS( SkinClusterDq::deregisterNode(plugin) );

	//MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	//command += TestDeformer::cClassification();
	//command += "\");}\n";
	//CHECK_MSTATUS( MGlobal::executeCommand( command ) );

    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('skinClusterDq')", true, false) );

	return MS::kSuccess;
}
