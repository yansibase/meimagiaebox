// skinClusterDqCmds.h - Copyright (C) Guido de Haan 2005-2007. All rights reserved. 
#ifndef INCLUDED_SKINCLUSTERDQCMDS_H
#define INCLUDED_SKINCLUSTERDQCMDS_H
#pragma once

// Forward declares
class MDGModifier;
class MFnPlugin;

// Includes
#include <iostream>
#include <maya/MPxCommand.h>



//---------------------------------------------------------------------------------------------------------------------
// CopyInputAttrCmd
//---------------------------------------------------------------------------------------------------------------------

//
// CopyInputAttrCmd
//
class CopyInputAttrCmd : public MPxCommand
{
public:
	static MStatus		sRegisterCommand(MFnPlugin& ioPlugin);
	static MStatus		sDeregisterCommand(MFnPlugin& ioPlugin);
	static void*		sCreateCommand();
	static MSyntax		sCreateSyntax();

						CopyInputAttrCmd();
						~CopyInputAttrCmd();

	// MPxCommand interface
	virtual MStatus   	doIt(const MArgList& args);
	virtual MStatus   	undoIt();
	virtual MStatus   	redoIt();
	virtual bool		isUndoable() const;
	virtual bool		hasSyntax() const;

protected:
	MStatus				ParseArgs(const MArgList& inArgs);

	MDGModifier*		mDgModifier;											// For undoing changes
	MString				mSrcInputAttrName;
	MString				mDstInputAttrName;

	static const char*	sSyntaxText;
};




//---------------------------------------------------------------------------------------------------------------------
// CopyOutputAttrCmd
//---------------------------------------------------------------------------------------------------------------------

//
// CopyOutputAttrCmd
//
class CopyOutputAttrCmd : public MPxCommand
{
public:
	static MStatus		sRegisterCommand(MFnPlugin& ioPlugin);
	static MStatus		sDeregisterCommand(MFnPlugin& ioPlugin);
	static void*		sCreateCommand();
	static MSyntax		sCreateSyntax();

						CopyOutputAttrCmd();
						~CopyOutputAttrCmd();

	// MPxCommand interface
	virtual MStatus   	doIt(const MArgList& args);
	virtual MStatus   	undoIt();
	virtual MStatus   	redoIt();
	virtual bool		isUndoable() const;
	virtual bool		hasSyntax() const;

protected:
	MStatus				ParseArgs(const MArgList& inArgs);

	MDGModifier*		mDgModifier;											// For undoing changes
	MString				mSrcOutputAttrName;
	MString				mDstOutputAttrName;

	static const char*	sSyntaxText;
};



#endif