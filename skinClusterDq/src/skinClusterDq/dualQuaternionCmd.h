
#pragma once

#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <common/IncludeMFnPluginClass.h>

class DualQuaternionCmd : public MPxCommand
{
public:
    DualQuaternionCmd() {};
    ~DualQuaternionCmd() {};

    virtual MStatus doIt( const MArgList &args );
    virtual MStatus undoIt();
    virtual MStatus redoIt();

    virtual bool isUndoable() const;

    virtual bool hasSyntax() const;

    bool isHistoryOn() const;



    static MString name();
    static MSyntax mySyntax();

    static void* creator() {return new DualQuaternionCmd();};

    static MStatus registerCmd(MFnPlugin &plugin);
    static MStatus deregisterCmd(MFnPlugin &plugin);


    static const MString getUsageString();

protected:
    MStatus _test(const MString& skinNode, const MString& vertexName);
    MString m_skinNode;
    MString m_skinVertexName;
};


