#include "dualQuaternionCmd.h"

#include <cassert>
#include <vector>

#include <maya/MArgList.h>
#include <maya/MArgDatabase.h>
#include <maya/MDagPath.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MGlobal.h>
#include <maya/MIntArray.h>
#include <maya/MMatrix.h>
#include <maya/MMatrixArray.h>
#include <maya/MSelectionList.h>

#include <common/log.h>
#include <common/node_ids.h>

#include "../MayaUtils/MDualQuaternion.h"


#define kHelp_s     ("-h")
#define kHelp_l     ("-help")

#define kSkinNode_s     ("-skn")
#define kSkinNode_l     ("-skinnode")
#define kSkinVertexName_s   ("-sv")
#define kSkinVertexName_l   ("-skinvtx")

//
// DualQuaternionCmd::doIt
//
//  This is the function used to invoke the command. The
// command is not undoable and it does not change any state,
// so it does not use the method to call back throught redoIt.


//////////////////////////////////////////////////////////////////////
const MString DualQuaternionCmd::getUsageString()
{
    MString usage;

    usage += "\n ";
    usage += "\n "+name()+"  [options]";
    usage += "\n Options:";
    usage += "\n "+MString(kHelp_s)             +" / "+MString(kHelp_l)             +"\t\t\t"+"print this message ";

    usage += "\n "+MString(kSkinNode_s)         +" / "+MString(kSkinNode_l)         +"\t\t\t"+"skin node";
    usage += "\n "+MString(kSkinVertexName_s)   +" / "+MString(kSkinVertexName_l)   +"\t\t\t"+"skin vertex name";
    usage += "\n ";
    usage += "\n Example:";
    usage += "\n "+name()+" "+MString(kHelp_l)+";";

    usage += "\n "+name()+" "+MString(kSkinNode_l) +" \"skinNode1\" "+MString(kSkinVertexName_l) +" \"pPlane1.vtx[1]\" ";

    usage += "\n ";

    return usage;
}
//
MSyntax DualQuaternionCmd::mySyntax()
{
    MSyntax syntax;

    // NOTE:
    // Short flags are three letters or less;
    // Long flags are four letters or more.
    CHECK_MSTATUS(syntax.addFlag( kHelp_s,              kHelp_l,                    MSyntax::kNoArg));

    CHECK_MSTATUS(syntax.addFlag( kSkinNode_s,          kSkinNode_l,                MSyntax::kString));
    CHECK_MSTATUS(syntax.addFlag( kSkinVertexName_s,    kSkinVertexName_l,          MSyntax::kString));

    syntax.enableQuery(true);
    syntax.enableEdit(true);

    return syntax;
}
//////////////////////////////////////////////////////////////////////
MStatus DualQuaternionCmd::doIt( const MArgList &args)
{
    MStatus status;

    MArgDatabase argData( syntax(), args, &status);
    CHECK_MSTATUS(status);

    //const bool queryUsed = argData.isQuery();
    // help
    if (argData.isFlagSet(kHelp_l))
    {
        MGlobal::displayInfo(getUsageString());
        return MS::kSuccess;
    }

   /////////////////////////// string ///////////////////////////////////////
    // string
    if (argData.isFlagSet(kSkinNode_l))
    {
        status = argData.getFlagArgument(kSkinNode_l, 0, m_skinNode);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("skin node flag parsing failed"));
            return MStatus::kFailure;
        }
    }else{
        MGlobal::displayError(MString("skin node flag is not specified"));
        return MStatus::kFailure;
    }

    if (argData.isFlagSet(kSkinVertexName_l))
    {
        status = argData.getFlagArgument(kSkinVertexName_l, 0, m_skinVertexName);
        if (status != MS::kSuccess)
        {
            MGlobal::displayError(MString("skin vertex name flag parsing failed"));
            return MStatus::kFailure;
        }
    }else{
        MGlobal::displayError(MString("skin vertex name flag is not specified"));
        return MStatus::kFailure;
    }

    CHECK_MSTATUS( _test(m_skinNode, m_skinVertexName) );
    return MStatus::kSuccess;
}
//
MStatus DualQuaternionCmd::_test(const MString& skinNode, const MString& skinVertexName)
{
    MStatus status;

    MIntArray matrixIndices;
    MGlobal::executePythonCommand("import maya.cmds as cmds; cmds.getAttr('"+skinNode+".matrix', multiIndices = True)", matrixIndices);
    //std::cout<<"matrixIndices="<< matrixIndices<<std::endl;// debug

    MDoubleArray weightList;
    MGlobal::executePythonCommand("import maya.cmds as cmds; cmds.skinPercent('"+skinNode+"' , '"+skinVertexName+"' , q = True , v = True)", weightList);
    //std::cout<<"weightList="<< weightList<<std::endl;// debug

    MMatrixArray matrixList;
    char cmd[1024];
    for(unsigned int w=0; w<weightList.length(); ++w)
    {
        //std::cout<< "w=" << w <<std::endl;
        sprintf(cmd, "import maya.cmds as cmds; cmds.getAttr('%s.bindPreMatrix[%d]')", skinNode.asChar() , matrixIndices[w]);
        MDoubleArray bindPreMMatrixBuf;
        MGlobal::executePythonCommand(cmd, bindPreMMatrixBuf);
        MMatrix bindPreMMatrix;
        for(unsigned int r=0; r<4; ++r)
             for(unsigned int c=0; c<4; ++c)
                    bindPreMMatrix(r, c) = bindPreMMatrixBuf[4 * r + c];
        //std::cout<<"bindPreMMatrix="<< bindPreMMatrix<<std::endl;// debug

        sprintf(cmd, "import maya.cmds as cmds; cmds.getAttr('%s.matrix[%d]')", skinNode.asChar() , matrixIndices[w]);
        MDoubleArray drivingMMatrixBuf;
        MGlobal::executePythonCommand(cmd, drivingMMatrixBuf);
        MMatrix drivingMMatrix;
        for(unsigned int r=0; r<4; ++r)
             for(unsigned int c=0; c<4; ++c)
                    drivingMMatrix(r, c) = drivingMMatrixBuf[4 * r + c];
        //std::cout<<"drivingMMatrix="<< drivingMMatrix<<std::endl;// debug

        matrixList.append(bindPreMMatrix * drivingMMatrix);
    }
    //std::cout<<"matrixList="<< matrixList<<std::endl;// debug

    std::vector<double> deformer_weights;
    deformer_weights.reserve(weightList.length());
    for(unsigned int i=0; i<weightList.length(); ++i)
    {
        deformer_weights.push_back(weightList[i]);
    }

    std::vector<MDualQuaternion> deformer_dualquats;
    deformer_dualquats.reserve(matrixList.length());
    for(unsigned int i=0; i<matrixList.length(); ++i)
    {
        MDualQuaternion dq;
        dq = matrixList[i];
        deformer_dualquats.push_back(dq);
    }

    std::vector<MMatrix> deformer_matrices;
    deformer_matrices.reserve(matrixList.length());
    for(unsigned int i=0; i<matrixList.length(); ++i)
    {
        deformer_matrices.push_back(matrixList[i]);
    }

    MDualQuaternion total_dq = MDualQuaternion::sDLB(deformer_weights.size(), &deformer_weights.front(), &deformer_dualquats.front());
    MMatrix transform(total_dq.asMatrixFromUnit()); //skinned_pt = skinned_pt * transform;

    uint num_skin_weights = (uint)deformer_weights.size();
    MMatrix total_weighted_mm;
    for (uint w=0; w<num_skin_weights; ++w)
    {
        MMatrix weigted_mm = deformer_weights[w]*MTransformationMatrix(deformer_matrices[w]).asScaleMatrix();
        total_weighted_mm = (w==0) ? weigted_mm : (total_weighted_mm+weigted_mm);
    }

	MMatrix	ret(total_weighted_mm * transform);
    // set result
    MPxCommand::clearResult ();
    for(int i=0; i<16; ++i)
    {
        MPxCommand::appendToResult(ret(i/4, i%4));
    }

    return MStatus::kSuccess;
}
//
//  There is never anything to undo.
//////////////////////////////////////////////////////////////////////
MStatus DualQuaternionCmd::undoIt()
{
    return MStatus::kSuccess;
}

//
//  There is never really anything to redo.
//////////////////////////////////////////////////////////////////////
MStatus DualQuaternionCmd::redoIt()
{
    return MStatus::kSuccess;
}
//////////////////////////////////////////////////////////////////////
bool DualQuaternionCmd::isUndoable() const
{
    return false;
}
//////////////////////////////////////////////////////////////////////
bool DualQuaternionCmd::hasSyntax() const
{
    return true;
}
//////////////////////////////////////////////////////////////////////
bool DualQuaternionCmd::isHistoryOn() const
{
    //what is this supposed to do?
    return false;
}
//////////////////////////////////////////////////////////////////////
MString DualQuaternionCmd::name()
{
    return CmdName_DualQuaternion;
}
//
MStatus DualQuaternionCmd::registerCmd(MFnPlugin &plugin)
{
    MStatus status;

    //register a helper command for querying shader information
    CHECK_MSTATUS(plugin.registerCommand(
                           DualQuaternionCmd::name(),
                           DualQuaternionCmd::creator,
                           DualQuaternionCmd::mySyntax)
                  );
    return MS::kSuccess;
}
//
MStatus DualQuaternionCmd::deregisterCmd(MFnPlugin &plugin)
{
    CHECK_MSTATUS(
                  plugin.deregisterCommand( DualQuaternionCmd::name() )
                  );

    return MS::kSuccess;
}
