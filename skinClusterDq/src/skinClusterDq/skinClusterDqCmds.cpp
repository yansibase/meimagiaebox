// skinClusterDqCmds.cpp - Copyright (C) Guido de Haan 2005-2007. All rights reserved.
#include "skinClusterDqCmds.h"

#include <assert.h>

#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MDGModifier.h>
#include <maya/MArgDatabase.h>
#include <maya/MArgList.h>
#include <maya/MFnSkinCluster.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MSyntax.h>
#include <maya/MPlugArray.h>
#include <maya/MIOStream.h>

#include "../MayaUtils/MayaUtils.h"
#include <common/IncludeMFnPluginClass.h>



#define COPYRIGHT_TEXT	\
	"(C) 2006-2007 Guido de Haan. All rights reserved."

#define GET_SYNTAX_SHORTNAME(varname)						sSyntaxName##varname
#define GET_SYNTAX_LONGNAME(varname)						sSyntaxLongName##varname

#define MAKE_SYNTAX_NAME(varname, shortname, longname)		\
	static const char* GET_SYNTAX_SHORTNAME(varname)	= shortname;\
	static const char* GET_SYNTAX_LONGNAME(varname)	= longname;	\

MAKE_SYNTAX_NAME(Help,				"-h",		"-help")



//---------------------------------------------------------------------------------------------------------------------
// CopyInputAttrCmd
//---------------------------------------------------------------------------------------------------------------------

#define MAYA_COPYINPUTATTRCMD_NAME		"copyInputAttr"

const char* CopyInputAttrCmd::sSyntaxText = \
	MAYA_COPYINPUTATTRCMD_NAME " " COPYRIGHT_TEXT "\n"
	MAYA_COPYINPUTATTRCMD_NAME "[flags] sourceInputAttribute destInputAttribute\n"
	"\n"
	"Flags:\n"
	" -h   -help         Display help text\n"
	"\n"
	"Copy the input attribute connections or values from one dependency \n"
	"node to another. It is assumed the input attributes have the same \n"
	"type. Existing connections to the destination attribute are removed.\n"
	"\n"
	"When the source attribute is connected, the destination attribute \n"
	"will also be connected to the same source. When no such connection \n"
	"exists, the attribute value is copied.\n"
	"\n"
	"When the attribute is an array attribute, all elements are copied \n"
	"(connections and/or values).\n"
	"\n"
	"When the attribute is a compound attribute, all child attributes are \n"
	"copied as well.\n"
	"\n";


//
// Create new syntax for command
//
MSyntax CopyInputAttrCmd::sCreateSyntax()
{
	MSyntax syntax;
	syntax.addFlag(GET_SYNTAX_SHORTNAME(Help),				GET_SYNTAX_LONGNAME(Help));
	syntax.addArg(MSyntax::kSelectionItem);		// src input attr
	syntax.addArg(MSyntax::kSelectionItem);		// dst input attr
	return syntax;
}

//
// Register command to plugin
//
MStatus CopyInputAttrCmd::sRegisterCommand(MFnPlugin& ioPlugin)
{
	MStatus status = ioPlugin.registerCommand(MAYA_COPYINPUTATTRCMD_NAME, &sCreateCommand, &sCreateSyntax);

	if (MFAIL(status))
		return MReturnStatus(status, "Failed to register " MAYA_COPYINPUTATTRCMD_NAME " command");

	return status;
}

//
// Deregister command to plugin
//
MStatus CopyInputAttrCmd::sDeregisterCommand(MFnPlugin& ioPlugin)
{
	MStatus status = ioPlugin.deregisterCommand(MAYA_COPYINPUTATTRCMD_NAME);

	if (MFAIL(status))
		return MReturnStatus(status, "Failed to deregister " MAYA_COPYINPUTATTRCMD_NAME " command");

	return status;
}

//
// Create CopyInputAttrCmd command
//
void* CopyInputAttrCmd::sCreateCommand()
{
	return new CopyInputAttrCmd();
}

CopyInputAttrCmd::CopyInputAttrCmd() : mDgModifier(NULL)
{
}

CopyInputAttrCmd::~CopyInputAttrCmd()
{
	delete mDgModifier;
}

bool CopyInputAttrCmd::isUndoable() const
{
	return (mDgModifier!=0);
}


MStatus CopyInputAttrCmd::undoIt()
{
	if (mDgModifier!=0)
		return mDgModifier->undoIt();

	return MStatus::kFailure;
}

MStatus CopyInputAttrCmd::redoIt()
{
	if (mDgModifier!=0)
		return mDgModifier->doIt();

	return MStatus::kFailure;
}

bool CopyInputAttrCmd::hasSyntax() const
{
	return true;
}


//
// Parse MArgList for command
//
MStatus CopyInputAttrCmd::ParseArgs(const MArgList& inArgs)
{
	MStatus status;

	// Parse arguments
	MArgDatabase parser(syntax(), inArgs, &status);
	if (status != MS::kSuccess)
	{
		// arguments could not be parsed!
		return status;
	}

	// Help flags
	if (parser.isFlagSet(GET_SYNTAX_SHORTNAME(Help)))
		return MS::kFailure;

	// Src attr name
	if (MFAIL(parser.getCommandArgument(0, mSrcInputAttrName)))
		return MS::kFailure;

	// Dst attr name
	if (MFAIL(parser.getCommandArgument(1, mDstInputAttrName)))
		return MS::kFailure;

	return MS::kSuccess;
}

//
// Do command
//
MStatus CopyInputAttrCmd::doIt(const MArgList& inArgs)
{
	// Parse command arguments
	MStatus status = ParseArgs(inArgs);
	if (MFAIL(status))
	{
		MGlobal::displayInfo(sSyntaxText);
		return status;
	}

	MString src_node_name, src_attr_name;
	MayaUtils::gSplitFullAttributeName(mSrcInputAttrName, src_node_name, src_attr_name);

	MString dst_node_name, dst_attr_name;
	MayaUtils::gSplitFullAttributeName(mDstInputAttrName, dst_node_name, dst_attr_name);

	MObject src_obj = MayaUtils::gNodeFromName(src_node_name);
	MObject dst_obj = MayaUtils::gNodeFromName(dst_node_name);

	if (src_obj.isNull())
	{
		displayWarning("Source node not found.");
		return MS::kFailure;
	}

	if (dst_obj.isNull())
	{
		displayWarning("Destination node not found.");
		return MS::kFailure;
	}

	if (mDgModifier==NULL)
		mDgModifier = new MDGModifier;

	status = MayaUtils::gCopyInputPlugStructure(src_obj,src_attr_name, dst_obj,dst_attr_name, mDgModifier);
	if (MFAIL(status))
		return status;

	mDgModifier->doIt();

	return MS::kSuccess;
}



//---------------------------------------------------------------------------------------------------------------------
// CopyOutputAttrCmd
//---------------------------------------------------------------------------------------------------------------------

#define MAYA_COPYOUTPUTATTRCMD_NAME		"copyOutputAttr"

const char* CopyOutputAttrCmd::sSyntaxText = \
	MAYA_COPYOUTPUTATTRCMD_NAME " " COPYRIGHT_TEXT "\n"
	MAYA_COPYOUTPUTATTRCMD_NAME " [flags] sourceOutputAttribute destOutputAttribute\n"
	"\n"
	"Flags:\n"
	" -h   -help         Display help text\n"
	"\n"
	"Copy the output attribute connections from one dependency node to \n"
	"another. It is assumed the output attributes have the same type.\n"
	"When a connection exists, a forced reconnection is done on the \n"
	"destination attribute.\n"
	"\n"
	"When the source attribute is connected, the destination attribute \n"
	"will also be connected to the same destination.\n"
	"\n"
	"When the attribute is an array attribute, all element connections are moved.\n"
	"\n"
	"When the attribute is a compound attribute, all child attribute connections\n"
	"are moved.\n"
	"\n";


//
// Create new syntax for command
//
MSyntax CopyOutputAttrCmd::sCreateSyntax()
{
	MSyntax syntax;
	syntax.addFlag(GET_SYNTAX_SHORTNAME(Help),				GET_SYNTAX_LONGNAME(Help));
	syntax.addArg(MSyntax::kSelectionItem);		// src output attr
	syntax.addArg(MSyntax::kSelectionItem);		// dst output attr
	return syntax;
}

//
// Register command to plugin
//
MStatus CopyOutputAttrCmd::sRegisterCommand(MFnPlugin& ioPlugin)
{
	MStatus status = ioPlugin.registerCommand(MAYA_COPYOUTPUTATTRCMD_NAME, &sCreateCommand, &sCreateSyntax);

	if (MFAIL(status))
		return MReturnStatus(status, "Failed to register " MAYA_COPYOUTPUTATTRCMD_NAME " command");

	return status;
}

//
// Deregister command to plugin
//
MStatus CopyOutputAttrCmd::sDeregisterCommand(MFnPlugin& ioPlugin)
{
	MStatus status = ioPlugin.deregisterCommand(MAYA_COPYOUTPUTATTRCMD_NAME);

	if (MFAIL(status))
		return MReturnStatus(status, "Failed to deregister " MAYA_COPYOUTPUTATTRCMD_NAME " command");

	return status;
}

//
// Create CopyOutputAttrCmd command
//
void* CopyOutputAttrCmd::sCreateCommand()
{
	return new CopyOutputAttrCmd();
}

CopyOutputAttrCmd::CopyOutputAttrCmd() : mDgModifier(NULL)
{
}

CopyOutputAttrCmd::~CopyOutputAttrCmd()
{
	delete mDgModifier;
}

bool CopyOutputAttrCmd::isUndoable() const
{
	return (mDgModifier!=0);
}


MStatus CopyOutputAttrCmd::undoIt()
{
	if (mDgModifier!=0)
		return mDgModifier->undoIt();

	return MStatus::kFailure;
}

MStatus CopyOutputAttrCmd::redoIt()
{
	if (mDgModifier!=0)
		return mDgModifier->doIt();

	return MStatus::kFailure;
}

bool CopyOutputAttrCmd::hasSyntax() const
{
	return true;
}


//
// Parse MArgList for command
//
MStatus CopyOutputAttrCmd::ParseArgs(const MArgList& inArgs)
{
	MStatus status;

	// Parse arguments
	MArgDatabase parser(syntax(), inArgs, &status);
	if (status != MS::kSuccess)
	{
		// arguments could not be parsed!
		return status;
	}

	// Help flags
	if (parser.isFlagSet(GET_SYNTAX_SHORTNAME(Help)))
		return MS::kFailure;

	// Src attr name
	if (MFAIL(parser.getCommandArgument(0, mSrcOutputAttrName)))
		return MS::kFailure;

	// Dst attr name
	if (MFAIL(parser.getCommandArgument(1, mDstOutputAttrName)))
		return MS::kFailure;

	return MS::kSuccess;
}

//
// Do command
//
MStatus CopyOutputAttrCmd::doIt(const MArgList& inArgs)
{
	// Parse command arguments
	MStatus status = ParseArgs(inArgs);
	if (MFAIL(status))
	{
		MGlobal::displayInfo(sSyntaxText);
		return status;
	}

	MString src_node_name, src_attr_name;
	MayaUtils::gSplitFullAttributeName(mSrcOutputAttrName, src_node_name, src_attr_name);

	MString dst_node_name, dst_attr_name;
	MayaUtils::gSplitFullAttributeName(mDstOutputAttrName, dst_node_name, dst_attr_name);

	MObject src_obj = MayaUtils::gNodeFromName(src_node_name);
	MObject dst_obj = MayaUtils::gNodeFromName(dst_node_name);

	if (src_obj.isNull())
	{
		displayWarning("Source node not found.");
		return MS::kFailure;
	}

	if (dst_obj.isNull())
	{
		displayWarning("Destination node not found.");
		return MS::kFailure;
	}

	if (mDgModifier==NULL)
		mDgModifier = new MDGModifier;

	status = MayaUtils::gMoveOutputPlugConnections(src_obj,src_attr_name, dst_obj,dst_attr_name, mDgModifier);
	if (MFAIL(status))
		return status;

	mDgModifier->doIt();

	return MS::kSuccess;
}





