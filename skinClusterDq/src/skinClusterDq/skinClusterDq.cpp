// skinClusterDq.cpp - Copyright (C) Guido de Haan 2005-2007. All rights reserved.
#include "skinClusterDq.h"

#include <string.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <math.h>

#include <maya/MPxDeformerNode.h>
#include <maya/MPxLocatorNode.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <common/IncludeMFnPluginClass.h>
#include <maya/MFnDependencyNode.h>

#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MNodeMessage.h>
#include <maya/MCommandMessage.h>

#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
//#include <maya/MUIntArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlugArray.h>
#include <maya/MItGeometry.h>
#include <maya/MPoint.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MTransformationMatrix.h>

#include <common/node_ids.h>
#include "../MayaUtils/MayaUtils.h"
#include "../MayaUtils/MDualQuaternion.h"
#include "../version.h"

// Needed iostream
using std::cout;
using std::cerr;
using std::endl;



//#define MAYA_SKINCLUSTERDQ_NAME		"skinClusterDq"



//---------------------------------------------------------------------------------------------------------------------
// Misc helpers
//---------------------------------------------------------------------------------------------------------------------

//
// Get logical number of elements for <ioArrayDataHandle>
// NB: MArrayDataHandle::elementCount() returns the number
//     of physical (i.e. valid logical) elements
//
static uint sGetLogicalCount(MArrayDataHandle& ioArrayDataHandle)
{
	uint total_logical_count = ioArrayDataHandle.elementCount();
	uint logical_count = 0;

	// Also do safety count
	for (uint i=0, logic_index_found_count=0; logic_index_found_count!=total_logical_count && i<=65535; ++i)
	{
		if (MFAIL(ioArrayDataHandle.jumpToElement(i)))
			continue;

		logic_index_found_count++;

		if (logical_count<=i)
			logical_count = i+1;
	}

	return logical_count;
}


//
// IndexedWeight
//
struct IndexedWeight
{
						IndexedWeight(uint inIndex, float inWeight) : mIndex(inIndex), mWeight(inWeight)	{ }
	uint				mIndex;
	float				mWeight;
};
typedef const IndexedWeight&		rcIndexedWeight;
typedef IndexedWeight&				rIndexedWeight;
typedef std::vector<IndexedWeight>	aIndexedWeight;

//
// For sorting IndexedWeight by most important weight first
//
struct IndexedWeightSortPredicate
{
	bool				operator()(rcIndexedWeight inLHS, rcIndexedWeight inRHS) const
	{
		return inLHS.mWeight>inRHS.mWeight;
	}
};

// Keystroke saver
inline float gGetFloatPlugValue(const MPlug& inPlug)
{
	float x;
	inPlug.getValue(x);
	return x;
}



//
// Find element <inElementToFind> in <inBegin>..<inEnd> data.
// Data is expected to be ordered according to <inCompare predicate.
// Returns iterator to the element when found. Returns end iterator otherwise.
//
template <typename taRANDOM_ACCESS_ITER, typename taTYPE, typename taCOMPAREPREDICATE>
taRANDOM_ACCESS_ITER gSortedFind(taRANDOM_ACCESS_ITER inBegin, taRANDOM_ACCESS_ITER inEnd, const taTYPE& inElementToFind, const taCOMPAREPREDICATE& inCompare)
{
	int n=inEnd-inBegin;

	// For small arrays, do linear search
	if (n<=8)
	{
		while (inBegin!=inEnd)
		{
			if (*inBegin==inElementToFind)
				return inBegin;
			++inBegin;
		}
	}
	else
	{
		inBegin = std::lower_bound(inBegin, inEnd, inElementToFind, inCompare);
		if (inBegin!=inEnd && !inCompare(inElementToFind, *inBegin))
			return inBegin;
	}

	return inEnd;
}

//
// Test if <inElementToFind> exists in <inBegin>..<inEnd> data.
// See gSortedFind() for conditions
//
template <typename taRANDOM_ACCESS_ITER, typename taTYPE, typename taCOMPAREPREDICATE>
bool gSortedSearch(taRANDOM_ACCESS_ITER inBegin, taRANDOM_ACCESS_ITER inEnd, const taTYPE& inElementToFind, const taCOMPAREPREDICATE& inCompare)
{
	return gSortedFind(inBegin, inEnd, inElementToFind, inCompare)!=inEnd;
}



//---------------------------------------------------------------------------------------------------------------------
// SkinClusterDq
//---------------------------------------------------------------------------------------------------------------------

//MTypeId     SkinClusterDq::id(0x001086FE);		// Use Guerrilla ID
VersionHelper   SkinClusterDq::m_version_helper;
MObject		SkinClusterDq::attrGeomMatrix;
MObject		SkinClusterDq::attrWorldMatrixList;
MObject		SkinClusterDq::attrBindPreMatrix;
MObject		SkinClusterDq::attrDropoff;
MObject		SkinClusterDq::attrSmoothness;
MObject		SkinClusterDq::attrLockWeights;
MObject		SkinClusterDq::attrMaintainMaxInfluences;
MObject		SkinClusterDq::attrMaxInfluences;
MObject		SkinClusterDq::attrBindMethod;
MObject		SkinClusterDq::attrPaintWeights;
MObject		SkinClusterDq::attrPaintTrans;
MObject		SkinClusterDq::attrPaintArrDirty;
MObject		SkinClusterDq::attrNormalizeWeights;
MObject		SkinClusterDq::attrDeformUserNormals;
MObject		SkinClusterDq::attrSkinningMode;
MObject		SkinClusterDq::attrOrigSkinCluster;

void* SkinClusterDq::sCreateNode()
{
	return new SkinClusterDq();
}

MStatus SkinClusterDq::sInitialize()
{
	MStatus status;

	//
	// Mimic SkinCluster attributes
	//

	MFnMatrixAttribute  fn_worldmatrix_attr;
	attrWorldMatrixList=fn_worldmatrix_attr.create("matrix", "ma");
	fn_worldmatrix_attr.setArray(true);
	fn_worldmatrix_attr.setReadable(false);
	fn_worldmatrix_attr.setKeyable(false);
	fn_worldmatrix_attr.setConnectable(true);

	MFnMatrixAttribute  fn_geom_attr;
	attrGeomMatrix=fn_geom_attr.create("geomMatrix", "gm");
	fn_geom_attr.setReadable(false);

	MFnMatrixAttribute  fn_bindprematrix_attr;
	attrBindPreMatrix=fn_bindprematrix_attr.create( "bindPreMatrix", "pm");
	fn_bindprematrix_attr.setArray(true);
	fn_bindprematrix_attr.setReadable(false);
	fn_bindprematrix_attr.setKeyable(false);
	fn_bindprematrix_attr.setConnectable(true);

	MFnNumericAttribute fn_dbl_attr;
	attrDropoff = fn_dbl_attr.create("dropoff", "dpf", MFnNumericData::kDouble, 4.0);
	fn_dbl_attr.setArray(true);
	fn_dbl_attr.setKeyable(false);
	fn_dbl_attr.setReadable(false);
	fn_dbl_attr.setConnectable(true);

	attrSmoothness = fn_dbl_attr.create("smoothness", "smt", MFnNumericData::kDouble, 0.0);
	fn_dbl_attr.setArray(true);
	fn_dbl_attr.setKeyable(false);
	fn_dbl_attr.setReadable(false);
	fn_dbl_attr.setConnectable(true);

	MFnNumericAttribute fn_bool_array_attr;
	attrLockWeights = fn_bool_array_attr.create("lockWeights", "lw", MFnNumericData::kBoolean, 0.0);
	fn_bool_array_attr.setArray(true);
	fn_bool_array_attr.setReadable(false);
	fn_bool_array_attr.setKeyable(false);
	fn_bool_array_attr.setConnectable(true);

	MFnNumericAttribute fn_bool_attr;
	attrMaintainMaxInfluences = fn_bool_attr.create("maintainMaxInfluences", "mmi", MFnNumericData::kBoolean, 0.0);
	fn_bool_attr.setKeyable(true);
	fn_bool_attr.setReadable(false);
	fn_bool_attr.setConnectable(true);

	MFnNumericAttribute fn_int_attr;
	attrMaxInfluences = fn_int_attr.create("maxInfluences", "mi", MFnNumericData::kInt, 2.0);
	fn_int_attr.setKeyable(false);
	fn_int_attr.setReadable(false);
	fn_int_attr.setConnectable(true);

	attrBindMethod = fn_int_attr.create("bindMethod", "bm", MFnNumericData::kInt, 1.0);
	fn_int_attr.setKeyable(false);
	fn_int_attr.setReadable(false);
	fn_int_attr.setConnectable(true);

	// Make sure the <kDoubleArray> attribute has a default value. Behaves weird otherwise.
	MDoubleArray		default_double_array;
	MFnDoubleArrayData	default_double_array_data;
	MObject				default_double_array_attr;
	default_double_array_attr = default_double_array_data.create(default_double_array);

	MFnTypedAttribute fn_paintweights_attr;
	attrPaintWeights = fn_paintweights_attr.create("paintWeights", "ptw", MFnDoubleArrayData::kDoubleArray);
	fn_paintweights_attr.setReadable(false);
	fn_paintweights_attr.setKeyable(false);
	fn_paintweights_attr.setConnectable(true);
	fn_paintweights_attr.setDefault(default_double_array_attr);

	MFnMessageAttribute fn_msg_attr;
	attrPaintTrans		= fn_msg_attr.create("paintTrans", "ptt");
	fn_msg_attr.setReadable(false);
	attrPaintArrDirty	= fn_msg_attr.create("paintArrDirty", "pad");
	fn_msg_attr.setReadable(false);

	attrDeformUserNormals = fn_bool_attr.create("deformUserNormals", "dun", MFnNumericData::kBoolean, 1.0);
	fn_bool_attr.setKeyable(true);
	fn_bool_attr.setReadable(false);
	fn_bool_attr.setConnectable(true);

	attrNormalizeWeights = fn_bool_attr.create("normalizeWeights", "nw", MFnNumericData::kBoolean, 1.0);
	fn_bool_attr.setKeyable(true);
	fn_bool_attr.setReadable(false);
	fn_bool_attr.setConnectable(true);



	//
	// Non-skinCluster attributes
	//

	// Then inside of the initialize function for your plugin:
	MFnEnumAttribute fn_skinmodeenum_attr;
	attrSkinningMode = fn_skinmodeenum_attr.create("skinningMode", "skm", SKINNING_MODE_DLB);
	fn_skinmodeenum_attr.setKeyable(false);
	fn_skinmodeenum_attr.addField("LBS",		SKINNING_MODE_LINEAR);
	fn_skinmodeenum_attr.addField("DQLB",		SKINNING_MODE_DLB);
	fn_skinmodeenum_attr.addField("DQIB",		SKINNING_MODE_DIB);

	attrOrigSkinCluster	= fn_msg_attr.create("origSkinCluster", "osc");

	// Add attributes
	addAttribute(attrBindPreMatrix);
	addAttribute(attrGeomMatrix);
	addAttribute(attrWorldMatrixList);
	addAttribute(attrDropoff);
	addAttribute(attrSmoothness);
	addAttribute(attrLockWeights);
	addAttribute(attrMaintainMaxInfluences);
	addAttribute(attrMaxInfluences);
	addAttribute(attrBindMethod);
	addAttribute(attrPaintWeights);
	addAttribute(attrPaintTrans);
	addAttribute(attrPaintArrDirty);
	addAttribute(attrNormalizeWeights);
	addAttribute(attrDeformUserNormals);
	addAttribute(attrSkinningMode);
	addAttribute(attrOrigSkinCluster);

	// Setup attribute dependencies
	attributeAffects(SkinClusterDq::attrBindPreMatrix,		SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrGeomMatrix,			SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrWorldMatrixList,	SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrLockWeights,		SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrPaintWeights,		SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrPaintTrans,			SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrPaintArrDirty,		SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrDeformUserNormals,	SkinClusterDq::outputGeom);
	attributeAffects(SkinClusterDq::attrSkinningMode,		SkinClusterDq::outputGeom);

	m_version_helper.initialize();

	return MStatus::kSuccess;
}

SkinClusterDq::SkinClusterDq() : mWeightListCallbackId(0)
{
}

SkinClusterDq::~SkinClusterDq()
{
	RemoveCallbacks();
}

void SkinClusterDq::postConstructor()
{
	MPxDeformerNode::postConstructor();

	MObject this_mobj		= thisMObject();
	mWeightListCallbackId	= MNodeMessage::addAttributeChangedCallback(this_mobj, sAttrChangedCallback, this);

	m_version_helper.postConstructor(thisMObject(), PLUGIN_VERSION_CODE);
}

MString SkinClusterDq::sTypeName()
{
    return NodeTypeName_SkinClusterDq;
}

MTypeId SkinClusterDq::sTypeId()
{
    return NodeID_SkinClusterDq;
}

MPxNode::Type SkinClusterDq::sType()
{
    return MPxNode::kDeformerNode;
}

void SkinClusterDq::RemoveCallbacks()
{
	if (mWeightListCallbackId!=0)
	{
		MNodeMessage::removeCallback(mWeightListCallbackId);
		mWeightListCallbackId = 0;
	}
}



//
// shouldSave
//
MStatus SkinClusterDq::shouldSave(const MPlug& inPlug, bool& outResult)
{
	// IMPORTANT! We must save all "weights" attributes as otherwise default weights will
	// be lost. A weights array is considered default if it has a single weight that is 1.0.
	//
	// For example, a vertex V is single-weighted to influence matrix #5. This means the
	// attribute "weightList[V].weights[5]" is 1.0 and any other index should be 0.0.
	// This is a sparse array and Maya seems to treat this as "default", hence it won't
	// be saved. So after loading your scene, vertex V will not know its weight was actually
	// bound to influence object #5.
	//
	// To fix this, we save each "weights" attribute
	//
	if (inPlug==MPxDeformerNode::weights)
	{
		outResult = true;
		return MS::kSuccess;
	}

	return MPxDeformerNode::shouldSave(inPlug, outResult);
}



//
// Find logical index of influence object <inInfluenceObjectToFind> to the "matrix" list attribute
//
MStatus SkinClusterDq::FindInfluenceObjectIndex(const MObject& inInfluenceObjectToFind, uint& outLogicalIndex) const
{
	MObject this_mobj = thisMObject();
	MPlug influence_list(this_mobj, SkinClusterDq::attrWorldMatrixList);	// Get "matrix" attribute

	MPlugArray connections;
	for (uint i=0, n=influence_list.numElements(); i<n; ++i)
	{
		MPlug influence_plug = influence_list[i];
		influence_plug.connectedTo(connections, true, false);
		if (connections.length()==1 && connections[0].node()==inInfluenceObjectToFind)
		{
			outLogicalIndex = influence_plug.logicalIndex();
			return MS::kSuccess;
		}
	}

	return MS::kFailure;
}



//
// Get current "paintWeights" values as MDoubleArray
//
void SkinClusterDq::GetPaintWeights(MDoubleArray& outWeights) const
{
	// Get "paintWeights" plug
	MObject this_mobj = thisMObject();
	MPlug paint_weights_plug(this_mobj, SkinClusterDq::attrPaintWeights);

	// Get array data object
	MStatus status;
	MObject double_array_obj;
	status = paint_weights_plug.getValue(double_array_obj);
	CHECK_MSTATUS(status);

	// Update "paintWeights" values. Needs to be done via MFnDoubleArrayData
	MFnDoubleArrayData double_array_data(double_array_obj, &status);
	CHECK_MSTATUS(status);
	outWeights = double_array_data.array(&status);
	CHECK_MSTATUS(status);
}



//
// Get list of weights for a given influence object <inInfluenceObjectIndex>
// List is returned in <outWeights>.
//
void SkinClusterDq::GetInfluenceObjectWeights(uint inInfluenceObjectIndex, MDoubleArray& outWeights) const
{
	MStatus status;

	MObject this_mobj = thisMObject();
	MPlug weight_list(this_mobj, MPxDeformerNode::weightList);

	// Clear output. Cannot reserve :(
	outWeights.setLength(0);

	uint weightlists_count = weight_list.numElements();
	for (uint w=0; w<weightlists_count; ++w)
	{
		MPlug vtx_weights_cmpd = weight_list.elementByPhysicalIndex(w, &status);
		if (MFAIL(status))
			continue;

		MPlug vtx_weights_list = vtx_weights_cmpd.child(MPxDeformerNode::weights, &status);
		if (MFAIL(status))
			continue;

		// Find weight for logical index <inInfluenceObjectIndex>.

		// We could be doing 3 things here:
		// 1) Call vtx_weights_list.getExistingArrayAttributeIndices() and check
		//    whether <inInfluenceObjectIndex> is in the list. BUT THIS DOES NOT SEEM TO WORK!
		// 2) Use the magic MPlug::selectAncestorLogicalIndex() function, which seems
		//    most efficient, but doesn't work as querying unknown logical indices will
		//    introduce the logical index and return a default value.
		// 3) Loop through all physical elements of <vtx_weights_list> and check
		//    the logical index of the element plug to be <inInfluenceObjectIndex>.
		//    This works and looks like:
		float weight = 0.0f;
		for (uint w=0, nw=vtx_weights_list.numElements(); w<nw; ++w)
		{
			MPlug weight_value_plug = vtx_weights_list.elementByPhysicalIndex(w);
			if (weight_value_plug.logicalIndex()==inInfluenceObjectIndex)
			{
				weight_value_plug.getValue(weight);
				break;
			}
		}

		// Don't append 0.0 weights
		if (weight==0.0f)
			continue;

		// Clear unused logical entries and make room for logical element index
		uint logical_index = vtx_weights_cmpd.logicalIndex();
		for (uint out_weights_size = outWeights.length(); out_weights_size<=logical_index; ++out_weights_size)
			outWeights.append(0.0);

		outWeights[logical_index] = (double)weight;
	}
}




//
// Apply constraints to weigths in <ioWeightListPlug> "weightList[x].weights" attr.
//
// When <inNormalize> is set, non-locked weights are normalized.
// When <inMaxInfluences> is >0, the number of non-zero weights remains to at most this amount.
//
// Weights are left untouched when the index of its influence object is in <inLockedInfluenceList>
//
// NB: <inLockedInfluenceList> must be sorted for faster lookups.
//
void SkinClusterDq::ConstrainWeightList(const aUInt& inLockedInfluenceList, MPlug& ioWeightListPlug, bool inNormalize, int inMaxInfluences) const
{
	bool check_maxinfluences = (inMaxInfluences>0);

	// Weight info
	float total_weight			= 0.0f;
	float total_locked_weight	= 0.0f;
	int nonzero_weight_count	= 0;
	aIndexedWeight free_weights;

	// Reserve weight info space
	uint infl_count = ioWeightListPlug.numElements();
	free_weights.reserve(infl_count);

	// Determine weight info
	// NB: Using ioWeightListPlug.getExistingArrayAttributeIndices() here does NOT seem to work!
	for (uint infl_idx=0; infl_idx<infl_count; ++infl_idx)
	{
		MPlug weight_value_plug = ioWeightListPlug.elementByPhysicalIndex(infl_idx);

		float weight;
		weight_value_plug.getValue(weight);
		total_weight += weight;

		if (weight>0.0f)
			nonzero_weight_count++;

		uint logical_weight_idx = weight_value_plug.logicalIndex();
		bool is_free_weight = !gSortedSearch(inLockedInfluenceList.begin(), inLockedInfluenceList.end(), logical_weight_idx, std::less<uint>());

		if (is_free_weight)
			free_weights.push_back(IndexedWeight(logical_weight_idx, weight));
		else
			total_locked_weight+=weight;
	}

	// If we have no free weights, we cannot apply constraints
	const uint free_weight_count = (uint)free_weights.size();
	if (free_weight_count==0)
		return;

	// Maintain max influences if necessary. Zeros out least important weights
	// until at most <inMaxInfluences> influences remain
	const bool fix_max_influences = (check_maxinfluences && nonzero_weight_count>inMaxInfluences);
	if (fix_max_influences)
	{
		sort(free_weights.begin(), free_weights.end(), IndexedWeightSortPredicate());

		// Determine number of influences to remove (# exceeding <inMaxInfluences>)
		uint influences_to_remove_count = nonzero_weight_count-inMaxInfluences;
		if (influences_to_remove_count>free_weight_count)
			influences_to_remove_count = free_weight_count;

		// Zero-out least important free weights
		for (uint i=free_weight_count-influences_to_remove_count; i<free_weight_count; ++i)
		{
			MPlug free_weight_plug = ioWeightListPlug.elementByLogicalIndex(free_weights[i].mIndex);
			free_weight_plug.setValue(0.0f);

			// Keep <total_weight> valid
			total_weight-=free_weights[i].mWeight;
			free_weights[i].mWeight = 0.0f;
		}
	}

	// Normalize weights? If not, we're done
	if (!inNormalize)
		return;

	const float weight_tolerance = 0.001f;

	// Need to normalize this weight list?
	if (!(total_weight<(1.0f-weight_tolerance) || total_weight>(1.0f+weight_tolerance)))
		return;

	// Normalize all free weights
	const float total_free_weight = total_weight-total_locked_weight;

	float free_rescale = 1.0f;
	if (total_weight<1.0f)
	{
		// If total weight of all other weights is almost 0.0, promote first free influence weight
		// (i.e. not in <inLockedInfluenceList>) to most important weight.
 		if (total_free_weight<weight_tolerance)
		{
			MPlug free_weight_plug = ioWeightListPlug.elementByLogicalIndex(free_weights[0].mIndex);
			float free_weight = gGetFloatPlugValue(free_weight_plug);
			free_weight_plug.setValue(1.0f-(total_weight-free_weight));
			return;
		}
		else
		{
			assert(total_locked_weight<1.0f);
			free_rescale	= (1.0f-total_locked_weight)/(total_free_weight);
		}
	}
	else // total_weight>1.0f
	{
		// Total weight exceeds 1.0. If locked weights are already >= 1.0, set all others to 0.0
		if (total_locked_weight>=1.0f)
		{
			free_rescale	= 0.0f;
		}
		else
		{
			free_rescale	= (1.0f-total_locked_weight)/(total_free_weight);
		}
	}

	// If rescale value was set, rescale free weights
	if (free_rescale != 1.0f)
	{
		for (uint i=0; i<free_weight_count; ++i)
		{
			MPlug free_weight_plug = ioWeightListPlug.elementByLogicalIndex(free_weights[i].mIndex);
			float free_weight = gGetFloatPlugValue(free_weight_plug);
			free_weight_plug.setValue(free_weight*free_rescale);
		}
	}
}



//
// Set list of weights for a given influence object <inInfluenceObjectIndex>
// Weights are in <inWeights>
//
void SkinClusterDq::SetInfluenceObjectWeights(uint inInfluenceObjectIndex, const MDoubleArray& inWeights) const
{
	MStatus status;

	MObject this_mobj = thisMObject();
	MPlug weight_list(this_mobj, MPxDeformerNode::weightList);

	// Get normalize weigths flag
	bool normalize_weights;
	MPlug normalize_weights_plug(this_mobj, SkinClusterDq::attrNormalizeWeights);
	if (MFAIL(normalize_weights_plug.getValue(normalize_weights)))
		normalize_weights = true;

	// Get maintain max influences flag
	bool maintain_maxinfluences;
	MPlug maintain_maxinfluences_plug(this_mobj, SkinClusterDq::attrMaintainMaxInfluences);
	if (MFAIL(maintain_maxinfluences_plug.getValue(maintain_maxinfluences)))
		maintain_maxinfluences = false;

	// Get number of max influences to maintain
	int max_influences = 0;
	if (maintain_maxinfluences)
	{
		MPlug max_influences_plug(this_mobj, SkinClusterDq::attrMaxInfluences);
		if (MFAIL(max_influences_plug.getValue(max_influences)))
			max_influences = 2;
	}


	// Keep track which influence objects are weight-locked
	bool queried_weight_locks = false;
	aUInt influence_weight_locks;

	for (uint w=0, nw=inWeights.length(); w<nw; ++w)
	{
		MPlug vtx_weights_cmpd = weight_list.elementByLogicalIndex(w, &status);
		if (MFAIL(status))
			continue;

		MPlug vtx_weights_list = vtx_weights_cmpd.child(MPxDeformerNode::weights, &status);
		if (MFAIL(status))
			continue;

		float weight = (float)inWeights[w];

		// When normalizing weights, it doesn't make much sense to exceed [0..1] range
		if (normalize_weights)
		{
			if (weight>1.0f)		weight = 1.0f;
			else if (weight<0.0f)	weight = 0.0f;
		}

		// Set new weight
		MPlug vtx_weight_plug = vtx_weights_list.elementByLogicalIndex(inInfluenceObjectIndex);
		float old_weight;
		vtx_weight_plug.getValue(old_weight);

		// If no change at all, no update needed
		if (old_weight==weight)
			continue;

		// Update painted weight
		vtx_weight_plug.setValue(weight);

		// See if we have to constrain the weightlist
		if (!normalize_weights && !maintain_maxinfluences)
			continue;

		// Get sorted list of locked weights we cannot touch during weight limiting
		if (!queried_weight_locks)
		{
			bool added_influence_object_idx = false;

			// Append influence indices for weights locked by "lockWeights" array attribute
			MPlug locked_weights_plug(this_mobj, SkinClusterDq::attrLockWeights);
			for (uint l=0, nl=locked_weights_plug.numElements(); l<nl; ++l)
			{
				MPlug locked_weight_plug	= locked_weights_plug.elementByPhysicalIndex(l);
				uint logical_influence_idx	= locked_weight_plug.logicalIndex();

				// Always append <inInfluenceObjectIndex> which is implicitly locked as it is currently being painted
				if (!added_influence_object_idx && (logical_influence_idx>=inInfluenceObjectIndex))
				{
					influence_weight_locks.push_back(inInfluenceObjectIndex);
					added_influence_object_idx = true;
				}

				// For all influences other than <inInfluenceObjectIndex>, check its lockWeights[] attr
				if (logical_influence_idx!=inInfluenceObjectIndex)
				{
					bool is_locked;
					if ((MS::kSuccess==locked_weight_plug.getValue(is_locked) && is_locked))
						influence_weight_locks.push_back(logical_influence_idx);
				}
			}

			// If <inInfluenceObjectIndex> not in list, append it now
			if (!added_influence_object_idx)
				influence_weight_locks.push_back(inInfluenceObjectIndex);

			queried_weight_locks = true;
		}

		// Constrain weight list
		ConstrainWeightList(influence_weight_locks, vtx_weights_list, normalize_weights, max_influences);
	}
}



//
// Dispatch to <this>
//
void SkinClusterDq::sAttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug, void* inUserPtr)
{
	SkinClusterDq* self = (SkinClusterDq*)inUserPtr;
	self->AttrChangedCallback(inMsg, ioPlug, ioOtherPlug);
}

//
// Called when some attribute changed for our node.
// When weight painting, this assures the weights are copied from "paintWeights"
// to the current influence object's weights. The current influence object
// is the object connected to the "paintTrans" plug.
//
void SkinClusterDq::AttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug)
{
	if (inMsg & MNodeMessage::kAttributeSet)
	{
		if (ioPlug==SkinClusterDq::attrPaintWeights)
		{
			// Get "paintTrans" plug
			MObject this_mobj = thisMObject();
			MPlug paint_trans_plug(this_mobj, SkinClusterDq::attrPaintTrans);

			// Painting influence objects
			MPlugArray connections;
			paint_trans_plug.connectedTo(connections, true, false);

			uint logical_influence_index;
			if (connections.length()==1 && MS::kSuccess==FindInfluenceObjectIndex(connections[0].node(), logical_influence_index))
			{
				MDoubleArray paint_weights;
				GetPaintWeights(paint_weights);
				SetInfluenceObjectWeights(logical_influence_index, paint_weights);
			}
		}
	}
}



//
// connectionMade
//
MStatus SkinClusterDq::connectionMade(const MPlug& inPlug, const MPlug& inOtherPlug, bool inAsSrc)
{
	MStatus result = MPxDeformerNode::connectionMade(inPlug, inOtherPlug, inAsSrc);

	// See if "paintTrans" attribute was (re)connected. If so, update "paintWeights"
	if (!inAsSrc && inPlug==SkinClusterDq::attrPaintTrans)
	{
		// Find out which influence objects was activated for weight painting
		uint logical_influence_index;
		if (MS::kSuccess==FindInfluenceObjectIndex(inOtherPlug.node(), logical_influence_index))
		{
			// Get set of influence weights for connected influence objects
			MDoubleArray influence_weights;
			GetInfluenceObjectWeights(logical_influence_index, influence_weights);

			// Update "paintWeights" values. Needs to be done via MFnDoubleArrayData
			MStatus status;
			MFnDoubleArrayData double_array_data;
			MObject double_array_obj = double_array_data.create(influence_weights, &status);
			CHECK_MSTATUS(status);

			// Get "paintWeights" plug and update it
			MObject this_mobj = thisMObject();
			MPlug paint_weights_plug(this_mobj, SkinClusterDq::attrPaintWeights);
			status = paint_weights_plug.setValue(double_array_obj);
			CHECK_MSTATUS(status);
		}
	}

	return result;
}



//
// Arguments:
//   ioDataBlock   : the datablock of the node
//	 inGeomIter    : an iterator for the geometry to be deformed
//   inWorldMatrix : matrix to transform the point into world space
//	 inMultiIndex  : the index of the geometry that we are deforming
//
MStatus SkinClusterDq::deform(MDataBlock& ioDataBlock, MItGeometry& inGeomIter, const MMatrix& inWorldMatrix, unsigned int inMultiIndex)
{
	MStatus status;

	MDataHandle envData = ioDataBlock.inputValue(MPxDeformerNode::envelope, &status);
	if (MFAIL(status)) return status;

	MArrayDataHandle worldMatrixData = ioDataBlock.inputArrayValue(attrWorldMatrixList, &status);
	if (MFAIL(status)) return status;

	MDataHandle geomData = ioDataBlock.inputValue(attrGeomMatrix, &status);
	if (MFAIL(status)) return status;

	MArrayDataHandle preBindMatrixData = ioDataBlock.inputArrayValue(attrBindPreMatrix, &status);
	if (MFAIL(status)) return status;

	MArrayDataHandle weightListData = ioDataBlock.inputArrayValue(MPxDeformerNode::weightList, &status) ;
	if (MFAIL(status)) return status;

	MDataHandle deformUserNormalsData = ioDataBlock.inputValue(attrDeformUserNormals, &status);
	if (MFAIL(status)) return status;

	MDataHandle skinning_mode_data = ioDataBlock.inputValue(attrSkinningMode, &status) ;
	if (MFAIL(status)) return status;

	// Get data
	const float env						= envData.asFloat();
	const bool use_envelope				= (fabs(env - 1.0) > 1e-6f); // do not use envelope if it is at default
	const ESkinningMode skinning_mode	= (ESkinningMode)skinning_mode_data.asShort();	// Get enums as a SHORT
	const MMatrix geom_mat				= geomData.asMatrix();
	const bool deform_normals			= deformUserNormalsData.asBool();
	//const uint joint_matrix_count		= worldMatrixData.elementCount();

	// Find out how many logical deformation matrices
	// NB: <worldMatrixData> and <preBindMatrixData> arrays are sparse
	uint logical_deformer_count = sGetLogicalCount(worldMatrixData);

	// Prepare vertex deformation matrices
	aMMatrix vertex_deform_matrices;
	vertex_deform_matrices.reserve(logical_deformer_count);

	// Prepare vertex deformation dual quaternions
	aMDualQuaternion vertex_deform_dualquats;
	if (skinning_mode!=SKINNING_MODE_LINEAR)
		vertex_deform_dualquats.reserve(logical_deformer_count);

	// Build vertex deformation matrices
	for (uint i=0; i<logical_deformer_count; ++i)
	{
		// Get joint world matrix
		MMatrix joint_matrix;
		if (MFAIL(worldMatrixData.jumpToElement(i)))
			joint_matrix.setToIdentity();
		else
			joint_matrix = worldMatrixData.inputValue().asMatrix();

		// Get bind matrix
		MMatrix bind_matrix;
		if (MFAIL(preBindMatrixData.jumpToElement(i)))
			bind_matrix.setToIdentity();
		else
			bind_matrix = preBindMatrixData.inputValue().asMatrix();

		// Compose deformation matrix
		MMatrix mat = geom_mat * bind_matrix * joint_matrix;
		vertex_deform_matrices.push_back(mat);

		if (skinning_mode!=SKINNING_MODE_LINEAR)
		{
			MDualQuaternion dq;
			dq = mat;
			vertex_deform_dualquats.push_back(dq);
		}
	}


	const MMatrix inv_geom_mat = geom_mat.inverse();

	// Temp cache for holding <matrix,weight> pairs for deforming a single vertex
	// <deformer_matrices> must have the same size as <deformer_dualquats> and
	// <deformer_weights>.
	//
	// <deformer_weights> holds a weight for each <deformer_matrices> how
	// much it influences the vertex.
	aMMatrix			deformer_matrices;
	aMDualQuaternion	deformer_dualquats;
	aDouble				deformer_weights;

	// When weight is <= <weight_tolerance>, it is ignored
	const float weight_tolerance = 0.0001f;

	for ( ; !inGeomIter.isDone(); inGeomIter.next())
	{
		int vtx_index = inGeomIter.index();

		//
		// Access weights using MDataHandle interface. Using MPxDeformerNode::weightValue() is slow
		// and returns 1.0 for non-existing elements.
		//

		// Jump to weightlist for this vertex
		status = weightListData.jumpToElement(vtx_index);

		// See if vtx has a valid weightlist
		if (MFAIL(status))
			continue;

		MDataHandle vtx_weights_cmpd = weightListData.inputValue(&status) ;
		if (MFAIL(status))
			continue;

		// Now from this compound, get the weights array child...
		MDataHandle vtx_weights_child = vtx_weights_cmpd.child(MPxDeformerNode::weights);

		MArrayDataHandle vtx_weight_list_data(vtx_weights_child, &status);
		if (MFAIL(status))
			continue;

		// Collect non-zero weight deformers for this vertex
		do
		{
			MDataHandle weight_data = vtx_weight_list_data.inputValue(&status) ;
			if (MFAIL(status))
				continue;

			float weight = weight_data.asFloat();
			if (weight<=weight_tolerance)
				continue;

			uint d = vtx_weight_list_data.elementIndex();
			if (d>=logical_deformer_count)
				continue;

			deformer_weights.push_back((double)weight);
			deformer_matrices.push_back(vertex_deform_matrices[d]);
			if (skinning_mode!=SKINNING_MODE_LINEAR)
				deformer_dualquats.push_back(vertex_deform_dualquats[d]);
		} while (vtx_weight_list_data.next()==MS::kSuccess);

		// No deformations? Leave it
		uint num_skin_weights = (uint)deformer_weights.size();
		if (num_skin_weights==0)
			continue;

		if (deform_normals)
		{
			// How on earth are people supposed to modify normals here?
			// MItGeometry only provides easy access to the position, but
			// no other properties.
		}

		const MPoint pt = inGeomIter.position(MSpace::kObject);
		MPoint skinned_pt;

		// If single weighted, just use rigid matrix skinning
		if (num_skin_weights==1)
		{
			skinned_pt = deformer_weights[0] * (pt * deformer_matrices[0]);
		}
		else
		if (skinning_mode==SKINNING_MODE_LINEAR)
		{
			skinned_pt = MPoint::origin;
			for (uint w=0; w<num_skin_weights; ++w)
			{
				MPoint dpt = pt * deformer_matrices[w];
				skinned_pt += deformer_weights[w]*dpt;
			}
		}
		else if (skinning_mode==SKINNING_MODE_DLB)
		{
			// Determine weighted scaling/shearing matrix
			MMatrix total_weighted_mm;
			for (uint w=0; w<num_skin_weights; ++w)
			{
				MMatrix weigted_mm = deformer_weights[w]*MTransformationMatrix(deformer_matrices[w]).asScaleMatrix();
				total_weighted_mm = (w==0) ? weigted_mm : (total_weighted_mm+weigted_mm);
			}
			// Apply scaling part
			skinned_pt = pt * total_weighted_mm;

			MDualQuaternion total_dq = MDualQuaternion::sDLB(num_skin_weights, &deformer_weights.front(), &deformer_dualquats.front());
			MMatrix transform(total_dq.asMatrixFromUnit()); skinned_pt = skinned_pt * transform; //skinned_pt = total_dq.transformFromUnit(skinned_pt);

			//MDualQuaternion total_dq_inv = total_dq.inverse();
			//skinned_pt = total_dq_inv.transformFromUnit(skinned_pt);//skinned_pt = total_dq_inv.transform(skinned_pt);
		}
		else if (skinning_mode==SKINNING_MODE_DIB)
		{
			// Determine weighted scaling/shearing matrix
			MMatrix total_weighted_mm;
			for (uint w=0; w<num_skin_weights; ++w)
			{
				MMatrix weigted_mm = deformer_weights[w]*MTransformationMatrix(deformer_matrices[w]).asScaleMatrix();
				total_weighted_mm = (w==0) ? weigted_mm : (total_weighted_mm+weigted_mm);
			}
			// Apply scaling part
			skinned_pt = pt * total_weighted_mm;

			MDualQuaternion total_dq = MDualQuaternion::sDIB(num_skin_weights, &deformer_weights.front(), &deformer_dualquats.front(), 0.01f);
			skinned_pt = total_dq.transformFromUnit(skinned_pt);
		}

		// Clear deformer info cache
		deformer_matrices.resize(0);
		deformer_dualquats.resize(0);
		deformer_weights.resize(0);

		if (use_envelope)
		{
			MVector delta_vec = (skinned_pt - pt*geom_mat);
			skinned_pt = pt*geom_mat + delta_vec*env;
		}

		skinned_pt *= inv_geom_mat;
		skinned_pt.w = pt.w;
		inGeomIter.setPosition(skinned_pt);
	}
	return status;
}

MStatus SkinClusterDq::registerNode(MFnPlugin &plugin)
{
	MStatus status;
	status = plugin.registerNode(SkinClusterDq::sTypeName(),
								 SkinClusterDq::sTypeId(),
								 SkinClusterDq::sCreateNode,
								 SkinClusterDq::sInitialize,
								 SkinClusterDq::sType());
	return status;
}

MStatus SkinClusterDq::deregisterNode(MFnPlugin &plugin)
{
	MStatus status;
	status = plugin.deregisterNode(SkinClusterDq::sTypeId());
    return status;
}
