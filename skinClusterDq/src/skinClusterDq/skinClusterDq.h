#pragma once

#include <vector>

#include <maya/MPxDeformerNode.h>
#include <maya/MPxLocatorNode.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPlugin.h>
#include <maya/MFnDependencyNode.h>

#include <maya/MTypeId.h>
//#include <maya/MPlug.h>
#include <maya/MNodeMessage.h>
#include <maya/MCommandMessage.h>

#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlugArray.h>
#include <maya/MItGeometry.h>

#include <maya/MPoint.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MTransformationMatrix.h>

#include <common/version_helper.h>
#include "../MayaUtils/MDualQuaternion.h"

typedef unsigned int						uint;

typedef std::vector<MDualQuaternion>		aMDualQuaternion;
typedef std::vector<MMatrix>				aMMatrix;
typedef std::vector<uint>					aUInt;
typedef std::vector<float>					aFloat;
typedef std::vector<double>					aDouble;
//
// Maya "skinClusterDq" node
//
class SkinClusterDq : public MPxDeformerNode
{
public:
	static  void*		sCreateNode();
	static  MStatus		sInitialize();

						SkinClusterDq();
						~SkinClusterDq();
	virtual void		postConstructor();
	static MString      sTypeName();
	static MTypeId      sTypeId();
	static MPxNode::Type    sType();
	static  MStatus     registerNode(MFnPlugin &plugin);
	static  MStatus     deregisterNode(MFnPlugin &plugin);

	// MPxNode interface
	virtual MStatus		connectionMade(const MPlug& plug, const MPlug& otherPlug, bool asSrc);
	virtual MStatus		shouldSave( const MPlug& plug, bool& result );

	// MPxDeformerNode interface
    virtual MStatus     deform(MDataBlock& ioDataBlock, MItGeometry& inGeomIter, const MMatrix& inWorldMatrix, unsigned int inMultiIndex);

	// Mimic SkinCluster attributes
	static  MObject     attrBindPreMatrix;				// The inclusive matrix inverse of the driving transform at the time of bind
	static  MObject     attrGeomMatrix;					// The inclusive matrix of the geometry path at the time of the bound
	static  MObject     attrWorldMatrixList;			// Driving transforms array world input matrices
	// dropoffRate      DEPRECATED
	static  MObject     attrDropoff;					// Rate of weight value dropoff per influence object (1 for linear, 2 for quadratic, etc. )
	static  MObject     attrSmoothness;					// Controls how smooth the deformation resulting from the a poly mesh influence object will be
	static  MObject     attrLockWeights;				// "lockWeights" attribute. Indicates whether the weights of the corresponding influence are allowed to change
	static  MObject     attrMaintainMaxInfluences;		// will not allow a vertex to be weighted to more then "maxInfluences" transforms, as defined by the maxInfluences attribute
	static  MObject		attrMaxInfluences;				// the maximum number of transforms that can influence a point.
	static  MObject		attrBindMethod;					// Stores the method by which the skinCluster was bound where 1 = ClosestDistance, 2 = Closest Joint
	// driverPoints		(drp) 							// The world space coordinates of the driver influence geometry
	// basePoints		(bsp)							// The world space coordinates of the base influence geometry
	// baseDirty		(bsd)							// Set to dirty when the world space coordinates of one of the base influence geometries change
	static  MObject     attrPaintWeights;				// Temporarily stores the weights for one of the influences
	static  MObject     attrPaintTrans;					// Identifies the influence whose weights are used in the paintWeights attribute
	static  MObject     attrPaintArrDirty;				// Gets marked dirty when the values in paintWeights change
	// useComponents	(uc)							// If set to true then moving the components of the influence objects will affect the skin deformation
	// nurbsSamples		(ns)							// The number of samples to be taken on influence curves/surfaces
	// useComponentsMatrix	(ucm)						// When this attribute is true, we use the new algorithm for component-type influence objects so that the matrix of the influence object is taken into account in addition to the tweaks on any influence components.
	static  MObject     attrNormalizeWeights;			// When set, at any time that the weight of a point is modified, all of the other weights on the point are changed such that the total of all the weights for the point sum to 1.0
	static  MObject     attrDeformUserNormals;			// If set to true then the deformation will affect user normals

	enum ESkinningMode
	{
		SKINNING_MODE_LINEAR = 0,						// Linear blend skinning (what skinCluster node does)
		SKINNING_MODE_DLB,								// Dual Quaternion Linear Blending
		SKINNING_MODE_DIB								// Dual Quaternion Iterative Linear Blending
	};
	static  MObject     attrSkinningMode;				// skinning mode (linear blend skinning, dual quats etc)
	static  MObject     attrOrigSkinCluster;			// original skinCluster node the settings were copied from

	//static  MTypeId		id;

protected:
	static void			sAttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug, void* inUserPtr);
	void				AttrChangedCallback(MNodeMessage::AttributeMessage inMsg, MPlug& ioPlug, MPlug& ioOtherPlug);
	MStatus				FindInfluenceObjectIndex(const MObject& inInfluenceObjectToFind, uint& outIndex) const;
	void				ConstrainWeightList(const aUInt& inLockedInfluenceList, MPlug& ioWeightListPlug, bool inNormalize, int inMaxInfluences) const;
	void				GetInfluenceObjectWeights(uint inInfluenceObjectIndex, MDoubleArray& outWeights) const;
	void				SetInfluenceObjectWeights(uint inInfluenceObjectIndex, const MDoubleArray& inWeights) const;
	void				GetPaintWeights(MDoubleArray& outWeights) const;
	void				RemoveCallbacks();

	MCallbackId			mWeightListCallbackId;

protected:
	static VersionHelper    m_version_helper;
};

