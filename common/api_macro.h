#pragma once

#define MAKE_INPUT(attr)                  \
    CHECK_MSTATUS(attr.setKeyable(true)); \
    CHECK_MSTATUS(attr.setStorable(true));\
    CHECK_MSTATUS(attr.setReadable(true));\
    CHECK_MSTATUS(attr.setWritable(true));

#define MAKE_OUTPUT(attr)                  \
    CHECK_MSTATUS(attr.setKeyable(false)); \
    CHECK_MSTATUS(attr.setStorable(false));\
    CHECK_MSTATUS(attr.setReadable(true)); \
    CHECK_MSTATUS(attr.setWritable(false));
