import os
import sys
import platform
import smtplib
import time
from email.mime.text import MIMEText
import mymagicbox.ipgetter  as ipgetter # https://github.com/phoemur/ipgetter

'''
import mymagicbox.mail as mail
mail.send_mail_wrap2(pluginName)
'''

def send_mail(me, to_list, msg):
    try:  
        server = smtplib.SMTP('localhost')
        server.sendmail(me, to_list, msg.as_string())  
        server.close()  
        return True  
    except Exception, e:  
        print str(e)  
        return False  

def send_mail_wrap(email_content, IP, user):
    to_list  = ['yaoyansibase@aliyun.com']

    msg = MIMEText(email_content)
    msg['Subject'] = IP
    msg['From']    = user+'@xx.com'
    msg['To']      = ";".join(to_list)
    
    send_mail(msg['From'], to_list, msg)

def getdata(message):
    import maya.cmds                    as cmds

    # system
    currentTime       = cmds.about(currentDate=True)+'  '+cmds.about(currentTime=True)    
    timezone          = time.timezone/3600
    hostname          = platform.node()
    user              = os.getlogin()
    OS                = cmds.about(operatingSystemVersion=True)
    IP                = ipgetter.myip()
    # maya
    batch             = cmds.about(batch=True)
    installedVersion  = cmds.about(installedVersion=True)
    languageResources = cmds.about(languageResources=True)
    mayafilepath      = cmds.file(q=True, sceneName=True)
    
    msg = ''
    msg += '\nSystem:\n'
    msg += '             time:     %s\n'%(currentTime)
    msg += '        time zone:     %d\n'%(timezone)
    msg += '        host name:     %s\n'%(hostname)
    msg += '             user:     %s\n'%(user)
    msg += '               OS:     %s\n'%(OS)
    msg += '               IP:     %s\n'%(IP)
    msg += '\nMaya:\n'
    msg += '            batch:     %s\n'%(batch)
    msg += 'installed version:     %s\n'%(installedVersion)
    msg += 'languageResources:     %s\n'%(languageResources)
    msg += '   maya file path:     %s\n'%(mayafilepath)
    msg += '\n\n\n'
    
    msg += 'Other Messages:\n'
    msg += message
    
    return msg, IP, user

def send_mail_wrap2(message):

    email_content, IP, user = getdata(message)
    
    send_mail_wrap(email_content, IP, user)

    
if __name__ == '__main__':  
    send_mail_wrap2(message='No other messages.');

        

