import sys

g_indent     = -1;
g_indentChar = '    ';

'''
usage:

import mymagicbox.mmb_flog	as flog

@flog.trace
def f1():
    ...
'''
def trace(func):
    def wrapper(*args):
        global g_indent; 
        global g_indentChar;

        g_indent += 1;
        print 'mybox|    |', g_indentChar*g_indent, func.__name__, args, 
        print

        ret = func(*args);

        g_indent -= 1;
        return ret;

    return wrapper


def filterSelfParam(function_param_dic):
    '''
    Filter 'self' key from the function_param_dic.
    The function_param_dic should contain function parameter list, and will contain
    'self' if the function is a class member function. I don't need 'self' data, so I filter it out.
    In some cases, @flog.trace can't be used for callback function. That's why I implemete this function.
    
    Usage:
    import inspect                      as isp
    import mymagicbox.mmb_flog          as flog
    import mymagicbox.log               as log
    
    def fool(int_param, str_param):
        log.debug('%s(%s)',  isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )
        ...
        
    class Fool(object):
        def fool(self, int_param, str_param):
            log.debug('%s.%s(%s)', self.__class__.__name__,   isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )
            ...
    '''
    if 'self' in function_param_dic:
        function_param_dic.pop('self')

    return ', '.join(str(x) for x in function_param_dic.values())
