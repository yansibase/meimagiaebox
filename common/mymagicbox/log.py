
gLogLevelFatal = 0
gLogLevelError = 1
gLogLevelWarn  = 2
gLogLevelInfo  = 3
gLogLevelDebug = 4

gLogLevel = gLogLevelDebug

#
def fatal(format, *params):
    global gLogLevel
    global gLogLevelFatal
    
    if gLogLevel >= gLogLevelFatal:
        print("mybox| Fat| "+format % params);

def err(format, *params):
    global gLogLevel
    global gLogLevelError
    
    if gLogLevel >= gLogLevelError:
        print("mybox| Err| "+format % params);

def warn(format, *params):
    global gLogLevel
    global gLogLevelWarn
    
    if gLogLevel >= gLogLevelWarn:
        print("mybox| Wrn| "+format % params);

def info(format, *params):
    global gLogLevel
    global gLogLevelInfo
    
    if gLogLevel >= gLogLevelInfo:
        print("mybox| Inf| "+format % params);

def debug(format, *params):
    global gLogLevel
    global gLogLevelDebug
    
    if gLogLevel >= gLogLevelDebug:
        print("mybox| Dbg| "+format % params);

#
if __name__ == "__main__":
	fatal("msg: %s", "aaa");
	fatal("msg: %s", 123);
	err("msg: %s", "aaa");
	err("msg: %s", 123);
	warn("msg: %s", "aaa");
	warn("msg: %s", 123);
	info("msg: %s", "aaa");
	info("msg: %s", 123);
	debug("msg: %s", "aaa");
	debug("msg: %s", 123);

