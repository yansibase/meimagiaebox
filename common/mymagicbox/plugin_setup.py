import mymagicbox.log               as log
import mymagicbox.mmb_flog          as flog

# callback functions for loading and unloading plugin

@flog.trace
def initializePlugin_beginCB(pluginName):
    log.info('initializePlugin() begin: %s', pluginName)

    pass

@flog.trace
def initializePlugin_endCB(pluginName):
    log.info('initializePlugin() end: %s', pluginName)

    pass

@flog.trace
def uninitializePlugin_beginCB(pluginName):
    log.info('uninitializePlugin() begin: %s', pluginName)

    pass

@flog.trace
def uninitializePlugin_endCB(pluginName):
    log.info('uninitializePlugin() end: %s', pluginName)

    pass

