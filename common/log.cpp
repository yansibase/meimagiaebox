#include "log.h"

#include <maya/MDagPath.h>
#include <maya/MIntArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnMesh.h>
#include <maya/MVectorArray.h>

void printMObjectInfo(const char* msg, MObject mobj)
{
    MStatus status;

    MFnDependencyNode fnDNode(mobj, &status);           //CHECK_MSTATUS(status);//
    LDbg("%s(), fnDNode.name()=%s", msg, fnDNode.name().asChar());

    MFnDagNode fnDagNode(mobj, &status);                //CHECK_MSTATUS(status);//

    MDagPath path2; status = fnDagNode.getPath(path2);  //CHECK_MSTATUS(status);//
    LDbg("%s(), fnDagNode.getPath(&): %s", msg, path2.fullPathName().asChar());

    MDagPath dagpath2(fnDagNode.dagPath(&status));     //CHECK_MSTATUS(status);//
    LDbg("%s(), fnDagNode.dagPath()=%s", msg, dagpath2.fullPathName().asChar());
}
//
void printMeshInfo(const char* msg, MObject mobj)
{
    printMObjectInfo(msg, mobj);

    MStatus status;

    MFnMesh fnMesh(mobj, &status);                  //CHECK_MSTATUS(status);

    LDbg("%s(), fnMesh.fullPathName()=%s", msg, fnMesh.fullPathName().asChar());
    LDbg("%s(), fnMesh.name()=%s", msg, fnMesh.name().asChar());

    MDagPath path; status = fnMesh.getPath(path);   //CHECK_MSTATUS(status);
    LDbg("%s(), fnMesh.getPath(&): %s", msg, path.fullPathName().asChar());

    MDagPath dagpath(fnMesh.dagPath(&status));      //CHECK_MSTATUS(status);
    LDbg("%s(), fnMesh.dagPath()=%s", msg, dagpath.fullPathName().asChar());


}

void print(const char* msg, const MIntArray& array)
{
    std::cout << msg <<", length="<< array.length() <<std::endl;
    for(unsigned int i = 0; i < array.length(); ++i)
    {
        std::cout << array[i] << ", ";
    }
    std::cout << std::endl;
}

void print(const char* msg, const MDoubleArray& array)
{
    std::cout << msg <<", length="<< array.length() <<std::endl;
    for(unsigned int i = 0; i < array.length(); ++i)
    {
        std::cout << array[i] << ", ";
    }
    std::cout << std::endl;
}

void print(const char* msg, const MFloatArray& array)
{
    std::cout << msg <<", length="<< array.length() <<std::endl;
    for(unsigned int i = 0; i < array.length(); ++i)
    {
        std::cout << array[i] << ", ";
    }
    std::cout << std::endl;
}

void print(const char* msg, const MVectorArray& array)
{
    std::cout << msg <<", length="<< array.length() <<std::endl;
    for(unsigned int i = 0; i < array.length(); ++i)
    {
        std::cout << array[i][0]<<","<< array[i][1]<<","<< array[i][2]<< ",  ";
    }
    std::cout << std::endl;
}
