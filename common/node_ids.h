/*
How to request your own Maya NodeIds?

Access http://www.autodesk.com/developmaya and go to 'Register a Maya Developer Node ID Block' section.
You'll find a link there to self-register your ID block.

*/

#pragma once

/************************************************
 *
 *           Node IDs & Names
 *
************************************************/
// 0x00122240:0x0012224F
#define	NodeID_node_template        0x00122240
extern const char* NodeTypeName_NodeTemplate;
#define	NodeID_mystring             0x00122241
#define	NodeID_dynamic_enum         0x00122242
#define	NodeID_tornado_field        0x00122243
#define	NodeID_VecToQuatAcc         0x00122244
#define NodeID_SkinClusterDq        0x00122245
extern const char* NodeTypeName_SkinClusterDq;
#define NodeID_PartialBlendShape    0x00122246
extern const char* NodeTypeName_PartialBlendShape;
#define NodeID_CreateArray          0x00122247
extern const char* NodeTypeName_CreateArray;
#define NodeID_BoundingObject       0x00122248
extern const char* NodeTypeName_BoundingObject;
#define NodeID_AttributeTransfer    0x00122249
extern const char* NodeTypeName_AttributeTransfer;
#define NodeID_Group                0x0012224A
extern const char* NodeTypeName_Group;
#define NodeID_Point                0x0012224B
extern const char* NodeTypeName_Point;
#define NodeID_ArrayDataContainer   0x0012224C
extern const char* NodeTypeName_ArrayDataContainer;
#define NodeID_VertexMapper         0x0012224D
extern const char* NodeTypeName_VertexMapper;

// 0x00122250:0x0012225F
// 0x00122260:0x0012226F
// 0x00122270:0x0012227F
// 0x00122280:0x0012228F
// 0x00122290:0x0012229F
// 0x001222A0:0x001222AF
// 0x001222B0:0x001222BF
// 0x001222C0:0x001222CF
// 0x001222D0:0x001222DF
// 0x001222E0:0x001222EF
// 0x001222F0:0x001222FF
// 0x00122300:0x0012230F
// 0x00122310:0x0012231F
// 0x00122320:0x0012232F
// 0x00122330:0x0012233F

/************************************************
 *
 *           Command Names
 *
************************************************/
extern const char* CmdName_CmdTemplate;
extern const char* CmdName_DualQuaternion;
extern const char* CmdName_ConsoleDebug;









