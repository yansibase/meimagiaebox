#pragma once

#include <stdio.h>
#include <iostream>
#include <iterator>
#include <vector>

#include <maya/MObject.h>

#ifdef _WINDOWS
// define _WINSOCKAPI_ before <windows.h> to solve compile errors on Windows platform(http://stackoverflow.com/questions/1372480/c-redefinition-header-files-winsock2-h)
#define _WINSOCKAPI_
#include <windows.h>
#define _LOG_Prefix(level)      printf("\n%6d|%30s|%s|: ", GetCurrentProcessId(), __FUNCTION__, level);
#else
#include <sys/types.h>
#include <unistd.h>
#include <string>
// __FUNCTION__ doesn't contain class name on Linux platform, so I use __MMB_METHOD_NAME__ to get the <class>::<function> string
inline std::string mmb_methodName(const std::string& prettyFunction)
{
    std::size_t colons = prettyFunction.find("::");
    std::size_t begin = prettyFunction.substr(0,colons).rfind(" ") + 1;
    std::size_t end = prettyFunction.rfind("(") - begin;

    return prettyFunction.substr(begin,end) + "()";
}
#define __MMB_METHOD_NAME__     mmb_methodName(__PRETTY_FUNCTION__).c_str()
#define _LOG_Prefix(level)      printf("\n%6d|%30s|%s|: ", getpid(), __MMB_METHOD_NAME__, level);
#endif

#define LFat           _LOG_Prefix("Fat"); printf
#define LErr           _LOG_Prefix("Err"); printf
#define LWrn           _LOG_Prefix("Wrn"); printf
#define LInf           _LOG_Prefix("Inf"); printf
#define LDbg           _LOG_Prefix("Dbg"); printf

// define osLFAT for the classes which implement the operator<<(os, ...),
// otherwise, use LFAT instead.
#define osLFAT(msg, v) _LOG_Prefix("Fat"); std::cout<< msg; std::cout<< v;
#define osLErr(msg, v) _LOG_Prefix("Err"); std::cout<< msg; std::cout<< v;
#define osLWrn(msg, v) _LOG_Prefix("Wrn"); std::cout<< msg; std::cout<< v;
#define osLInf(msg, v) _LOG_Prefix("Inf"); std::cout<< msg; std::cout<< v;
#define osLDbg(msg, v) _LOG_Prefix("Dbg"); std::cout<< msg; std::cout<< v;

// open console for debug
#ifdef _DEBUG
#	ifdef _WINDOWS
#		define OPEN_CONSOLE_WINDOW_FOR_DEBUG()	{AllocConsole(); freopen("CONOUT$","w+t",stdout);}
#	else//_WINDOWS
#		define OPEN_CONSOLE_WINDOW_FOR_DEBUG()
#	endif
#else//_DEBUG
#	define OPEN_CONSOLE_WINDOW_FOR_DEBUG()
#endif
//
void printMObjectInfo(const char* msg, MObject mobj);
void printMeshInfo(const char* msg, MObject mobj);

class MIntArray;
class MDoubleArray;
class MFloatArray;
class MVectorArray;
void print(const char* msg, const MIntArray& array);
void print(const char* msg, const MDoubleArray& array);
void print(const char* msg, const MFloatArray& array);
void print(const char* msg, const MVectorArray& array);

template<typename T>
void print(std::ostream &os, const char* msg, const std::vector<T>& array)
{
    os<< msg;
    std::copy(array.begin(), array.end(), std::ostream_iterator<T>(os, ","));
    os<< std::endl;
}

