#pragma once

#include <maya/MObject.h>
#include <maya/MString.h>

// this class holds the version data, and can be integreated into another node
class VersionHelper
{
public:
    VersionHelper();
    ~VersionHelper();

    MStatus	initialize();
	void	postConstructor(const MObject& mobjec, const int plugin_version_code/*, const MString& commitID*/);

    // the attribute which holds the version data
    static MObject aVersion;

    // I'm trying to add a string attribute to hold the commit id string,
    // but it always crashes when I save the Maya file.
    // I don't know why! So, I have to give it up.
    //static MObject aCommitID;

protected:
    const MString m_version_long_name;
    const MString m_version_short_name;

    //const MString m_commit_id_long_name;
    //const MString m_commit_id_short_name;

protected:
    VersionHelper(const VersionHelper& o);
    VersionHelper& operator=(const VersionHelper& o);
};
