#include "node_ids.h"

/************************************************
 *
 *           Node IDs & Names
 *
************************************************/
const char* NodeTypeName_NodeTemplate       = "node_template";
const char* NodeTypeName_SkinClusterDq      = "skinClusterDq";
const char* NodeTypeName_PartialBlendShape  = "testDeformer";
const char* NodeTypeName_CreateArray        = "os_createArray";
const char* NodeTypeName_BoundingObject     = "os_boundingObject";
const char* NodeTypeName_AttributeTransfer  = "os_attributeTransfer";
const char* NodeTypeName_Group              = "os_group";
const char* NodeTypeName_Point              = "os_point";
const char* NodeTypeName_ArrayDataContainer = "os_arrayDataContainer";
const char* NodeTypeName_VertexMapper       = "os_vertexMapper";

/************************************************
 *
 *           Command Names
 *
************************************************/
const char* CmdName_CmdTemplate             = "cmd_template";
const char* CmdName_DualQuaternion          = "dualQuaternionCmd";
const char* CmdName_ConsoleDebug            = "console_debug";
