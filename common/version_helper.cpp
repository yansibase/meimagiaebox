#include "version_helper.h"

#include <maya/MFnDependencyNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPlug.h>
#include <maya/MPxNode.h>

MObject  VersionHelper::aVersion;
//MObject  VersionHelper::aCommitID;
//
VersionHelper::VersionHelper():
m_version_long_name("mmbversion"),
m_version_short_name("mmbv")
//m_commit_id_long_name("mmbcommitid"),
//m_commit_id_short_name("mmbcid")
{

}
//
VersionHelper::~VersionHelper()
{

}
//
MStatus	VersionHelper::initialize()
{
    MStatus status;
    MStatus status2;

	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	/*
	By default, attributes are:
        Readable.
        Writable.
        Connectable.
        Storable.
        Cached.
        Not arrays.
        Have indices that matter.
        Do not use an array builder.
        Not keyable.
        Not hidden.
        Not used as colors.
        Not indeterminant.
        Set to disconnect behavior kNothing.
	*/
	aVersion = nAttr.create( m_version_long_name, m_version_short_name, MFnNumericData::kInt, 0, &status );
	CHECK_MSTATUS(status);
	CHECK_MSTATUS(MPxNode::addAttribute(aVersion));

	//MFnStringData commitIDData;
	//aCommitID = tAttr.create( m_commit_id_long_name, m_commit_id_short_name, MFnData::kString, commitIDData.create(&status2), &status );
	//CHECK_MSTATUS(status);
	//CHECK_MSTATUS(status2);
	//CHECK_MSTATUS(MPxNode::addAttribute(aCommitID));

	return MS::kSuccess;
}
//
void VersionHelper::postConstructor(const MObject& mobject, const int plugin_version_code/*, const MString& commitID*/)
{
    MStatus status;
    MFnDependencyNode fnDepNode(mobject, &status);
    CHECK_MSTATUS(status);

    MPlug versionPlug(fnDepNode.findPlug(m_version_long_name, false, &status));
    CHECK_MSTATUS(status);
    CHECK_MSTATUS(versionPlug.setValue(plugin_version_code));

    //MPlug commitIDPlug(fnDepNode.findPlug(m_commit_id_long_name, false, &status));
    //CHECK_MSTATUS(status);
    //CHECK_MSTATUS(commitIDPlug.setValue(commitID));
}
