#!/bin/bash

#crontab -e
#*/10 * * * * sh /backup/lhome/dev/mybox/backup/backup.sh # every 10 min
#0  */1 * * * sh /backup/lhome/dev/mybox/backup/backup.sh # every 1 hour
#1   18 * * * sh /backup/lhome/dev/mybox/backup/backup.sh # 18:01 everyday

echo "this is backup.sh"

#zip -P "yys&yys" -r -9 /backup/backup/home/$FILENAME.zip /job/HOME/yaoys/*.*

echo "############### Backing up files on the system... ###############"
TIMESTR=`date +"%F--%H-%M"`
 
echo "----- Now tar, then zip up all files to be saved -----"
tar pcvzf /backup/backup/lhome/lhome_${TIMESTR}.tar.gz /backup/lhome/.
cp        /backup/backup/lhome/lhome_2015-07-18--21-00.tar.gz /job/PLE/workspace/yaoys/tmp/lhome_${TIMESTR}.tar.gz
cp        /backup/backup/lhome/lhome_2015-08-03--18-01.tar.gz /usr/tmp/lhome_${TIMESTR}.tar.gz

tar pcvzf /backup/backup/home/home_${TIMESTR}.tar.gz   /job/HOME/yaoys/.
cp        /backup/backup/home/home_${TIMESTR}.tar.gz   /job/PLE/workspace/yaoys/tmp
cp        /backup/backup/home/home_${TIMESTR}.tar.gz   /usr/tmp



echo "############### Completed backing up system... ###############"
date
