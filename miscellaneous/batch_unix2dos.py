import os
import sys
import errno
import mimetypes

gModuleName = ''

def log(format, *params):
    global gModuleName
    print(gModuleName+'| '+format % params);


#
def ensure_dir(dirname):
    """
    Ensure that a named directory exists; if it does not, attempt to create it.
    """
    try:
        os.makedirs(dirname)
    except OSError, e:
        if e.errno != errno.EEXIST:
            raise
#
#def is_binary_string(bytes):
#    textchars = bytearray({7,8,9,10,12,13,27} | set(range(0x20, 0x100)) - {0x7f})
#    return bool(bytes.translate(None, textchars))
    
def is_binary(filename):
    """Return true if the given filename is binary.
    @raise EnvironmentError: if the file does not exist or cannot be accessed.
    @attention: found @ http://bytes.com/topic/python/answers/21222-determine-file-type-binary-text on 6/08/2010
    @author: Trent Mick <TrentM@ActiveState.com>
    @author: Jorge Orpinel <jorge@orpinel.com>"""
    fin = open(filename, 'rb')
    try:
        CHUNKSIZE = 1024
        while 1:
            chunk = fin.read(CHUNKSIZE)
            if '\0' in chunk: # found null byte
                return True
            if len(chunk) < CHUNKSIZE:
                break # done
    # A-wooo! Mira, python no necesita el "except:". Achis... Que listo es.
    finally:
        fin.close()

    return False
#
def convertFile(file_path):
    #log('convert %s', file_path)
    os.system('unix2dos --keepdate --oldfile "'+file_path+'"')
#            
def copyRecursively(source_dir, filters=[]):
    '''
    copyRecursively('/a/b/c', '/d/e/f', ['*.h'])
    copyRecursively('/a/b/c', '/d/e/f', ['*.h', '*.py'])
    copyRecursively('/a/b/c', '/d/e/f', ['*'])
    copyRecursively('/a/b/c', '/d/e/f', '*')
    '''
    #log('copyRecursively(%s, %s, %s)', source_dir, ' '.join(filters))
    ensure_dir(source_dir)

    import glob, os, shutil
    files = []
    for filter_ in filters:
        files += glob.iglob(os.path.join(source_dir, filter_))
    #log('files=%s', ' '.join(files)) 

    # convert files
    i = 0
    for f in files:
        if os.path.isfile(f):
            #if is_binary_string(open(f, 'rb').read(1024)):
            if is_binary(f):
                log('skip binary file: %s', f)
            else:
                convertFile(f)
                i=i+1;
    log("%d file(s) converted", i)

    # visit subdirectories
    subdirs = os.listdir(source_dir)
    for d in subdirs:
        if d == '.git':
            log('skip .git directory')
            continue
        if os.path.isdir(source_dir+'/'+d):
            copyRecursively(source_dir+'/'+d, filters)

if '__main__' == __name__:
    #copyRecursively('/backup/lhome/dev/mybox/AbcExport', ['*'])    #copyRecursively('/backup/lhome/dev/mybox/AbcImport', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/automation', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/backup', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/basetest', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/checkmesh', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/cmd_template', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/cmd_template_py', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/common', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/dynamic_enum', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/knife_cut', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/mystring', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/node_template', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/node_template_py', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/opensoup', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/partial_blendshape', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/rename_test0', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/skinClusterDq', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/tornado_field', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/VecToQuatAcc', ['*'])
    #copyRecursively('/backup/lhome/dev/mybox/version_control', ['*'])
    copyRecursively('/backup/lhome/dev/mybox', ['*.h', '*.cpp', '*.py', '*.mel', '*.sh', '*.bat', '*.md', '*.txt'])
    
    
    
    
    
    
