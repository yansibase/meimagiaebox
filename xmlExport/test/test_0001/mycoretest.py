import os
import sys

import logging
logger = logging.getLogger('mycoretest|')


import maya.cmds as cmds
import maya.OpenMaya as OpenMaya

if os.path.isfile(     os.path.dirname(os.path.abspath(__file__)) + '/../../scripts/xmlexport.py'):
    sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../../scripts/');
import xmlexport

def main(mayaFilePath):
    logger.info('this is main()')
    
    #cmds.file(mayaFilePath, force=True, open=True, options="v=0", type="mayaAscii")
    #a= cmds.ls()
    #logger.info(a)
    

    xmlexport.main(mayaFilePath)
    

if __name__ == '__main__':
    main(sys.argv[1])
