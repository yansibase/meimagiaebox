import os
import sys
import unittest
#import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class BasicTest(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(BasicTest, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(BasicTest, self).setUp()
        #self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test_hw.ma')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(BasicTest, self).tearDown()

    def test_basic(self):
        log.debug('---- begin %s.test_basic() ----', self.m_id);
        #super(BasicTest, self).test_animation()

        # method 1:
        #import mycoretest
        #mycoretest.main(os.path.dirname(__file__)+'/scenes/test.ma')
        
        # method 2:
        import maya.mel as mel
        MAYA_LOCATION = mel.eval('getenv "MAYA_LOCATION"')
        os.system(MAYA_LOCATION+'/bin/mayapy '+os.path.dirname(__file__)+'/test-standalone.py')

        log.debug('---- end   %s.test_basic() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(BasicTest, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(BasicTest)
    unittest.TextTestRunner(verbosity=3).run(suite)
