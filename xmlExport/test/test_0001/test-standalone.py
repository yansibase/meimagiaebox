'''
Usage:
/usr/autodesk/maya2014-x64/bin/mayapy /backup/lhome/dev/mybox/xmlExport/test/test_0001/test-standalone.py
'''
import os
import sys
import logging
logger = logging.getLogger('test-standalone|')

def initialize():
    try:
        logger.info('initialization begins')
        import maya.standalone
        maya.standalone.initialize()
        logger.info('initialization ends')

        import mycoretest
        mycoretest.main(os.path.dirname(__file__)+'/scenes/test.ma')
    except:
        logger.critical( "Failed in initialize standalone application" )
        raise
        

if __name__ == '__main__':
    initialize()
