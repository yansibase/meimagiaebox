import os
import sys

import logging
logger = logging.getLogger('xmlexport|')


import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
################################################################################
class OutputBase(object):
    '''
    '''
    m_mayaFilePath   = ''
    m_outputFilePath = ''
    
    def __init__(self, mayaFilePath):
        self.m_mayaFilePath = mayaFilePath
    
    def open(self, outputFilePath):
        self.m_outputFilePath = outputFilePath
        
    def close(self):
        self.m_outputFilePath = ''
    
#-------------------------------------------------------------------------------
class OutputConsole(OutputBase):
    '''
    '''

    def __init__(self, mayaFilePath):
        super(OutputConsole, self).__init__(mayaFilePath)

    def open(self, outputFilePath):
        super(OutputConsole, self).open(outputFilePath)
    
    def close(self):
        super(OutputConsole, self).close()

#-------------------------------------------------------------------------------
class OutputXml(OutputBase):
    '''
    '''
    m_dom       = None
    m_root_node = None
    
    def __init__(self, mayaFilePath):
        super(OutputXml, self).__init__(mayaFilePath)

    def open(self, outputFilePath):
        super(OutputXml, self).open(outputFilePath)
                
        from xml.dom import minidom
        impl = minidom.getDOMImplementation()
        self.m_dom = impl.createDocument(None, None, None)#namespaceURI, qualifiedName, doctype
        
        self.m_root_node = self.m_dom.createElement("root")
        self.m_dom.appendChild( self.m_root_node )

    def close(self):
        f=file(self.m_outputFilePath,'w')
        self.m_dom.writexml(f, addindent='  ', newl='\n', encoding='utf-8')
        f.close()
    
        super(OutputXml, self).close()

#    def add(self, dagpath, type_, xform, visibility):
#        # create xml node
#        item = self.m_dom.createElement('item')
#        self.m_root_node.appendChild(item)
#        
#        # set attributes for this xml node
#        item.setAttribute('dagpath', str(dagpath))
#        item.setAttribute('type', str(type_))
#        item.setAttribute('xform', str(xform))
#        item.setAttribute('visibility', str(visibility))
        
    def add2(self, *args, **kwargs):
        # create xml node
        item = self.m_dom.createElement('item')
        self.m_root_node.appendChild(item)
        
        for key in kwargs:
            if key == 'xform':
                item.setAttribute(key, ' '.join(map(str, kwargs[key])))
            else:
                item.setAttribute(key, str(kwargs[key]))            
        
################################################################################
class BasicTest(object):
    m_mayaFilePath  = ''
    m_output        = None
    
    def __init__(self, mayaFilePath):
        self.m_mayaFilePath = mayaFilePath

    def getWrapFilePath(self):
        return self.m_mayaFilePath+'_wrap.ma'
        
    def createWrapMayaFile(self):
        cmds.file(newFile=True)
        cmds.file(rename=self.getWrapFilePath())
        baseName  = os.path.basename(self.m_mayaFilePath) # e.g. test.ma
        nameSpace = os.path.splitext(baseName)[0]  # e.g. test
        cmds.file(self.m_mayaFilePath, reference=True, gl=True, mergeNamespacesOnClash=False, options='v=0;', namespace=nameSpace)
        cmds.file(force=True, save=True, typ='mayaAscii')

    def export(self, type_):
        if type_=='xml':
            self.m_output = OutputXml(self.getWrapFilePath())
        else:
            self.m_output = OutputConsole(self.getWrapFilePath())
        
        # open xml
        self.m_output.open(self.getWrapFilePath()+'.xml')
        # open maya file
        cmds.file(self.getWrapFilePath(), force=True, open=True, options='v=0', type='mayaAscii')
        
        dagpaths = cmds.ls(long=True)
        
        for dagpath in dagpaths:
            self.exportOneDagPath(dagpath)
            
        # close maya file
        cmds.quit(abort=True)
        # close xml
        self.m_output.close()
        
    def exportOneDagPath(self, dagpath):
        #logger.info('dagpath=%s', dagpath)
        # type
        type_ = cmds.nodeType(dagpath)
        if type_== 'mesh':
            self.exportMesh(dagpath, type_)
        elif type_== 'transform':
            self.exportTransform(dagpath, type_)
        else:
            logger.warning('object type is not supported: %s, \t\t\t\t %s', type_, dagpath)

        
    def exportMesh(self, dagpath, type_):
        # xform
        xform = []
        if 1 == cmds.attributeQuery('matrix', node=dagpath, exists=True):
            xform = cmds.getAttr(dagpath+'.matrix')
        
        # visibility
        visibility=''
        if 1 == cmds.attributeQuery('visibility', node=dagpath, exists=True):
            visibility = cmds.getAttr(dagpath+'.visibility')

        # vrts
        vrts=[]
        if 1 == cmds.attributeQuery('vrts', node=dagpath, exists=True):
            vrts = cmds.getAttr(dagpath+'.vrts[*]')

        # face normal
        f = cmds.polyInfo(dagpath+'.f[*]', faceNormals=True)

        self.m_output.add2(dagpath=dagpath, type_=type_, xform=xform, visibility=visibility, vrts=vrts, f=f )

    def exportTransform(self, dagpath, type_):
        # xform
        xform = []
        if 1 == cmds.attributeQuery('matrix', node=dagpath, exists=True):
            xform = cmds.getAttr(dagpath+'.matrix')
        
        # visibility
        visibility=''
        if 1 == cmds.attributeQuery('visibility', node=dagpath, exists=True):
            visibility = cmds.getAttr(dagpath+'.visibility')
        #
        self.m_output.add2(dagpath=dagpath, type_=type_, xform=xform, visibility=visibility )
        
################################################################################
def main(mayaFilePath):
    logger.info('this is main()')
    
    test = BasicTest(mayaFilePath)
    test.createWrapMayaFile()
    
    test.export('xml')


if __name__ == '__main__':
    main(sys.argv[1])
