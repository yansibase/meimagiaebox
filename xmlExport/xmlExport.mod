+ MAYAVERSION:2014 PLATFORM:linux automation any ~/automation_root/maya2014-x64_mybox_install/automation
PATH +:= bin
XBMLANGPATH +:= icons
MAYA_RENDER_DESC_PATH +:= .
PYTHONPATH +:= scripts
MAYA_PLUG_IN_PATH +:= plug-ins
MAYA_SCRIPT_PATH  +:= scripts

+ MAYAVERSION:2014 PLATFORM:win64 automation any C:/Users/yaoys/automation_root/maya2014-x64_mybox_install/automation
PATH +:= bin
XBMLANGPATH +:= icons
MAYA_RENDER_DESC_PATH +:= .
PYTHONPATH +:= scripts
MAYA_PLUG_IN_PATH +:= plug-ins
MAYA_SCRIPT_PATH  +:= scripts


+ MAYAVERSION:2015 PLATFORM:linux automation any ~/automation_root/maya2015-x64_mybox_install/automation
PATH +:= bin
XBMLANGPATH +:= icons
MAYA_RENDER_DESC_PATH +:= .
PYTHONPATH +:= scripts
MAYA_PLUG_IN_PATH +:= plug-ins
MAYA_SCRIPT_PATH  +:= scripts

+ MAYAVERSION:2015 PLATFORM:win64 automation any C:/Users/yaoys/automation_root/maya2015-x64_mybox_install/automation
PATH +:= bin
XBMLANGPATH +:= icons
MAYA_RENDER_DESC_PATH +:= .
PYTHONPATH +:= scripts
MAYA_PLUG_IN_PATH +:= plug-ins
MAYA_SCRIPT_PATH  +:= scripts



+ MAYAVERSION:2016 PLATFORM:linux automation any ~/automation_root/maya2016_mybox_install/automation
PATH +:= bin
XBMLANGPATH +:= icons
MAYA_RENDER_DESC_PATH +:= .
PYTHONPATH +:= scripts
MAYA_PLUG_IN_PATH +:= plug-ins
MAYA_SCRIPT_PATH  +:= scripts

+ MAYAVERSION:2016 PLATFORM:win64 automation any C:/Users/yaoys/automation_root/maya2016_mybox_install/automation
PATH +:= bin
XBMLANGPATH +:= icons
MAYA_RENDER_DESC_PATH +:= .
PYTHONPATH +:= scripts
MAYA_PLUG_IN_PATH +:= plug-ins
MAYA_SCRIPT_PATH  +:= scripts
