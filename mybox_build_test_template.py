import os
import sys
import subprocess
import time
import datetime
#################################################
def setBasicConfig():
    """
    Setup the configuration for your automation building
    """
    if sys.platform == 'win32':
        os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = 'cmake.exe'
        os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2014-x64'
        os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = 'C:/Program Files/Autodesk/Maya2014'
        os.environ['AUTO_BUILD_GENERATOR']      = 'Visual Studio 10 2010 Win64'
    elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
        os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = '/usr/bin/cmake'
        os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2014-x64'
        os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = '/usr/autodesk/maya2014-x64'
        os.environ['AUTO_BUILD_GENERATOR']      = 'CodeBlocks - Unix Makefiles'
        
    os.environ['AUTO_BUILD_BUILD_TYPES']     = 'Release Debug'
    os.environ['AUTO_BUILD_INSTALL_TYPE']   = 'Release' # we may build many types(Debug, Release), but which one will be installed for Maya?       
#################################################

def __log(format, *params):
    print('auto_build| '+format % params);

def buildtype_list2str(buildtype_list):
    buildtype_str = ' '.join(buildtype_list)
    # print 'buildtype_str=', buildtype_str
    return buildtype_str

def buildtype_str2list(buildtype_str):
    buildtype_list = buildtype_str.split(' ')
    # print 'buildtype_list=', buildtype_list
    return buildtype_list
    
def git_pull(repository_dir):
    __log('---------------------------------------')
    __log('   Git Pull (%s)', repository_dir)
    __log('---------------------------------------')
    
    output = ''
    try:
        # Method 1:
        # I take this command parameters from windows TortoiseGit "Pull" command. It seems that this method is much faster than method 2.
        gitproc = subprocess.Popen(['git', 'pull', '-v', '--no-rebase', '--progress', 'origin'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=repository_dir)
        # Method 2:
    # http://stackoverflow.com/questions/15315573/how-can-i-call-git-pull-from-within-python
        #gitproc = subprocess.Popen(["git", "pull"], stdout=subprocess.PIPE, cwd = repository_dir)
        
        (output, err) = gitproc.communicate()
    except Exception as e:
        raise RuntimeError('make sure git is added to PATH on Windows. Unexpected error: '+e.message)

    return output
    

def git_version(repository_dir):
    __log('---------------------------------------')
    __log('   repository version', )
    __log('---------------------------------------')
    
    output = ''
    try:
        gitproc = subprocess.Popen(['git', 'rev-parse','HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=repository_dir)
        (output, err) = gitproc.communicate()
    except Exception as e:
        raise RuntimeError('make sure git is added to PATH on Windows. Unexpected error: '+e.message)


    repository_version_SHA = output.strip()
    return repository_version_SHA
    
def git_version_num(repository_dir):
    __log('---------------------------------------')
    __log('   repository version num', )
    __log('---------------------------------------')

    output = ''
    try:
        gitproc = subprocess.Popen(['git', 'rev-list','HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=repository_dir)
        (output, err) = gitproc.communicate()
    except Exception as e:
        raise RuntimeError('make sure git is added to PATH on Windows. Unexpected error: '+e.message)

    how_many_lines = output.count('\n') # one line for one commit
    return how_many_lines
    
def get_repository_version():
    current_file_path = os.path.realpath(__file__)
    current_file_path = current_file_path.replace('\\','/')
    current_file_dir  = os.path.dirname(current_file_path)
    
    output = git_pull(current_file_dir)
    __log('%s', output)
    time.sleep(1)
    
    sha = git_version(current_file_dir)
    __log('%s', sha)

    num     = git_version_num(current_file_dir)
    num_str = format(num, '06') # pad zero before the number
    __log('%s', num_str)

    time.sleep(2)    
    return sha, num_str
    
def debug_dir():
    __log("Path at terminal when executing this file")
    __log(os.getcwd() + "\n")

    __log("This file path, relative to os.getcwd()")
    __log(__file__ + "\n")

    __log("This file full path (following symlinks)")
    full_path = os.path.realpath(__file__)
    __log(full_path + "\n")

    __log("This file directory and name")
    path, file = os.path.split(full_path)
    __log(path + ' --> ' + file + "\n")

    __log("This file directory only")
    __log(os.path.dirname(full_path))
 
def build(which_plugins_to_build):
    __log('\n\n\n\n')
    __log('---------------------------------------')
    __log('   Build and Install                   ')
    __log('---------------------------------------')

    #debug_dir()  # debug 
    
    current_file_path = os.path.realpath(__file__)
    current_file_path = current_file_path.replace('\\','/')
    current_file_dir  = os.path.dirname(current_file_path)
    '''
    plugins = []
    if len(which_plugins_to_build) == 0: # build all plugins
        plugins = os.listdir(current_file_dir)
    else:
        plugins = which_plugins_to_build # build the specified plugins
    
    plugins2 = []
    for p in plugins:
        scriptFileFullPath = current_file_dir+'/'+p+'/build/cmake/cmake_setup.py'
        if os.path.isfile(scriptFileFullPath):
            plugins2.append(p)
    
    __log('build plugins: %s', ', '.join(plugins2))
    time.sleep(2) 
    '''
    for p in which_plugins_to_build:
        scriptFileFullPath = current_file_dir+'/'+p+'/build/cmake/cmake_setup.py'
        if os.path.isfile(scriptFileFullPath):
            __log('%s has automation building scripts, build it.', p)
            #execfile(scriptFileFullPath) # it's weird that python complains errors: NameError: global name 'log' is not defined
            
            ret = os.system('python '+scriptFileFullPath)
            __log('ret=%d', ret)
            if ret != 0:
                raise
            
            __log('done.(%s)\n\n\n\n\n', p)
            
    __log('---------------------------------------')
    __log('   Build and Install Ends           ')
    __log('---------------------------------------')

def _test(plugin):
    __log('\n\n\n\n')
    __log('---------------------------------------')
    __log('   Test (%s)                   ', plugin)
    __log('---------------------------------------')

    # use <mybox>/<plugin>/test_cmd.mel, so we don't have to write plugin name
    # into automation_test_plugin.txt anymore
    
    current_file_path = os.path.realpath(__file__)
    current_file_path = current_file_path.replace('\\','/')
    current_file_dir  = os.path.dirname(current_file_path)
    
    MAYA_BASE_DIR  = os.environ['AUTO_BUILD_MAYA_BASE_DIR']
    
    cmd = '"'+MAYA_BASE_DIR+'/bin/maya"'
    cmd+= ' -script "'+current_file_dir+'/'+plugin+'/test_cmd.mel"' 
    __log(cmd)
    import subprocess
    ps = subprocess.Popen(cmd, shell=True); 
    ps.wait();

def test(which_plugins_to_test):
    __log('\n\n\n\n')
    __log('---------------------------------------')
    __log('   Test                                ')
    __log('---------------------------------------')
    #debug_dir()  # debug
    
    # get time stamp for this test
    tt = time.time()
    timestamp = datetime.datetime.fromtimestamp(tt).strftime('%Y-%m-%d %H:%M:%S')
    
    # remove the test log file before the test
    # NOTE: 
    #      keep the log file name synchronous with the log file name in mybox_test_cmd.mel
    testlogfilepath = os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR']+'/___unittest'+os.environ['AUTO_BUILD_MAYA_VERSION']+'.log'
    of = open(testlogfilepath, 'w')
    of.write('Start Test   '+timestamp+'\n\n')
    of.close()
    
    for p in which_plugins_to_test:
        _test(p)
    
    __log("automation test is finished (exit Maya in mybox_test_cmd.mel)");
    
    __log('---------------------------------------')
    __log('   Test Ends                           ')
    __log('---------------------------------------')

def parseArgument():
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-mv', '--mayaversion', 
                        type    = str, 
                        choices = ['2014', '2015', '2016'],
                        required=True,
                        help    = "to build the plugin for which Maya version")
                        
    parser.add_argument('-b', '--build', 
                        action  = 'store_true', 
                        default = False,  
                        help    = "to build the plugin")
                        
    parser.add_argument('-t', '--test', 
                        action  = 'store_true',
                        default = False,   
                        help    = "to test the plugin")
                        
    parser.add_argument('-bc', '--buildconfig',
                        nargs   = '+',
                        default = ['Release', 'Debug'],
                        help    = "specify which configuration to build, Release or Debug. If this parameter is not set, both of Release and Debug version will be built.")
                        
    parser.add_argument('-ic', '--installconfig',
                        nargs   = 1,
                        default = 'Release',  
                        help    = "which build will be installed for Maya, Release or Debug?")

    # which plugins the current directory contains
    plugins = []
    current_file_path = os.path.realpath(__file__)
    current_file_path = current_file_path.replace('\\','/')
    current_file_dir  = os.path.dirname(current_file_path)

    for p in os.listdir(current_file_dir):
        scriptFileFullPath = current_file_dir+'/'+p+'/build/cmake/cmake_setup.py'
        if os.path.isfile(scriptFileFullPath):
            plugins.append(p)

    parser.add_argument('-p', '--plugins', 
                        nargs   = '+',
                        choices = plugins,
                        default = plugins,
                        help    = "which plugin(s) would be processed. The plugin project which contains /build/cmake/cmake_setup.py will be considered.")

    args = parser.parse_args()
    return args

def setBasicConfig(mayaversion, buildconfig, installconfig):
    """
    Setup the configuration for your automation building.
    These settings will be used in mybox/<plugin>/build/cmake/cmake_setup.py
    """
    if mayaversion == '2014':
        if sys.platform == 'win32':
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = 'C:/ProgramFiles/CMake/bin/cmake.exe'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2014-x64'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = 'C:/Program Files/Autodesk/Maya2014'
            os.environ['AUTO_BUILD_GENERATOR']      = 'Visual Studio 10 2010 Win64'
        elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = '/usr/bin/cmake'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2014-x64'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = '/usr/autodesk/maya2014-x64'
            os.environ['AUTO_BUILD_GENERATOR']      = 'CodeBlocks - Unix Makefiles'

    elif mayaversion == '2015':
        if sys.platform == 'win32':
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = 'C:/ProgramFiles/CMake/bin/cmake.exe'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2015-x64'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = 'C:/Program Files/Autodesk/Maya2015'
            os.environ['AUTO_BUILD_GENERATOR']      = 'Visual Studio 10 2010 Win64'
        elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = '/usr/bin/cmake'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2015-x64'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = '/usr/autodesk/maya2015-x64'
            os.environ['AUTO_BUILD_GENERATOR']      = 'CodeBlocks - Unix Makefiles'

    elif mayaversion == '2016':
        if sys.platform == 'win32':
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = 'C:/ProgramFiles/CMake/bin/cmake.exe'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2016'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = 'C:/Program Files/Autodesk/Maya2016'
            os.environ['AUTO_BUILD_GENERATOR']      = 'Visual Studio 10 2010 Win64'
        elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = '/usr/bin/cmake'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2016'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = '/usr/autodesk/maya2016'
            os.environ['AUTO_BUILD_GENERATOR']      = 'CodeBlocks - Unix Makefiles'
    else:
        raise Exception('Only Maya2014/2015/2016 are supported now. You can modify setBasicConfig() and parseArgument() to support other Maya versions')

    os.environ['AUTO_BUILD_BUILD_TYPES']    = buildtype_list2str(buildconfig)
    os.environ['AUTO_BUILD_INSTALL_TYPE']   = installconfig # we may build many types(Debug, Release), but which one will be installed for Maya?
    
    # update code from repository and get some info of the mast/HEAD. These info will be recorded in our plugins
    (sha, numstring) = get_repository_version()
    os.environ['AUTO_BUILD_REPOSITORY_SHA'] = sha       # SHA code of the HEAD
    os.environ['AUTO_BUILD_REPOSITORY_NUM'] = numstring # how many commits in this repository
    
    # NOTE: if you modify AUTO_BUILD_OUTPUT_ROOT_DIR, please update the following files:
    # - <mybox>/automation/docs/readme.txt, section "How to run automation in Maya GUI"
    # - mod file of each plugin
    os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR']= os.path.expanduser('~')+'/automation_root'

def _main(mayaversion, buildconfig, installconfig, doBuild, doTest, plugins):
    setBasicConfig(mayaversion, buildconfig, installconfig)
    if doBuild:
        build(plugins)
    if doTest:
        test(plugins)    
    
def main():
    args = parseArgument()

    # maya version
    mayaversion   = args.mayaversion
    doBuild       = args.build
    doTest        = args.test
    buildconfig   = args.buildconfig
    installconfig = args.installconfig
    plugins       = args.plugins
    
    __log('arguments:')
    __log('args.mayaversion:    %s', args.mayaversion)
    __log('args.build:          %s', args.build)
    __log('args.test:           %s', args.test)
    __log('args.buildconfig:    %s', ', '.join(args.buildconfig))
    __log('args.installconfig:  %s', args.installconfig)
    __log('args.plugins:        %s', ', '.join(args.plugins))

    _main(mayaversion, buildconfig, installconfig, doBuild, doTest, plugins)

        
if '__main__' == __name__:
    main()
