
import maya.cmds as cmds 


ret = cmds.polySphere();
xformToBeSplit = ret[0]; #print xformToBeSplit; # xformToBeSplit = "pSphere1"


ret = cmds.curve(d=3, p=[(8, 0, -2), (4, 0, -1), (-4, 0, 0), (4, 0, 1), (8, 0, 1)], k=[0, 0, 0, 1, 2, 2, 2]);
xform_curve = ret; #print xform_curve; # curve1

cmds.setAttr("perspShape.orthographic", 1)
ret = cmds.polyProjectCurve(xformToBeSplit, xform_curve, ch=True, pointsOnEdges=0, curveSamples=50, automatic=1);
#print ret;# [u'polyProjectionCurve1', u'polyProjectCurve1']
cmds.setAttr("perspShape.orthographic", 0)

ret = cmds.listRelatives('polyProjectionCurve1', type="transform")
projectedCurve = ret; #print ret; # it could have many children

ret = cmds.polySplit(xformToBeSplit, detachEdges=1, pc=projectedCurve);
xformGeneratedAfterSplit = ret[0];#print ret; #[u'pSphere2', u'polySplit1']



ret = cmds.listRelatives(xformGeneratedAfterSplit)
shapeGeneratedAfterSplit = ret[0];# print ret;#[u'pSphereShape2']
  
#In order to get the names of each part explicitly, we have to separate the mesh.
xformAfterSeparation = cmds.polySeparate(shapeGeneratedAfterSplit, ch=False)
#print xformAfterSeparation;#[u'polySurface1', u'polySurface2']

# gather the faces of each part into a new set
for xform_node in xformAfterSeparation:
    cmds.sets( xform_node+'.f[:]', n="set_"+xform_node );
    #cmds.select( 'set0', noExpand=True );


