"""
1. I set PYTHONPATH :+= scripts in mod file, so this userSetup.py is loaded automatically
"""
import os
import sys
import maya.cmds as cmds

modulePath = cmds.getModulePath(moduleName='knife_cut')
modulePath = modulePath.replace('\\','/')

# get fullpath of current py file
this_file_path = modulePath + '/scripts/userSetup.py';
print("this is: "+this_file_path);

# add common module to python path
commonPath = modulePath+'/../common';
sys.path.insert(0, commonPath);

