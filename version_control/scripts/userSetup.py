"""
1. I set PYTHONPATH :+= scripts in mod file, so this userSetup.py is loaded automatically
"""
import os
import sys
import inspect

# get fullpath of current py file
'''
http://stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-the-current-executed-file-in-python

I've used lambda _: None for this for a variety of scripts running on a variety of OSs with various 
versions of Python over the past 2 years and it always worked, but just now when I was trying to condense a 
line that was a bit unwieldy, I tried lambda:0 instead and found it seems to work just as well. I just bring 
this up because this new method is a lot less tested than the old one was, so if you find this doesn't work, 
maybe using lambda _: None instead will work for you, although IDK why that would work but not this one.
 - ArtOfWarfare Mar 26 at 17:42 
'''
this_file_path = os.path.abspath(inspect.getsourcefile(lambda:None))


# add common module to python path if necessary
this_dir = os.path.dirname(this_file_path)
commonPath = this_dir+'/../../common'
commonPath = os.path.abspath(commonPath)

InsertCommonModuleIntoPath = False
if commonPath not in sys.path:
    InsertCommonModuleIntoPath = True
    sys.path.insert(0, commonPath);

# echo messages
import mymagicbox.log  as log
log.debug("this is: "+this_file_path)

if InsertCommonModuleIntoPath:
    log.debug("common module path is inserted into python path: "+commonPath)

"""
2. There are so many ``AETemplates`` directories, so I have to load the modules with the full path.
"""
import imp
imp.load_source('AEtestNodeATemplate', modulePath+'/scripts/AETemplates/AEtestNodeATemplate.py')
imp.load_source('AEtestNodeBTemplate', modulePath+'/scripts/AETemplates/AEtestNodeBTemplate.py')
