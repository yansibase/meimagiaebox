------------------------------------
Automation process
------------------------------------
- run command:
python <mybox_root>/build.py
It envokes <mybox_root>/mybox_build_test_template.py to build mybox plugins for Maya2014/2015/2016

- mybox_build_test_template.py
It has several parameters which are defined in parseArgument()

    -bc/--buildconfig
    Specify which configuration to build, Release or Debug. If this parameter is not set, both of Release and Debug version will be built.

    -ic/--installconfig
    Which build will be installed for Maya, Release or Debug?

    -p/--plugins
    which plugin(s) would be processed. The plugin project which contains /build/cmake/cmake_setup.py 
    will be considered.
    Only process(build or test) specified plugins, e.g. 
    python ./mybox_build_test_template.py -mv 2014 -b -t -p AbcExport

    - For now this automation script only supports Maya2014/2015/2016.
    you can support other maya versions by modify setBasicConfig(),
    and choices = ['2014', '2015', '2016'] in parseArgument()


   - Setup the configuration for your automation building.(in setBasicConfig())
     These settings will be used in mybox/<plugin>/build/cmake/cmake_setup.py
   - update code from repository and get some info of the mast/HEAD. (in get_repository_version())
     These info will be recorded in our plugins. get_repository_version()
   - for each plugin:   ( _main() in mybox_build_test_template.py)
   
         - build and install:   (see build() in mybox_build_test_template.py)
             call python mybox/<plugin>/build/cmake/cmake_setup.py':
             (see doOneBuildType() in cmake_setup.py)
             '-------------------------------------------------------------------'
             'generating cmake scripts ...(Release or Debug)'
             '-------------------------------------------------------------------'
             This will create the following directories:
             ~/XX_XXXX_mayaXX_mybox_Release_build/<plugin>/
             ~/XX_XXXX_mayaXX_mybox_Debug_build/<plugin>/
             These directories contains cmake scripts and codeblocks solutions.
             
             '-------------------------------------------------------------------'
             'building ...(Release or Debug)'
             '-------------------------------------------------------------------'
             Run the build scripts at the following directories:
             ~/XX_XXXX_mayaXX_mybox_Release_build/<plugin>/
             ~/XX_XXXX_mayaXX_mybox_Debug_build/<plugin>/
             and the *.so will be generated at the following directories:
             ~/XX_XXXX_mayaXX_mybox_Release_build/<plugin>/src/*.so
             ~/XX_XXXX_mayaXX_mybox_Debug_build/<plugin>/src/*.so
             
             '-------------------------------------------------------------------'
             ''build-install ...(Release or Debug)'
             '-------------------------------------------------------------------'             
             cmake will install the project to 
             ~/XX_XXXX_mayaXX_mybox_Release_install/<plugin>/
             ~/XX_XXXX_mayaXX_mybox_Debug_install/<plugin>/
             
             
             - (see main() in cmake_setup.py)
             tryToResetDir(INSTALL_DIR_FOR_MAYA)
             copyRecursively(BUILD_INSTALL_DIR, INSTALL_DIR_FOR_MAYA, ['*'])
             This code will copy 
             ~/XX_XXXX_mayaXX_mybox_Release_install/<plugin>/
             or 
             ~/XX_XXXX_mayaXX_mybox_Debug_install/<plugin>/
             to 
             ~/mayaXX_mybox_install/<plugin>/
             
             
             
             For example, if you run 'python ./mybox_build_test_template.py -mv 2014 -b -t -p AbcExport',
             The following directories would be created:
                ~/000781_60428eead7dd3351f4f7987023df8fc0061bb22f_maya2014-x64_mybox_Debug_build/
                ~/000781_60428eead7dd3351f4f7987023df8fc0061bb22f_maya2014-x64_mybox_Debug_install/
                ~/000781_60428eead7dd3351f4f7987023df8fc0061bb22f_maya2014-x64_mybox_Release_build/
                ~/000781_60428eead7dd3351f4f7987023df8fc0061bb22f_maya2014-x64_mybox_Release_install/
                
                                                  000781: There are 781 commits in this repository
                60428eead7dd3351f4f7987023df8fc0061bb22f: SHA code for master/HEAD
                                       XXXXX_Debug_build: this direcotory contains the cmake scripts and codeblocks project files
                                     XXXXX_Debug_install: this direcotory contains the files which are built from XXXXX_Debug_build
             
             
         
         
         - test:      ( test() in mybox_build_test_template.py)
            - write the plugin name into ~/automation_test_plugin.txt
            - launch maya with parameter '-script "mybox/mybox_test_cmd.mel"'
            - mybox_test_cmd.mel
               - import unittest
               - set test root dir:
                  unittest.defaultTestLoader.discover(start_dir=...)
               - run unittest:
                  unittest.TextTestRunner()
               - test log is output to ~/___unittestmayaXXXX.log
               - exit maya
            - 测试产生的中间文件在~/automation_tmp/
                测试log为～/___unittestmaya2014-x64.log，每个testcase如果通过了，会记录为OK
               
               



------------------------------------
 How to run automation in Maya GUI
------------------------------------
os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR'] = os.path.expanduser('~')+'/automation_root'
import maya.cmds
maya.cmds.loadPlugin('automation')

import unittest
reload(unittest)

# which testcase do you want to test?
suite = unittest.defaultTestLoader.discover(start_dir='/backup/lhome/dev/mybox/AbcExport/test/test_0001')

# This will test all the testcases under directory /backup/lhome/dev/mybox/AbcExport/test
#suite = unittest.defaultTestLoader.discover(start_dir='/backup/lhome/dev/mybox/AbcExport/test')


testlog = open(os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR']+'/___unittestmaya2014-x64.log', 'w')
unittest.TextTestRunner(stream=testlog, verbosity=3).run(suite)
testlog.close()



------------------------------------
python unittest
------------------------------------
After you set test root dir:
    unittest.defaultTestLoader.discover(start_dir=...)
only the sub-directory which contains __init__.py will be visited.

That's why I add the following files:
    mybox/<plugin>/__init__.py
    mybox/<plugin>/test/__init__.py


------------------------------------
mod文件
------------------------------------
- mybox使用mod文件告诉maya需要加载插件的路径
mybox的mod文件模板:/ibrix3/PLE/workspace/yaoys/dev/mybox/modules
可以拷贝到本地~/workspace/maya，

在~/.bashrc里设置MAYA_MODULE_PATH,比如
export MAYA_MODULE_PATH="/ibrix3/PLE/workspace/yaoys/dev/mybox/modules"



在windows上, 由于maya无法解析mod文件里的~字符,所以需要替换mod文件里的路径为你机器上的有效路径，比如
假设你的用户名是tbb的话,需要替换路径
C:/Users/yaoys/maya2014-x64_mybox_install/AbcExport为C:/Users/tbb/maya2014-x64_mybox_install/AbcExport

需要注意:
用编辑器打开mod文件并修改后，可能会生成mod～文件或其他文件。(无论在linux还是windows平台)一定要删除这些多余的文件，
否则maya会读这些文件从而刷掉mod文件里的设置




--------------------------
如何在mybox里添加一个plugin
--------------------------
- mybox里有如下几个插件, 以供快速开发:
    cmd_template        : command插件模板(c++)
    cmd_template_py     : command插件模板(python)
    node_template       : node插件模板(c++)
    node_template_py    : node插件模板(python)
    如果想在mybox里新添加一个插件,可以从这几个模板里选择一个. 下面以node_template为例, 介绍如何在在mybox里添加一个plugin

- 假设要创建的新的插件名为mynewnode0. 拷贝mybox/node_template到mybox/mynewnode0

- mybox/node_template.mod:
    重命名 mybox/node_template.mod  --> mybox/mynewnode0.mod
    打开mynewnode0.mod, 替换所有的"node_template"字符串为"mynewnode0"
  
- mybox/CMakeLists.txt
    替换所有的"node_template"字符串为"mynewnode0"

- mybox/mynewnode0/scripts/*.*
  - rename /AETemplates/AEnode_templateTemplate.py  to /AETemplates/AEmynewnode0Template.py,
  
    open AEmynewnode0Template.py, and replace all "node_template"  with "mynewnode0". e.g.
    replace AEnode_templateTemplate with AEmynewnode0Template.
    
    buildBody() 里定义了node上个属性, 需要替换为mynewnode0上自己添加的属性
  
  - userSetup.py
    replace all "node_template"  with "mynewnode0". 
    
  - node_templateSetup.py
    rename node_templateSetup.py to mynewnode0Setup.py
    open mynewnode0Setup.py, and replace all "node_template"  with "mynewnode0".
    
- mybox/mynewnode0/src/*.*
  - rename node_template.h   to mynewnode0.h
  - rename node_template.cpp to mynewnode0.cpp
  - 修改CMakeLists.txt
    replace all "node_template"  with "mynewnode0"
    
    如果文件路径不正确, 下面步骤里可能无法生成codeblocks工程文件
        
    当开发过程中添加新的.h和.cpp文件时, 需要把文件路径添加到HDRS和SRCS里.
    SCRIPTS里的文件虽然不会被codeblocks编译; 但添加进来后,可以在codeblocks里打开这些文件, 便于编译.
    

- 编译mynewnode0, 以生成codeblocks工程文件
    python <mybox_root>/mybox_build_test_template.py -mv 2014 -b -p mynewnode0

    会生成~/XXX_XXX_maya2014-x64_mybox_Release_build/mynewnode0/mynewnode0.cbp, 同时编译出错(因为要修改.cpp和.h文件)
    XXX需要根据编译时具体而定, 会在终端里有相应的信息

- 打开上步骤里的mynewnode0.cbp
- 替换mynewnode0.h文件里的"node_template" 为 "mynewnode0".
- 在node_ids.h添加node id和node name:
    注意:我为basefx申请了256个node id:0x00122240 ~ 0x0012233F
        可仿照node_ids.h里node id的规则, 递增地使用.
        例如, 在我写这个文档时, 最后一个使用的id是
        #define NodeID_ArrayDataContainer   0x0012224C
        那么, 我现在可以使用的id为0x0012224D,
        所以我为mynewnode0设置的id为0x0012224D,如下:

        #define NodeID_mynewnode0   0x0012224D
        extern const char* NodeTypeName_mynewnode0;

- 在node_ids.cpp添加node name:
    const char* NodeTypeName_mynewnode0         = "mynewnode0";
- 打开mynewnode0.cpp
    确认下面两个函数返回node name和node id:
    MString mynewnode0::cTypeName()
    {
	    return NodeTypeName_mynewnode0;
    }

    MTypeId mynewnode0::cTypeId()
    {
	    return NodeID_mynewnode0;
    }

- 打开plugin.cpp, 替换"node_template" 为 "mynewnode0".
- 在codeblocks里编译mynewnode0, 直到没有编译链接错误

- 编译mynewnode0
    python <mybox_root>/mybox_build_test_template.py -mv 2014 -b -p mynewnode0
    会生成如下目录:
    ~/XXX_XXX_maya2014-x64_mybox_Release_build/mynewnode0
    ~/XXX_XXX_maya2014-x64_mybox_Release_install/mynewnode0
    ~/XXX_XXX_maya2014-x64_mybox_Debug_build/mynewnode0
    ~/XXX_XXX_maya2014-x64_mybox_Debug_install/mynewnode0
    ~/maya2014-x64_mybox_install/mynewnode0

- 查看环境变量MAYA_MODULE_PATH, (我目前在.bashrc里设置了它)
    拷贝 <mybox_root>/mynewnode0/mynewnode0.mod 到MAYA_MODULE_PATH所指的目录下
    
    需要注意:
    在MAYA_MODULE_PATH所指的目录, 用编辑器打开mod文件并修改后，可能会生成mod～文件或其他文件。(无论在linux还是windows平台)一定要删除这些多余的文件，
    否则maya会读这些文件从而刷掉mod文件里的设置

- 启动maya2014, 加载该插件
- 编译maya其他版本的该插件
python <mybox_root>/mybox_build_test_template.py -mv 2015 -b -p mynewnode0
python <mybox_root>/mybox_build_test_template.py -mv 2016 -b -p mynewnode0

--------------------------
如何针对一个新的maya版本( Maya2017 ? )编译mybox
--------------------------
- 下面以支持maya2013-x64为例, 演示如何扩展automation脚本以支持maya的其他版本
- open <mybox_root>/mybox_build_test_template.py
- parseArgument() 函数里
    parser.add_argument('-mv', '--mayaversion', 
                        type    = str, 
                        choices = ['2014', '2015', '2016'],
                        required=True,
                        help    = "to build the plugin for which Maya version")
                        
  改为:
    parser.add_argument('-mv', '--mayaversion', 
                        type    = str, 
                        choices = ['2013', '2014', '2015', '2016'],
                        required=True,
                        help    = "to build the plugin for which Maya version")
- setBasicConfig()函数里:
  添加:
    if mayaversion == '2013':
        if sys.platform == 'win32':
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = 'C:/ProgramFiles/CMake/bin/cmake.exe'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2013-x64'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = 'C:/Program Files/Autodesk/Maya2013'
            os.environ['AUTO_BUILD_GENERATOR']      = 'Visual Studio 10 2010 Win64'
        elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
            os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = '/usr/bin/cmake'
            os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2013-x64'
            os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = '/usr/autodesk/maya2013-x64'
            os.environ['AUTO_BUILD_GENERATOR']      = 'CodeBlocks - Unix Makefiles'
  elif ...
  
- 编译一个插件来测试编译脚本是否正确:
python <mybox_root>/mybox_build_test_template.py -mv 2013 -b -p VecToQuatAcc

- 在<mybox_root>/build.py里添加
    ret = os.system('python '+this_dir+'/mybox_build_test_template.py -mv 2013 -b -t')
    if ret != 0:
        raise
        
- 更新每个插件的mod文件并更新到环境变量MAYA_MODULE_PATH所指的目录下.
  
  
  

--------------------------
其他
--------------------------
每次修改脚本
修改的都是/ibrix3/PLE/workspace/yaoys/dev/mybox目录下的文件
修改脚本后启动maya测试, 使用的是~/maya2014-x64_mybox_install目录下的文件
所以你会发现,修改了代码, 怎么会没有效果呢?
这时需要运行
python ./mybox_build_test_template.py -mv 2014  -b -p <your plugin name>
把相关文件拷贝到~/maya2014-x64_mybox_install目录下

