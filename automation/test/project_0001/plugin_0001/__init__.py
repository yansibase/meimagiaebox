import sys

def load_tests(loader, standard_tests, pattern):
    '''
    It should return a TestSuite.
    '''
    print('visit: '+__file__)
    sys.stdout.flush()
    # top level directory cached on loader instance
    this_dir = os.path.dirname(__file__)
    '''
    class unittest.TestLoader.discover(start_dir, pattern='test*.py', top_level_dir=None)
        Find all the test modules by recursing into subdirectories from the specified start directory, and return a TestSuite object containing them.
    '''
    #package_tests=('test_0001', 'test_0003')
    
    package_tests = loader.discover(start_dir=this_dir, pattern='test_0001')
    standard_tests.addTests(package_tests)
    return package_tests
