import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()

        self.m_helper.moveRenderedImages();

        isEqual = self.m_helper.compare_image()
        if isEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isEqual, True, "Failed. "+__file__)

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        renderer = rndr.CreateRenderer('mentalRay')
        renderer.render()
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass

class MyTest01(unittest.TestCase):
    m_id = ''

    def __init__(self, methodName='runTest'):
        super(MyTest01, self).__init__(methodName)
        self.m_id = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__

    def setUp(self):
        log.debug('%s.setUp', self.m_id);

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);

    def test_oneframe(self):
        log.debug('---- begin %s.test_oneframe() ----', self.m_id);
        log.debug('---- end   %s.test_oneframe() ----', self.m_id);

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest()', self.m_id);

class MyTest02(unittest.TestCase):
    m_id = ''

    def __init__(self, methodName='runTest'):
        super(MyTest02, self).__init__(methodName)
        self.m_id = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__

    def setUp(self):
        log.debug('%s.setUp', self.m_id);

    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);

    def test_oneframe(self):
        log.debug('---- begin %s.test_oneframe() ----', self.m_id);
        log.debug('---- end   %s.test_oneframe() ----', self.m_id);

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest()', self.m_id);

test_cases = (MyTest00, MyTest02)

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    for test_class in test_cases:
        tests = loader.loadTestsFromTestCase(test_class)
        suite.addTests(tests)
    return suite



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
