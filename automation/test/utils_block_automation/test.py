import os
import sys
import unittest
import mymagicbox.log           as log


#-------------------------------------------------------------------------------
class BlockAutomation(unittest.TestCase):
    m_id        = ''

    def __init__(self, methodName='runTest'):
        super(BlockAutomation, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__

    def test00(self):
        log.debug('---- begin %s.test00() ----', self.m_id);
        #super(MyTest00, self).test_animation()
        
        Usage = '''
        NOTE:
        Sometimes I need to block Maya to get the PID and feed it to IDE for debuging. 
        That is why I use this test case to block Maya on automation stage. '
        If you dont need this blocking, remove the file /automation/test/utils_block_automation/__init__.py
        '''
        
        log.debug('---------------------------')
        log.debug('%s', Usage)
        log.debug('---------------------------')        
        
        DialogMessage = 'Automation test is blocked, press ENTER to continue. (See Maya Script Editor for more details.)'
        data = raw_input(DialogMessage)   # Python 2.x
        #data = input(DialogMessage)   # Python 3
        log.debug('Your input data is: %s', data)

        log.debug('---- end   %s.test00() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(BlockAutomation)
    unittest.TextTestRunner(verbosity=3).run(suite)
