import os
import sys
import maya.cmds                as cmds
import mymagicbox.log           as log
import mymagicbox.plugin_setup  as plugin_setup

# Initialize the script plug-in
def initializePlugin(mobject):
    # callback functions
    plugin_setup.initializePlugin_beginCB('automation')
    
    modulePath = cmds.getModulePath(moduleName='automation')
    modulePath = os.path.abspath(modulePath)
    modulePath = modulePath.replace('\\','/')

    # in order to be able to import automationbase,
    # I add the path of automationbase.py to sys.path
    moduleScriptPath = modulePath+'/plug-ins';
    if moduleScriptPath in sys.path:
        log.info('moduleScriptPath is already in sys.path: '+moduleScriptPath);
    else:
        sys.path.insert(0, moduleScriptPath);

    # in order to use compare.exe
    os_env_path = os.environ['PATH']
    os_env_path = os_env_path.replace('\\','/')
    moduleBinPath = modulePath+'/bin'
    if moduleBinPath in os_env_path:
        log.info('moduleBinPath is already in os.environ[PATH]: '+moduleBinPath);
    else:
        os.environ['PATH'] = ';'+moduleBinPath+';'+os.environ['PATH'];
        
    # callback functions
    plugin_setup.initializePlugin_endCB('automation')
    
    pass

# Uninitialize the script plug-in
def uninitializePlugin(mobject):
    # callback functions
    plugin_setup.uninitializePlugin_beginCB('automation')
    
    # callback functions
    plugin_setup.uninitializePlugin_endCB('automation')
    
    pass
