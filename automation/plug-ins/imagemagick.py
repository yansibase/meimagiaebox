import sys
import os
import hashlib
import glob
import mymagicbox.log           as log


def getDiffImagePath(fileFullPath, diff_image_dir):
    """diff image file has the same directory with diff_image_dir

    >>>getDiffImageName('/home/$user/test_0001/images/tmp/test_f0001.png', '/home/$user/automationtest_tmp/test_0001/images/diff')
    /home/$user/test_0001/images/diff/test_f0001_diff.png
    """
    diff_postfix="_diff";
    pathWithoutExt, dotExt = os.path.splitext(fileFullPath) 
    fileWithoutExt = os.path.basename(pathWithoutExt)
    return (diff_image_dir+'/'+fileWithoutExt+diff_postfix+dotExt)


def getDiffImagePath2(filePath):
    """diff image file has the same directory with filePath

    >>>getDiffImageName2('/home/$user/test_0001/images/tmp/test_f0001.png')
    /home/$user/test_0001/images/tmp/test_f0001_diff.png
    """
    pathWithoutExt, dotExt = os.path.splitext(filePath) #
    return pathWithoutExt+'_diff'+dotExt


def doDiff(refImage, desImage):
    #diffImagePath = getDiffImagePath(desImage, diff_image_dir)
    diffImagePath = getDiffImagePath2(desImage)

    # create diff directory if it doesn't exist
    diffImageDir  = os.path.dirname(diffImagePath)
    if not os.path.exists(diffImageDir):
        log.debug("create dir: %s", diffImageDir)
        os.makedirs(diffImageDir)

    cmd='compare '+refImage+' '+desImage+' '+diffImagePath
    #log.debug("cmd="+cmd+"\n")
    os.system(cmd)


def file_equal(filepath0, filepath1):
    data0 = open(filepath0, 'rb').read()
    data1 = open(filepath1, 'rb').read()

    return (hashlib.md5(data0).hexdigest() == hashlib.md5(data1).hexdigest())==True


def image_equal(refImage, desImage):
    #log.debug("image_equal("+src+", "+des+")\n")
    if os.path.isfile(refImage) == False:
        log.err("refImage not exist: %s", refImage)
        return False
    if os.path.isfile(desImage) == False:
        log.err("desImage not exist: %s", desImage)
        return False

    if file_equal(refImage, desImage)==True:
        #log.debug("two images are equal. %s", src)
        return True
    else:
        log.err("Images are NOT equal: %s", refImage)
        doDiff(refImage, desImage)
        return False

# compare all the files with the reference files even there are some different.
# It means that even there is a file which is different with the reference file, the automation will continue to compare subsequent files.
def image_dir_equal(refdir, desdir, filter_='*.*'):
    #log.debug("image_dir_equal("+refdir+", "+desdir+", "+filter_+")")

    ret = True
    reffiles = glob.glob(os.path.join(refdir, filter_))
    if 0 == len(reffiles):
        log.warn('No ref file in refdir: '+refdir)

    for reffile in reffiles:
        testfile = desdir+'/'+os.path.basename(reffile)
        if image_equal(reffile, testfile):
            pass
        else:
            log.err('Not equal: ref=%s,  %s', reffile, testfile)
            ret = False
            
    # visit subdirectories       
    subdirs = os.listdir(refdir)
    for d in subdirs:
        if os.path.isdir(refdir+'/'+d):
            if image_dir_equal(refdir+'/'+d, desdir+'/'+d, filter_):
                pass
            else:
                log.err('Not equal: refdir=%s,  %s', refdir+'/'+d, desdir+'/'+d)
                ret = False
    return ret
    
def abcecho_dir_equal(refdir, desdir, filter_='*.echo'):
    log.debug("abcecho_dir_equal("+refdir+", "+desdir+", "+filter_+")")

    ret = True
    reffiles = glob.glob(os.path.join(refdir, filter_))
    if 0 == len(reffiles):
        log.warn('No ref file in refdir: '+refdir)

    for reffile in reffiles:
        testfile = desdir+'/'+os.path.basename(reffile)
        if file_equal(reffile, testfile):
            pass
        else:
            log.err('Not equal: ref=%s,  %s', reffile, testfile)
            ret = False

    return ret    


if '__main__' == __name__:
    dir_src = 'D:/dev/mymagicbox/automation/test/project_0001/plugin_0001/test/test_0001/images'
    image_equal(dir_src+'/tmp/test.0001.png', dir_src+'/tmp/test.0002.png',)
    image_dir_equal(dir_src+'/tmp', dir_src+'/tmp', '*.png')
