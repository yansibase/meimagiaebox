import maya.cmds                as cmds
import maya.mel                 as mel
import mymagicbox.log           as log
import at_helper                as hp

class ATRenderer(object):
    """
    Automation Test Renderer base class
    """
    m_renderer_name = ''
    
    def __init__ (self, rndr_name):
        self.m_renderer_name = rndr_name

    def render (self):
        """
        template method
        """
        if self._preRender():
            self._render()
            self._postRender()
        else:
            log.err('Not render.')

    # override the following methods
    def _preRender (self):
        assert(0, "this function should not be called")
        return True

    def _render (self):
        assert(0, "this function should not be called")

    def _postRender (self):
        assert(0, "this function should not be called")

    def _renderCurrentFrame(self):
        assert(0, "this function should not be called")


class ATMayaSoftware(ATRenderer):
    """
    Automation Test Renderer for MayaSoftware
    """
    def __init__ (self):
        super(ATMayaSoftware, self).__init__('mayaSoftware')

    def _preRender (self):
        # switch to renderer
        cmds.setAttr('defaultRenderGlobals.ren', self.m_renderer_name, type='string')
        return True

    def _render (self):
        startFrame = cmds.getAttr("defaultRenderGlobals.startFrame");
        endFrame   = cmds.getAttr("defaultRenderGlobals.endFrame");
        frameStep  = cmds.getAttr("defaultRenderGlobals.byFrameStep");

        for i in range(int(startFrame), int(endFrame)+1, int(frameStep)):
            cmds.currentTime( i, update=True, edit=True )
            self._renderCurrentFrame()
        pass

    def _postRender (self):
        pass

    def _renderCurrentFrame(self):
        mel.eval('mayaBatchRenderProcedure(1,"","","'+self.m_renderer_name+'","")')

class ATMayaHardware(ATRenderer):
    """
    Automation Test Renderer for MayaHardware
    """
    m_width         = 0
    m_height        = 0
    m_camera        = ''
    m_render_layer  = ''
    m_renderProc    = ''

    def __init__ (self):
        super(ATMayaHardware, self).__init__('mayaHardware')
        self.m_width         = 0
        self.m_height        = 0
        self.m_camera        = ''
        self.m_render_layer  = ''
        self.m_renderProc    = ''
        
    def _preRender (self):
        self.m_renderProc = cmds.renderer(self.m_renderer_name, query=True, renderProcedure=True)

        # switch to renderer
        cmds.setAttr('defaultRenderGlobals.ren', self.m_renderer_name, type='string')

        self.m_width        = cmds.getAttr("defaultResolution.width")
        self.m_height       = cmds.getAttr("defaultResolution.height")
        self.m_camera       = 'utcam|utcamShape'
        self.m_render_layer = 'defaultRenderLayer'
        return True

    def _render (self):
        startFrame = cmds.getAttr("defaultRenderGlobals.startFrame");
        endFrame   = cmds.getAttr("defaultRenderGlobals.endFrame");
        frameStep  = cmds.getAttr("defaultRenderGlobals.byFrameStep");

        for i in range(int(startFrame), int(endFrame)+1, int(frameStep)):
            cmds.currentTime( i, update=True, edit=True )
            self._renderCurrentFrame()
        pass

    def _postRender (self):
        pass

    def _renderCurrentFrame(self):
        log.debug('render proc=%s(%d, %d, 1, 1, %s, -layer %s)', self.m_renderProc, self.m_width, self.m_height, self.m_camera, self.m_render_layer)
        mel.eval(self.m_renderProc+'('+str(self.m_width)+', '+str(self.m_height)+', 1, 1, "'+self.m_camera+'", " -layer '+self.m_render_layer+'")')

class ATMayaHardware2(ATRenderer):
    """
    Automation Test Renderer for MayaHardware2
    """
    def __init__ (self):
        super(ATMayaHardware2, self).__init__('mayaHardware2')

    def _preRender (self):
        # switch to renderer
        cmds.setAttr('defaultRenderGlobals.ren', self.m_renderer_name, type='string')
        return True

    def _render (self):
        startFrame = cmds.getAttr("defaultRenderGlobals.startFrame");
        endFrame   = cmds.getAttr("defaultRenderGlobals.endFrame");
        frameStep  = cmds.getAttr("defaultRenderGlobals.byFrameStep");

        for i in range(int(startFrame), int(endFrame)+1, int(frameStep)):
            cmds.currentTime( i, update=True, edit=True )
            self._renderCurrentFrame()
        pass

    def _postRender (self):
        pass

    def _renderCurrentFrame(self):
        mel.eval('mayaBatchRenderProcedure(1,"","","'+self.m_renderer_name+'","")')
        
class ATMentalRay(ATRenderer):
    """
    Automation Test Renderer for MentalRay
    """
    m_width         = 0
    m_height        = 0
    m_camera        = ''
    m_render_layer  = ''

    def __init__ (self):
        super(ATMentalRay, self).__init__('mentalRay')
        self.m_width         = 0
        self.m_height        = 0
        self.m_camera        = ''
        self.m_render_layer  = ''

    def _preRender (self):
        self.__tryToLoadPlugin('Mayatomr')

        # switch to renderer
        cmds.setAttr('defaultRenderGlobals.ren', self.m_renderer_name, type='string')

        self.m_width        = cmds.getAttr("defaultResolution.width")
        self.m_height       = cmds.getAttr("defaultResolution.height")
        self.m_camera       = self.__getRenderableCamera()
        self.m_render_layer = 'defaultRenderLayer'
        return True

    def _render (self):
        startFrame = cmds.getAttr("defaultRenderGlobals.startFrame");
        endFrame   = cmds.getAttr("defaultRenderGlobals.endFrame");
        frameStep  = cmds.getAttr("defaultRenderGlobals.byFrameStep");

        for i in range(int(startFrame), int(endFrame)+1, int(frameStep)):
            cmds.currentTime( i, update=True, edit=True )
            self._renderCurrentFrame()

    def _postRender (self):
        pass

    def _renderCurrentFrame(self):
        mel.eval('mentalrayRender('+str(self.m_width)+', '+str(self.m_height)+', 1, 1, "'+self.m_camera+'", " -layer '+self.m_render_layer+'")')

    def __tryToLoadPlugin (self, plugin):
        hp.ATHelper.tryToLoadPlugin(plugin)

    def __getRenderableCamera (self):
        cameras = cmds.ls(type='camera')
        for cam in cameras: 
            if cmds.getAttr(cam+'.renderable'):
                return cam
        return '<render camera is not found!>'

class ATArnold(ATRenderer):
    """
    Automation Test Renderer for Arnold
    """
    m_width         = 0
    m_height        = 0
    m_camera        = ''
    m_render_layer  = ''
    m_renderProc    = ''

    def __init__ (self):
        super(ATArnold, self).__init__('arnold')
        self.m_width         = 0
        self.m_height        = 0
        self.m_camera        = ''
        self.m_render_layer  = ''
        self.m_renderProc    = ''

    def _preRender (self):
        self.__tryToLoadPlugin('mtoa')

        self.m_renderProc = cmds.renderer('arnold', query=True, renderProcedure=True)

        # switch to renderer
        cmds.setAttr('defaultRenderGlobals.ren', self.m_renderer_name, type='string')

        self.m_width        = cmds.getAttr("defaultResolution.width")
        self.m_height       = cmds.getAttr("defaultResolution.height")
        self.m_camera       = 'utcam|utcamShape'
        self.m_render_layer = 'defaultRenderLayer'
        return True

    def _render (self):
        startFrame = cmds.getAttr("defaultRenderGlobals.startFrame");
        endFrame   = cmds.getAttr("defaultRenderGlobals.endFrame");
        frameStep  = cmds.getAttr("defaultRenderGlobals.byFrameStep");

        for i in range(int(startFrame), int(endFrame)+1, int(frameStep)):
            cmds.currentTime( i, update=True, edit=True )
            self._renderCurrentFrame()

    def _postRender (self):
        pass

    def _renderCurrentFrame(self):
        log.debug('render proc=%s(%d, %d, 1, 1, %s, -layer %s)', self.m_renderProc, self.m_width, self.m_height, self.m_camera, self.m_render_layer)
        mel.eval(self.m_renderProc+'('+str(self.m_width)+', '+str(self.m_height)+', 1, 1, "'+self.m_camera+'", " -layer '+self.m_render_layer+'")')

    def __tryToLoadPlugin (self, plugin):
        hp.ATHelper.tryToLoadPlugin(plugin)

    def __getRenderableCamera (self):
        cameras = cmds.ls(type='camera')
        for cam in cameras: 
            if cmds.getAttr(cam+'.renderable'):
                return cam
        return '<render camera is not found!>'

def CreateRenderer (rendererName):
    """
    The factory method
    """
    renderers = dict(mayaSoftware=ATMayaSoftware, mayaHardware=ATMayaHardware, mayaHardware2=ATMayaHardware2, mentalRay=ATMentalRay, arnold=ATArnold)

    return renderers[rendererName]()


if __name__ == '__main__':
    cmds.file('D:/test.ma', force=True, open=True, options="v=0", type="mayaAscii")
    rnd0 = CreateRenderer('mayaSoftware')
    rnd0.render()
    rnd1 = CreateRenderer('mentalRay')
    rnd1.render()
