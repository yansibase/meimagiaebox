import os
import sys
import hashlib
import maya.cmds                as cmds
import pymel.core               as pm
import imagemagick              as im
import mymagicbox.log           as log

class ATHelper(object):
    m_maya_file_path    = ''
    m_testcase_dir      = ''
    m_maya_project_dir  = ''
    m_automationtesttmp_images_tmp_dir    = ''
    m_automationtesttmp_cache_alembic_dir = ''
    '''
    def setUp(self):
        log.debug('AutomationBase.setUp');

    def tearDown(self):
        log.debug('AutomationBase.tearDown');

    def test_oneframe(self):
        log.debug('---- begin AutomationBase.test_oneframe() ----');
        log.debug('---- end   AutomationBase.test_oneframe() ----');

    def test_animation(self):
        log.debug('---- begin AutomationBase.test_animation() ----');
        log.debug('---- end   AutomationBase.test_animation() ----');

    def runTest(self):
        log.debug('\nthis is AutomationBase.runTest');
    '''
    def openMayaFile(self, mayaFilePath):
        log.debug('openMayaFile(%s)', mayaFilePath)

        log.warn('self.m_testcase_dir is deduced from mayaFilePath and will be used in self.compare_image(), so make sure maya file is located at scenes/ dir, e.g. $(testcase_dir)/scenes/*.ma')
        self.m_maya_file_path                      = mayaFilePath
        self.m_testcase_dir                        = os.path.abspath(os.path.dirname(mayaFilePath)+'/..')
        self.m_automationtesttmp_images_tmp_dir    = os.path.abspath(self.mapToAutomationTestTmpDir(self.m_testcase_dir+'/images/tmp'))
        self.m_automationtesttmp_cache_alembic_dir = os.path.abspath(self.mapToAutomationTestTmpDir(self.m_testcase_dir+'/cache/alembic'))
        
        cmds.file(mayaFilePath, force=True, open=True, options="v=0", type="mayaAscii")
        #mel.eval('setProject("'+dir_name+'")') # set project directory
        self.m_maya_project_dir = cmds.workspace( q=True, fullName=True )

        log.debug('-------- DEBUG: Basic Config -------')
        log.debug('m_maya_file_path                      = %s',    self.m_maya_file_path)        
        log.debug('m_testcase_dir                        = %s',    self.m_testcase_dir)
        log.debug('m_automationtesttmp_images_tmp_dir    = %s',    self.m_automationtesttmp_images_tmp_dir)
        log.debug('m_automationtesttmp_cache_alembic_dir = %s',    self.m_automationtesttmp_cache_alembic_dir)        
        log.debug('m_maya_project_dir                    = %s',    self.m_maya_project_dir)
        log.debug('------------------------------------')

    def openMayaFile2(self, mayaFilePath, **kwargs):
        log.debug('openMayaFile2(%s, %s)', mayaFilePath,   ', '.join("%s=%r" % (key,val) for (key,val) in kwargs.iteritems())  )

        log.warn('self.m_testcase_dir is deduced from mayaFilePath and will be used in self.compare_image(), so make sure maya file is located at scenes/ dir, e.g. $(testcase_dir)/scenes/*.ma')
        self.m_maya_file_path                      = mayaFilePath
        self.m_testcase_dir                        = os.path.abspath(os.path.dirname(mayaFilePath)+'/..')
        self.m_automationtesttmp_images_tmp_dir    = os.path.abspath(self.mapToAutomationTestTmpDir(self.m_testcase_dir+'/images/tmp'))
        self.m_automationtesttmp_cache_alembic_dir = os.path.abspath(self.mapToAutomationTestTmpDir(self.m_testcase_dir+'/cache/alembic'))
        
        pm.system.openFile(mayaFilePath, **kwargs);

        self.m_maya_project_dir = cmds.workspace( q=True, fullName=True )

        log.debug('-------- DEBUG: Basic Config -------')
        log.debug('m_maya_file_path                      = %s',    self.m_maya_file_path)        
        log.debug('m_testcase_dir                        = %s',    self.m_testcase_dir)
        log.debug('m_automationtesttmp_images_tmp_dir    = %s',    self.m_automationtesttmp_images_tmp_dir)
        log.debug('m_automationtesttmp_cache_alembic_dir = %s',    self.m_automationtesttmp_cache_alembic_dir)        
        log.debug('m_maya_project_dir                    = %s',    self.m_maya_project_dir)
        log.debug('------------------------------------')

    def mapToAutomationTestTmpDir(self, ref_image_dir):
        """Map the input directory to automationtest_tmp/ directory.

        Examples:
        on windows:
        >>> getDiffImageDir('d:/dev/project_0001/plugin_0001/test_0001/images/tmp')
        C:/Users/$user/Documents/automationtest_tmp/dev/project_0001/plugin_0001/test_0001/images/tmp

        on linux:
        >>> getDiffImageDir('/home/$user/dev/project_0001/plugin_0001/test_0001/images/tmp')
        /home/$user/automationtest_tmp/dev/project_0001/plugin_0001/test_0001/images/tmp

        """
        ret = os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR'] + '/automationtest_tmp'
        if sys.platform == 'win32':
            ret = ret + ref_image_dir[2:] # skip driver label, e.g. c:, d:, e:, and etc
        elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
            ret = ret + ref_image_dir
        return ret

    def moveRenderedImages (self):
        # e.g. move  C:/Users/$user/Documents/maya/projects/default/images/tmp/*.*
        #      to    D:/automationtest_tmp/dev/mymagicbox/automation/test/project_0001/plugin_0001/test/test_0001/images/tmp/*.*
        import shutil
        if os.path.exists(self.m_automationtesttmp_images_tmp_dir):
            log.debug('removing the previous directory: %s', self.m_automationtesttmp_images_tmp_dir);
            try:
                shutil.rmtree(self.m_automationtesttmp_images_tmp_dir)# remove the dir, it means removing /images/tmp.
            except Exception as e:
                log.err('Exception: %d, %s: shutil.rmtree(%s).',
                         e.errno, e.strerror, self.m_automationtesttmp_images_tmp_dir)
                log.err('Maybe you should leave that directory or close files in that directory.');
                raise
            except:
                log.err('Unexcepted error when calling shutil.rmtree(%s)', self.m_automationtesttmp_images_tmp_dir)
                raise

        log.debug('moving output images under automationtest_tmp/ ...')
        maya_project_images_tmp_dir = self.m_maya_project_dir+'/images/tmp' # renderer generates images here
        try:
            shutil.move(maya_project_images_tmp_dir, self.m_automationtesttmp_images_tmp_dir)
        except Exception as e:
            log.err('Exception: %d, %s: shutil.move(%s, %s)',  e.errno, e.strerror, maya_project_images_tmp_dir, self.m_automationtesttmp_images_tmp_dir);
            log.err('Maybe you should leave the source directory or close files in that directory.');
            raise
        except:
            log.err('Unexcepted error: when calling shutil.move(%s, %s)', maya_project_images_tmp_dir, self.m_automationtesttmp_images_tmp_dir)
            raise
        log.debug('done. output images are moved to: %s',  self.m_automationtesttmp_images_tmp_dir);

    def moveCacheAlembic (self):
        # e.g. move  C:/Users/$user/Documents/maya/projects/default/cache/alembic/*.*
        #      to    D:/automationtest_tmp/dev/mymagicbox/automation/test/project_0001/plugin_0001/test/test_0001/cache/alembic/*.*
        import shutil
        if os.path.exists(self.m_automationtesttmp_cache_alembic_dir):
            log.debug('removing the previous directory: %s', self.m_automationtesttmp_cache_alembic_dir);
            try:
                shutil.rmtree(self.m_automationtesttmp_cache_alembic_dir)# remove the dir, it means removing /images/tmp.
            except Exception as e:
                log.err('Exception: %d, %s: shutil.rmtree(%s).',
                         e.errno, e.strerror, self.m_automationtesttmp_cache_alembic_dir)
                log.err('Maybe you should leave that directory or close files in that directory.');
                raise
            except:
                log.err('Unexcepted error when calling shutil.rmtree(%s)', self.m_automationtesttmp_cache_alembic_dir)
                raise

        log.debug('moving output abc cache under automationtest_tmp/ ...')
        maya_project_cache_alembic_dir = self.m_maya_project_dir+'/cache/alembic' # renderer generates images here
        try:
            shutil.move(maya_project_cache_alembic_dir, self.m_automationtesttmp_cache_alembic_dir)
        except Exception as e:
            log.err('Exception: %d, %s: shutil.move(%s, %s)',  e.errno, e.strerror, maya_project_cache_alembic_dir, self.m_automationtesttmp_cache_alembic_dir);
            log.err('Maybe you should leave the source directory or close files in that directory.');
            raise
        except:
            log.err('Unexcepted error: when calling shutil.move(%s, %s)', maya_project_cache_alembic_dir, self.m_automationtesttmp_cache_alembic_dir)
            raise
        log.debug('done. output abc cache are moved to: %s',  self.m_automationtesttmp_cache_alembic_dir);


    def compare_image(self):
        log.debug('comparing images ...');
        testcase_images_ref_dir  = self.m_testcase_dir+'/images/tmp'
        isEqual = im.image_dir_equal(testcase_images_ref_dir, self.m_automationtesttmp_images_tmp_dir)

        return isEqual
        
    def compare_abcecho(self):
        log.debug('comparing abc echo ...');
        testcase_cache_alembic_ref_dir  = self.m_testcase_dir+'/cache/alembic'
        isEqual = im.abcecho_dir_equal(testcase_cache_alembic_ref_dir, self.m_automationtesttmp_cache_alembic_dir, '*.echo')

        return isEqual

    @staticmethod    def tryToLoadPlugin (plugin):
        if not cmds.pluginInfo(plugin, loaded=True, q=True):
            cmds.loadPlugin(plugin, quiet=True)
            log.info('plugin loaded: %s', plugin);
        else:
            log.info('plugin is already loaded, skip it: %s', plugin);
            
    @staticmethod
    def tryToUnloadPlugin (plugin):
        if cmds.pluginInfo(plugin, loaded=True, q=True):
            cmds.unloadPlugin(plugin)
            log.info('plugin unloaded: %s', plugin);
        else:
            log.info('plugin is already unloaded, skip it: %s', plugin);     
            
    def exportAlembicAndEcho(self, cache_base_name=None, options_except_filepath=None):
        if cache_base_name == None:
            cache_base_name      = os.path.basename(self.m_maya_file_path)+'.abc'
            
        echo_base_name           = cache_base_name+'.echo'
        
        # cache path under maya project directory
        cache_full_path_maya_proj= self.m_maya_project_dir+'/cache/alembic/'+cache_base_name
        # cache path under automation directory
        cache_full_path_auto_tmp = self.m_automationtesttmp_cache_alembic_dir+'/'+cache_base_name
        
        # echo file path under maya project directory
        echo_full_path_maya_proj = self.m_maya_project_dir+'/cache/alembic/'+echo_base_name
        # echo file path under automation directory
        echo_full_path_auto_tmp  = self.m_automationtesttmp_cache_alembic_dir+'/'+echo_base_name           

        # export abc cache
        log.debug('cmds.AbcExport(jobArg='+options_except_filepath+' -file "'+cache_full_path_maya_proj+'")');
        cmds.AbcExport(jobArg=options_except_filepath+' -file "'+cache_full_path_maya_proj+'"')
        # generate echo file
        log.debug('abcecho2 -ht -hud "'+cache_full_path_maya_proj+'" > "'+echo_full_path_maya_proj+'"')
        os.system('abcecho2 -ht -hud "'+cache_full_path_maya_proj+'" > "'+echo_full_path_maya_proj+'"')
                
        #os.system('abcecho2 -ht -hud "'+cache_full_path_auto_tmp+'" > "'+echo_full_path_auto_tmp+'"')        

