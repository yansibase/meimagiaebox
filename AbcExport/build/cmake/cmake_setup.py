import os
import sys
import errno

gModuleName = ''

def log(format, *params):
    global gModuleName
    print(gModuleName+'| '+format % params);


def debug_dir():
    log("Path at terminal when executing this file")
    log(os.getcwd() + "\n")

    log("This file path, relative to os.getcwd()")
    log(__file__ + "\n")

    log("This file full path (following symlinks)")
    full_path = os.path.realpath(__file__)
    log(full_path + "\n")

    log("This file directory and name")
    path, file = os.path.split(full_path)
    log(path + ' --> ' + file + "\n")

    log("This file directory only")
    log(os.path.dirname(full_path))

def tryToResetDir(directory):
    import shutil
    
    # remove the directory if it exists
    if os.path.exists(directory):
        log('removing the previous directory: %s', directory);
        try:
            shutil.rmtree(directory)
        except Exception as e:
            log('Exception: %d, %s: shutil.rmtree(%s).', e.errno, e.strerror, directory)
            log('Maybe you should leave that directory or close files in that directory.');
            raise
        except:
            log('Unexcepted error when calling shutil.rmtree(%s)', directory)
            raise    
    
    # create this directory
    try:
        log('creating directory: %s', directory)
        os.makedirs(directory)
    except OSError, e:
        if e.errno != errno.EEXIST:
            log('the directory still exists, please remove it at first.')
            raise
#
def ensure_dir(dirname):
    """
    Ensure that a named directory exists; if it does not, attempt to create it.
    """
    try:
        os.makedirs(dirname)
    except OSError, e:
        if e.errno != errno.EEXIST:
            raise
#
def copyRecursively(source_dir, dest_dir, filters=[]):
    '''
    copyRecursively('/a/b/c', '/d/e/f', ['*.h'])
    copyRecursively('/a/b/c', '/d/e/f', ['*.h', '*.py'])
    copyRecursively('/a/b/c', '/d/e/f', ['*'])
    copyRecursively('/a/b/c', '/d/e/f', '*')
    '''
    #log('copyRecursively(%s, %s, %s)', source_dir, dest_dir, ' '.join(filters))
    ensure_dir(dest_dir)

    import glob, os, shutil
    files = []
    for filter_ in filters:
        files += glob.iglob(os.path.join(source_dir, filter_))
    #log('files=%s', ' '.join(files)) 

    # copy files
    i = 0
    for f in files:
        if os.path.isfile(f):
            log('copy %s', f);
            #log('  to %s', dest_dir);            
            shutil.copy2(f, dest_dir)
            i=i+1;
    log("%d file(s) copied", i)

    # copy subdirectories
    subdirs = os.listdir(source_dir)
    for d in subdirs:
        if os.path.isdir(source_dir+'/'+d):
            copyRecursively(source_dir+'/'+d, dest_dir+'/'+d, filters)

def getBasicConfig():
    CMAKE_CMD_PATH = os.environ['AUTO_BUILD_CMAKE_CMD_PATH']
    MAYA_VERSION   = os.environ['AUTO_BUILD_MAYA_VERSION']
    MAYA_BASE_DIR  = os.environ['AUTO_BUILD_MAYA_BASE_DIR']
    CMAKE_G        = os.environ['AUTO_BUILD_GENERATOR']
    BUILD_TYPES    = os.environ['AUTO_BUILD_BUILD_TYPES']
    INSTALL_TYPE   = os.environ['AUTO_BUILD_INSTALL_TYPE'] # we may build many types(Debug, Release), but which one will be installed for Maya?
    REPOSITORY_SHA = os.environ['AUTO_BUILD_REPOSITORY_SHA']
    REPOSITORY_NUM = os.environ['AUTO_BUILD_REPOSITORY_NUM']
            
    log('AUTO_BUILD_CMAKE_CMD_PATH ='+CMAKE_CMD_PATH);
    log('AUTO_BUILD_MAYA_VERSION   ='+MAYA_VERSION);    
    log('AUTO_BUILD_MAYA_BASE_DIR  ='+MAYA_BASE_DIR);      
    log('AUTO_BUILD_GENERATOR      ='+CMAKE_G);
    log('AUTO_BUILD_BUILD_TYPES    ='+BUILD_TYPES);
    log('AUTO_BUILD_INSTALL_TYPE   ='+INSTALL_TYPE); 
    log('AUTO_BUILD_REPOSITORY_SHA ='+REPOSITORY_SHA);  
    log('AUTO_BUILD_REPOSITORY_NUM ='+REPOSITORY_NUM);          
    
    # inorder to use mybox_build_test_template.buildtype_str2list(), I have to import this module at first.
    current_dirname = os.path.dirname(os.path.realpath(__file__))
    import imp
    imp.load_source('mybox_build_test_template', current_dirname+'/../../../mybox_build_test_template.py')
    import mybox_build_test_template    as template    
    buildtype_list = template.buildtype_str2list(BUILD_TYPES)
    
    return CMAKE_CMD_PATH, MAYA_VERSION, MAYA_BASE_DIR, CMAKE_G, buildtype_list, INSTALL_TYPE, REPOSITORY_SHA, REPOSITORY_NUM
    
def main():
    global gModuleName
    # debug
    #debug_dir()
   
    AUTO_BUILD_CMAKE_CMD_PATH, AUTO_BUILD_MAYA_VERSION, AUTO_BUILD_MAYA_BASE_DIR, AUTO_BUILD_GENERATOR, buildtype_list, INSTALL_TYPE, REPOSITORY_SHA, REPOSITORY_NUM = getBasicConfig();
    
    for BUILD_TYPE in buildtype_list:
        doOneBuildType(AUTO_BUILD_CMAKE_CMD_PATH, AUTO_BUILD_MAYA_VERSION, AUTO_BUILD_MAYA_BASE_DIR, AUTO_BUILD_GENERATOR, BUILD_TYPE, REPOSITORY_SHA, REPOSITORY_NUM)
        
    log('-------------------------------------------------------------------')
    log(' copy a build-install version for Maya (%s)', INSTALL_TYPE)
    log('-------------------------------------------------------------------')   
    # we may build many types(Debug, Release), but which one will be installed for Maya?
    INSTALL_DIR_FOR_MAYA = os.path.abspath(os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR'] + '/'                   +AUTO_BUILD_MAYA_VERSION+'_mybox_install'                 +'/'+gModuleName)
    BUILD_INSTALL_DIR    = os.path.abspath(os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR'] + '/'+REPOSITORY_NUM+'_'+REPOSITORY_SHA+'_'+AUTO_BUILD_MAYA_VERSION+'_mybox_'+INSTALL_TYPE+'_install'+'/'+gModuleName)    
    log('INSTALL_DIR_FOR_MAYA ='+INSTALL_DIR_FOR_MAYA);
    log('BUILD_INSTALL_DIR    ='+BUILD_INSTALL_DIR);
    
    tryToResetDir(INSTALL_DIR_FOR_MAYA)
    copyRecursively(BUILD_INSTALL_DIR, INSTALL_DIR_FOR_MAYA, ['*'])
    
    log('-------------------------------------------------------------------')
    log(' Done')
    log('-------------------------------------------------------------------') 
    
def doOneBuildType(AUTO_BUILD_CMAKE_CMD_PATH, AUTO_BUILD_MAYA_VERSION, AUTO_BUILD_MAYA_BASE_DIR, AUTO_BUILD_GENERATOR, BUILD_TYPE, REPOSITORY_SHA, REPOSITORY_NUM):
    global gModuleName
    log('-------------------------------------------------------------------')
    log(' build & build-install (%s)', BUILD_TYPE)
    log('-------------------------------------------------------------------') 
    
    current_file_path = os.path.realpath(__file__)
    current_file_path = current_file_path.replace('\\','/')
    current_file_dir  = os.path.dirname(current_file_path)
    
    # get current module name
    gModuleName = current_file_dir.split('/')[-3]


    BUILD_DIR   = os.path.abspath(os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR'] + '/'+REPOSITORY_NUM+'_'+REPOSITORY_SHA+'_'+AUTO_BUILD_MAYA_VERSION+'_mybox_'+BUILD_TYPE+'_build'  +'/'+gModuleName)
    log('BUILD_DIR   ='+BUILD_DIR);
    INSTALL_DIR = os.path.abspath(os.environ['AUTO_BUILD_OUTPUT_ROOT_DIR'] + '/'+REPOSITORY_NUM+'_'+REPOSITORY_SHA+'_'+AUTO_BUILD_MAYA_VERSION+'_mybox_'+BUILD_TYPE+'_install'+'/'+gModuleName)
    log('INSTALL_DIR ='+INSTALL_DIR);
    SOURCE_DIR  = os.path.abspath(current_file_dir + '/../..')
    log('SOURCE_DIR  ='+SOURCE_DIR)
    
    tryToResetDir(INSTALL_DIR)
    
    tryToResetDir(BUILD_DIR)
    log('\n')
    log('cd '+BUILD_DIR);
    os.chdir(BUILD_DIR)
    
    log('\n')     
    cmd = ''
    cmd  +=AUTO_BUILD_CMAKE_CMD_PATH+' '
    if sys.platform == 'win32':
        cmd  +=' -D CMAKE_BUILD_TYPE='     +BUILD_TYPE              +' '
        cmd  +=' -D "MAYA_BASE_DIR:string='+AUTO_BUILD_MAYA_BASE_DIR+'" '
        cmd  +=' -D CMAKE_INSTALL_PREFIX=' +INSTALL_DIR             +' '
        cmd  +=' -DAlembic_Depends_Libs_Dir="d:/dev/alembic_depends/install/lib" '
        #cmd +=' -D CMAKE_VERBOSE_MAKEFILE=1 '
        cmd  +=' -G "'+AUTO_BUILD_GENERATOR+'" '
    elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
        cmd  +=' -DCMAKE_BUILD_TYPE='     +           BUILD_TYPE   +' '
        cmd  +=' -DMAYA_BASE_DIR='        +AUTO_BUILD_MAYA_BASE_DIR+' '
        cmd  +=' -DCMAKE_INSTALL_PREFIX=' +INSTALL_DIR             +' '
        cmd  +=' -DAlembic_Depends_Libs_Dir="/backup/lhome/dev/alembic_depends/install/lib" '
        #cmd +=' -DCMAKE_VERBOSE_MAKEFILE=1 '
        cmd  +=' -G"'+AUTO_BUILD_GENERATOR+'" '
    cmd  +=' '+SOURCE_DIR
    log('-------------------------------------------------------------------')
    log('generating cmake scripts ...(%s)', BUILD_TYPE)
    log('-------------------------------------------------------------------') 
    log(cmd);#debug
    if os.system(cmd) != 0:
        raise Exception(gModuleName)
        
    log('\n') 
    cmd = AUTO_BUILD_CMAKE_CMD_PATH+' --build '+BUILD_DIR+' '
    if sys.platform == 'win32':
        # http://stackoverflow.com/questions/3785976/cmake-generate-visual-studio-2008-solution-for-win32-and-x64
        cmd += ' --target ALL_BUILD --config '+BUILD_TYPE+' '
    log('-------------------------------------------------------------------')
    log('building ...(%s)', BUILD_TYPE)
    log('-------------------------------------------------------------------') 
    log(cmd);#debug
    if os.system(cmd) != 0:
        raise Exception(gModuleName)
    
    log('\n')
    cmd = AUTO_BUILD_CMAKE_CMD_PATH+' -DBUILD_TYPE='+BUILD_TYPE+' -P cmake_install.cmake'
    log('-------------------------------------------------------------------')
    log('build-install ...(%s)', BUILD_TYPE)
    log('-------------------------------------------------------------------') 
    log(cmd);#debug
    if os.system(cmd) != 0:
        raise Exception(gModuleName)
    

    
if '__main__' == __name__:
    main();
