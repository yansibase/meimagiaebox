#include <maya/MFnPlugin.h>
#include <maya/MStatus.h>

#include <common/log.h>
#include "version.h"
#include "AbcExport.h"

// These methods load and unload the plugin, registerNode registers the
// new node type with maya

PLUGIN_EXPORT MStatus initializePlugin( MObject obj )
{
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_beginCB('AbcExport')", true, false) );

    MString version;
    version += PLUGIN_VERSION_STRING;
    version += "_";
    version += MString(GIT_SHA1);
#ifdef DEBUG
    version += "_Debug";
#endif
#ifdef NDEBUG
    version += "_Release";
#endif

	MFnPlugin plugin( obj, "yaoyu", version.asChar(), "Any" );

    CHECK_MSTATUS(plugin.registerCommand("AbcExport", AbcExport::creator, AbcExport::createSyntax));


    MString info = "AbcExport v";
    info += ABCEXPORT_VERSION;
    info += " using ";
    info += Alembic::Abc::GetLibraryVersion().c_str();
    MGlobal::displayInfo(info);

    MGlobal::executeCommandOnIdle("AlembicCreateUI");

	// import node_templateSetup
	CHECK_MSTATUS( MGlobal::executePythonCommand("import AbcExportSetup", true, false) );
	CHECK_MSTATUS(plugin.registerUI(
                    "AbcExport_setupUI()",
                    "AbcExport_unsetupUI()",
                    "AbcExport_setup()",
                    "AbcExport_unsetup()"
                    ));
                    
    // callback functions
	CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.initializePlugin_endCB('AbcExport')", true, false) );

	return( MS::kSuccess );
}

PLUGIN_EXPORT MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );

    // callback functions
    CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_beginCB('AbcExport')", true, false) );

    //CHECK_MSTATUS_AND_RETURN_IT(uninitializePlugin_ArrayDataContainer(plugin));
    CHECK_MSTATUS(plugin.deregisterCommand("AbcExport"));

    MGlobal::executeCommandOnIdle("AlembicDeleteUI");

    // callback functions
    CHECK_MSTATUS( MGlobal::executePythonCommand("import mymagicbox.plugin_setup as plugin_setup; plugin_setup.uninitializePlugin_endCB('AbcExport')", true, false) );

	return MS::kSuccess;
}

