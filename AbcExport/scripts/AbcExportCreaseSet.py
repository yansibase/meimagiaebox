import collections
import inspect                      as isp
from maya import OpenMaya           as om
import maya.cmds                    as cmds
import mymagicbox.log               as log
import mymagicbox.mmb_flog          as flog

gCreaseSetsPlugName = 'crease____set'
gCreaseNamePrefix   = 'crease__'
#----------------------
def trimForAttrName(val):
    return val.replace(":", "_") # reference object may contain characters '.' which is invalid for attribute name
#----------------------
class CreaseSet(object):
    ''
    name     = ''
    owner    = ''
    level    = 0.0
    edges    = []
    edgecnt  = 0  # edge count
    vertex   = []

    @staticmethod
    def attrCount():
        return 6

    def __init__(self):
        self.name     = ''
        self.owner    = ''
        self.level    = 0.0
        self.edges    = []
        self.edgecnt = 0
        self.vertex   = []

    def log(self, msg=''):
        log.debug('%s.%s(%s)', self.__class__.__name__,   isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )
        log.debug('   name:%s', self.name)
        log.debug('  owner:%s', self.owner)
        log.debug('  level:%s', self.level)
        log.debug('edgecnt:%s', self.edgecnt)
        log.debug('  edges:%s', self.edges)
        log.debug(' vertex:%s', self.vertex)

#----------------------
class CreaseSetOp(object):
    def _addAttr(self, nodeName, parentAttrName, attrNamePostfix, dtype=None, atype=None):
        attrName = parentAttrName+attrNamePostfix
        if not cmds.attributeQuery(attrName, node=nodeName, exists=True):
            if dtype is not None:
                cmds.addAttr(nodeName, ln=attrName,    sn=attrName,    dt=dtype,       readable=True, writable=True, storable=True, keyable=True, parent=parentAttrName)
            elif atype is not None:
                cmds.addAttr(nodeName, ln=attrName,    sn=attrName,    at=atype,       readable=True, writable=True, storable=True, keyable=True, parent=parentAttrName)
        else:
            log.debug('attr  already exist: '+nodeName+'.'+attrName)

    def addAttrs(self, nodeName, setname):
        global gCreaseNamePrefix
        cmpAttrName           = trimForAttrName(gCreaseNamePrefix+setname)
        numberOfChildrenAttrs = CreaseSet.attrCount()
        #log.debug('nodeName=%s', nodeName)

        ##
        attrName = cmpAttrName
        if not cmds.attributeQuery(attrName, node=nodeName, exists=True):
            cmds.addAttr(nodeName, ln=attrName,           sn=attrName,           at='compound',     readable=True, writable=True, storable=True, keyable=True, numberOfChildren=numberOfChildrenAttrs)
        else:
            log.debug( 'attr  already exist: '+nodeName+'.'+attrName)
        #
        self._addAttr(nodeName, cmpAttrName, 'name',    dtype='string')
        self._addAttr(nodeName, cmpAttrName, 'owner',   dtype='string')
        self._addAttr(nodeName, cmpAttrName, 'level',   atype='float')
        self._addAttr(nodeName, cmpAttrName, 'edgecnt', atype='long')
        self._addAttr(nodeName, cmpAttrName, 'edges',   dtype='Int32Array')
        self._addAttr(nodeName, cmpAttrName, 'vertex',  dtype='Int32Array')
        #

    def setAttrs(self, creaseset):
        global gCreaseNamePrefix
        nodeName     = creaseset.owner
        cmpAttrName  = trimForAttrName(gCreaseNamePrefix+creaseset.name)

        attrName = cmpAttrName+'name';     cmds.setAttr(nodeName+'.'+attrName, creaseset.name,    type='string')
        attrName = cmpAttrName+'owner';    cmds.setAttr(nodeName+'.'+attrName, creaseset.owner,   type='string')
        attrName = cmpAttrName+'level';    cmds.setAttr(nodeName+'.'+attrName, creaseset.level)
        attrName = cmpAttrName+'edgecnt';  cmds.setAttr(nodeName+'.'+attrName, creaseset.edgecnt)
        attrName = cmpAttrName+'edges';    cmds.setAttr(nodeName+'.'+attrName, creaseset.edges,   type='Int32Array')
        attrName = cmpAttrName+'vertex';   cmds.setAttr(nodeName+'.'+attrName, creaseset.vertex,  type='Int32Array')


    def getAttrs(self, nodeName, setname):
        global gCreaseNamePrefix

        #nodeName = self.owner
        cmpAttrName  = trimForAttrName(gCreaseNamePrefix+setname)

        cs = CreaseSet()

        attrName = cmpAttrName+'name';      cs.name    = cmds.getAttr(nodeName+'.'+attrName)
        attrName = cmpAttrName+'owner';     cs.owner   = cmds.getAttr(nodeName+'.'+attrName)
        attrName = cmpAttrName+'level';     cs.level   = cmds.getAttr(nodeName+'.'+attrName)
        attrName = cmpAttrName+'edgecnt';   cs.edgecnt = cmds.getAttr(nodeName+'.'+attrName)
        attrName = cmpAttrName+'edges';     cs.edges   = cmds.getAttr(nodeName+'.'+attrName)
        attrName = cmpAttrName+'vertex';    cs.vertex  = cmds.getAttr(nodeName+'.'+attrName)

        # cs.log()
        return cs

#----------------------
class CreaseSetArray(object):
    items = []

    def __init__(self):
        self.items = []

    def append(self, item):
        self.items.append(item)

    def log(self, msg=''):
        global gCreaseSetsPlugName
        log.debug(msg)
        for i in self.items:
            i.log('')
            CreaseSetOp().getAttrs(i.owner, i.name)

            creaseSets = cmds.getAttr(i.owner+'.'+gCreaseSetsPlugName)
            log.debug('creaseSets=%s', creaseSets)

            log.debug('')

    def addCreaseSetNamesPlug(self, nodeName):
        global gCreaseSetsPlugName
        global gCreaseNamePrefix
        if len(self.items) == 0: # this mesh has not creaseSet
            return

        creaseSetNames = []
        for i in self.items:
            plugName = trimForAttrName(gCreaseNamePrefix+i.name)
            creaseSetNames.append(plugName)

        # add attribute
        attrName = gCreaseSetsPlugName
        if not cmds.attributeQuery(attrName, node=nodeName, exists=True):
            cmds.addAttr(nodeName, ln=attrName, sn=attrName, dt='string',  readable=True, writable=True, storable=True, keyable=True)
        else:
            log.debug( 'attr  already exist: '+nodeName+'.'+attrName)

        # set attribute
        value = ';'.join(creaseSetNames) # seperate with ';'
        cmds.setAttr(nodeName+'.'+attrName, value,    type='string')

#----------------------
class HashableMObjectHandle(om.MObjectHandle):
    '''Hashable MObjectHandle referring to an MObject that can be used as a key in a dict.

    :See: MObjectHandle documentation for more information.
    '''
    def __hash__(self):
        '''Use the proper unique hash value unique to the MObject that the MObjectHandle points to so this class can be used as a key in a dict.

        :Return:
            MObjectHandle.hasCode() unique memory address for the MObject that is hashable

        :See: MObjectHandle.hashCode() documentation for more information.
        '''
        return self.hashCode()

#@flog.trace
def getCreaseSetsContainingItems(items=None, asDict=False):
    '''Return list of sets that have edge/vertex members with the specified items

    :Parameters:
        items ([string])
            The edge and vertex items to look for in the creaseSet nodes.
            If mesh shapes are included, it will return all creaseSet nodes
            used by the mesh shapes as well.  If "None", then operate on the
            selected items.
        asDict (bool)
            if true, return a dict
            if false, return a set
            (default=False)

    :Limitation:
        If both a mesh and some of its components are selected/specified, then this function
        will return an artificially elevated count of selected members for the asDict=True option.
        It will add the total number of member components for the mesh and then additionally add
        the number of member components for the selected/specified components.

    :Return: Set of creaseSet names or dict in the format {creaseSet: numItems}
    '''
    log.debug('%s(%s)',  isp.getframeinfo(isp.currentframe())[2],   flog.filterSelfParam(locals()) )

    # Results to return
    dictContainingItems = collections.defaultdict(int)  # dict: {creaseSet: numItems)
    setsContainingItems = set()

    #
    # NOTE: Using the API operations to provide faster performance than the less direct MEL-style commands
    # In some large models with 50,000+ crease items it was taking 17 seconds to select items.  With the API calls, it is negligible.
    #

    # Create MSelectionList
    selList = om.MSelectionList()
    if items != None:
        for item in items:
            selList.add(item)
    else:
        # Get the current selection
        om.MGlobal.getActiveSelectionList(selList)


    # Loop over items and perform operations if is a meshShape
    dagPath   = om.MDagPath()
    component = om.MObject()
    processedTransObjHandles = set()  # track processed items
    itSelList = om.MItSelectionList(selList)
    while (not itSelList.isDone() ):
        #log.debug('Processing item...')

        # Process only dag objects and components
        if (itSelList.itemType() == om.MItSelectionList.kDagSelectionItem):

            # Process only dagPaths that are mesh shapes (or transforms of mesh shapes)
            itSelList.getDagPath(dagPath, component)
            if dagPath.hasFn(om.MFn.kMesh):

                csArray = CreaseSetArray()

                # Filter to not re-iterate over both the transform and the shape if both are in the selection list
                dagPathTransObjHandle =  HashableMObjectHandle(dagPath.transform())
                if (not component.isNull()) or (dagPathTransObjHandle not in processedTransObjHandles):
                    processedTransObjHandles.add( HashableMObjectHandle(dagPath.transform()) ) # update processed item set

                    # Process mesh and mesh components
                    #log.debug("selList item: %s", dagPath.fullPathName())
                    meshFn = om.MFnMesh(dagPath)

                    connectedSets = om.MObjectArray()
                    connectedSetMembers = om.MObjectArray()
                    meshFn.getConnectedSetsAndMembers(dagPath.instanceNumber(), connectedSets, connectedSetMembers, False)

                    # Add all members for each creaseSet
                    for iConnectedSets in range(connectedSets.length()):
                        set_  = connectedSets[iConnectedSets]
                        comp_ = connectedSetMembers[iConnectedSets]
                        if set_.hasFn(om.MFn.kCreaseSet):  # if is a creaseSet
                            setFn = om.MFnSet(set_)
                            edgeIt= om.MItMeshEdge(dagPath, comp_);
                            level = cmds.getAttr(setFn.name()+'.creaseLevel')

                            cs = CreaseSet()
                            cs.name     = setFn.name()
                            cs.owner    = dagPath.fullPathName()
                            cs.level    = level
                            cs.edgecnt = edgeIt.count()

                            while not edgeIt.isDone():
                                edgeIdx = edgeIt.index()  # index of the current edge
                                v0Idx   = edgeIt.index(0) # index of the vertex0
                                v1Idx   = edgeIt.index(1) # index of the vertex1

                                cs.edges.append(edgeIdx)
                                cs.vertex.append(v0Idx)
                                cs.vertex.append(v1Idx)

                                edgeIt.next()

                            CreaseSetOp().addAttrs(cs.owner, cs.name)
                            CreaseSetOp().setAttrs(cs)

                            csArray.append(cs)

                            # Gather the creaseSet name and members for this mesh
                            creaseSetName          = setFn.name()
                            creaseSetMemberSelList = om.MSelectionList()
                            if not comp_.isNull():
                                creaseSetMemberSelList.add(dagPath, comp_)

                            # Filter to the selected components (if a component is specified, otherwise add all the items)
                            # NOTE: Suggestion made to add MSelectionList.intersection() in MAYA-14750
                            if not component.isNull():
                                # perform intersection of the lists
                                selListToRemoveItems = om.MSelectionList(creaseSetMemberSelList)
                                selListToRemoveItems.merge(dagPath, component, om.MSelectionList.kRemoveFromList)
                                if not selListToRemoveItems.isEmpty():
                                    creaseSetMemberSelList.merge(selListToRemoveItems, om.MSelectionList.kRemoveFromList)

                            # Update results to return
                            if asDict:

                                # Convert MSelectionList to strings
                                creaseSetMemberSelListStrs = []
                                creaseSetMemberSelList.getSelectionStrings(creaseSetMemberSelListStrs) # OpenMaya uses [] instead of MStringArray
                                # Assign strings to the dict
                                #log.debug("%s: %s",creaseSetName, creaseSetMemberSelListStrs)

                                for iCreaseSetMemberSelList in range(creaseSetMemberSelList.length()):
                                    creaseSetMemberSelList_dagPath = om.MDagPath()
                                    creaseSetMemberSelList_component = om.MObject()
                                    creaseSetMemberSelList.getDagPath(iCreaseSetMemberSelList, creaseSetMemberSelList_dagPath, creaseSetMemberSelList_component)
                                    if not creaseSetMemberSelList_component.isNull():
                                        dictContainingItems[creaseSetName] += om.MFnComponent(creaseSetMemberSelList_component).elementCount()
                            else:
                                if not creaseSetMemberSelList.isEmpty():
                                    setsContainingItems.add(creaseSetName)

                csArray.addCreaseSetNamesPlug(dagPath.fullPathName())
                csArray.log('')

        # Iterate to next itSelList item
        itSelList.next()

    # return results
    if asDict:
        #log.debug("dictContainingItems: %s", dictContainingItems)
        return dictContainingItems
    else:
        #log.debug("setsContainingItems: %s", setsContainingItems)
        return setsContainingItems

@flog.trace
def main():
    log.debug('=======================================================')
    meshes =  cmds.ls(type='mesh');
    for mesh in meshes:
        log.debug('---------------------------------------------------')
        getCreaseSetsContainingItems(items=[mesh])

