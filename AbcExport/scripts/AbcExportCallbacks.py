# -*- coding: utf-8 -*-
import mymagicbox.mmb_flog          as flog
import mymagicbox.log               as log
import AbcExportCreaseSet           as CreaseSet
import AbcExportAddJobArgs          as AddJobArgs

@flog.trace
def AbcExport_CommandBegin():
    CreaseSet.main()
    pass

@flog.trace
def AbcExport_ParseAllJobsBegin(jobSize):
    pass

@flog.trace
def AbcExport_ParseJobBegin(jobIndex, jobArgsStr):
    log.debug('Before cook job[%d]=%s', jobIndex, jobArgsStr)
    jobArgsStr = AddJobArgs.main(jobIndex, jobArgsStr)
    log.debug(' After cook job[%d]=%s', jobIndex, jobArgsStr)
    return jobArgsStr

@flog.trace
def AbcExport_ParseJobEnd(jobIndex):
    pass

@flog.trace
def AbcExport_ParseAllJobsEnd(jobSize):
    pass

@flog.trace
def AbcExport_ExportBegin():
    pass

@flog.trace
def AbcExport_ExportFrameBegin(frame):
    pass

@flog.trace
def AbcExport_EvalFrameBegin(iFrame):
    pass

@flog.trace
def AbcExport_PerFrameCallbackEntry(frame, minX, minY, minZ,  maxX, maxY, maxZ):
    pass

@flog.trace
def AbcExport_PostJobCallbackEntry(frame, minX, minY, minZ,  maxX, maxY, maxZ):
    pass

@flog.trace
def AbcExport_EvalFrameEnd(iFrame):
    pass

@flog.trace
def AbcExport_ExportFrameEnd(frame):
    pass

@flog.trace
def AbcExport_ExportEnd():
    pass

@flog.trace
def AbcExport_CommandEnd():
    pass
