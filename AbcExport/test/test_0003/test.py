import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        self.attrprefix();
                
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass
        
    def attrprefix(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        
        _cache_base_name = 'test.ma.attrprefix.abc'
        
        # export abc cache --------------------------
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 10 -attrprefix "translate" -attrprefix "scale" ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
        # reference data
        '''
        How to generate these reference data:
        open /scenes/test.ma, and run the following scripts

T_ref  =[(0.0, 0.0, 0.0)]
S_ref  =[(0.0, 0.0, 0.0)]
SP_ref =[(0.0, 0.0, 0.0)]
SPT_ref=[(0.0, 0.0, 0.0)]

for i in xrange(1, 11):
    cmds.currentTime( i )
    
    t   = cmds.getAttr('objects:pCube1.translate')[0]
    s   = cmds.getAttr('objects:pCube1.scale')[0]
    sp  = cmds.getAttr('objects:pCube1.scalePivot')[0]
    spt = cmds.getAttr('objects:pCube1.scalePivotTranslate')[0]
              
    T_ref.append(t);
    S_ref.append(s);    
    SP_ref.append(sp); 
    SPT_ref.append(spt);     

print 'T_ref   =', T_ref;
print 'S_ref   =', S_ref;
print 'SP_ref  =', SP_ref;
print 'SPT_ref =', SPT_ref;
        '''
        T_ref   = [(0.0, 0.0, 0.0), (1.0, 0.0, 0.0), (1.308642010629915, 0.0, 0.0), (2.135802500041916, 0.0, 0.0), (3.3333335717519237, 0.0, 0.0), (4.753086375601498, 0.0, 0.0), (6.246913756853274, 0.0, 0.0), (7.666666547457375, 0.0, 0.0), (8.864197963549751, 0.0, 0.0), (9.691358148315802, 0.0, 0.0), (10.0, 0.0, 0.0)]
        S_ref   = [(0.0, 0.0, 0.0), (1.0, 1.0, 1.0), (1.308642010629915, 1.0, 1.0), (2.135802500041916, 1.0, 1.0), (3.3333335717519237, 1.0, 1.0), (4.753086375601498, 1.0, 1.0), (6.246913756853274, 1.0, 1.0), (7.666666547457375, 1.0, 1.0), (8.864197963549751, 1.0, 1.0), (9.691358148315802, 1.0, 1.0), (10.0, 1.0, 1.0)]
        SP_ref  = [(0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0)]
        SPT_ref = [(0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0)]

        for i in xrange(1, 11):
            cmds.currentTime( i )
            
            t   = cmds.getAttr('pCube1.translate')[0] # the imported data doesn't has namespace 'objects'
            s   = cmds.getAttr('pCube1.scale')[0]
            sp  = cmds.getAttr('pCube1.scalePivot')[0]
            spt = cmds.getAttr('pCube1.scalePivotTranslate')[0]
            
            # compare with the reference data
            self.assertAlmostEqual(t[0],   T_ref[i][0],   places=7, msg='time: '+str(i)+', '+str(t[0])+'!='+str(T_ref[i][0]))
            self.assertAlmostEqual(t[1],   T_ref[i][1],   places=7, msg='time: '+str(i)+', '+str(t[1])+'!='+str(T_ref[i][1]))
            self.assertAlmostEqual(t[2],   T_ref[i][2],   places=7, msg='time: '+str(i)+', '+str(t[2])+'!='+str(T_ref[i][2]))
            
            self.assertAlmostEqual(s[0],   S_ref[i][0],   places=7, msg='time: '+str(i)+', '+str(s[0])+'!='+str(S_ref[i][0]))
            self.assertAlmostEqual(s[1],   S_ref[i][1],   places=7, msg='time: '+str(i)+', '+str(s[1])+'!='+str(S_ref[i][1]))
            self.assertAlmostEqual(s[2],   S_ref[i][2],   places=7, msg='time: '+str(i)+', '+str(s[2])+'!='+str(S_ref[i][2]))
            
            self.assertAlmostEqual(sp[0],  SP_ref[i][0],  places=7, msg='time: '+str(i)+', '+str(sp[0])+'!='+str(SP_ref[i][0]))
            self.assertAlmostEqual(sp[1],  SP_ref[i][1],  places=7, msg='time: '+str(i)+', '+str(sp[1])+'!='+str(SP_ref[i][1]))
            self.assertAlmostEqual(sp[2],  SP_ref[i][2],  places=7, msg='time: '+str(i)+', '+str(sp[2])+'!='+str(SP_ref[i][2]))
            
            self.assertAlmostEqual(spt[0], SPT_ref[i][0], places=7, msg='time: '+str(i)+', '+str(spt[0])+'!='+str(SPT_ref[i][0]))
            self.assertAlmostEqual(spt[1], SPT_ref[i][1], places=7, msg='time: '+str(i)+', '+str(spt[1])+'!='+str(SPT_ref[i][1]))
            self.assertAlmostEqual(spt[2], SPT_ref[i][2], places=7, msg='time: '+str(i)+', '+str(spt[2])+'!='+str(SPT_ref[i][2]))
            
        pass
        
if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
