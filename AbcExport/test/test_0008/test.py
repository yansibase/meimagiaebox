import os
import sys
import time
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        self.frameRelativeSample()
        
        log.debug('---- end   %s.test_animation() ----', self.m_id);
                
    def frameRelativeSample(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        
        _cache_base_name = 'test.ma.frameRelativeSample.abc'
        
        # export abc cache --------------------------
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 10 -frameRelativeSample -0.1 -frameRelativeSample 0.2 -step 5 ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
        nodeName = 'pCube1' # the imported data doesn't has namespace 'objects'
        
        # reference data
        '''
        How to generate these reference data:
        open /scenes/test.ma, and run the following scripts

RX_ref = [0]
RY_ref = [0]
RZ_ref = [0]

for i in range(1, 11):
    cmds.currentTime( i )
    
    rx = cmds.getAttr('objects:pCube1.rx')
    ry = cmds.getAttr('objects:pCube1.ry')
    rz = cmds.getAttr('objects:pCube1.rz')
          
    RX_ref.append(rx);
    RY_ref.append(ry);
    RZ_ref.append(rz);
    
print 'RX_ref =', RX_ref;
print 'RY_ref =', RY_ref;
print 'RZ_ref =', RZ_ref;
        '''
        # maya animation data
        #RX_ref = [0, 1.0, 1.030098973610628, 1.119579640415681, 1.2672176671571516, 1.4717885944713283, 1.7320682036024302, 2.046831943891474, 2.4148558163919227, 2.834914929784244, 3.3057851642706106]
        #RY_ref = [0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        #RZ_ref = [0, 1.0, 1.030098973610628, 1.119579640415681, 1.2672176671571516, 1.4717885944713283, 1.7320682036024302, 2.046831943891474, 2.4148558163919227, 2.834914929784244, 3.3057851642706106]
        
        # test.ma.frameRelativeSample.abc's data
        RX_ref = [0, 1.0004034962251291, 1.1207607509509687, 1.2701985787954455, 1.4196364066399223, 1.569074234484399, 1.7326126368460542, 1.7907013514493055, 1.7907013514493055, 1.7907013514493055, 1.7907013514493055]
        RY_ref = [0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        RZ_ref = [0, 1.0004034962251291, 1.1207607509509687, 1.2701985787954455, 1.4196364066399223, 1.569074234484399, 1.7326126368460542, 1.7907013514493055, 1.7907013514493055, 1.7907013514493055, 1.7907013514493055]


        for i in range(1, 11):
            cmds.currentTime( i )
            
            rx = cmds.getAttr(nodeName+'.rx')
            ry = cmds.getAttr(nodeName+'.ry')
            rz = cmds.getAttr(nodeName+'.rz')
        
            self.assertAlmostEqual(rx,  RX_ref[i],  places=7, msg='time: '+str(i)+', '+str(rx)+'!='+str(RX_ref[i]))
            self.assertAlmostEqual(ry,  RY_ref[i],  places=7, msg='time: '+str(i)+', '+str(ry)+'!='+str(RY_ref[i]))
            self.assertAlmostEqual(rz,  RZ_ref[i],  places=7, msg='time: '+str(i)+', '+str(rz)+'!='+str(RZ_ref[i]))
        
        # render image ------------------------------
        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)   
        # render image end---------------------------                 
        
        


    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
