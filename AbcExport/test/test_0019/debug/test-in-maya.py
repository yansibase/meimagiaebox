import os
import sys
import time
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im


m_id        = ''
m_helper    = None

#m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
m_helper   = hp.ATHelper()



m_helper.openMayaFile('D:/dev/mybox/AbcExport/test/test_0019/scenes/test.ma')
hp.ATHelper.tryToLoadPlugin('AbcExport')
hp.ATHelper.tryToLoadPlugin('AbcImport')
 
 
m_helper.openMayaFile('D:/dev/mybox/AbcExport/test/test_0019/scenes/test.ma')
        
_cache_base_name = 'test.ma.writeColorSets.abc'
        
# export abc cache --------------------------
m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 10 -writeColorSets ')
m_helper.moveCacheAlembic()
        
bEchoFilesEqual = m_helper.compare_abcecho()
print bEchoFilesEqual
#tc.assertEqual(bEchoFilesEqual, True)
# export abc cache end-----------------------
        
# open maya file but not load reference. Then import the abc cache and render image
m_helper.openMayaFile2('D:/dev/mybox/AbcExport/test/test_0019/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
cmds.AbcImport('C:/Users/yaoys/Documents/automationtest_tmp/dev/mybox/AbcExport/test/test_0019/cache/alembic/'+_cache_base_name, mode='import')
        
# set color set
cmds.select('pPlaneShape1', r=True)
cmds.polyColorSet(currentColorSet=True, colorSet='testColorSet1')

        
# render image ------------------------------
renderer = rndr.CreateRenderer('mayaHardware2')
renderer.render()
m_helper.moveRenderedImages();

isImageEqual = m_helper.compare_image()
if isImageEqual:
    log.info('Passed. %s', __file__)
else:
    print('image not equal') 
# render image end---------------------------                 
        
        
 
