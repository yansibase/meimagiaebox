import os
import sys
import time
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        self.preRoll()
        
        log.debug('---- end   %s.test_animation() ----', self.m_id);
                
    def preRoll(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        
        _cache_base_name = 'test.ma.preRoll.abc'
        
        # export abc cache --------------------------
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 10 -preRoll  -frameRange 11 20  -frameRange 21 30 -preRoll ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
        nodeName = 'pCube1' # the imported data doesn't has namespace 'objects'
        
        # reference data
        '''
        How to generate these reference data:
        open /scenes/test.ma, and run the following scripts
        
nodeName = 'objects:pCube1'

TX_ref = [0]

for i in range(1, 51):
    cmds.currentTime( i )
    
    tx = cmds.getAttr(nodeName+'.tx')
          
    TX_ref.append(tx);
    
print 'TX_ref =', TX_ref;
        '''
        # maya animation data
        #TX_ref = [0, 1.0, 1.002736270328239, 1.0108708764014256, 1.0242925151961046, 1.0428898722246662, 1.0665516548729481, 1.0951665403537703, 1.1286232560356293, 1.166810448162204, 1.20961683311551, 1.2569311404931853, 1.308641996715474, 1.3646381697530892, 1.4248082291035609, 1.489041040979039, 1.5572252161957816, 1.6292493429601815, 1.7050023213991805, 1.784372745132804, 1.8672491870819055, 1.9535205784221792, 2.0430754970104372, 2.1358025019150135, 2.231590552494316, 2.330328068973029, 2.4319041751883326, 2.536206984061872, 2.6431254766065724, 2.752548663769534, 2.8643646135711283, 2.97846232995259, 3.0947308429679428, 3.2130581814683654, 3.333333369457362, 3.455445453229841, 3.5792824281998654, 3.704733335567169, 3.831687235001509, 3.9600320942119076, 4.089656968728294, 4.220450928729228, 4.352301919950776, 4.485099009387015, 4.618731274859279, 4.753086645862748, 4.888054197989614, 5.023523013837942, 5.159381012394105, 5.295517664763628, 5.431820496011799]

        # test.ma.frameRelativeSample.abc's data
        TX_ref = [0, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.2569311404931853, 1.308641996715474, 1.3646381697530892, 1.4248082291035609, 1.489041040979039, 1.5572252161957816, 1.6292493429601815, 1.7050023213991805, 1.784372745132804, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055, 1.8672491870819055]


        for i in range(1, 51):
            cmds.currentTime( i )
            
            tx = cmds.getAttr(nodeName+'.tx')
        
            self.assertAlmostEqual(tx,  TX_ref[i],  places=7, msg='time: '+str(i)+', '+str(tx)+'!='+str(TX_ref[i]))
         
        # render image ------------------------------
        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)   
        # render image end---------------------------                 
        
        


    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
