import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        self.attr();
                
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass
        
    def attr(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        
        _cache_base_name = 'test.ma.attr.abc'
        
        # export abc cache --------------------------
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 10 -attr "translateX" -attr "rotateX" -attr "scaleX" ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
        # reference data
        '''
        How to generate these reference data:
        open /scenes/test.ma, and run the following scripts

TX_ref=[0]
RX_ref=[0]
SX_ref=[0]

for i in xrange(1, 11):
    cmds.currentTime( i )
    
    tx = cmds.getAttr('objects:pCube1.tx')
    rx = cmds.getAttr('objects:pCube1.rx')
    sx = cmds.getAttr('objects:pCube1.sx') 
          
    TX_ref.append(tx);
    RX_ref.append(rx);    
    SX_ref.append(sx); 

print 'TX_ref=', TX_ref;
print 'RX_ref=', RX_ref;
print 'SX_ref=', SX_ref;
        '''
        TX_ref= [0, 1.0, 1.308642010629915, 2.135802500041916, 3.3333335717519237, 4.753086375601498, 6.246913756853274, 7.666666547457375, 8.864197963549751, 9.691358148315802, 10.0]
        RX_ref= [0, 1.0, 1.308642010629915, 2.135802500041916, 3.333333571751923, 4.753086375601496, 6.246913756853274, 7.666666547457374, 8.86419796354975, 9.6913581483158, 10.0]
        SX_ref= [0, 1.0, 1.308642010629915, 2.135802500041916, 3.3333335717519237, 4.753086375601498, 6.246913756853274, 7.666666547457375, 8.864197963549751, 9.691358148315802, 10.0]

        for i in range(1, 11):
            cmds.currentTime( i )
            
            tx = cmds.getAttr('pCube1.tx') # the imported data doesn't has namespace 'objects'
            rx = cmds.getAttr('pCube1.rx')
            sx = cmds.getAttr('pCube1.sx') 
            
            # compare with the reference data
            self.assertAlmostEqual(tx, TX_ref[i], places=7, msg='time: '+str(i)+', '+str(tx)+'!='+str(TX_ref[i]))
            self.assertAlmostEqual(rx, RX_ref[i], places=7, msg='time: '+str(i)+', '+str(rx)+'!='+str(RX_ref[i]))
            self.assertAlmostEqual(sx, SX_ref[i], places=7, msg='time: '+str(i)+', '+str(sx)+'!='+str(SX_ref[i]))

        pass

        
if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
