import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()


        self.eulerfilter();
                
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass
        
    def eulerfilter(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        
        _cache_base_name = 'test.ma.eulerfilter.abc'
        
        # export abc cache --------------------------
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 100 -eulerfilter ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
        nodeName = 'pCube1' # the imported data doesn't has namespace 'objects'
        
        # assign materials
        cmds.select(nodeName+'.f[0]', r=True)
        cmds.hyperShade(assign='m0')
        cmds.select(nodeName+'.f[1]', r=True)
        cmds.hyperShade(assign='m1')
        cmds.select(nodeName+'.f[2]', r=True)
        cmds.hyperShade(assign='m2')
        cmds.select(nodeName+'.f[3]', r=True)
        cmds.hyperShade(assign='m3')
        cmds.select(nodeName+'.f[4]', r=True)
        cmds.hyperShade(assign='m4')
        cmds.select(nodeName+'.f[5]', r=True)
        cmds.hyperShade(assign='m5')
                
        # reference data
        '''
        How to generate these reference data:
        open /scenes/test.ma, and run the following scripts

RX_ref = [0]
RY_ref = [0]
RZ_ref = [0]

for i in range(1, 100):
    cmds.currentTime( i )
    
    rx = cmds.getAttr('objects:pCube1.rx')
    ry = cmds.getAttr('objects:pCube1.ry')
    rz = cmds.getAttr('objects:pCube1.rz')
          
    RX_ref.append(rx);
    RY_ref.append(ry);
    RZ_ref.append(rz);
    
print 'RX_ref =', RX_ref;
print 'RY_ref =', RY_ref;
print 'RZ_ref =', RZ_ref;
        '''
        RX_ref = [0, 1.0, 1.030098973610628, 1.119579640415681, 1.2672176671571516, 1.4717885944713283, 1.7320682036024302, 2.046831943891474, 2.4148558163919227, 2.834914929784244, 3.3057851642706106, 3.8262425454250377, 4.395061963870215, 5.011019867283982, 5.672890520139169, 6.379451450769432, 7.129477378153598, 7.921742772561997, 8.755025535390988, 9.628100196460846, 10.539741057900962, 11.488726362643973, 12.47383046711481, 13.493827521065151, 14.54749607743748, 15.63360875870332, 16.750945927071662, 17.898276824680597, 19.0743802426723, 20.27803530146487, 21.508010749282416, 22.763085629478496, 24.042039272647376, 25.343639996152028, 26.666667064030985, 28.009899985528257, 29.37210671019853, 30.752066691238863, 32.148559585016606, 33.560353036330994, 34.986226656011226, 36.42496021602152, 37.87532111945853, 39.33608910325716, 40.806044023452074, 42.28395310449023, 43.76859617788576, 45.25875315221738, 46.753191136335175, 48.250694312399915, 49.750025456129784, 51.24997735990246, 52.74931707652109, 54.246811672800675, 55.741253920101165, 57.231410871225776, 58.716049677060006, 60.1939629618404, 61.66391784142307, 63.124681613816854, 64.57504663002923, 66.01378013202203, 67.43964962798033, 68.85144706957679, 70.24793988793179, 71.62789586445955, 72.99010642539214, 74.33333925406141, 75.65636246816355, 76.95796684238438, 78.23692037532001, 79.49199158400145, 80.72197046546258, 81.92562539661667, 83.10172535688235, 84.24905943953586, 85.36639646286045, 86.45250593171532, 87.50617590951329, 88.5261757189605, 89.51127545340948, 90.46026202030397, 91.37190530982586, 92.24497606687387, 93.07825991681699, 93.87052738036564, 94.62054991701764, 95.32711174396147, 95.98898407548894, 96.60493914874985, 97.17375964664645, 97.69421754010483, 98.16508590697956, 98.58514576978104, 98.95316991912243, 99.26793233661593, 99.52821225827432, 99.73278234860607, 99.88042126213608, 99.96990125703542]
        RY_ref = [0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        RZ_ref = [0, 1.0, 1.030098973610628, 1.119579640415681, 1.2672176671571516, 1.4717885944713283, 1.7320682036024302, 2.046831943891474, 2.4148558163919227, 2.834914929784244, 3.3057851642706106, 3.8262425454250377, 4.395061963870215, 5.011019867283982, 5.672890520139169, 6.379451450769432, 7.129477378153598, 7.921742772561997, 8.755025535390988, 9.628100196460846, 10.539741057900962, 11.488726362643973, 12.47383046711481, 13.493827521065151, 14.54749607743748, 15.63360875870332, 16.750945927071662, 17.898276824680597, 19.0743802426723, 20.27803530146487, 21.508010749282416, 22.763085629478496, 24.042039272647376, 25.343639996152028, 26.666667064030985, 28.009899985528257, 29.37210671019853, 30.752066691238863, 32.148559585016606, 33.560353036330994, 34.986226656011226, 36.42496021602152, 37.87532111945853, 39.33608910325716, 40.806044023452074, 42.28395310449023, 43.76859617788576, 45.25875315221738, 46.753191136335175, 48.250694312399915, 49.750025456129784, 51.24997735990246, 52.74931707652109, 54.246811672800675, 55.741253920101165, 57.231410871225776, 58.716049677060006, 60.1939629618404, 61.66391784142307, 63.124681613816854, 64.57504663002923, 66.01378013202203, 67.43964962798033, 68.85144706957679, 70.24793988793179, 71.62789586445955, 72.99010642539214, 74.33333925406141, 75.65636246816355, 76.95796684238438, 78.23692037532001, 79.49199158400145, 80.72197046546258, 81.92562539661667, 83.10172535688235, 84.24905943953586, 85.36639646286045, 86.45250593171532, 87.50617590951329, 88.5261757189605, 89.51127545340948, 90.46026202030397, 91.37190530982586, 92.24497606687387, 93.07825991681699, 93.87052738036564, 94.62054991701764, 95.32711174396147, 95.98898407548894, 96.60493914874985, 97.17375964664645, 97.69421754010483, 98.16508590697956, 98.58514576978104, 98.95316991912243, 99.26793233661593, 99.52821225827432, 99.73278234860607, 99.88042126213608, 99.96990125703542]

        for i in range(1, 100):
            cmds.currentTime( i )
            
            rx = cmds.getAttr(nodeName+'.rx')
            ry = cmds.getAttr(nodeName+'.ry')
            rz = cmds.getAttr(nodeName+'.rz')
        
            self.assertAlmostEqual(rx,  RX_ref[i],  places=7, msg='time: '+str(i)+', '+str(rx)+'!='+str(RX_ref[i]))
            self.assertAlmostEqual(ry,  RY_ref[i],  places=7, msg='time: '+str(i)+', '+str(ry)+'!='+str(RY_ref[i]))
            self.assertAlmostEqual(rz,  RZ_ref[i],  places=7, msg='time: '+str(i)+', '+str(rz)+'!='+str(RZ_ref[i]))
        
        # render image ------------------------------
        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)   
        # render image end---------------------------                 
        
        pass

        
if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
