import _alembic_hom_extensions as abc 
import hou
print '\n\n\n\n\n\n##############################################'


def test(nodePath, meshPathInAbc):
    print('test=================================')
    node = hou.node(nodePath)
    p = node.parm('fileName')
    AbcFilePath = p.eval()
    #print AbcFilePath
    
    # what are the crease set on the mesh
    creaseSetsString = abc.alembicUserProperty(AbcFilePath, meshPathInAbc, 'crease____set', 1)[0][0]
    #print creaseSetsString
    creaseSetsArray =creaseSetsString.split(';')
    print 'This mesh has these creaseSets: ',creaseSetsArray
    
    for creaseSet in creaseSetsArray:
        print '--------'
        name   = abc.alembicUserProperty(AbcFilePath, meshPathInAbc, creaseSet+'name',    1)
        owner  = abc.alembicUserProperty(AbcFilePath, meshPathInAbc, creaseSet+'owner',   1)
        level  = abc.alembicUserProperty(AbcFilePath, meshPathInAbc, creaseSet+'level',   1)
        edgecnt= abc.alembicUserProperty(AbcFilePath, meshPathInAbc, creaseSet+'edgecnt', 1)
        edges  = abc.alembicUserProperty(AbcFilePath, meshPathInAbc, creaseSet+'edges',   1)
        vertex = abc.alembicUserProperty(AbcFilePath, meshPathInAbc, creaseSet+'vertex',  1)
        print '   name:', name
        print '  owner:', owner
        print '  level:', level
        print 'edgecnt:', edgecnt
        print '  edges:', edges
        print ' vertex:', vertex
test('/obj/geo1/alembic1', '/objects:pCylinder1/objects:pCylinderShape1')
test('/obj/geo1/alembic1', '/objects:pCube1/objects:pCubeShape1')'''
output:
##############################################
test=================================
This mesh has these creaseSets:  ['crease__objects_cyCreaseSet0', 'crease__objects_cyCreaseSet1']
--------
   name: (['objects:cyCreaseSet0'], True)
  owner: (['|objects:pCylinder1|objects:pCylinderShape1'], True)
  level: ([10.0], True)
edgecnt: ([20], True)
  edges: ([20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39], True)
 vertex: ([20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33, 33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 20], True)
--------
   name: (['objects:cyCreaseSet1'], True)
  owner: (['|objects:pCylinder1|objects:pCylinderShape1'], True)
  level: ([8.0], True)
edgecnt: ([3], True)
  edges: ([57, 77, 97], True)
 vertex: ([17, 37, 40, 17, 37, 41], True)
test=================================
This mesh has these creaseSets:  ['crease__objects_cubeCreaseSet0']
--------
   name: (['objects:cubeCreaseSet0'], True)
  owner: (['|objects:pCube1|objects:pCubeShape1'], True)
  level: ([8.0], True)
edgecnt: ([2], True)
  edges: ([22, 25], True)
 vertex: ([6, 9, 9, 12], True)

'''
