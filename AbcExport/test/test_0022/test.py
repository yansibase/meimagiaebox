import os
import sys
import time
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

#===============================================================================
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        self.crease()
        
        log.debug('---- end   %s.test_animation() ----', self.m_id);
                
    def crease(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

        _cache_base_name = 'test.ma.crease__.abc'
        
        # export abc cache --------------------------
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 2  -userAttrPrefix "crease__"   ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
        ##############
        import AbcExportCreaseSet as CreaseSet
        cs0 = CreaseSet.CreaseSetOp().getAttrs('|objects:pCylinder1|objects:pCylinderShape1', 'objects:cyCreaseSet0')
        cs1 = CreaseSet.CreaseSetOp().getAttrs('|objects:pCylinder1|objects:pCylinderShape1', 'objects:cyCreaseSet1')
        cs2 = CreaseSet.CreaseSetOp().getAttrs('|objects:pCube1|objects:pCubeShape1',         'objects:cubeCreaseSet0')
        creaseSets_cylinder = cmds.getAttr('|objects:pCylinder1|objects:pCylinderShape1.crease____set')
        creaseSets_cube     = cmds.getAttr('|objects:pCube1|objects:pCubeShape1.crease____set')
        
        self.assertEqual(creaseSets_cylinder,    'crease__objects_cyCreaseSet0;crease__objects_cyCreaseSet1')
        self.assertEqual(creaseSets_cube,        'crease__objects_cubeCreaseSet0')
        ##############        
        
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
        # 
        cs0_ex = CreaseSet.CreaseSetOp().getAttrs('pCylinderShape1', 'objects:cyCreaseSet0')
        cs1_ex = CreaseSet.CreaseSetOp().getAttrs('pCylinderShape1', 'objects:cyCreaseSet1')
        cs2_ex = CreaseSet.CreaseSetOp().getAttrs('pCubeShape1',     'objects:cubeCreaseSet0')
        
        # compare the data
        self.assertEqual(cs0.name,    cs0_ex.name)
        self.assertEqual(cs0.owner,   cs0_ex.owner)  
        self.assertEqual(cs0.level,   cs0_ex.level)
        self.assertEqual(cs0.edgecnt, cs0_ex.edgecnt)
        self.assertEqual(cs0.edges,   cs0_ex.edges)
        self.assertEqual(cs0.vertex,  cs0_ex.vertex)        

        creaseSets_cylinder = cmds.getAttr('pCylinderShape1.crease____set')
        creaseSets_cube     = cmds.getAttr('pCubeShape1.crease____set')
        
        self.assertEqual(creaseSets_cylinder,    'crease__objects_cyCreaseSet0;crease__objects_cyCreaseSet1')
        self.assertEqual(creaseSets_cube,        'crease__objects_cubeCreaseSet0')
        
    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
