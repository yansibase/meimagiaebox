import os
import sys
import time
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

#===============================================================================
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        cache_base_name = 'test.ma.trueNorth-high.abc'
        selected_obj = [u'hull_geo', u'mastBVBeam_geo', u'mastBV01BLK_line', u'mastBV02BLK_line', u'mastBV03BLK_line', u'mastBV04BLK_line', u'mastBV05BLK_line', u'mastBV06BLK_line', u'mastBV07BLK_line', u'mastBV08BLK_line', u'mastBV09BLK_line', u'mastBV10BLK_line', u'mastBV11BLK_line', u'mastBV12BLK_line', u'mastBV13BLK_line', u'mastBV14BLK_line', u'mastBV15BLK_line', u'mastBV16BLK_line', u'mastBV17BLK_line', u'mastBV18BLK_line', u'mastBV22BLK_line', u'mastBV23BLK_line', u'mastBV24BLK_line', u'mastBV27BLK_line', u'mastBV28BLK_line', u'mastBV29BLK_line']
        jobArgs_except_filepath = ' -selection -filterFaceWithSet -stripNamespaces -uvWrite -writeuvsets -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root objects:HIGH_GEO|objects:hullAll|objects:hull_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp -root objects:mastBV_grp  '
        self.trueNorth(cache_base_name, selected_obj, jobArgs_except_filepath, 1, 2)
        
        cache_base_name = 'test.ma.trueNorth-ani.abc'
        selected_obj = [u'hull_ani']
        jobArgs_except_filepath = ' -selection -filterFaceWithSet -stripNamespaces -uvWrite -writeuvsets -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root objects:ANI_GEO|objects:hullAll|objects:hull_grp  '
        self.trueNorth(cache_base_name, selected_obj, jobArgs_except_filepath, 3, 4)
        
        cache_base_name = 'test.ma.trueNorth-efx.abc'
        selected_obj = [u'hull_cageShape']
        jobArgs_except_filepath = ' -selection -filterFaceWithSet -stripNamespaces -uvWrite -writeuvsets -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root objects:hull_cage  '
        self.trueNorth(cache_base_name, selected_obj, jobArgs_except_filepath, 5, 6)
        
        # move the output abc and echo files and compare them with the reference files
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        
        # move the output images and compare the images
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)   
            
        log.debug('---- end   %s.test_animation() ----', self.m_id);
                
    def trueNorth(self, cache_base_name, selected_obj, jobArgs_except_filepath, startFrame, endFrame):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

        _cache_base_name = cache_base_name
        
        # export abc cache --------------------------
        #{'|master|HIGH_GEO': u'-selection  -stripNamespaces -uvWrite -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root teethUP -root teethDn -root l_eyeball -root l_eyeball -root r_eyeball -root r_eyeball -root glass -root glass -root l_eyeOut -root l_eyeOut -root r_eyeOut -root r_eyeOut -root leafAll -root caruncular -root wuba -root wuba -file /isilon/TST/asset/chr/wuba/mod/publish/wuba_mod_v001/wuba_mod_v001_u001/wuba.abc '}
        selected_obj_ref = ['objects:'+obj for obj in selected_obj ] # add 'objects:' to each element of the list
        cmds.select(selected_obj_ref, r=True)
            
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=jobArgs_except_filepath)

        # export abc cache end-----------------------
        
     
        
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_maya_project_dir+'/cache/alembic/'+_cache_base_name, mode='import')
        
         
        # assign material
        #faceset=['l_armBk01_mtl', 'l_armBk02_mtl', 'l_armBk03_mtl', 'l_armFt01_mtl', 'l_armFt02_mtl', 'l_armFt03_mtl', 'l_armFt04_mtl', 'l_armFt05_mtl']
        cmds.select(selected_obj, r=True, noExpand=True)
        cmds.pickWalk(direction='down')
        cmds.hyperShade(assign='lambert3')


        cmds.setAttr("defaultRenderGlobals.startFrame", startFrame);
        cmds.setAttr("defaultRenderGlobals.endFrame", endFrame);
        # render image ------------------------------
        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        # render image end---------------------------                 
        
    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
