import os
import sys
import time
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

#===============================================================================
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        self.wuba()
        
        log.debug('---- end   %s.test_animation() ----', self.m_id);
                
    def wuba(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

        _cache_base_name = 'test.ma.wuba.abc'
        
        # export abc cache --------------------------
        #{'|master|HIGH_GEO': u'-selection  -stripNamespaces -uvWrite -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root teethUP -root teethDn -root l_eyeball -root l_eyeball -root r_eyeball -root r_eyeball -root glass -root glass -root l_eyeOut -root l_eyeOut -root r_eyeOut -root r_eyeOut -root leafAll -root caruncular -root wuba -root wuba -file /isilon/TST/asset/chr/wuba/mod/publish/wuba_mod_v001/wuba_mod_v001_u001/wuba.abc '}
        selected_obj=[u'objects:teethUPShape', u'objects:teethDnShape', u'objects:l_eyeballShape', u'objects:l_eyeballShapeOrig', u'objects:r_eyeballShape', u'objects:r_eyeballShapeOrig', u'objects:glassShape', u'objects:glassShapeOrig', u'objects:l_eyeOutShape', u'objects:l_eyeOutShapeOrig', u'objects:r_eyeOutShape', u'objects:r_eyeOutShapeOrig', u'objects:leafAllShape', u'objects:caruncularShape', u'objects:wubaShape', u'objects:wubaShapeOrig']
        cmds.select(selected_obj, r=True)
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -selection -filterFaceWithSet -stripNamespaces -uvWrite -writeuvsets -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root objects:teethUP -root objects:teethDn -root objects:l_eyeball -root objects:l_eyeball -root objects:r_eyeball -root objects:r_eyeball -root objects:glass -root objects:glass -root objects:l_eyeOut -root objects:l_eyeOut -root objects:r_eyeOut -root objects:r_eyeOut -root objects:leafAll -root objects:caruncular -root objects:wuba -root objects:wuba ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
     
        
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
         
        # assign material
        #faceset=['l_armBk01_mtl', 'l_armBk02_mtl', 'l_armBk03_mtl', 'l_armFt01_mtl', 'l_armFt02_mtl', 'l_armFt03_mtl', 'l_armFt04_mtl', 'l_armFt05_mtl']
        faceset=['wuba']
        cmds.select(faceset, r=True, noExpand=True)
        cmds.pickWalk(direction='down')
        cmds.hyperShade(assign='lambert3')

        
        # render image ------------------------------
        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)   
        # render image end---------------------------                 
        
    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
