import os
import sys
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()


        self.userattrprefix();
                
        log.debug('---- end   %s.test_animation() ----', self.m_id);

    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass
        
    def adduserattr(self, nodeName):
        
        cmds.addAttr(nodeName, ln='myAttr_float0', sn='ma_f0', at='float', readable=True, writable=True, storable=True, keyable=True)
        cmds.setKeyframe( nodeName, attribute='myAttr_float0', t=1 , v=1)
        cmds.setKeyframe( nodeName, attribute='myAttr_float0', t=10, v=10)

        cmds.addAttr(nodeName, ln='myAttr_int0',   sn='ma_i0', at='long',  readable=True, writable=True, storable=True, keyable=True)
        cmds.setKeyframe( nodeName, attribute='myAttr_int0', t=1 , v=2)
        cmds.setKeyframe( nodeName, attribute='myAttr_int0', t=10, v=20)
       
        cmds.addAttr(nodeName, ln='myAttr2_float0', sn='ma2_f0', at='float', readable=True, writable=True, storable=True, keyable=True)
        cmds.setKeyframe( nodeName, attribute='myAttr2_float0', t=1 , v=1)
        cmds.setKeyframe( nodeName, attribute='myAttr2_float0', t=10, v=10)

        cmds.addAttr(nodeName, ln='myAttr2_int0',   sn='ma2_i0', at='long',  readable=True, writable=True, storable=True, keyable=True)
        cmds.setKeyframe( nodeName, attribute='myAttr2_int0', t=1 , v=2)
        cmds.setKeyframe( nodeName, attribute='myAttr2_int0', t=10, v=20)
       
        cmds.addAttr(nodeName, ln='myAttr3_float0', sn='ma3_f0', at='float', readable=True, writable=True, storable=True, keyable=True)
        cmds.setKeyframe( nodeName, attribute='myAttr3_float0', t=1 , v=1)
        cmds.setKeyframe( nodeName, attribute='myAttr3_float0', t=10, v=10)

        cmds.addAttr(nodeName, ln='myAttr3_int0',   sn='ma3_i0', at='long',  readable=True, writable=True, storable=True, keyable=True)
        cmds.setKeyframe( nodeName, attribute='myAttr3_int0', t=1 , v=2)
        cmds.setKeyframe( nodeName, attribute='myAttr3_int0', t=10, v=20)
        pass
        
    def userattrprefix(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        
        self.adduserattr('objects:pCubeShape1');        
        #self.adduserattr('objects:pCubeShape2'); # this object already has userattr
                
        _cache_base_name = 'test.ma.userattrprefix.abc'
        
        # export abc cache --------------------------
        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -frameRange 1 10 -userattrprefix "myAttr_" -userattrprefix "myAttr2_" ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
        nodeName = 'pCubeShape1' # the imported data doesn't has namespace 'objects'
        # reference data
        '''
        How to generate these reference data:
        open /scenes/test.ma, and run the following scripts

float0_ref  = [0]
int0_ref    = [0]
float30_ref = [(0,0,0)]
float31_ref = [(0,0,0)]

for i in xrange(1, 11):
    cmds.currentTime( i )
    
    float0  = cmds.getAttr('objects:pCubeShape1.myAttr_float0')
    int0    = cmds.getAttr('objects:pCubeShape1.myAttr_int0')
    float30 = cmds.getAttr('objects:pCubeShape1.myAttr_float30')[0]
    #float31 = cmds.getAttr('objects:pCubeShape1.myAttr_float31')[0]
          
    float0_ref.append(float0);
    int0_ref.append(int0);    
    float30_ref.append(float30); 
    #float31_ref.append(float31); 
    
print 'float0_ref  =', float0_ref;
print 'int0_ref    =', int0_ref;
print 'float30_ref =', float30_ref;
#print 'float31_ref=', float31_ref;
        '''
        float0_ref  = [0, 1.0, 1.308642029762268, 2.1358025074005127, 3.3333334922790527, 4.753086566925049, 6.246913909912109, 7.666666507720947, 8.864197731018066, 9.69135856628418, 10.0]
        int0_ref    = [0, 2, 3, 4, 7, 10, 12, 15, 18, 19, 20]
        float30_ref = [(0, 0, 0), (3.0, 3.0, 3.0), (3.9259259700775146, 3.9259259700775146, 3.9259259700775146), (6.407407283782959, 6.407407283782959, 6.407407283782959), (10.000000953674316, 10.000000953674316, 10.000000953674316), (14.259259223937988, 14.259259223937988, 14.259259223937988), (18.740741729736328, 18.740741729736328, 18.740741729736328), (23.0, 23.0, 23.0), (26.592594146728516, 26.592594146728516, 26.592594146728516), (29.074073791503906, 29.074073791503906, 29.074073791503906), (30.0, 30.0, 30.0)]

        for i in range(1, 11):
            cmds.currentTime( i )
            
            float0  = cmds.getAttr(nodeName+'.myAttr_float0')
            int0    = cmds.getAttr(nodeName+'.myAttr_int0')
            
            # compare with the reference data
            self.assertAlmostEqual(float0,  float0_ref[i],  places=7, msg='time: '+str(i)+', '+str(float0)+'!='+str(float0_ref[i]))
            self.assertAlmostEqual(int0,    int0_ref[i],    places=7, msg='time: '+str(i)+', '+str(int0)  +'!='+str(int0_ref[i]))

        pass

        
if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
