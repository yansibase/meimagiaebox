import os
import sys
import time
import unittest
import maya.cmds                as cmds
import mymagicbox.log           as log
import at_helper                as hp
import at_renderer              as rndr
import imagemagick              as im

#===============================================================================
class MyTest00(unittest.TestCase):
    m_id        = ''
    m_helper    = None

    def __init__(self, methodName='runTest'):
        super(MyTest00, self).__init__(methodName)
        self.m_id       = os.path.dirname(os.path.abspath(__file__)) + '/' + self.__class__.__name__
        self.m_helper   = hp.ATHelper()

    def setUp(self):
        log.debug('%s.setUp', self.m_id);
        #super(MyTest00, self).setUp()
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')
        hp.ATHelper.tryToLoadPlugin('AbcExport')
        hp.ATHelper.tryToLoadPlugin('AbcImport')
        
    def tearDown(self):
        log.debug('%s.tearDown', self.m_id);
        #super(MyTest00, self).tearDown()
        #hp.ATHelper.tryToUnloadPlugin('AbcExport')

    def test_animation(self):
        log.debug('---- begin %s.test_animation() ----', self.m_id);
        #super(MyTest00, self).test_animation()

        self.pipeline_mod()
        
        log.debug('---- end   %s.test_animation() ----', self.m_id);
                
    def pipeline_mod(self):
        self.m_helper.openMayaFile(os.path.dirname(__file__)+'/scenes/test.ma')

        _cache_base_name = 'test.ma.pipeline_mod.abc'
        
        # export abc cache --------------------------
        #cmdJobArg ::  {'|master|HIGH_GEO': u'-selection  -stripNamespaces -uvWrite -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root eye_L_glass_outer -root eye_L_glass_inside -root eye_L_pupil -root eye_R_glass_outer -root eye_R_glass_inside -root eye_R_pupil -root Tongue -root nail -root teeth -root tear -root lashR1 -root lashR2 -root lashR3 -root lashR4 -root lashR5 -root lashR6 -root lashR7 -root lashR8 -root lashR9 -root lashR10 -root lashR11 -root lashR12 -root lashR13 -root lashR14 -root lashR15 -root lashR16 -root lashR17 -root lashR18 -root lashR19 -root lashR20 -root lashR21 -root lashR22 -root lashR23 -root lashR24 -root lashR25 -root lashR26 -root lashR27 -root lashR28 -root lashR29 -root lashR30 -root lashR31 -root lashR32 -root lashR33 -root lashR34 -root lashR35 -root lashR36 -root lashR37 -root lashR38 -root lashR39 -root lashR40 -root lashR41 -root lashR42 -root lashR43 -root lashR44 -root lashR45 -root lashR46 -root lashR47 -root lashR48 -root lashR49 -root lashR50 -root lashR51 -root lashR52 -root lashR53 -root lashR54 -root lashR55 -root lashR56 -root lashR57 -root lashR58 -root lashR59 -root lashR60 -root lashR61 -root lashR62 -root lashR63 -root lashR64 -root lashR65 -root lashR66 -root lashR67 -root lashR68 -root lashR69 -root lashR70 -root lashR71 -root lashR72 -root lashR73 -root lashR74 -root lashR75 -root lashR76 -root lashR77 -root lashR78 -root lashL1 -root lashL2 -root lashL3 -root lashL4 -root lashL5 -root lashL6 -root lashL7 -root lashL8 -root lashL9 -root lashL10 -root lashL11 -root lashL12 -root lashL13 -root lashL14 -root lashL15 -root lashL16 -root lashL17 -root lashL18 -root lashL19 -root lashL20 -root lashL21 -root lashL22 -root lashL23 -root lashL24 -root lashL25 -root lashL26 -root lashL27 -root lashL28 -root lashL29 -root lashL30 -root lashL31 -root lashL32 -root lashL33 -root lashL34 -root lashL35 -root lashL36 -root lashL37 -root lashL38 -root lashL39 -root lashL40 -root lashL41 -root lashL42 -root lashL43 -root lashL44 -root lashL45 -root lashL46 -root lashL47 -root lashL48 -root lashL49 -root lashL50 -root lashL51 -root lashL52 -root lashL53 -root lashL54 -root lashL55 -root lashL56 -root lashL57 -root lashL58 -root lashL59 -root lashL60 -root lashL61 -root lashL62 -root lashL63 -root lashL64 -root lashL65 -root lashL66 -root lashL67 -root lashL68 -root lashL69 -root lashL70 -root lashL71 -root lashL72 -root lashL73 -root lashL74 -root lashL75 -root lashL76 -root lashL77 -root lashL78 -root body -file /isilon/TST/asset/chr/catTst/mod/publish/catTst_mod_v006/catTst_mod_v006_u005/catTst.abc '}

        self.m_helper.exportAlembicAndEcho(cache_base_name=_cache_base_name, options_except_filepath=' -filterFaceWithSet -stripNamespaces -uvWrite -worldSpace -writeVisibility -writeFaceSets -faceSetName look -faceSetName deform -faceSetName sdv -faceSetName smoo -faceSetName fur -faceSetName mtl -frameRange 0.0 2.0 -root objects:eye_L_glass_outer -root objects:eye_L_glass_inside -root objects:eye_L_pupil -root objects:eye_R_glass_outer -root objects:eye_R_glass_inside -root objects:eye_R_pupil -root objects:Tongue -root objects:nail -root objects:teeth -root objects:tear -root objects:lashR1 -root objects:lashR2 -root objects:lashR3 -root objects:lashR4 -root objects:lashR5 -root objects:lashR6 -root objects:lashR7 -root objects:lashR8 -root objects:lashR9 -root objects:lashR10 -root objects:lashR11 -root objects:lashR12 -root objects:lashR13 -root objects:lashR14 -root objects:lashR15 -root objects:lashR16 -root objects:lashR17 -root objects:lashR18 -root objects:lashR19 -root objects:lashR20 -root objects:lashR21 -root objects:lashR22 -root objects:lashR23 -root objects:lashR24 -root objects:lashR25 -root objects:lashR26 -root objects:lashR27 -root objects:lashR28 -root objects:lashR29 -root objects:lashR30 -root objects:lashR31 -root objects:lashR32 -root objects:lashR33 -root objects:lashR34 -root objects:lashR35 -root objects:lashR36 -root objects:lashR37 -root objects:lashR38 -root objects:lashR39 -root objects:lashR40 -root objects:lashR41 -root objects:lashR42 -root objects:lashR43 -root objects:lashR44 -root objects:lashR45 -root objects:lashR46 -root objects:lashR47 -root objects:lashR48 -root objects:lashR49 -root objects:lashR50 -root objects:lashR51 -root objects:lashR52 -root objects:lashR53 -root objects:lashR54 -root objects:lashR55 -root objects:lashR56 -root objects:lashR57 -root objects:lashR58 -root objects:lashR59 -root objects:lashR60 -root objects:lashR61 -root objects:lashR62 -root objects:lashR63 -root objects:lashR64 -root objects:lashR65 -root objects:lashR66 -root objects:lashR67 -root objects:lashR68 -root objects:lashR69 -root objects:lashR70 -root objects:lashR71 -root objects:lashR72 -root objects:lashR73 -root objects:lashR74 -root objects:lashR75 -root objects:lashR76 -root objects:lashR77 -root objects:lashR78 -root objects:lashL1 -root objects:lashL2 -root objects:lashL3 -root objects:lashL4 -root objects:lashL5 -root objects:lashL6 -root objects:lashL7 -root objects:lashL8 -root objects:lashL9 -root objects:lashL10 -root objects:lashL11 -root objects:lashL12 -root objects:lashL13 -root objects:lashL14 -root objects:lashL15 -root objects:lashL16 -root objects:lashL17 -root objects:lashL18 -root objects:lashL19 -root objects:lashL20 -root objects:lashL21 -root objects:lashL22 -root objects:lashL23 -root objects:lashL24 -root objects:lashL25 -root objects:lashL26 -root objects:lashL27 -root objects:lashL28 -root objects:lashL29 -root objects:lashL30 -root objects:lashL31 -root objects:lashL32 -root objects:lashL33 -root objects:lashL34 -root objects:lashL35 -root objects:lashL36 -root objects:lashL37 -root objects:lashL38 -root objects:lashL39 -root objects:lashL40 -root objects:lashL41 -root objects:lashL42 -root objects:lashL43 -root objects:lashL44 -root objects:lashL45 -root objects:lashL46 -root objects:lashL47 -root objects:lashL48 -root objects:lashL49 -root objects:lashL50 -root objects:lashL51 -root objects:lashL52 -root objects:lashL53 -root objects:lashL54 -root objects:lashL55 -root objects:lashL56 -root objects:lashL57 -root objects:lashL58 -root objects:lashL59 -root objects:lashL60 -root objects:lashL61 -root objects:lashL62 -root objects:lashL63 -root objects:lashL64 -root objects:lashL65 -root objects:lashL66 -root objects:lashL67 -root objects:lashL68 -root objects:lashL69 -root objects:lashL70 -root objects:lashL71 -root objects:lashL72 -root objects:lashL73 -root objects:lashL74 -root objects:lashL75 -root objects:lashL76 -root objects:lashL77 -root objects:lashL78 -root objects:body ')
        self.m_helper.moveCacheAlembic()
        
        bEchoFilesEqual = self.m_helper.compare_abcecho()
        self.assertEqual(bEchoFilesEqual, True)
        # export abc cache end-----------------------
        
     
        
        
        # open maya file but not load reference. Then import the abc cache and render image
        self.m_helper.openMayaFile2(os.path.dirname(__file__)+'/scenes/test.ma', loadReferenceDepth='none', force=True, open=True, options="v=0", type="mayaAscii")
        cmds.AbcImport(self.m_helper.m_automationtesttmp_cache_alembic_dir+'/'+_cache_base_name, mode='import')
        
         
        # assign material
        cmds.select('body', r=True)
        cmds.hyperShade(assign='lambert3')

        
        # render image ------------------------------
        renderer = rndr.CreateRenderer('mayaHardware')
        renderer.render()
        self.m_helper.moveRenderedImages();

        isImageEqual = self.m_helper.compare_image()
        if isImageEqual:
            log.info('Passed. %s', __file__)
        else:
            self.assertEqual(isImageEqual, True, "Failed. "+__file__)   
        # render image end---------------------------                 
        
    def runTest(self):
        log.debug('\nthis is %s.runTest', self.m_id);
        #super(MyTest00, self).runTest()

        pass



if __name__ == '__main__':
    # if we use mayapy.exe, we must call initilize_maya_standalone()


#    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTest00)
    unittest.TextTestRunner(verbosity=3).run(suite)
