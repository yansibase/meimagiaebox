'''
How to use this script?

On linux platform:
$ cd <this directory>
$ python mybox_build_test_2014.py

On windows platform:
\>cd <this directory>
\>path=D:\Program Files (x86)\CMake\bin;C:\Python27;%path%;
\>python mybox_build_test_2014.py

'''
import os
import sys
import mybox_build_test_template    as template

#################################################
def setBasicConfig(buildtypes_list, install_type):
    """
    Setup the configuration for your automation building
    """
    if sys.platform == 'win32':
        os.environ['AUTO_BUILD_CMAKE_CMD_PATH']                         = 'C:/ProgramFiles/CMake/bin/cmake.exe'
        os.environ['AUTO_BUILD_MAYA_VERSION']                           = 'maya2014-x64'
        os.environ['AUTO_BUILD_MAYA_BASE_DIR']                          = 'C:/Program Files/Autodesk/Maya2014'
        os.environ['AUTO_BUILD_GENERATOR']                              = 'Visual Studio 10 2010 Win64'
        
        os.environ['AUTO_BUILD_ABC_ILMBASE_ROOT']                       = 'd:/dev/alembic_depends/install'
        os.environ['AUTO_BUILD_ABC_MAYA_ROOT']                          = 'C:/Program Files/Autodesk/Maya2014'
        os.environ['AUTO_BUILD_ABC_BOOST_INCLUDEDIR']                   = 'D:/dev/boost/1_48_0'
        os.environ['AUTO_BUILD_ABC_BOOST_LIBRARYDIR']                   = 'D:/dev/boost/1_48_0/lib64'
        os.environ['AUTO_BUILD_ABC_ARNOLD_ROOT']                        = 'D:/dev/alembic_depends/Arnold-4.1.3.3-windows'
        os.environ['AUTO_BUILD_ABC_PRMAN_ROOT']                         = 'D:/dev/pixar/RenderManProServer-20.2'
        os.environ['AUTO_BUILD_ABC_ALEMBIC_PYILMBASE_PYIMATH_MODULE']   = 'D:/dev/alembic_depends/install/lib64/python2.6/site-packages/imathmodule.dll'
                
    elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
        os.environ['AUTO_BUILD_CMAKE_CMD_PATH']                         = '/usr/bin/cmake'
        os.environ['AUTO_BUILD_MAYA_VERSION']                           = 'maya2014-x64'
        os.environ['AUTO_BUILD_MAYA_BASE_DIR']                          = '/usr/autodesk/maya2014-x64'
        os.environ['AUTO_BUILD_GENERATOR']                              = 'CodeBlocks - Unix Makefiles'
        
        os.environ['AUTO_BUILD_ABC_ILMBASE_ROOT']                       = '/backup/lhome/dev/alembic_depends/ilmbase-2.2.0'
        os.environ['AUTO_BUILD_ABC_MAYA_ROOT']                          = '/usr/autodesk/maya2014-x64'
        os.environ['AUTO_BUILD_ABC_BOOST_INCLUDEDIR']                   = '/backup/lhome/dev/alembic_depends/boost/1.48/install/include/boost-1_48'
        os.environ['AUTO_BUILD_ABC_BOOST_LIBRARYDIR']                   = '/backup/lhome/dev/alembic_depends/boost/1.48/install/lib'
        os.environ['AUTO_BUILD_ABC_ARNOLD_ROOT']                        = '/backup/lhome/dev/alembic_depends/Arnold-4.1.3.3-linux'
        os.environ['AUTO_BUILD_ABC_PRMAN_ROOT']                         = '/opt/pixar/RenderManProServer-20.2'
        os.environ['AUTO_BUILD_ABC_ALEMBIC_PYILMBASE_PYIMATH_MODULE']   = '/usr/local/lib64/python2.6/site-packages/imathmodule.so'
        
    os.environ['AUTO_BUILD_BUILD_TYPES']     = template.buildtype_list2str(buildtypes_list)
    os.environ['AUTO_BUILD_INSTALL_TYPE']   = install_type # we may build many types(Debug, Release), but which one will be installed for Maya?
        
#################################################

def main(buildtypes_list, install_type):
    setBasicConfig(buildtypes_list, install_type)
    template.build()
    #template.test()


if '__main__' == __name__:
    buildtypes_list = ['Release', 'Debug']
    install_type = 'Release' # we may build many types(Debug, Release), but which one will be installed for Maya?

    main(buildtypes_list, install_type);
