import os
import sys
    
#################################################
def setBasicConfig():
    """
    Setup the configuration for your automation building
    """
    if sys.platform == 'win32':
        os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = 'cmake.exe'
        os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2014-x64'
        os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = 'C:/Program Files/Autodesk/Maya2014'
        os.environ['AUTO_BUILD_GENERATOR']      = 'Visual Studio 10 2010 Win64'
    elif (sys.platform == 'linux') or (sys.platform == 'linux2'):
        os.environ['AUTO_BUILD_CMAKE_CMD_PATH'] = '/usr/bin/cmake'
        os.environ['AUTO_BUILD_MAYA_VERSION']   = 'maya2014-x64'
        os.environ['AUTO_BUILD_MAYA_BASE_DIR']  = '/usr/autodesk/maya2014-x64'
        os.environ['AUTO_BUILD_GENERATOR']      = 'CodeBlocks - Unix Makefiles'
        
    os.environ['AUTO_BUILD_BUILD_TYPES']     = 'Release Debug'
    os.environ['AUTO_BUILD_INSTALL_TYPE']   = 'Release' # we may build many types(Debug, Release), but which one will be installed for Maya?       
#################################################
    
def buildtype_list2str(buildtype_list):
    buildtype_str = ' '.join(buildtype_list)
    # print 'buildtype_str=', buildtype_str
    return buildtype_str

def buildtype_str2list(buildtype_str):
    buildtype_list = buildtype_str.split(' ')
    # print 'buildtype_list=', buildtype_list
    return buildtype_list
    
def __log(format, *params):
    print('auto_build| '+format % params);

def debug_dir():
    __log("Path at terminal when executing this file")
    __log(os.getcwd() + "\n")

    __log("This file path, relative to os.getcwd()")
    __log(__file__ + "\n")

    __log("This file full path (following symlinks)")
    full_path = os.path.realpath(__file__)
    __log(full_path + "\n")

    __log("This file directory and name")
    path, file = os.path.split(full_path)
    __log(path + ' --> ' + file + "\n")

    __log("This file directory only")
    __log(os.path.dirname(full_path))
 
def build():
    __log('\n\n\n\n')
    __log('---------------------------------------')
    __log('   Build and Install                   ')
    __log('---------------------------------------')

    #debug_dir()  # debug 
    
    current_file_path = os.path.realpath(__file__)
    current_file_path = current_file_path.replace('\\','/')
    current_file_dir  = os.path.dirname(current_file_path)
    
    scriptFileFullPath = current_file_dir+'/build/cmake/cmake_setup.py'
    plugin= 'alembic'
    if os.path.isfile(scriptFileFullPath):
        __log('%s has automation building scripts, build it.', plugin)
        #execfile(scriptFileFullPath) # it's weird that python complains errors: NameError: global name 'log' is not defined
        os.system('python '+scriptFileFullPath)
        __log('done.(%s)\n\n\n\n\n', plugin)
            
    __log('---------------------------------------')
    __log('   Build and Install Ends           ')
    __log('---------------------------------------')
    
def test():
    __log('\n\n\n\n')
    __log('---------------------------------------')
    __log('   Test                                ')
    __log('---------------------------------------')
    #debug_dir()  # debug
    
    current_file_path = os.path.realpath(__file__)
    current_file_path = current_file_path.replace('\\','/')
    current_file_dir  = os.path.dirname(current_file_path)
    
    MAYA_BASE_DIR  = os.environ['AUTO_BUILD_MAYA_BASE_DIR']
    
    cmd = '"'+MAYA_BASE_DIR+'/bin/maya"'
    cmd+= ' -script "'+current_file_dir+'/mybox_test_cmd.mel"' 
    __log(cmd)
    import subprocess
    ps = subprocess.Popen(cmd, shell=True); 
    ps.wait();
    
    __log("automation test is finished (exit Maya in mybox_test_cmd.mel)");
    
    __log('---------------------------------------')
    __log('   Test Ends                               ')
    __log('---------------------------------------')

if '__main__' == __name__:
    setBasicConfig() 
    build();
    test();
