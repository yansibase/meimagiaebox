--------------
AbcExport command options
--------------

Options:
-h / -help  Print this message.

-prs / -preRollStartFrame double
The frame to start scene evaluation at.  This is used to set the starting frame for time dependent translations and can be used to evaluate run-up that isn't actually translated.

-duf / -dontSkipUnwrittenFrames
When evaluating multiple translate jobs, the presence of this flag decides whether to evaluate frames between jobs when there is a gap in their frame ranges.

-v / -verbose
Prints the current frame that is being evaluated.
    Examples & Output:
        cmds.AbcExport(verbose=True, jobArg='-frameRange 1 10 -file "test7.abc"')
        # 1 # 
        # 2 # 
        # 3 # 
        # 4 # 
        # 5 # 
        # 6 # 
        # 7 # 
        # 8 # 
        # 9 # 
        # 10 # 

-j / -jobArg string REQUIRED (multi)
String which contains flags for writing data to a particular file. Multiple jobArgs can be specified.

-jobArg flags:
    ------------------------------------------------------------------------
    -df / -dataFormat string
    The data format to use to write the file.  Can be either HDF or Ogawa. The default is Ogawa.
    
        Examples:
            cmds.AbcExport(verbose=True, j='-frameRange 1 10  -dataFormat "hdf" -file "test_hdf2.abc"')
            cmds.AbcExport(verbose=True, j='-frameRange 1 10  -dataFormat "ogawa" -file "test_ogawa2.abc"')
        Note:
            Ogawa has higher compression rate and better performance than HDF.

    ------------------------------------------------------------------------
    -a / -attr string (multi)
    A specific geometric attribute to write out. This flag may occur more than once.
        Examples & Output:
        cmds.AbcExport(jobArg='-frameRange 1 10 -attr "translateX" -attr "rotateX" -attr "scaleX" -file "test-attr.abc"');
        
        The attributes 'translateX', 'rotateX', 'scaleX' will be exported, like this:
        Object name=/camera1
          CompoundProperty name=.xform;schema=AbcGeom_Xform_v3
            CompoundProperty name=.arbGeomParams;schema=
              ArrayProperty name=translateX;interpretation=;datatype=float64_t;arraysize=1;numsamps=1
              ArrayProperty name=rotateX;interpretation=;datatype=float64_t;arraysize=1;numsamps=1
              ArrayProperty name=scaleX;interpretation=;datatype=float64_t;arraysize=1;numsamps=1

    ------------------------------------------------------------------------
    -atp / -attrPrefix string (default ABC_) (multi)
    Prefix filter for determining which geometric attributes to write out. This flag may occur more than once.
        Examples & Output:
        cmds.AbcExport(jobArg='-frameRange 1 10 -attrprefix "translate" -attrprefix "scale" -file "test-attrprefix.abc"');
        
        The attributes 'translate', 'scale', 'scalePivot', 'scalePivotTranslate' will be exported, like this:
        Object name=/camera1
          CompoundProperty name=.xform;schema=AbcGeom_Xform_v3
            CompoundProperty name=.arbGeomParams;schema=
              ArrayProperty name=translate;interpretation=vector;datatype=float64_t[3];arraysize=1;numsamps=1
              ArrayProperty name=scale;interpretation=vector;datatype=float64_t[3];arraysize=1;numsamps=1
              ArrayProperty name=scalePivot;interpretation=vector;datatype=float64_t[3];arraysize=1;numsamps=1
              ArrayProperty name=scalePivotTranslate;interpretation=vector;datatype=float64_t[3];arraysize=1;numsamps=1
              
    ------------------------------------------------------------------------
    -u / -userattr string (multi)
    A specific user attribute to write out.  This flag may occur more than once.
    
        Examples & Output:
            cmds.AbcExport(jobArg='-frameRange 1 10 -userattr "myAttr_float0" -userattr "myAttr_int0" -userattr "myAttr_float30" -userattr "myAttr_float31" -file "test-userattr.abc"');
        
        The attributes 'myAttr_float0', 'myAttr_int0', 'myAttr_float30' will be exported, like this:
        Object name=/objects:pCube1/objects:pCubeShape1
          CompoundProperty name=.geom;schema=AbcGeom_PolyMesh_v1
            ...
            CompoundProperty name=.userProperties;schema=
              ScalarProperty name=myAttr_float0;interpretation=;datatype=float32_t;arraysize=1;numsamps=10
              ScalarProperty name=myAttr_int0;interpretation=;datatype=int32_t;arraysize=1;numsamps=10
              ScalarProperty name=myAttr_float30;interpretation=vector;datatype=float32_t[3];arraysize=3;numsamps=10
            ...

    ------------------------------------------------------------------------
    -uatp / -userattrprefix string (multi)
    Prefix filter for determining which user attributes to write out. This flag may occur more than once.
    
        Examples & Output:
            cmds.AbcExport(jobArg='-frameRange 1 10 -userattrprefix "myAttr_" -userattrprefix "myAttr2_" -file "test-userattrprefix.abc"');
        
        The attributes 'myAttr_float0', 'myAttr_int0', 'myAttr_float30', 'myAttr2_float0', 'myAttr2_int0' will be exported, like this:
        Object name=/objects:pCube1/objects:pCubeShape1
          CompoundProperty name=.geom;schema=AbcGeom_PolyMesh_v1
            ...
            CompoundProperty name=.userProperties;schema=
              ScalarProperty name=myAttr_float0;interpretation=;datatype=float32_t;arraysize=1;numsamps=10
              ScalarProperty name=myAttr_int0;interpretation=;datatype=int32_t;arraysize=1;numsamps=10
              ScalarProperty name=myAttr2_float0;interpretation=;datatype=float32_t;arraysize=1;numsamps=10
              ScalarProperty name=myAttr2_int0;interpretation=;datatype=int32_t;arraysize=1;numsamps=10
              ...
        Object name=/objects:pCube2/objects:pCubeShape2
          CompoundProperty name=.geom;schema=AbcGeom_PolyMesh_v1
            ...
            CompoundProperty name=.userProperties;schema=
              ScalarProperty name=myAttr_float0;interpretation=;datatype=float32_t;arraysize=1;numsamps=10
              ScalarProperty name=myAttr_int0;interpretation=;datatype=int32_t;arraysize=1;numsamps=10
              ScalarProperty name=myAttr_float30;interpretation=vector;datatype=float32_t[3];arraysize=3;numsamps=10
              ...

    ------------------------------------------------------------------------
    -ef / -eulerfilter
    If this flag is present, apply Euler filter while sampling rotations.
        Examples & Output:
            cmds.AbcExport(jobArg='-frameRange 1 10 -eulerfilter -file "test-eulerfilter.abc"');

    ------------------------------------------------------------------------
    -f / -file string REQUIRED
    File location to write the Alembic data.


    -fr / -framerange double double (multi)
    The frame range to write. Multiple occurrences of -framerange are supported within a job. Each -frameRange defines a new frame range. -step or -frs will affect the current frame range only.
    
    -s / -step double (default 1.0)
    The time interval (expressed in frames) at which the frame range is sampled. Additional samples around each frame can be specified with -frs.

        Examples & Output:
            cmds.AbcExport(jobArg='-frameRange 1 10 -step 0.1 -file "test-step.abc"');
        
        The attribute numsamps is changed in abc file, like this:
          ScalarProperty name=.childBnds;interpretation=box;datatype=float64_t[6];arraysize=6;numsamps=91
          ...
        Object name=/objects:pCube1
          CompoundProperty name=.xform;schema=AbcGeom_Xform_v3
            ScalarProperty name=.inherits;interpretation=;datatype=bool_t;arraysize=1;numsamps=91
            ScalarProperty name=.ops;interpretation=;datatype=uint8_t[3];arraysize=3;numsamps=91
            ScalarProperty name=.vals;interpretation=;datatype=float64_t[3];arraysize=3;numsamps=91
    
    ------------------------------------------------------------------------    
    -frs / -frameRelativeSample double (multi)
    frame relative sample that will be written out along the frame range. This flag may occur more than once.
        AbcExport -j "-fr 0 10 -frs -0.1 -frs 0.2 -step 5 -file /tmp/test.abc"
        Writes out everything in the scene to /tmp/test.abc sampling at frames: -0.1 0.2  4.9 5.2  9.9 10.2

    Note: The difference between your highest and lowest frameRelativeSample can not be greater than your step size.
        AbcExport -j "-step 0.25 -frs 0.3 -frs 0.60 -fr 1 5 -root foo -file test.abc"
        Is illegal because the highest and lowest frameRelativeSamples are 0.3 frames apart.
        
    ------------------------------------------------------------------------  
    -nn / -noNormals
    If this flag is present normal data for Alembic poly meshes will not be written.
    
        Examples & Output:
        AbcExport -j "-frameRange 1 10 -noNormals -file /tmp/test-noNormals.abc"  
        
        The attribute N is not ecxported at all, like this:
        Object name=/objects:pCube1/objects:pCubeShape1
          CompoundProperty name=.geom;schema=AbcGeom_PolyMesh_v1
            ScalarProperty name=.selfBnds;interpretation=box;datatype=float64_t[6];arraysize=6;numsamps=1
            ArrayProperty name=P;interpretation=point;datatype=float32_t[3];arraysize=8;numsamps=1
            ArrayProperty name=.faceIndices;interpretation=;datatype=int32_t;arraysize=24;numsamps=1
            ArrayProperty name=.faceCounts;interpretation=;datatype=int32_t;arraysize=6;numsamps=1      
    
    ------------------------------------------------------------------------  
    -pr / -preRoll
    If this flag is present, this frame range will not be sampled.
        Examples & Output:
        AbcExport -j " -frameRange 1 10 -preRoll  -frameRange 11 20  -frameRange 21 30 -preRoll -file /tmp/test-noNormals.abc"  

        frameRange1-10 and frameRange21-30 are not sampled at all. It means that frame11-20 are same as frame10, frame31-end are same as frame30.
        
    ------------------------------------------------------------------------ 
    
    -ro / -renderableOnly
    If this flag is present non-renderable hierarchy (invisible, or templated, or intermediate) will not be written out.

    ------------------------------------------------------------------------ 
    -rt / -root (multi)
    Maya dag path which will be parented to the root of the Alembic file.
    This flag may occur more than once.  If unspecified, it defaults to '|' which
    means the entire scene will be written out.

    Examples & Output:
        Maya scene:
            A
            |`-AShape
            |
            +--B
            |  |`-BShape
            |  |
            |  +--C
            |     `-CShape
            |  
            +--B2
                `-BShape2
                
            D
            |`-DShape
            |
            +--E
                `-EShape
                
        Export command:
            AbcExport -j " -frameRange 1 10 -root "|A|B|C" -root "|A|B2"  -root "|D|E" -file test-root.abc"
        
        Object layers in ABC cache:
            Object name=/B2
            ...
            Object name=/B2/BShape2
            ...
            Object name=/C
            ...
            Object name=/C/CShape
            ...
            Object name=/E
            ...
            Object name=/E/EShape
            ...

    ------------------------------------------------------------------------ 

    -sl / -selection
    If this flag is present, write out all all selected nodes from the active
    selection list that are descendents of the roots specified with -root.
    
    

    ------------------------------------------------------------------------ 
    -sn / -stripNamespaces (optional int)
    If this flag is present all namespaces will be stripped off of the node before
    being written to Alembic.  If an optional int is specified after the flag
    then that many namespaces will be stripped off of the node name. Be careful
    that the new stripped name does not collide with other sibling node names.

        Examples: 
        taco:foo:bar would be written as just bar with -sn
        taco:foo:bar would be written as foo:bar with -sn 1

        Examples & Output:
            Maya scene references:
                objects:pCube1
                  `--objects:pCubeShape1
                
                objects:object2:pCube2
                  `--objects:object2:pCubeShape2
                  
                objects:object2:object3:pCube3
                  `--objects:object2:object3:pCubeShape3
                  
                  
        AbcExport -j "-frameRange 1 10 -stripNamespaces -file test-stripNamespaces.abc"
            Object name=/pCube3
            Object name=/pCube3/pCubeShape3
            Object name=/pCube2
            Object name=/pCube2/pCubeShape2
            Object name=/pCube1
            Object name=/pCube1/pCubeShape1

        AbcExport -j "-frameRange 1 10 -stripNamespaces 1 -file test-stripNamespaces.abc"
            Object name=/object2:object3:pCube3
            Object name=/object2:object3:pCube3/object2:object3:pCubeShape3
            Object name=/object2:pCube2
            Object name=/object2:pCube2/object2:pCubeShape2
            Object name=/pCube1
            Object name=/pCube1/pCubeShape1

    ------------------------------------------------------------------------ 
    -uv / -uvWrite
    If this flag is present, uv data for PolyMesh and SubD shapes will be written to the Alembic file.  Only the current uv map is used.

    -wcs / -writeColorSets
    Write all color sets on MFnMeshes as color 3 or color 4 indexed geometry parameters with face varying scope.

        Examples: 
            AbcExport -j "-frameRange 1 10 -writeColorSets  -file test-stripNamespaces.abc"
            
            Object name=/objects:pPlane1/objects:pPlaneShape1
              CompoundProperty name=.geom;schema=AbcGeom_PolyMesh_v1
                ScalarProperty name=.selfBnds;interpretation=box;datatype=float64_t[6];arraysize=6;numsamps=1
                ArrayProperty name=P;interpretation=point;datatype=float32_t[3];arraysize=36;numsamps=1
                ArrayProperty name=.faceIndices;interpretation=;datatype=int32_t;arraysize=100;numsamps=1
                ArrayProperty name=.faceCounts;interpretation=;datatype=int32_t;arraysize=25;numsamps=1
                ArrayProperty name=N;interpretation=normal;datatype=float32_t[3];arraysize=100;numsamps=1
                CompoundProperty name=.arbGeomParams;schema=
                  CompoundProperty name=testColorSet0;schema=
                    ArrayProperty name=.vals;interpretation=rgba;datatype=float32_t[4];arraysize=51;numsamps=1
                    ArrayProperty name=.indices;interpretation=;datatype=uint32_t;arraysize=100;numsamps=1
                  CompoundProperty name=testColorSet1;schema=
                    ArrayProperty name=.vals;interpretation=rgba;datatype=float32_t[4];arraysize=53;numsamps=1
                    ArrayProperty name=.indices;interpretation=;datatype=uint32_t;arraysize=100;numsamps=1
    
    
    -wfs / -writeFaceSets
    Write all Face sets on MFnMeshes.
        Examples: 
            AbcExport -j "-frameRange 1 10 -writeFaceSets  -file test-stripNamespaces.abc"
            
            Object name=/objects:pPlane1/objects:pPlaneShape1/objects:mYellowSG
              CompoundProperty name=.faceset;schema=AbcGeom_FaceSet_v1
                ScalarProperty name=.selfBnds;interpretation=box;datatype=float64_t[6];arraysize=6;numsamps=1
                ArrayProperty name=.faces;interpretation=;datatype=int32_t;arraysize=2;numsamps=1
                ScalarProperty name=.facesExclusive;interpretation=;datatype=uint32_t;arraysize=1;numsamps=1
            Object name=/objects:pPlane1/objects:pPlaneShape1/objects:mRedSG
              CompoundProperty name=.faceset;schema=AbcGeom_FaceSet_v1
                ScalarProperty name=.selfBnds;interpretation=box;datatype=float64_t[6];arraysize=6;numsamps=1
                ArrayProperty name=.faces;interpretation=;datatype=int32_t;arraysize=4;numsamps=1
                ScalarProperty name=.facesExclusive;interpretation=;datatype=uint32_t;arraysize=1;numsamps=1
            Object name=/objects:pPlane1/objects:pPlaneShape1/objects:mGreenSG
              CompoundProperty name=.faceset;schema=AbcGeom_FaceSet_v1
                ScalarProperty name=.selfBnds;interpretation=box;datatype=float64_t[6];arraysize=6;numsamps=1
                ArrayProperty name=.faces;interpretation=;datatype=int32_t;arraysize=3;numsamps=1
                ScalarProperty name=.facesExclusive;interpretation=;datatype=uint32_t;arraysize=1;numsamps=1
    ------------------------------------------------------------------------         
    -wuvs / -writeUVSets
    Write all uv sets on MFnMeshes as vector 2 indexed geometry parameters with face varying scope.
    
        Examples & Output:
            AbcExport -j "-frameRange 1 10 -writeUVSets -file test-writeUVSets.abc"
           
            Object name=/objects:pPlane1/objects:pPlaneShape1
              CompoundProperty name=.geom;schema=AbcGeom_PolyMesh_v1
                ScalarProperty name=.selfBnds;interpretation=box;datatype=float64_t[6];arraysize=6;numsamps=1
                ArrayProperty name=P;interpretation=point;datatype=float32_t[3];arraysize=9;numsamps=1
                ArrayProperty name=.faceIndices;interpretation=;datatype=int32_t;arraysize=16;numsamps=1
                ArrayProperty name=.faceCounts;interpretation=;datatype=int32_t;arraysize=4;numsamps=1
                CompoundProperty name=uv;schema=
                  ArrayProperty name=.vals;interpretation=vector;datatype=float32_t[2];arraysize=9;numsamps=1
                  ArrayProperty name=.indices;interpretation=;datatype=uint32_t;arraysize=16;numsamps=1
                ArrayProperty name=N;interpretation=normal;datatype=float32_t[3];arraysize=16;numsamps=1
                CompoundProperty name=.arbGeomParams;schema=
                  CompoundProperty name=map1;schema=
                    ArrayProperty name=.vals;interpretation=vector;datatype=float32_t[2];arraysize=9;numsamps=1
                    ArrayProperty name=.indices;interpretation=;datatype=uint32_t;arraysize=16;numsamps=1
                  CompoundProperty name=uvSet2;schema=
                    ArrayProperty name=.vals;interpretation=vector;datatype=float32_t[2];arraysize=9;numsamps=1
                    ArrayProperty name=.indices;interpretation=;datatype=uint32_t;arraysize=16;numsamps=1
                    
    ------------------------------------------------------------------------ 
    -wfg / -wholeFrameGeo
    If this flag is present, data for geometry will only be written out on whole frames.
    ------------------------------------------------------------------------ 
    -ws / -worldSpace
    If this flag is present, any root nodes will be stored in world space.

    Examples & Output:
        AbcExport -j "-frameRange 1 10 -worldSpace -file test-worldSpace.abc"
    ------------------------------------------------------------------------ 
    -wv / -writeVisibility
    If this flag is present, visibility state will be stored in the Alembic file.  Otherwise everything written out is treated as visible.
    
    Examples & Output:
        AbcExport -j "-frameRange 1 10 -writeVisibility -file test-writeVisibility.abc"
        
        ABC cache echo:
        Object name=/objects:group1/objects:pCube4/objects:pCubeShape1
          CompoundProperty name=.geom;schema=AbcGeom_PolyMesh_v1
          ...
          ScalarProperty name=visible;interpretation=;datatype=int8_t;arraysize=1;numsamps=1
    NOTE: 
        The templated object will be set visible in abc file.

    ------------------------------------------------------------------------ 
    -mfc / -melPerFrameCallback string
    When each frame (and the static frame) is evaluated the string specified is evaluated as a Mel command. See below for special processing rules.

    -mpc / -melPostJobCallback string
    When the translation has finished the string specified is evaluated as a Mel command. See below for special processing rules.

    -pfc / -pythonPerFrameCallback string
    When each frame (and the static frame) is evaluated the string specified is evaluated as a python command. See below for special processing rules.

    -ppc / -pythonPostJobCallback string
    When the translation has finished the string specified is evaluated as a python command. See below for special processing rules.

        Special callback information:
        On the callbacks, special tokens are replaced with other data, these tokens and what they are replaced with are as follows:

        #FRAME# replaced with the frame number being evaluated.
        #FRAME# is ignored in the post callbacks.

        #BOUNDS# replaced with a string holding bounding box values in minX minY minZ
        maxX maxY maxZ space seperated order.

        #BOUNDSARRAY# replaced with the bounding box values as above, but in array form.
        In Mel: {minX, minY, minZ, maxX, maxY, maxZ}
        In Python: [minX, minY, minZ, maxX, maxY, maxZ]

    Examples:
        AbcExport -j " -frameRange 1 2 -melPerFrameCallback print("-------------melPerFrameCallback-\\n") -melPostJobCallback print("-------------melPostJobCallback-\\n") -pythonPerFrameCallback print("-------------pythonPerFrameCallback-\\n") -pythonPostJobCallback print("-------------pythonPostJobCallback-\\n")  -file /tmp/test.abc"
        
        AbcExport -j "-framerange 1 5 -step 0.5 -root |group|foo -file /tmp/test.abc"
        Writes out everything at foo and below to /tmp/test.abc sampling at frames:
        1 1.5 2 2.5 3 3.5 4 4.5 5



        AbcExport -j "-sl -root |group|foo -file /tmp/test.abc"
        Writes out all selected nodes and it's ancestor nodes including up to foo.
        foo will be parented to the root of the Alembic scene.


-----------------------------
 Lanuch Maya with local PLE configuration
-----------------------------
/backup/lhome/workspace/studio/tool/gene/applauncher/bin/applauncher_linux maya -v 2014 -s tst

-----------------------------
About test case
-----------------------------
- AbcExport的testcase的目录:
  /ibrix3/PLE/workspace/yaoys/dev/mybox/AbcExport/test
  
- python unittest
  mybox automtaion 采用python unittest作为框架.可以先了解一下:https://docs.python.org/2/library/unittest.html

- 每一个testcase有如下目录:
    test/
    |
    +---test_00XX/
        |
        +----cache/
        |    |
        |    +--XX.abc      该testcase生成的cache文件
        |    +--XX.abc.echo 用abcecho2生成的, 不会打印时间戳等信息
        |    
        +----images/
        +----scenes/
        |    |
        |    +----test.ma
        |
        +----__init__.py
        +----test_*.txt 该testcase的说明
        +----test.py    test script
- test.py的测试代码在
  testXX()函数里. 例如test_0028/test.py的test_animation()函数





